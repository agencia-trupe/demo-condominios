<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosInformacoesPeriodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacos_informacoes_periodo', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('espacos_id')->nullable()->unsigned();
            $table->foreign('espacos_id')->references('id')->on('espacos')->onDelete('cascade');

            $table->time('horario_abertura');
            $table->time('horario_encerramento');
            $table->time('tempo_de_reserva');
            $table->integer('max_reservas_dia')->unsigned()->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('espacos_informacoes_periodo');
    }
}
