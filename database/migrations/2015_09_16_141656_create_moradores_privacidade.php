<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoradoresPrivacidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moradores_privacidade', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->string('dado');
            $table->integer('visibilidade');
            /*
             Visibilidade :
                Todos            => 1
                Admin            => 2
                Portaria         => 3
                Zelador          => 4
                Moradores        => 5
                Moradores Amigos => 6
            */
            /*
                A ideia inicial era ter 1 registro pra cada usuário pra cada
                dado sensível. Mas como talvez seja possível selecionar um combinação
                de diferentes valores de visibilidade pra um mesmo dado
                (Ex: somente portaria + moradores amigos), vão haver registros múltiplos
                para o mesmo dado do mesmo usuário.

                Visibilidade | moradores_id | dado  | visibilidade
                             |       3      | email |      3
                             |       3      | email |      6
            */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('moradores_privacidade');
    }
}
