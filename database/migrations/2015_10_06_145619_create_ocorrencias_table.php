<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcorrenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocorrencias', function (Blueprint $table) {
            $table->increments('id');

            // Armazena quem criou a ocorrência
            // null para mensagem criada pela administração
            $table->integer('autor_id')->nullable()->unsigned();
            $table->foreign('autor_id')->references('id')->on('moradores')->onDelete('set null');

            // Armazena o destinatário da mensagem
            // null para mensagem direcionada à administração
            $table->integer('moradores_id')->nullable()->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('set null');

            $table->text('texto');

            $table->integer('em_resposta_a')->nullable()->unsigned();
            $table->foreign('em_resposta_a')->references('id')->on('ocorrencias')->onDelete('set null');

            $table->string('origem'); // morador | admin

            $table->tinyInteger('finalizada')->default(0); // 0 | 1

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocorrencias');
    }
}
