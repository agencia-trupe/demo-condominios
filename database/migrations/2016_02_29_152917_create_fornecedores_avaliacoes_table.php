<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresAvaliacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores_avaliacoes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fornecedores_id')
                  ->nullable()
                  ->unsigned();

            $table->foreign('fornecedores_id')
                  ->references('id')
                  ->on('fornecedores')
                  ->onDelete('cascade');

            $table->integer('moradores_id')
                  ->nullable()
                  ->unsigned();

            $table->foreign('moradores_id')
                  ->references('id')
                  ->on('moradores')
                  ->onDelete('cascade');

            $table->text('texto');
            $table->integer('nota');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fornecedores_avaliacoes');
    }
}
