<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaleComMensagensLidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fale_com_mensagens_lidas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fale_com_mensagens_id')->nullable()->unsigned();
            $table->foreign('fale_com_mensagens_id')->references('id')->on('fale_com_mensagens')->onDelete('cascade');

            $table->integer('administradores_id')->nullable()->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fale_com_mensagens_lidas');
    }
}
