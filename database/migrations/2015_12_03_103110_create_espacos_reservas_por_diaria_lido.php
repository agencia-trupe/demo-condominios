<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosReservasPorDiariaLido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacos_reservas_por_diaria_lidos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('espacos_reservas_id')->unsigned();
            $table->foreign('espacos_reservas_id')->references('id')->on('espacos_reservas_por_diaria')->onDelete('cascade');

            $table->integer('administradores_id')->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('espacos_reservas_por_diaria_lidos');
    }
}
