<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoradores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moradores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('foto');
            $table->string('email')->unique();
            $table->string('senha', 60);
            $table->string('telefone_fixo');
            $table->string('telefone_celular');
            $table->string('telefone_comercial');
            $table->string('activation_code')->nullable();
            $table->string('status')->default(0);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('moradores');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
