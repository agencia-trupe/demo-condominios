<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMoradoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moradores', function (Blueprint $table) {
            $table->integer('unidades_id')->unsigned()->after('id');
            $table->foreign('unidades_id')->references('id')->on('unidades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moradores', function (Blueprint $table) {
            $table->dropForeign('moradores_unidades_id_foreign');
            $table->dropColumn('unidades_id');
        });
    }
}
