<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaleComTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fale_com', function (Blueprint $table) {
            $table->increments('id');

            $table->text('contato_sindico');
            $table->text('contato_conselho');
            $table->text('contato_zelador');
            $table->text('contato_portaria');
            $table->text('contato_administradora');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fale_com');
    }
}
