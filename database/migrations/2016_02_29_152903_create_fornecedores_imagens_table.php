<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores_imagens', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fornecedores_id')
                  ->nullable()
                  ->unsigned();

            $table->foreign('fornecedores_id')
                  ->references('id')
                  ->on('fornecedores')
                  ->onDelete('cascade');

            $table->string('imagem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fornecedores_imagens');
    }
}
