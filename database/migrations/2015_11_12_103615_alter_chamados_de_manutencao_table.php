<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChamadosDeManutencaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chamados_de_manutencao', function (Blueprint $table) {

            $table->datetime('visto_pela_adm_em')->nullable()->after('descricao');

            $table->integer('visto_pela_adm_id')->nullable()->unsigned()->after('visto_pela_adm_em');
            $table->foreign('visto_pela_adm_id')->references('id')->on('administradores')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chamados_de_manutencao', function (Blueprint $table) {
            $table->dropForeign('chamados_de_manutencao_visto_pela_adm_id_foreign');
            $table->dropColumn('visto_pela_adm_em');
            $table->dropColumn('visto_pela_adm_id');
        });
    }
}
