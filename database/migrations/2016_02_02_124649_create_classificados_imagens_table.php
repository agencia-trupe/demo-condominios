<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassificadosImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classificados_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classificados_id')->unsigned();
            $table->foreign('classificados_id')->references('id')->on('classificados')->onDelete('cascade');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('classificados_imagens');
    }
}
