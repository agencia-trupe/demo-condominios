<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestasParticularesLidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festas_particulares_lidas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('festas_particulares_id')->unsigned();
            $table->foreign('festas_particulares_id')->references('id')->on('festas_particulares')->onDelete('cascade');

            $table->integer('administradores_id')->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('festas_particulares_lidas');
    }
}
