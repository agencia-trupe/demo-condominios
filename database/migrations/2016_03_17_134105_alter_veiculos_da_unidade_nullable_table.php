<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVeiculosDaUnidadeNullableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('veiculos_da_unidade', function (Blueprint $table) {
            $table->integer('moradores_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('veiculos_da_unidade', function (Blueprint $table) {
          DB::statement('UPDATE `veiculos_da_unidade` SET `moradores_id` = 0 WHERE `moradores_id` IS NULL;');
          //DB::statement('ALTER TABLE `veiculos_da_unidade` MODIFY `moradores_id` INTEGER UNSIGNED NOT NULL;');
        });
    }
}
