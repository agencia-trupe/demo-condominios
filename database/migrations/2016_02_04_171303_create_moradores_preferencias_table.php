<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoradoresPreferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moradores_preferencias', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            // Serve para correspondencias e encomendas
            $table->tinyInteger('enviar_email_nova_correspondencia')->default(0);

            $table->tinyInteger('enviar_email_novo_aviso')->default(0);

            $table->tinyInteger('enviar_email_novo_documento')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('moradores_preferencias');
    }
}
