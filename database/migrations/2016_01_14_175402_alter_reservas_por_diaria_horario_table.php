<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReservasPorDiariaHorarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('espacos_reservas_por_diaria', function (Blueprint $table) {
            $table->string('horario', 30)->after('reserva')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('espacos_reservas_por_diaria', function (Blueprint $table) {
            $table->dropColumn('horario');
        });
    }
}
