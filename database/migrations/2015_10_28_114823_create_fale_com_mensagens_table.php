<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaleComMensagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fale_com_mensagens', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moradores_id')->nullable()->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('set null');

            $table->integer('em_resposta_a')->nullable()->unsigned();
            $table->foreign('em_resposta_a')->references('id')->on('fale_com_mensagens')->onDelete('cascade');

            $table->string('assunto');
            $table->text('mensagem');
            $table->string('destino');

            /*
            *   destinos:
            *
            *   sindico
            *   conselho
            *   zelador
            *   portaria
            *   administradora
            *
            *   morador (em caso de resposta)
            */

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('fale_com_mensagens');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
