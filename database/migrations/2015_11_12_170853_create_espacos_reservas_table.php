<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspacosReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacos_reservas_por_periodo', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('espacos_id')->nullable()->unsigned();
            $table->foreign('espacos_id')->references('id')->on('espacos')->onDelete('cascade');

            $table->integer('moradores_id')->nullable()->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->datetime('reserva');

            $table->text('anotacoes');

            $table->tinyInteger('is_bloqueio')->default(0); // 0 | 1

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('espacos_reservas_por_periodo');
    }
}
