<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadosDeManutencaoFotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chamados_de_manutencao_fotos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('chamados_de_manutencao_id')->unsigned();
            $table->foreign('chamados_de_manutencao_id')->references('id')->on('chamados_de_manutencao')->onDelete('cascade');

            $table->string('foto');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chamados_de_manutencao_fotos');
    }
}
