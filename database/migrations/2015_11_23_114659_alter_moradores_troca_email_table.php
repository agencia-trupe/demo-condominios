<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMoradoresTrocaEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moradores', function (Blueprint $table) {

            $table->string('novo_email')
                  ->nullable()
                  ->after('status');

            $table->string('novo_email_activation_code')
                  ->nullable()
                  ->after('novo_email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moradores', function (Blueprint $table) {
            $table->dropColumn('novo_email');
            $table->dropColumn('novo_email_activation_code');
        });
    }
}
