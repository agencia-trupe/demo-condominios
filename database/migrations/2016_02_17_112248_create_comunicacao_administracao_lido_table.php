<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicacaoAdministracaoLidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunicacao_administracao_lidos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('comunicacao_adm_id')->unsigned();
            $table->foreign('comunicacao_adm_id')->references('id')->on('comunicacao_administracao')->onDelete('cascade');

            $table->integer('administradores_id')->unsigned();
            $table->foreign('administradores_id')->references('id')->on('administradores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comunicacao_administracao_lidos');
    }
}
