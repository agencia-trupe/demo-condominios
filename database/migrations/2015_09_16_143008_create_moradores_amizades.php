<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoradoresAmizades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moradores_amizades', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moradores_id')->unsigned();
            $table->foreign('moradores_id')->references('id')->on('moradores')->onDelete('cascade');

            $table->integer('amigo_id')->unsigned();
            $table->foreign('amigo_id')->references('id')->on('moradores')->onDelete('cascade');
            /*
              Amizades unilaterais não-recíprocas
            */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('moradores_amizades');
    }
}
