<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoasNaoAutorizadasPrivacidadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas_nao_autorizadas_privacidade', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('pessoas_id')->unsigned();
            $table->foreign('pessoas_id')->references('id')->on('pessoas_nao_autorizadas')->onDelete('cascade');

            /*
             Visibilidade :
                Todos            => 1
                Admin            => 2
                Portaria         => 3
                Zelador          => 4
                Moradores        => 5
                Moradores Amigos => 6
            */
            /*
                Como é possível selecionar uma combinação
                de diferentes valores de visibilidade
                (Ex: portaria + moradores amigos),
                vão haver registros múltiplos
                para o mesmo registro.

                Visibilidade | moradores_da_unidade_id | visibilidade
                             |       3                 |       3
                             |       3                 |       6
            */
            $table->string('visibilidade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pessoas_nao_autorizadas_privacidade');
    }
}
