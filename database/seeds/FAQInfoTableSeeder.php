<?php

use Illuminate\Database\Seeder;

class FAQInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('faq_info')->delete();

      DB::table('faq_info')->insert([
        'texto' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Magni modi ducimus, nemo ex maiores perferendis, nam esse facere ipsa. Tempore,
        sed molestiae voluptatibus magnam accusamus saepe vel alias commodi vero.</p>'
      ]);
    }
}
