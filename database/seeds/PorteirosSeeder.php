<?php

use Illuminate\Database\Seeder;

class PorteirosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('porteiros')->delete();

        DB::table('porteiros')->insert([
            'login' => 'portaria',
            'email' => 'portaria@trupe.net',
            'senha' => bcrypt('COND1')
        ]);
    }
}
