<?php

use Illuminate\Database\Seeder;

class MudancasInfoSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('mudancas_informacoes')->delete();

    DB::table('mudancas_informacoes')->insert([
      'horario_manha' => 'das 9h às 12h',
      'horario_tarde' => 'das 14h às 18h',
      'regulamento'   => 'arquivo'
    ]);
  }
}
