<?php

use Illuminate\Database\Seeder;

class UnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidades')->delete();

        DB::table('unidades')->insert([
			[
				'unidade' => '11',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 11 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '11',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 11 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '12',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 12 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '12',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 12 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '13',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 13 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '13',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 13 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '14',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 14 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '14',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 14 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '21',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 21 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '21',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 21 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '22',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 22 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '22',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 22 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '23',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 23 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '23',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 23 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '24',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 24 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '24',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 24 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '31',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 31 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '31',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 31 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '32',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 32 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '32',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 32 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '33',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 33 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '33',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 33 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '34',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 34 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '34',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 34 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '41',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 41 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '41',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 41 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '42',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 42 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '42',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 42 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '43',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 43 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '43',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 43 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '44',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 44 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '44',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 44 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '51',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 51 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '51',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 51 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '52',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 52 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '52',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 52 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '53',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 53 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '53',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 53 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '54',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 54 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '54',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 54 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '61',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 61 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '61',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 61 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '62',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 62 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '62',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 62 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '63',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 63 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '63',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 63 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '64',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 64 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '64',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 64 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '71',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 71 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '71',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 71 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '72',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 72 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '72',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 72 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '73',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 73 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '73',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 73 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '74',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 74 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '74',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 74 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '81',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 81 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '81',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 81 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '82',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 82 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '82',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 82 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '83',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 83 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '83',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 83 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '84',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 84 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '84',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 84 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '91',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 91 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '91',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 91 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '92',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 92 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '92',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 92 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '93',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 93 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '93',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 93 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '94',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 94 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '94',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 94 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '101',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 101 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '101',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 101 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '102',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 102 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '102',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 102 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '103',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 103 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '103',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 103 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '104',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 104 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '104',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 104 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '111',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 111 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '111',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 111 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '112',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 112 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '112',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 112 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '113',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 113 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '113',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 113 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '114',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 114 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '114',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 114 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '121',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 121 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '121',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 121 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '122',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 122 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '122',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 122 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '123',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 123 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '123',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 123 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '124',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 124 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '124',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 124 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '131',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 131 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '131',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 131 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '132',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 132 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '132',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 132 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '133',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 133 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '133',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 133 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '134',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 134 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '134',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 134 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '141',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 141 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '141',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 141 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '142',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 142 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '142',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 142 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '143',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 143 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '143',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 143 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '144',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 144 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '144',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 144 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '151',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 151 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '151',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 151 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '152',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 152 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '152',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 152 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '153',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 153 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '153',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 153 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '154',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 154 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '154',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 154 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '161',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 161 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '161',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 161 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '162',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 162 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '162',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 162 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '163',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 163 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '163',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 163 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '164',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 164 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '164',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 164 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '171',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 171 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '171',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 171 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '172',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 172 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '172',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 172 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '173',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 173 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '173',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 173 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '174',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 174 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '174',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 174 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '181',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 181 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '181',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 181 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '182',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 182 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '182',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 182 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '183',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 183 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '183',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 183 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '184',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 184 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '184',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 184 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '191',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 191 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '191',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 191 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '192',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 192 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '192',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 192 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '193',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 193 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '193',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 193 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '194',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 194 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '194',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 194 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '201',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 201 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '201',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 201 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '202',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 202 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '202',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 202 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '203',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 203 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '203',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 203 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '204',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 204 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '204',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 204 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '211',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 211 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '211',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 211 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '212',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 212 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '212',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 212 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '213',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 213 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '213',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 213 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '214',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 214 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '214',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 214 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '221',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 221 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '221',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 221 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '222',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 222 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '222',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 222 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '223',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 223 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '223',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 223 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '224',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 224 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '224',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 224 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '231',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 231 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '231',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 231 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '232',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 232 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '233',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 233 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '233',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 233 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '234',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 234 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '234',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 234 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '241',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 241 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '241',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 241 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '242',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 242 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '242',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 242 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '243',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 243 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '243',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 243 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '244',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 244 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '244',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 244 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '251',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 251 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '251',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 251 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '252',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 252 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '253',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 253 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '253',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 253 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '254',
				'bloco' => 'A',
				'proprietario' => 'Proprietário Unidade 254 A',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			],
			[
				'unidade' => '254',
				'bloco' => 'B',
				'proprietario' => 'Proprietário Unidade 254 B',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]
        ]);
    }
}
