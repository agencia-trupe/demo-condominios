<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AdministradoresSeeder::class);
        $this->call(DocumentoCategoriasSeeder::class);
        $this->call(UnidadesSeeder::class);
        $this->call(FaleComSeeder::class);
        $this->call(MudancasInfoSeeder::class);
        $this->call(MoradoresSeeder::class);
        $this->call(MoradoresPrivacidadeSeeder::class);
        $this->call(EspacosSeeder::class);
        $this->call(FAQSeeder::class);
        $this->call(FAQInfoTableSeeder::class);
        $this->call(PorteirosSeeder::class);
        $this->call(RegulamentosTableSeeder::class);

        Model::reguard();
    }
}
