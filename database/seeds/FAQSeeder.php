<?php

use Illuminate\Database\Seeder;

class FAQSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('faq')->delete();

    DB::table('faq')->insert(
      [
        'questao'  => 'Questão Exemplo',
        'resposta' => '<p>Resposta exemplo, Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Magni modi ducimus, nemo ex maiores perferendis, nam esse facere ipsa. Tempore,
        sed molestiae voluptatibus magnam accusamus saepe vel alias commodi vero.</p>',
        'ordem'    => 0
      ],
      [
        'questao'  => 'Segunda Questão Exemplo',
        'resposta' => '<p>Segunda Resposta exemplo, Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Magni modi ducimus, nemo ex maiores perferendis, nam esse facere ipsa. Tempore,
        sed molestiae voluptatibus magnam accusamus saepe vel alias commodi vero.</p>',
        'ordem'    => 1
      ]);
    }
  }
