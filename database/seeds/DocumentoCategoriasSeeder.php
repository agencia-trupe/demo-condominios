<?php

use Illuminate\Database\Seeder;

class DocumentoCategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documentos_categorias')->delete();

        DB::table('documentos_categorias')->insert([
            [
                'titulo' => 'Atas de Reunião',
                'slug'   => str_slug('Atas de Reunião'),
                'texto' => '',
                'ordem'  => 1
            ],
            [
                'titulo' => 'Padronizações de Instalação',
                'slug'   => str_slug('Padronizações de Instalação'),
                'texto' => '',
                'ordem'  => 2
            ],
            [
                'titulo' => 'Regulamentos',
                'slug'   => str_slug('Regulamentos'),
                'texto' => '',
                'ordem'  => 0
            ],
            [
                'titulo' => 'Diversos',
                'slug'   => str_slug('Diversos'),
                'texto' => '',
                'ordem'  => 3
            ]
        ]);
    }
}
