<?php

use Illuminate\Database\Seeder;

class RegulamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('regulamentos')->delete();

      DB::table('regulamentos')->insert([
        'completo' => '',
        'reduzido_reservas' => ''
      ]);
    }
}
