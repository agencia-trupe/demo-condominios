<?php

use Illuminate\Database\Seeder;

class MoradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('moradores')->delete();

        DB::table('moradores')->insert([
		      'unidades_id' => 1,
          'relacao_unidade' => 'proprietario', // proprietario | locatario
          'data_mudanca' => '2014-09-01',
          'data_nascimento' => '1986-12-06',
          'nome' => 'Morador Teste',
          'apelido' => 'Morador',
          'foto' => '',
          'email' => 'condominios@trupe.net',
          'senha' => bcrypt('COND1'),
          'telefone_fixo' => '(11) 5555-5555',
          'telefone_celular' => '(11) 5555-5555',
          'telefone_comercial' => '(11) 5555-5555',
          'status' => 1,
          'created_at' => date('Y-m-d')
        ]);

    }
}
