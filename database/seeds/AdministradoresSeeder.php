<?php

use Illuminate\Database\Seeder;

class AdministradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('administradores')->delete();

        DB::table('administradores')->insert([
            'tipo' => 'master',
            'login' => 'trupe',
            'email' => 'contato@trupe.net',
            'senha' => bcrypt('COND1')
        ]);

        DB::table('administradores')->insert([
            'tipo' => 'sindico',
            'login' => 'sindico',
            'email' => 'contato@trupe.net',
            'senha' => bcrypt('COND1')
        ]);

        DB::table('administradores')->insert([
            'tipo' => 'zelador',
            'login' => 'zelador',
            'email' => 'contato@trupe.net',
            'senha' => bcrypt('COND1')
        ]);

        DB::table('administradores')->insert([
            'tipo' =>  'conselho',
            'login' => 'conselho',
            'email' => 'contato@trupe.net',
            'senha' => bcrypt('COND1')
        ]);
    }
}
