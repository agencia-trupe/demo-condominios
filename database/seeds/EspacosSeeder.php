<?php

use Illuminate\Database\Seeder;

class EspacosSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('espacos')->delete();

    DB::table('espacos')->insert([
      'titulo' => 'Salão de Festas',
      'slug' => 'salao_de_festas',
      'tipo' => 'diaria'
    ]);

    DB::table('espacos')->insert([
      'titulo' => 'Churrasqueira',
      'slug' => 'churrasqueira',
      'tipo' => 'diaria'
    ]);

    DB::table('espacos')->insert([
      'titulo' => 'Sala de Squash',
      'slug' => 'sala_de_squash',
      'tipo' => 'por_periodo'
    ]);

    DB::table('espacos_informacoes_diaria')->insert([
      'espacos_id' => 1,
      'valor' => 20000,
      'dias_carencia' => 7,
      'texto' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam quae excepturi corporis pariatur cupiditate voluptates, veritatis quia necessitatibus, doloribus ut nesciunt dolorem reiciendis a? Iure, quisquam, dolor? Dignissimos, ex, nihil!</p>'
    ]);

    DB::table('espacos_informacoes_diaria')->insert([
      'espacos_id' => 2,
      'valor' => 20000,
      'dias_carencia' => 7,
      'texto' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam quae excepturi corporis pariatur cupiditate voluptates, veritatis quia necessitatibus, doloribus ut nesciunt dolorem reiciendis a? Iure, quisquam, dolor? Dignissimos, ex, nihil!</p>'
    ]);

    DB::table('espacos_informacoes_periodo')->insert([
      'espacos_id' => 3,
      'horario_abertura' => '08:00',
      'horario_encerramento' => '21:30',
      'tempo_de_reserva' => '00:30',
      'max_reservas_dia' => '4'
    ]);
  }
}
