-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: condominios
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administradores`
--

DROP TABLE IF EXISTS `administradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administradores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `administradores_login_unique` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administradores`
--

LOCK TABLES `administradores` WRITE;
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
INSERT INTO `administradores` VALUES (1,'master','trupe','contato@trupe.net','$2y$10$ZAMdpmhHd0JGDeanVAb9quOALsTI3Eoxz/t3ps7GNetglh9Ga6rU2','Ql1vXJ83LwwNxdx2UumlvtkiWazWDGqGMWImbxg9M5yJbDJhiDJjNmU7G6oo','0000-00-00 00:00:00','2017-12-05 07:15:30'),(2,'sindico','sindico','contato@trupe.net','$2y$10$yoIa4J21zZdKfCIZLFnr/ehZhqtRiGRnOFkiAGD7EyepsgXIaL9a6',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'zelador','zelador','contato@trupe.net','$2y$10$IZTTmNF.DUJICkU/zUSy.uOvLcCvKBxzKydF/OPV8fMyaACY8y0gO',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'conselho','conselho','contato@trupe.net','$2y$10$/PP/a1DzKNAcK3uyD3sGn.xG58Ej9LjO2XcvFDfe1jS4nMpDtoz4q',NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administradores_notificacoes`
--

DROP TABLE IF EXISTS `administradores_notificacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administradores_notificacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_notificacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `ocorrencias_id` int(10) unsigned DEFAULT NULL,
  `fale_com_mensagens_id` int(10) unsigned DEFAULT NULL,
  `mudancas_agendamento_id` int(10) unsigned DEFAULT NULL,
  `chamados_de_manutencao_id` int(10) unsigned DEFAULT NULL,
  `espacos_reservas_por_periodo_id` int(10) unsigned DEFAULT NULL,
  `espacos_reservas_por_diaria_id` int(10) unsigned DEFAULT NULL,
  `diaria_com_multa_id` int(10) unsigned DEFAULT NULL,
  `festas_particulares_id` int(10) unsigned DEFAULT NULL,
  `comunicacao_adm_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `administradores_notificacoes_administradores_id_foreign` (`administradores_id`),
  KEY `administradores_notificacoes_ocorrencias_id_foreign` (`ocorrencias_id`),
  KEY `administradores_notificacoes_fale_com_mensagens_id_foreign` (`fale_com_mensagens_id`),
  KEY `administradores_notificacoes_mudancas_agendamento_id_foreign` (`mudancas_agendamento_id`),
  KEY `administradores_notificacoes_chamados_de_manutencao_id_foreign` (`chamados_de_manutencao_id`),
  KEY `fk_reservas_periodo` (`espacos_reservas_por_periodo_id`),
  KEY `fk_reservas_diaria` (`espacos_reservas_por_diaria_id`),
  KEY `administradores_notificacoes_diaria_com_multa_id_foreign` (`diaria_com_multa_id`),
  KEY `administradores_notificacoes_festas_particulares_id_foreign` (`festas_particulares_id`),
  KEY `administradores_notificacoes_comunicacao_adm_id_foreign` (`comunicacao_adm_id`),
  CONSTRAINT `administradores_notificacoes_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_chamados_de_manutencao_id_foreign` FOREIGN KEY (`chamados_de_manutencao_id`) REFERENCES `chamados_de_manutencao` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_comunicacao_adm_id_foreign` FOREIGN KEY (`comunicacao_adm_id`) REFERENCES `comunicacao_administracao` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_diaria_com_multa_id_foreign` FOREIGN KEY (`diaria_com_multa_id`) REFERENCES `espacos_reservas_por_diaria` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_fale_com_mensagens_id_foreign` FOREIGN KEY (`fale_com_mensagens_id`) REFERENCES `fale_com_mensagens` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_festas_particulares_id_foreign` FOREIGN KEY (`festas_particulares_id`) REFERENCES `festas_particulares` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_mudancas_agendamento_id_foreign` FOREIGN KEY (`mudancas_agendamento_id`) REFERENCES `mudancas_agendamento` (`id`) ON DELETE CASCADE,
  CONSTRAINT `administradores_notificacoes_ocorrencias_id_foreign` FOREIGN KEY (`ocorrencias_id`) REFERENCES `ocorrencias` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_reservas_diaria` FOREIGN KEY (`espacos_reservas_por_diaria_id`) REFERENCES `espacos_reservas_por_diaria` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_reservas_periodo` FOREIGN KEY (`espacos_reservas_por_periodo_id`) REFERENCES `espacos_reservas_por_periodo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administradores_notificacoes`
--

LOCK TABLES `administradores_notificacoes` WRITE;
/*!40000 ALTER TABLE `administradores_notificacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `administradores_notificacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agenda_administracao`
--

DROP TABLE IF EXISTS `agenda_administracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda_administracao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `horario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda_administracao`
--

LOCK TABLES `agenda_administracao` WRITE;
/*!40000 ALTER TABLE `agenda_administracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenda_administracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animais_de_estimacao`
--

DROP TABLE IF EXISTS `animais_de_estimacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animais_de_estimacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `especie` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `unidades_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `animais_de_estimacao_moradores_id_foreign` (`moradores_id`),
  KEY `animais_de_estimacao_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `animais_de_estimacao_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `animais_de_estimacao_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animais_de_estimacao`
--

LOCK TABLES `animais_de_estimacao` WRITE;
/*!40000 ALTER TABLE `animais_de_estimacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `animais_de_estimacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `animais_de_estimacao_privacidade`
--

DROP TABLE IF EXISTS `animais_de_estimacao_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animais_de_estimacao_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `animais_de_estimacao_id` int(10) unsigned NOT NULL,
  `visibilidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `animais_de_estimacao_privacidade_animais_de_estimacao_id_foreign` (`animais_de_estimacao_id`),
  CONSTRAINT `animais_de_estimacao_privacidade_animais_de_estimacao_id_foreign` FOREIGN KEY (`animais_de_estimacao_id`) REFERENCES `animais_de_estimacao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animais_de_estimacao_privacidade`
--

LOCK TABLES `animais_de_estimacao_privacidade` WRITE;
/*!40000 ALTER TABLE `animais_de_estimacao_privacidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `animais_de_estimacao_privacidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avisos`
--

DROP TABLE IF EXISTS `avisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avisos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `olho` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `assinatura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avisos`
--

LOCK TABLES `avisos` WRITE;
/*!40000 ALTER TABLE `avisos` DISABLE KEYS */;
/*!40000 ALTER TABLE `avisos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avisos_lidos`
--

DROP TABLE IF EXISTS `avisos_lidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avisos_lidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `avisos_id` int(10) unsigned NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `avisos_lidos_avisos_id_foreign` (`avisos_id`),
  KEY `avisos_lidos_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `avisos_lidos_avisos_id_foreign` FOREIGN KEY (`avisos_id`) REFERENCES `avisos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `avisos_lidos_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avisos_lidos`
--

LOCK TABLES `avisos_lidos` WRITE;
/*!40000 ALTER TABLE `avisos_lidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `avisos_lidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chamados_de_manutencao`
--

DROP TABLE IF EXISTS `chamados_de_manutencao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chamados_de_manutencao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `data` date NOT NULL,
  `horario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `localizacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `visto_pela_adm_em` datetime DEFAULT NULL,
  `visto_pela_adm_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `chamados_de_manutencao_moradores_id_foreign` (`moradores_id`),
  KEY `chamados_de_manutencao_visto_pela_adm_id_foreign` (`visto_pela_adm_id`),
  CONSTRAINT `chamados_de_manutencao_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chamados_de_manutencao_visto_pela_adm_id_foreign` FOREIGN KEY (`visto_pela_adm_id`) REFERENCES `administradores` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chamados_de_manutencao`
--

LOCK TABLES `chamados_de_manutencao` WRITE;
/*!40000 ALTER TABLE `chamados_de_manutencao` DISABLE KEYS */;
/*!40000 ALTER TABLE `chamados_de_manutencao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chamados_de_manutencao_fotos`
--

DROP TABLE IF EXISTS `chamados_de_manutencao_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chamados_de_manutencao_fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chamados_de_manutencao_id` int(10) unsigned NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `chamados_de_manutencao_fotos_chamados_de_manutencao_id_foreign` (`chamados_de_manutencao_id`),
  CONSTRAINT `chamados_de_manutencao_fotos_chamados_de_manutencao_id_foreign` FOREIGN KEY (`chamados_de_manutencao_id`) REFERENCES `chamados_de_manutencao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chamados_de_manutencao_fotos`
--

LOCK TABLES `chamados_de_manutencao_fotos` WRITE;
/*!40000 ALTER TABLE `chamados_de_manutencao_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `chamados_de_manutencao_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chamados_de_manutencao_lidos`
--

DROP TABLE IF EXISTS `chamados_de_manutencao_lidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chamados_de_manutencao_lidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `chamados_de_manutencao_id` int(10) unsigned NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `chamados_de_manutencao_lidos_chamados_de_manutencao_id_foreign` (`chamados_de_manutencao_id`),
  KEY `chamados_de_manutencao_lidos_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `chamados_de_manutencao_lidos_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `chamados_de_manutencao_lidos_chamados_de_manutencao_id_foreign` FOREIGN KEY (`chamados_de_manutencao_id`) REFERENCES `chamados_de_manutencao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chamados_de_manutencao_lidos`
--

LOCK TABLES `chamados_de_manutencao_lidos` WRITE;
/*!40000 ALTER TABLE `chamados_de_manutencao_lidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `chamados_de_manutencao_lidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificados`
--

DROP TABLE IF EXISTS `classificados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `contato` text COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `classificados_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `classificados_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificados`
--

LOCK TABLES `classificados` WRITE;
/*!40000 ALTER TABLE `classificados` DISABLE KEYS */;
/*!40000 ALTER TABLE `classificados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificados_imagens`
--

DROP TABLE IF EXISTS `classificados_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificados_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classificados_id` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `classificados_imagens_classificados_id_foreign` (`classificados_id`),
  CONSTRAINT `classificados_imagens_classificados_id_foreign` FOREIGN KEY (`classificados_id`) REFERENCES `classificados` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificados_imagens`
--

LOCK TABLES `classificados_imagens` WRITE;
/*!40000 ALTER TABLE `classificados_imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `classificados_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comunicacao_administracao`
--

DROP TABLE IF EXISTS `comunicacao_administracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comunicacao_administracao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prioridade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remetente` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destinatario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_resposta` tinyint(4) NOT NULL DEFAULT '0',
  `em_resposta_a` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `comunicacao_administracao_em_resposta_a_foreign` (`em_resposta_a`),
  CONSTRAINT `comunicacao_administracao_em_resposta_a_foreign` FOREIGN KEY (`em_resposta_a`) REFERENCES `comunicacao_administracao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comunicacao_administracao`
--

LOCK TABLES `comunicacao_administracao` WRITE;
/*!40000 ALTER TABLE `comunicacao_administracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `comunicacao_administracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comunicacao_administracao_lidos`
--

DROP TABLE IF EXISTS `comunicacao_administracao_lidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comunicacao_administracao_lidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comunicacao_adm_id` int(10) unsigned NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `comunicacao_administracao_lidos_comunicacao_adm_id_foreign` (`comunicacao_adm_id`),
  KEY `comunicacao_administracao_lidos_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `comunicacao_administracao_lidos_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comunicacao_administracao_lidos_comunicacao_adm_id_foreign` FOREIGN KEY (`comunicacao_adm_id`) REFERENCES `comunicacao_administracao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comunicacao_administracao_lidos`
--

LOCK TABLES `comunicacao_administracao_lidos` WRITE;
/*!40000 ALTER TABLE `comunicacao_administracao_lidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `comunicacao_administracao_lidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correspondencias`
--

DROP TABLE IF EXISTS `correspondencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correspondencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned NOT NULL,
  `is_entregue` int(11) NOT NULL DEFAULT '0',
  `entregue_em` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `correspondencias_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `correspondencias_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correspondencias`
--

LOCK TABLES `correspondencias` WRITE;
/*!40000 ALTER TABLE `correspondencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `correspondencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos`
--

DROP TABLE IF EXISTS `documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentos_categoria_id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `documentos_documentos_categoria_id_foreign` (`documentos_categoria_id`),
  CONSTRAINT `documentos_documentos_categoria_id_foreign` FOREIGN KEY (`documentos_categoria_id`) REFERENCES `documentos_categorias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos`
--

LOCK TABLES `documentos` WRITE;
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos_categorias`
--

DROP TABLE IF EXISTS `documentos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos_categorias`
--

LOCK TABLES `documentos_categorias` WRITE;
/*!40000 ALTER TABLE `documentos_categorias` DISABLE KEYS */;
INSERT INTO `documentos_categorias` VALUES (1,'Atas de Reunião','atas-de-reuniao','',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Padronizações de Instalação','padronizacoes-de-instalacao','',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Regulamentos','regulamentos','',0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Diversos','diversos','',3,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `documentos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encomendas`
--

DROP TABLE IF EXISTS `encomendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encomendas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned NOT NULL,
  `is_entregue` int(11) NOT NULL DEFAULT '0',
  `entregue_em` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `encomendas_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `encomendas_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encomendas`
--

LOCK TABLES `encomendas` WRITE;
/*!40000 ALTER TABLE `encomendas` DISABLE KEYS */;
INSERT INTO `encomendas` VALUES (1,1,0,NULL,'2017-12-05 07:28:21','2017-12-05 07:28:21');
/*!40000 ALTER TABLE `encomendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquete_opcoes`
--

DROP TABLE IF EXISTS `enquete_opcoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquete_opcoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enquete_questoes_id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `enquete_opcoes_enquete_questoes_id_foreign` (`enquete_questoes_id`),
  CONSTRAINT `enquete_opcoes_enquete_questoes_id_foreign` FOREIGN KEY (`enquete_questoes_id`) REFERENCES `enquete_questoes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquete_opcoes`
--

LOCK TABLES `enquete_opcoes` WRITE;
/*!40000 ALTER TABLE `enquete_opcoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquete_opcoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquete_questoes`
--

DROP TABLE IF EXISTS `enquete_questoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquete_questoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ativa` int(11) NOT NULL DEFAULT '0',
  `historico` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquete_questoes`
--

LOCK TABLES `enquete_questoes` WRITE;
/*!40000 ALTER TABLE `enquete_questoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquete_questoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquete_votos`
--

DROP TABLE IF EXISTS `enquete_votos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquete_votos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enquete_questoes_id` int(10) unsigned NOT NULL,
  `enquete_opcoes_id` int(10) unsigned NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `enquete_votos_enquete_questoes_id_foreign` (`enquete_questoes_id`),
  KEY `enquete_votos_enquete_opcoes_id_foreign` (`enquete_opcoes_id`),
  KEY `enquete_votos_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `enquete_votos_enquete_opcoes_id_foreign` FOREIGN KEY (`enquete_opcoes_id`) REFERENCES `enquete_opcoes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `enquete_votos_enquete_questoes_id_foreign` FOREIGN KEY (`enquete_questoes_id`) REFERENCES `enquete_questoes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `enquete_votos_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquete_votos`
--

LOCK TABLES `enquete_votos` WRITE;
/*!40000 ALTER TABLE `enquete_votos` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquete_votos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos`
--

DROP TABLE IF EXISTS `espacos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regras_de_uso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos`
--

LOCK TABLES `espacos` WRITE;
/*!40000 ALTER TABLE `espacos` DISABLE KEYS */;
INSERT INTO `espacos` VALUES (1,'Salão de Festas','salao_de_festas','','diaria','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'Churrasqueira','churrasqueira','','diaria','0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Sala de Squash','sala_de_squash','','por_periodo','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `espacos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_informacoes_diaria`
--

DROP TABLE IF EXISTS `espacos_informacoes_diaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_informacoes_diaria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `espacos_id` int(10) unsigned DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `dias_carencia` int(11) NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_informacoes_diaria_espacos_id_foreign` (`espacos_id`),
  CONSTRAINT `espacos_informacoes_diaria_espacos_id_foreign` FOREIGN KEY (`espacos_id`) REFERENCES `espacos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_informacoes_diaria`
--

LOCK TABLES `espacos_informacoes_diaria` WRITE;
/*!40000 ALTER TABLE `espacos_informacoes_diaria` DISABLE KEYS */;
INSERT INTO `espacos_informacoes_diaria` VALUES (1,1,20000,7,'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam quae excepturi corporis pariatur cupiditate voluptates, veritatis quia necessitatibus, doloribus ut nesciunt dolorem reiciendis a? Iure, quisquam, dolor? Dignissimos, ex, nihil!</p>','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,20000,7,'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam quae excepturi corporis pariatur cupiditate voluptates, veritatis quia necessitatibus, doloribus ut nesciunt dolorem reiciendis a? Iure, quisquam, dolor? Dignissimos, ex, nihil!</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `espacos_informacoes_diaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_informacoes_periodo`
--

DROP TABLE IF EXISTS `espacos_informacoes_periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_informacoes_periodo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `espacos_id` int(10) unsigned DEFAULT NULL,
  `horario_abertura` time NOT NULL,
  `horario_encerramento` time NOT NULL,
  `tempo_de_reserva` time NOT NULL,
  `max_reservas_dia` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_informacoes_periodo_espacos_id_foreign` (`espacos_id`),
  CONSTRAINT `espacos_informacoes_periodo_espacos_id_foreign` FOREIGN KEY (`espacos_id`) REFERENCES `espacos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_informacoes_periodo`
--

LOCK TABLES `espacos_informacoes_periodo` WRITE;
/*!40000 ALTER TABLE `espacos_informacoes_periodo` DISABLE KEYS */;
INSERT INTO `espacos_informacoes_periodo` VALUES (1,3,'08:00:00','21:30:00','00:30:00',4,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `espacos_informacoes_periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_reservas_por_diaria`
--

DROP TABLE IF EXISTS `espacos_reservas_por_diaria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_reservas_por_diaria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `espacos_id` int(10) unsigned DEFAULT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `unidades_id` int(10) unsigned DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reserva` date NOT NULL,
  `horario` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_bloqueio` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_reservas_por_diaria_espacos_id_foreign` (`espacos_id`),
  KEY `espacos_reservas_por_diaria_moradores_id_foreign` (`moradores_id`),
  KEY `espacos_reservas_por_diaria_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `espacos_reservas_por_diaria_espacos_id_foreign` FOREIGN KEY (`espacos_id`) REFERENCES `espacos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `espacos_reservas_por_diaria_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `espacos_reservas_por_diaria_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_reservas_por_diaria`
--

LOCK TABLES `espacos_reservas_por_diaria` WRITE;
/*!40000 ALTER TABLE `espacos_reservas_por_diaria` DISABLE KEYS */;
/*!40000 ALTER TABLE `espacos_reservas_por_diaria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_reservas_por_diaria_convidados`
--

DROP TABLE IF EXISTS `espacos_reservas_por_diaria_convidados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_reservas_por_diaria_convidados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservas` int(10) unsigned DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `presenca` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_reservas_por_diaria_convidados_reservas_foreign` (`reservas`),
  CONSTRAINT `espacos_reservas_por_diaria_convidados_reservas_foreign` FOREIGN KEY (`reservas`) REFERENCES `espacos_reservas_por_diaria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_reservas_por_diaria_convidados`
--

LOCK TABLES `espacos_reservas_por_diaria_convidados` WRITE;
/*!40000 ALTER TABLE `espacos_reservas_por_diaria_convidados` DISABLE KEYS */;
/*!40000 ALTER TABLE `espacos_reservas_por_diaria_convidados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_reservas_por_diaria_lidos`
--

DROP TABLE IF EXISTS `espacos_reservas_por_diaria_lidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_reservas_por_diaria_lidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `espacos_reservas_id` int(10) unsigned NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_reservas_por_diaria_lidos_espacos_reservas_id_foreign` (`espacos_reservas_id`),
  KEY `espacos_reservas_por_diaria_lidos_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `espacos_reservas_por_diaria_lidos_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `espacos_reservas_por_diaria_lidos_espacos_reservas_id_foreign` FOREIGN KEY (`espacos_reservas_id`) REFERENCES `espacos_reservas_por_diaria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_reservas_por_diaria_lidos`
--

LOCK TABLES `espacos_reservas_por_diaria_lidos` WRITE;
/*!40000 ALTER TABLE `espacos_reservas_por_diaria_lidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `espacos_reservas_por_diaria_lidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_reservas_por_periodo`
--

DROP TABLE IF EXISTS `espacos_reservas_por_periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_reservas_por_periodo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `espacos_id` int(10) unsigned DEFAULT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `unidades_id` int(10) unsigned DEFAULT NULL,
  `reserva` datetime NOT NULL,
  `anotacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `is_bloqueio` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_reservas_por_periodo_espacos_id_foreign` (`espacos_id`),
  KEY `espacos_reservas_por_periodo_moradores_id_foreign` (`moradores_id`),
  KEY `espacos_reservas_por_periodo_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `espacos_reservas_por_periodo_espacos_id_foreign` FOREIGN KEY (`espacos_id`) REFERENCES `espacos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `espacos_reservas_por_periodo_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `espacos_reservas_por_periodo_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_reservas_por_periodo`
--

LOCK TABLES `espacos_reservas_por_periodo` WRITE;
/*!40000 ALTER TABLE `espacos_reservas_por_periodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `espacos_reservas_por_periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espacos_reservas_por_periodo_lidos`
--

DROP TABLE IF EXISTS `espacos_reservas_por_periodo_lidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espacos_reservas_por_periodo_lidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `espacos_reservas_id` int(10) unsigned NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `espacos_reservas_por_periodo_lidos_espacos_reservas_id_foreign` (`espacos_reservas_id`),
  KEY `espacos_reservas_por_periodo_lidos_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `espacos_reservas_por_periodo_lidos_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `espacos_reservas_por_periodo_lidos_espacos_reservas_id_foreign` FOREIGN KEY (`espacos_reservas_id`) REFERENCES `espacos_reservas_por_periodo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espacos_reservas_por_periodo_lidos`
--

LOCK TABLES `espacos_reservas_por_periodo_lidos` WRITE;
/*!40000 ALTER TABLE `espacos_reservas_por_periodo_lidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `espacos_reservas_por_periodo_lidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fale_com`
--

DROP TABLE IF EXISTS `fale_com`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fale_com` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contato_sindico` text COLLATE utf8_unicode_ci NOT NULL,
  `contato_conselho` text COLLATE utf8_unicode_ci NOT NULL,
  `contato_zelador` text COLLATE utf8_unicode_ci NOT NULL,
  `contato_portaria` text COLLATE utf8_unicode_ci NOT NULL,
  `contato_administradora` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fale_com`
--

LOCK TABLES `fale_com` WRITE;
/*!40000 ALTER TABLE `fale_com` DISABLE KEYS */;
INSERT INTO `fale_com` VALUES (1,'<p>Informações de contato do síndico</p>','<p>Informações de contato do conselho</p>','<p>Informações de contato do zelador</p>','<p>Informações de contato da portaria</p>','<p>Informações de contato da administrador</p>a','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `fale_com` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fale_com_mensagens`
--

DROP TABLE IF EXISTS `fale_com_mensagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fale_com_mensagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `em_resposta_a` int(10) unsigned DEFAULT NULL,
  `assunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `destino` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fale_com_mensagens_moradores_id_foreign` (`moradores_id`),
  KEY `fale_com_mensagens_em_resposta_a_foreign` (`em_resposta_a`),
  CONSTRAINT `fale_com_mensagens_em_resposta_a_foreign` FOREIGN KEY (`em_resposta_a`) REFERENCES `fale_com_mensagens` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fale_com_mensagens_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fale_com_mensagens`
--

LOCK TABLES `fale_com_mensagens` WRITE;
/*!40000 ALTER TABLE `fale_com_mensagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `fale_com_mensagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fale_com_mensagens_lidas`
--

DROP TABLE IF EXISTS `fale_com_mensagens_lidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fale_com_mensagens_lidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fale_com_mensagens_id` int(10) unsigned DEFAULT NULL,
  `administradores_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fale_com_mensagens_lidas_fale_com_mensagens_id_foreign` (`fale_com_mensagens_id`),
  KEY `fale_com_mensagens_lidas_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `fale_com_mensagens_lidas_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fale_com_mensagens_lidas_fale_com_mensagens_id_foreign` FOREIGN KEY (`fale_com_mensagens_id`) REFERENCES `fale_com_mensagens` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fale_com_mensagens_lidas`
--

LOCK TABLES `fale_com_mensagens_lidas` WRITE;
/*!40000 ALTER TABLE `fale_com_mensagens_lidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `fale_com_mensagens_lidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questao` text COLLATE utf8_unicode_ci NOT NULL,
  `resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (1,'Questão Exemplo','<p>Resposta exemplo, Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n							Magni modi ducimus, nemo ex maiores perferendis, nam esse facere ipsa. Tempore,\n							sed molestiae voluptatibus magnam accusamus saepe vel alias commodi vero.</p>',0,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_info`
--

DROP TABLE IF EXISTS `faq_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_info`
--

LOCK TABLES `faq_info` WRITE;
/*!40000 ALTER TABLE `faq_info` DISABLE KEYS */;
INSERT INTO `faq_info` VALUES (2,'<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n        Magni modi ducimus, nemo ex maiores perferendis, nam esse facere ipsa. Tempore,\n        sed molestiae voluptatibus magnam accusamus saepe vel alias commodi vero.</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `faq_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `festas_particulares`
--

DROP TABLE IF EXISTS `festas_particulares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `festas_particulares` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned DEFAULT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `reserva` date NOT NULL,
  `horario` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `festas_particulares_unidades_id_foreign` (`unidades_id`),
  KEY `festas_particulares_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `festas_particulares_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `festas_particulares_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `festas_particulares`
--

LOCK TABLES `festas_particulares` WRITE;
/*!40000 ALTER TABLE `festas_particulares` DISABLE KEYS */;
/*!40000 ALTER TABLE `festas_particulares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `festas_particulares_convidados`
--

DROP TABLE IF EXISTS `festas_particulares_convidados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `festas_particulares_convidados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `festas_particulares_id` int(10) unsigned NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `presenca` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `festas_particulares_convidados_festas_particulares_id_foreign` (`festas_particulares_id`),
  CONSTRAINT `festas_particulares_convidados_festas_particulares_id_foreign` FOREIGN KEY (`festas_particulares_id`) REFERENCES `festas_particulares` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `festas_particulares_convidados`
--

LOCK TABLES `festas_particulares_convidados` WRITE;
/*!40000 ALTER TABLE `festas_particulares_convidados` DISABLE KEYS */;
/*!40000 ALTER TABLE `festas_particulares_convidados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `festas_particulares_lidas`
--

DROP TABLE IF EXISTS `festas_particulares_lidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `festas_particulares_lidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `festas_particulares_id` int(10) unsigned NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `festas_particulares_lidas_festas_particulares_id_foreign` (`festas_particulares_id`),
  KEY `festas_particulares_lidas_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `festas_particulares_lidas_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `festas_particulares_lidas_festas_particulares_id_foreign` FOREIGN KEY (`festas_particulares_id`) REFERENCES `festas_particulares` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `festas_particulares_lidas`
--

LOCK TABLES `festas_particulares_lidas` WRITE;
/*!40000 ALTER TABLE `festas_particulares_lidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `festas_particulares_lidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fila_emails`
--

DROP TABLE IF EXISTS `fila_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fila_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `destino_nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `destino_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fila_emails`
--

LOCK TABLES `fila_emails` WRITE;
/*!40000 ALTER TABLE `fila_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `fila_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fornecedores_categorias_id` int(10) unsigned DEFAULT NULL,
  `outra_categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descritivo` text COLLATE utf8_unicode_ci NOT NULL,
  `indicado_por` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fornecedores_fornecedores_categorias_id_foreign` (`fornecedores_categorias_id`),
  KEY `fornecedores_indicado_por_foreign` (`indicado_por`),
  CONSTRAINT `fornecedores_fornecedores_categorias_id_foreign` FOREIGN KEY (`fornecedores_categorias_id`) REFERENCES `fornecedores_categorias` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fornecedores_indicado_por_foreign` FOREIGN KEY (`indicado_por`) REFERENCES `moradores` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores`
--

LOCK TABLES `fornecedores` WRITE;
/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores_avaliacoes`
--

DROP TABLE IF EXISTS `fornecedores_avaliacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores_avaliacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fornecedores_id` int(10) unsigned DEFAULT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `nota` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fornecedores_avaliacoes_fornecedores_id_foreign` (`fornecedores_id`),
  KEY `fornecedores_avaliacoes_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `fornecedores_avaliacoes_fornecedores_id_foreign` FOREIGN KEY (`fornecedores_id`) REFERENCES `fornecedores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fornecedores_avaliacoes_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores_avaliacoes`
--

LOCK TABLES `fornecedores_avaliacoes` WRITE;
/*!40000 ALTER TABLE `fornecedores_avaliacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedores_avaliacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores_categorias`
--

DROP TABLE IF EXISTS `fornecedores_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fornecedores_categorias_titulo_unique` (`titulo`),
  UNIQUE KEY `fornecedores_categorias_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores_categorias`
--

LOCK TABLES `fornecedores_categorias` WRITE;
/*!40000 ALTER TABLE `fornecedores_categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedores_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornecedores_imagens`
--

DROP TABLE IF EXISTS `fornecedores_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornecedores_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fornecedores_id` int(10) unsigned DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fornecedores_imagens_fornecedores_id_foreign` (`fornecedores_id`),
  CONSTRAINT `fornecedores_imagens_fornecedores_id_foreign` FOREIGN KEY (`fornecedores_id`) REFERENCES `fornecedores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores_imagens`
--

LOCK TABLES `fornecedores_imagens` WRITE;
/*!40000 ALTER TABLE `fornecedores_imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedores_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrucoes_de_uso`
--

DROP TABLE IF EXISTS `instrucoes_de_uso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instrucoes_de_uso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questao` text COLLATE utf8_unicode_ci NOT NULL,
  `resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrucoes_de_uso`
--

LOCK TABLES `instrucoes_de_uso` WRITE;
/*!40000 ALTER TABLE `instrucoes_de_uso` DISABLE KEYS */;
/*!40000 ALTER TABLE `instrucoes_de_uso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lavanderia_fichas`
--

DROP TABLE IF EXISTS `lavanderia_fichas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lavanderia_fichas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quantidade` int(11) NOT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `is_faturado` int(11) NOT NULL DEFAULT '0',
  `is_retirada_solicitada` int(11) NOT NULL DEFAULT '0',
  `motivo_cancelamento` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `lavanderia_fichas_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `lavanderia_fichas_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lavanderia_fichas`
--

LOCK TABLES `lavanderia_fichas` WRITE;
/*!40000 ALTER TABLE `lavanderia_fichas` DISABLE KEYS */;
/*!40000 ALTER TABLE `lavanderia_fichas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acao` text COLLATE utf8_unicode_ci NOT NULL,
  `registro` text COLLATE utf8_unicode_ci NOT NULL,
  `usuario` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,'documento_criado','{\"documentos_categoria_id\":\"3\",\"titulo\":\"Teste\",\"slug\":\"teste\",\"texto\":\"Teste\",\"arquivo\":\"teste.pdf\",\"updated_at\":\"2017-12-05 05:09:09\",\"created_at\":\"2017-12-05 05:09:09\",\"id\":1}','{\"tipo\":\"master\",\"login\":\"trupe\",\"email\":\"contato@trupe.net\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\"}','2017-12-05 07:09:09','0000-00-00 00:00:00'),(2,'documento_removido','{\"id\":1,\"documentos_categoria_id\":3,\"titulo\":\"Teste\",\"slug\":\"teste\",\"texto\":\"Teste\",\"arquivo\":\"teste.pdf\",\"created_at\":\"2017-12-05 05:09:09\",\"updated_at\":\"2017-12-05 05:09:09\"}','{\"tipo\":\"master\",\"login\":\"trupe\",\"email\":\"contato@trupe.net\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\"}','2017-12-05 07:09:43','0000-00-00 00:00:00'),(3,'portaria_cadastrou_encomenda','{\"unidades_id\":1,\"updated_at\":\"2017-12-05 05:28:21\",\"created_at\":\"2017-12-05 05:28:21\",\"id\":1}','{\"login\":\"portaria\",\"email\":\"portaria@trupe.net\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"2017-12-05 05:25:02\"}','2017-12-05 07:28:21','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_09_16_140613_create_moradores',1),('2015_09_16_141426_create_moradores_recuperacao_senha',1),('2015_09_16_141656_create_moradores_privacidade',1),('2015_09_16_143008_create_moradores_amizades',1),('2015_09_29_091825_create_administradores_table',1),('2015_09_30_103926_create_avisos_table',1),('2015_09_30_104034_create_avisos_lidos_table',1),('2015_10_06_145619_create_ocorrencias_table',1),('2015_10_08_100341_create_notificacoes_table',1),('2015_10_14_111726_alter_ocorrencias_table',1),('2015_10_14_175043_create_documentos_categorias_table',1),('2015_10_14_175059_create_documentos_table',1),('2015_10_16_111346_create_unidades_table',1),('2015_10_16_114655_alter_moradores_table',1),('2015_10_19_133133_create_moradores_da_unidade_table',1),('2015_10_19_140052_create_moradores_da_unidade_privacidade_table',1),('2015_10_21_150837_create_veiculos_da_unidade_table',1),('2015_10_21_150859_create_veiculos_da_unidade_privacidade_table',1),('2015_10_22_113343_create_animais_de_estimacao_table',1),('2015_10_22_113355_create_animais_de_estimacao_privacidade_table',1),('2015_10_26_154235_create_agenda_administracao_table',1),('2015_10_28_100328_alter_admin_table',1),('2015_10_28_114806_create_fale_com_table',1),('2015_10_28_114823_create_fale_com_mensagens_table',1),('2015_10_30_100233_create_faq_table',1),('2015_10_30_170613_create_mudancas_agendamento_table',1),('2015_10_30_170631_create_mudancas_horarios_table',1),('2015_11_05_162207_create_ocorrencias_lidas_table',1),('2015_11_06_100608_create_fale_com_mensagens_lidas_table',1),('2015_11_06_103922_create_mudancas_agendamento_lidos',1),('2015_11_09_095902_create_chamados_de_manutencao_table',1),('2015_11_09_095927_create_chamados_de_manutencao_lidos_table',1),('2015_11_09_100135_create_chamados_de_manutencao_fotos_table',1),('2015_11_11_101333_create_espacos_table',1),('2015_11_12_103615_alter_chamados_de_manutencao_table',1),('2015_11_12_170853_create_espacos_reservas_table',1),('2015_11_17_092904_create_espacos_reservas_por_periodo_lidos',1),('2015_11_18_102400_create_pessoas_autorizadas_table',1),('2015_11_18_102422_create_pessoas_autorizadas_privacidade_table',1),('2015_11_18_143445_create_pessoas_nao_autorizadas_table',1),('2015_11_18_143500_create_pessoas_nao_autorizadas_privacidade_table',1),('2015_11_18_172551_create_prestadores_de_servico_table',1),('2015_11_18_172618_create_prestadores_de_servico_privacidade_table',1),('2015_11_23_114659_alter_moradores_troca_email_table',1),('2015_11_30_181458_create_instrucoes_de_uso_table',1),('2015_12_03_103101_create_espacos_reservas_por_diaria',1),('2015_12_03_103110_create_espacos_reservas_por_diaria_lido',1),('2015_12_04_103154_create_espacos_informacoes_diaria_table',1),('2015_12_04_131012_create_espacos_informacoes_periodo_table',1),('2015_12_14_141727_create_porteiros_table',1),('2015_12_15_162907_create_correspondencias_table',1),('2015_12_15_162926_create_encomendas_table',1),('2015_12_18_095329_alter_moradores_apelido_table',1),('2016_01_07_115244_alter_moradores_nascimento_table',1),('2016_01_08_165318_alter_reservas_titulo_table',1),('2016_01_11_110109_create_espacos_reservas_por_diaria_convidados_table',1),('2016_01_12_102138_create_festas_particulares_table',1),('2016_01_12_103007_create_festas_particulares_convidados_table',1),('2016_01_14_144827_alter_reservas_por_diaria_table',1),('2016_01_14_155506_alter_reservas_por_periodo_table',1),('2016_01_14_175402_alter_reservas_por_diaria_horario_table',1),('2016_01_14_180757_alter_festas_particulares_table',1),('2016_01_18_094233_create_novo_notificacoes_table',1),('2016_01_18_095024_create_novo_notificacoes_admin_table',1),('2016_01_19_105837_create_festas_particulares_lidas',1),('2016_01_29_092114_create_enquete_table',1),('2016_01_29_092150_create_enquete_opcoes_table',1),('2016_01_29_092159_create_enquete_votos_table',1),('2016_02_01_164039_alter_enquetes_table',1),('2016_02_02_124620_create_classificados_table',1),('2016_02_02_124649_create_classificados_imagens_table',1),('2016_02_04_171303_create_moradores_preferencias_table',1),('2016_02_11_135315_create_fila_emails_table',1),('2016_02_15_112517_create_comunicacao_administracao_table',1),('2016_02_16_140531_alter_notificacoes_table_comunicacao',1),('2016_02_17_112248_create_comunicacao_administracao_lido_table',1),('2016_02_18_164158_create_lavanderia_fichas_table',1),('2016_02_24_092000_alter_lavanderia_fichas_table',1),('2016_02_29_152840_create_fornecedores_categorias_table',1),('2016_02_29_152851_create_fornecedores_table',1),('2016_02_29_152903_create_fornecedores_imagens_table',1),('2016_02_29_152917_create_fornecedores_avaliacoes_table',1),('2016_03_14_154542_alter_espacos_table',1),('2016_03_14_164207_alter_unidades_table',1),('2016_03_14_190526_create_faq_info_table',1),('2016_03_14_202938_create_regulamentos_table',1),('2016_03_16_151313_alter_veiculos_da_unidade_table',1),('2016_03_17_134105_alter_veiculos_da_unidade_nullable_table',1),('2016_04_05_171649_alter_ocorrencias_table_visibilidade',1),('2016_05_27_124705_create_log_table',1),('2016_08_16_160228_alter_festas_particulares_convidados_presenca_table',1),('2016_08_16_160250_alter_espacos_reservas_por_diaria_convidados_presenca_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores`
--

DROP TABLE IF EXISTS `moradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned NOT NULL,
  `relacao_unidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_mudanca` date DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apelido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_fixo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_celular` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone_comercial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `novo_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `novo_email_activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `moradores_email_unique` (`email`),
  KEY `moradores_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `moradores_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores`
--

LOCK TABLES `moradores` WRITE;
/*!40000 ALTER TABLE `moradores` DISABLE KEYS */;
INSERT INTO `moradores` VALUES (1,1,'proprietario','2014-09-01','1986-12-06','Morador Teste','Morador','','condominios@trupe.net','$2y$10$5iPyCaiGGMeBzXKtKl9EpenVED0ol3Eqjj3xaI/B5KDQ9sx0vZyHy','(11) 5555-5555','(11) 5555-5555','(11) 5555-5555',NULL,'1',NULL,NULL,'pwQfLQVfYy61h7QwdBiHjoMjx7VBXCemCgkh4YerTjARKnm5sWB7hsFgT6vi',NULL,'2017-12-05 02:00:00','2017-12-05 07:15:20');
/*!40000 ALTER TABLE `moradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores_amizades`
--

DROP TABLE IF EXISTS `moradores_amizades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores_amizades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_id` int(10) unsigned NOT NULL,
  `amigo_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `moradores_amizades_moradores_id_foreign` (`moradores_id`),
  KEY `moradores_amizades_amigo_id_foreign` (`amigo_id`),
  CONSTRAINT `moradores_amizades_amigo_id_foreign` FOREIGN KEY (`amigo_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `moradores_amizades_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores_amizades`
--

LOCK TABLES `moradores_amizades` WRITE;
/*!40000 ALTER TABLE `moradores_amizades` DISABLE KEYS */;
/*!40000 ALTER TABLE `moradores_amizades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores_da_unidade`
--

DROP TABLE IF EXISTS `moradores_da_unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores_da_unidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date NOT NULL,
  `parentesco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `unidades_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `moradores_da_unidade_moradores_id_foreign` (`moradores_id`),
  KEY `moradores_da_unidade_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `moradores_da_unidade_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `moradores_da_unidade_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores_da_unidade`
--

LOCK TABLES `moradores_da_unidade` WRITE;
/*!40000 ALTER TABLE `moradores_da_unidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `moradores_da_unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores_da_unidade_privacidade`
--

DROP TABLE IF EXISTS `moradores_da_unidade_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores_da_unidade_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_da_unidade_id` int(10) unsigned NOT NULL,
  `visibilidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `moradores_da_unidade_privacidade_moradores_da_unidade_id_foreign` (`moradores_da_unidade_id`),
  CONSTRAINT `moradores_da_unidade_privacidade_moradores_da_unidade_id_foreign` FOREIGN KEY (`moradores_da_unidade_id`) REFERENCES `moradores_da_unidade` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores_da_unidade_privacidade`
--

LOCK TABLES `moradores_da_unidade_privacidade` WRITE;
/*!40000 ALTER TABLE `moradores_da_unidade_privacidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `moradores_da_unidade_privacidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores_notificacoes`
--

DROP TABLE IF EXISTS `moradores_notificacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores_notificacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_notificacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `ocorrencias_id` int(10) unsigned DEFAULT NULL,
  `correspondencias_id` int(10) unsigned DEFAULT NULL,
  `encomendas_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `moradores_notificacoes_moradores_id_foreign` (`moradores_id`),
  KEY `moradores_notificacoes_ocorrencias_id_foreign` (`ocorrencias_id`),
  KEY `moradores_notificacoes_correspondencias_id_foreign` (`correspondencias_id`),
  KEY `moradores_notificacoes_encomendas_id_foreign` (`encomendas_id`),
  CONSTRAINT `moradores_notificacoes_correspondencias_id_foreign` FOREIGN KEY (`correspondencias_id`) REFERENCES `correspondencias` (`id`) ON DELETE CASCADE,
  CONSTRAINT `moradores_notificacoes_encomendas_id_foreign` FOREIGN KEY (`encomendas_id`) REFERENCES `encomendas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `moradores_notificacoes_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `moradores_notificacoes_ocorrencias_id_foreign` FOREIGN KEY (`ocorrencias_id`) REFERENCES `ocorrencias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores_notificacoes`
--

LOCK TABLES `moradores_notificacoes` WRITE;
/*!40000 ALTER TABLE `moradores_notificacoes` DISABLE KEYS */;
INSERT INTO `moradores_notificacoes` VALUES (1,'encomenda','Tem encomenda para ser retirada na portaria.',1,NULL,NULL,1,'2017-12-05 07:28:21','2017-12-05 07:28:21');
/*!40000 ALTER TABLE `moradores_notificacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores_preferencias`
--

DROP TABLE IF EXISTS `moradores_preferencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores_preferencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_id` int(10) unsigned NOT NULL,
  `enviar_email_nova_correspondencia` tinyint(4) NOT NULL DEFAULT '0',
  `enviar_email_novo_aviso` tinyint(4) NOT NULL DEFAULT '0',
  `enviar_email_novo_documento` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `moradores_preferencias_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `moradores_preferencias_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores_preferencias`
--

LOCK TABLES `moradores_preferencias` WRITE;
/*!40000 ALTER TABLE `moradores_preferencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `moradores_preferencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moradores_privacidade`
--

DROP TABLE IF EXISTS `moradores_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moradores_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_id` int(10) unsigned NOT NULL,
  `dado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visibilidade` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `moradores_privacidade_moradores_id_foreign` (`moradores_id`),
  CONSTRAINT `moradores_privacidade_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moradores_privacidade`
--

LOCK TABLES `moradores_privacidade` WRITE;
/*!40000 ALTER TABLE `moradores_privacidade` DISABLE KEYS */;
INSERT INTO `moradores_privacidade` VALUES (1,1,'email',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,'telefone_fixo',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,'telefone_celular',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,'telefone_comercial',1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `moradores_privacidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mudancas_agendamento`
--

DROP TABLE IF EXISTS `mudancas_agendamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mudancas_agendamento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `unidades_id` int(10) unsigned DEFAULT NULL,
  `data` date NOT NULL,
  `periodo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `mudancas_agendamento_moradores_id_foreign` (`moradores_id`),
  KEY `mudancas_agendamento_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `mudancas_agendamento_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mudancas_agendamento_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mudancas_agendamento`
--

LOCK TABLES `mudancas_agendamento` WRITE;
/*!40000 ALTER TABLE `mudancas_agendamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `mudancas_agendamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mudancas_agendamento_lidas`
--

DROP TABLE IF EXISTS `mudancas_agendamento_lidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mudancas_agendamento_lidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mudancas_agendamento_id` int(10) unsigned DEFAULT NULL,
  `administradores_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `mudancas_agendamento_lidas_mudancas_agendamento_id_foreign` (`mudancas_agendamento_id`),
  KEY `mudancas_agendamento_lidas_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `mudancas_agendamento_lidas_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mudancas_agendamento_lidas_mudancas_agendamento_id_foreign` FOREIGN KEY (`mudancas_agendamento_id`) REFERENCES `mudancas_agendamento` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mudancas_agendamento_lidas`
--

LOCK TABLES `mudancas_agendamento_lidas` WRITE;
/*!40000 ALTER TABLE `mudancas_agendamento_lidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `mudancas_agendamento_lidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mudancas_informacoes`
--

DROP TABLE IF EXISTS `mudancas_informacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mudancas_informacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `horario_manha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `horario_tarde` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regulamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mudancas_informacoes`
--

LOCK TABLES `mudancas_informacoes` WRITE;
/*!40000 ALTER TABLE `mudancas_informacoes` DISABLE KEYS */;
INSERT INTO `mudancas_informacoes` VALUES (1,'das 9h às 12h','das 14h às 18h','arquivo','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `mudancas_informacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocorrencias`
--

DROP TABLE IF EXISTS `ocorrencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocorrencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `autor_id` int(10) unsigned DEFAULT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `em_resposta_a` int(10) unsigned DEFAULT NULL,
  `origem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ocorrencia_publica` int(11) NOT NULL DEFAULT '0',
  `finalizada` tinyint(4) NOT NULL DEFAULT '0',
  `finalizada_em` datetime DEFAULT NULL,
  `finalizada_por` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ocorrencias_autor_id_foreign` (`autor_id`),
  KEY `ocorrencias_moradores_id_foreign` (`moradores_id`),
  KEY `ocorrencias_em_resposta_a_foreign` (`em_resposta_a`),
  KEY `ocorrencias_finalizada_por_foreign` (`finalizada_por`),
  CONSTRAINT `ocorrencias_autor_id_foreign` FOREIGN KEY (`autor_id`) REFERENCES `moradores` (`id`) ON DELETE SET NULL,
  CONSTRAINT `ocorrencias_em_resposta_a_foreign` FOREIGN KEY (`em_resposta_a`) REFERENCES `ocorrencias` (`id`) ON DELETE SET NULL,
  CONSTRAINT `ocorrencias_finalizada_por_foreign` FOREIGN KEY (`finalizada_por`) REFERENCES `moradores` (`id`) ON DELETE SET NULL,
  CONSTRAINT `ocorrencias_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocorrencias`
--

LOCK TABLES `ocorrencias` WRITE;
/*!40000 ALTER TABLE `ocorrencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocorrencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocorrencias_lidas`
--

DROP TABLE IF EXISTS `ocorrencias_lidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocorrencias_lidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ocorrencias_id` int(10) unsigned NOT NULL,
  `administradores_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `ocorrencias_lidas_ocorrencias_id_foreign` (`ocorrencias_id`),
  KEY `ocorrencias_lidas_administradores_id_foreign` (`administradores_id`),
  CONSTRAINT `ocorrencias_lidas_administradores_id_foreign` FOREIGN KEY (`administradores_id`) REFERENCES `administradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ocorrencias_lidas_ocorrencias_id_foreign` FOREIGN KEY (`ocorrencias_id`) REFERENCES `ocorrencias` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocorrencias_lidas`
--

LOCK TABLES `ocorrencias_lidas` WRITE;
/*!40000 ALTER TABLE `ocorrencias_lidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocorrencias_lidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas_autorizadas`
--

DROP TABLE IF EXISTS `pessoas_autorizadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas_autorizadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date NOT NULL,
  `parentesco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `unidades_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pessoas_autorizadas_moradores_id_foreign` (`moradores_id`),
  KEY `pessoas_autorizadas_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `pessoas_autorizadas_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pessoas_autorizadas_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas_autorizadas`
--

LOCK TABLES `pessoas_autorizadas` WRITE;
/*!40000 ALTER TABLE `pessoas_autorizadas` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoas_autorizadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas_autorizadas_privacidade`
--

DROP TABLE IF EXISTS `pessoas_autorizadas_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas_autorizadas_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pessoas_autorizadas_id` int(10) unsigned NOT NULL,
  `visibilidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pessoas_autorizadas_privacidade_pessoas_autorizadas_id_foreign` (`pessoas_autorizadas_id`),
  CONSTRAINT `pessoas_autorizadas_privacidade_pessoas_autorizadas_id_foreign` FOREIGN KEY (`pessoas_autorizadas_id`) REFERENCES `pessoas_autorizadas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas_autorizadas_privacidade`
--

LOCK TABLES `pessoas_autorizadas_privacidade` WRITE;
/*!40000 ALTER TABLE `pessoas_autorizadas_privacidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoas_autorizadas_privacidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas_nao_autorizadas`
--

DROP TABLE IF EXISTS `pessoas_nao_autorizadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas_nao_autorizadas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date NOT NULL,
  `parentesco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `unidades_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pessoas_nao_autorizadas_moradores_id_foreign` (`moradores_id`),
  KEY `pessoas_nao_autorizadas_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `pessoas_nao_autorizadas_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pessoas_nao_autorizadas_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas_nao_autorizadas`
--

LOCK TABLES `pessoas_nao_autorizadas` WRITE;
/*!40000 ALTER TABLE `pessoas_nao_autorizadas` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoas_nao_autorizadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoas_nao_autorizadas_privacidade`
--

DROP TABLE IF EXISTS `pessoas_nao_autorizadas_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoas_nao_autorizadas_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pessoas_id` int(10) unsigned NOT NULL,
  `visibilidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pessoas_nao_autorizadas_privacidade_pessoas_id_foreign` (`pessoas_id`),
  CONSTRAINT `pessoas_nao_autorizadas_privacidade_pessoas_id_foreign` FOREIGN KEY (`pessoas_id`) REFERENCES `pessoas_nao_autorizadas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoas_nao_autorizadas_privacidade`
--

LOCK TABLES `pessoas_nao_autorizadas_privacidade` WRITE;
/*!40000 ALTER TABLE `pessoas_nao_autorizadas_privacidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `pessoas_nao_autorizadas_privacidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `porteiros`
--

DROP TABLE IF EXISTS `porteiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `porteiros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `porteiros_login_unique` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `porteiros`
--

LOCK TABLES `porteiros` WRITE;
/*!40000 ALTER TABLE `porteiros` DISABLE KEYS */;
INSERT INTO `porteiros` VALUES (1,'portaria','portaria@trupe.net','$2y$10$gcWRtyNTi6krfKow8VYFK.lmIG9s1iXcrKKDTp3qQYwiMx8PDQBnq','dhyMnPR9JHWbcAm5pv98msVdjkJxHl5uvlnKu7cuxUdbjhQXln0h96TD1BXY','0000-00-00 00:00:00','2017-12-05 07:28:33');
/*!40000 ALTER TABLE `porteiros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestadores_de_servico`
--

DROP TABLE IF EXISTS `prestadores_de_servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestadores_de_servico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `documento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date DEFAULT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissao_inicio` date NOT NULL,
  `permissao_termino` date NOT NULL,
  `moradores_id` int(10) unsigned NOT NULL,
  `unidades_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `prestadores_de_servico_moradores_id_foreign` (`moradores_id`),
  KEY `prestadores_de_servico_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `prestadores_de_servico_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `prestadores_de_servico_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestadores_de_servico`
--

LOCK TABLES `prestadores_de_servico` WRITE;
/*!40000 ALTER TABLE `prestadores_de_servico` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestadores_de_servico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestadores_de_servico_privacidade`
--

DROP TABLE IF EXISTS `prestadores_de_servico_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestadores_de_servico_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prestador_id` int(10) unsigned NOT NULL,
  `visibilidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `prestadores_de_servico_privacidade_prestador_id_foreign` (`prestador_id`),
  CONSTRAINT `prestadores_de_servico_privacidade_prestador_id_foreign` FOREIGN KEY (`prestador_id`) REFERENCES `prestadores_de_servico` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestadores_de_servico_privacidade`
--

LOCK TABLES `prestadores_de_servico_privacidade` WRITE;
/*!40000 ALTER TABLE `prestadores_de_servico_privacidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestadores_de_servico_privacidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recuperacao_senha`
--

DROP TABLE IF EXISTS `recuperacao_senha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recuperacao_senha` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `recuperacao_senha_email_index` (`email`),
  KEY `recuperacao_senha_token_index` (`token`),
  KEY `recuperacao_senha_type_index` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recuperacao_senha`
--

LOCK TABLES `recuperacao_senha` WRITE;
/*!40000 ALTER TABLE `recuperacao_senha` DISABLE KEYS */;
/*!40000 ALTER TABLE `recuperacao_senha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regulamentos`
--

DROP TABLE IF EXISTS `regulamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regulamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `completo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reduzido_reservas` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regulamentos`
--

LOCK TABLES `regulamentos` WRITE;
/*!40000 ALTER TABLE `regulamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `regulamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bloco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proprietario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_teste` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (1,'11','A','Proprietário Unidade 11 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(2,'11','B','Proprietário Unidade 11 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(3,'12','A','Proprietário Unidade 12 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(4,'12','B','Proprietário Unidade 12 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(5,'13','A','Proprietário Unidade 13 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(6,'13','B','Proprietário Unidade 13 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(7,'14','A','Proprietário Unidade 14 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(8,'14','B','Proprietário Unidade 14 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(9,'21','A','Proprietário Unidade 21 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(10,'21','B','Proprietário Unidade 21 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(11,'22','A','Proprietário Unidade 22 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(12,'22','B','Proprietário Unidade 22 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(13,'23','A','Proprietário Unidade 23 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(14,'23','B','Proprietário Unidade 23 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(15,'24','A','Proprietário Unidade 24 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(16,'24','B','Proprietário Unidade 24 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(17,'31','A','Proprietário Unidade 31 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(18,'31','B','Proprietário Unidade 31 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(19,'32','A','Proprietário Unidade 32 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(20,'32','B','Proprietário Unidade 32 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(21,'33','A','Proprietário Unidade 33 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(22,'33','B','Proprietário Unidade 33 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(23,'34','A','Proprietário Unidade 34 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(24,'34','B','Proprietário Unidade 34 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(25,'41','A','Proprietário Unidade 41 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(26,'41','B','Proprietário Unidade 41 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(27,'42','A','Proprietário Unidade 42 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(28,'42','B','Proprietário Unidade 42 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(29,'43','A','Proprietário Unidade 43 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(30,'43','B','Proprietário Unidade 43 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(31,'44','A','Proprietário Unidade 44 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(32,'44','B','Proprietário Unidade 44 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(33,'51','A','Proprietário Unidade 51 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(34,'51','B','Proprietário Unidade 51 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(35,'52','A','Proprietário Unidade 52 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(36,'52','B','Proprietário Unidade 52 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(37,'53','A','Proprietário Unidade 53 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(38,'53','B','Proprietário Unidade 53 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(39,'54','A','Proprietário Unidade 54 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(40,'54','B','Proprietário Unidade 54 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(41,'61','A','Proprietário Unidade 61 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(42,'61','B','Proprietário Unidade 61 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(43,'62','A','Proprietário Unidade 62 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(44,'62','B','Proprietário Unidade 62 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(45,'63','A','Proprietário Unidade 63 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(46,'63','B','Proprietário Unidade 63 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(47,'64','A','Proprietário Unidade 64 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(48,'64','B','Proprietário Unidade 64 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(49,'71','A','Proprietário Unidade 71 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(50,'71','B','Proprietário Unidade 71 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(51,'72','A','Proprietário Unidade 72 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(52,'72','B','Proprietário Unidade 72 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(53,'73','A','Proprietário Unidade 73 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(54,'73','B','Proprietário Unidade 73 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(55,'74','A','Proprietário Unidade 74 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(56,'74','B','Proprietário Unidade 74 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(57,'81','A','Proprietário Unidade 81 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(58,'81','B','Proprietário Unidade 81 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(59,'82','A','Proprietário Unidade 82 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(60,'82','B','Proprietário Unidade 82 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(61,'83','A','Proprietário Unidade 83 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(62,'83','B','Proprietário Unidade 83 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(63,'84','A','Proprietário Unidade 84 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(64,'84','B','Proprietário Unidade 84 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(65,'91','A','Proprietário Unidade 91 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(66,'91','B','Proprietário Unidade 91 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(67,'92','A','Proprietário Unidade 92 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(68,'92','B','Proprietário Unidade 92 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(69,'93','A','Proprietário Unidade 93 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(70,'93','B','Proprietário Unidade 93 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(71,'94','A','Proprietário Unidade 94 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(72,'94','B','Proprietário Unidade 94 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(73,'101','A','Proprietário Unidade 101 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(74,'101','B','Proprietário Unidade 101 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(75,'102','A','Proprietário Unidade 102 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(76,'102','B','Proprietário Unidade 102 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(77,'103','A','Proprietário Unidade 103 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(78,'103','B','Proprietário Unidade 103 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(79,'104','A','Proprietário Unidade 104 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(80,'104','B','Proprietário Unidade 104 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(81,'111','A','Proprietário Unidade 111 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(82,'111','B','Proprietário Unidade 111 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(83,'112','A','Proprietário Unidade 112 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(84,'112','B','Proprietário Unidade 112 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(85,'113','A','Proprietário Unidade 113 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(86,'113','B','Proprietário Unidade 113 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(87,'114','A','Proprietário Unidade 114 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(88,'114','B','Proprietário Unidade 114 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(89,'121','A','Proprietário Unidade 121 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(90,'121','B','Proprietário Unidade 121 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(91,'122','A','Proprietário Unidade 122 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(92,'122','B','Proprietário Unidade 122 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(93,'123','A','Proprietário Unidade 123 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(94,'123','B','Proprietário Unidade 123 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(95,'124','A','Proprietário Unidade 124 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(96,'124','B','Proprietário Unidade 124 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(97,'131','A','Proprietário Unidade 131 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(98,'131','B','Proprietário Unidade 131 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(99,'132','A','Proprietário Unidade 132 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(100,'132','B','Proprietário Unidade 132 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(101,'133','A','Proprietário Unidade 133 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(102,'133','B','Proprietário Unidade 133 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(103,'134','A','Proprietário Unidade 134 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(104,'134','B','Proprietário Unidade 134 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(105,'141','A','Proprietário Unidade 141 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(106,'141','B','Proprietário Unidade 141 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(107,'142','A','Proprietário Unidade 142 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(108,'142','B','Proprietário Unidade 142 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(109,'143','A','Proprietário Unidade 143 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(110,'143','B','Proprietário Unidade 143 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(111,'144','A','Proprietário Unidade 144 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(112,'144','B','Proprietário Unidade 144 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(113,'151','A','Proprietário Unidade 151 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(114,'151','B','Proprietário Unidade 151 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(115,'152','A','Proprietário Unidade 152 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(116,'152','B','Proprietário Unidade 152 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(117,'153','A','Proprietário Unidade 153 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(118,'153','B','Proprietário Unidade 153 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(119,'154','A','Proprietário Unidade 154 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(120,'154','B','Proprietário Unidade 154 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(121,'161','A','Proprietário Unidade 161 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(122,'161','B','Proprietário Unidade 161 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(123,'162','A','Proprietário Unidade 162 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(124,'162','B','Proprietário Unidade 162 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(125,'163','A','Proprietário Unidade 163 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(126,'163','B','Proprietário Unidade 163 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(127,'164','A','Proprietário Unidade 164 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(128,'164','B','Proprietário Unidade 164 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(129,'171','A','Proprietário Unidade 171 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(130,'171','B','Proprietário Unidade 171 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(131,'172','A','Proprietário Unidade 172 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(132,'172','B','Proprietário Unidade 172 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(133,'173','A','Proprietário Unidade 173 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(134,'173','B','Proprietário Unidade 173 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(135,'174','A','Proprietário Unidade 174 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(136,'174','B','Proprietário Unidade 174 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(137,'181','A','Proprietário Unidade 181 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(138,'181','B','Proprietário Unidade 181 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(139,'182','A','Proprietário Unidade 182 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(140,'182','B','Proprietário Unidade 182 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(141,'183','A','Proprietário Unidade 183 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(142,'183','B','Proprietário Unidade 183 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(143,'184','A','Proprietário Unidade 184 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(144,'184','B','Proprietário Unidade 184 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(145,'191','A','Proprietário Unidade 191 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(146,'191','B','Proprietário Unidade 191 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(147,'192','A','Proprietário Unidade 192 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(148,'192','B','Proprietário Unidade 192 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(149,'193','A','Proprietário Unidade 193 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(150,'193','B','Proprietário Unidade 193 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(151,'194','A','Proprietário Unidade 194 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(152,'194','B','Proprietário Unidade 194 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(153,'201','A','Proprietário Unidade 201 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(154,'201','B','Proprietário Unidade 201 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(155,'202','A','Proprietário Unidade 202 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(156,'202','B','Proprietário Unidade 202 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(157,'203','A','Proprietário Unidade 203 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(158,'203','B','Proprietário Unidade 203 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(159,'204','A','Proprietário Unidade 204 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(160,'204','B','Proprietário Unidade 204 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(161,'211','A','Proprietário Unidade 211 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(162,'211','B','Proprietário Unidade 211 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(163,'212','A','Proprietário Unidade 212 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(164,'212','B','Proprietário Unidade 212 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(165,'213','A','Proprietário Unidade 213 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(166,'213','B','Proprietário Unidade 213 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(167,'214','A','Proprietário Unidade 214 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(168,'214','B','Proprietário Unidade 214 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(169,'221','A','Proprietário Unidade 221 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(170,'221','B','Proprietário Unidade 221 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(171,'222','A','Proprietário Unidade 222 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(172,'222','B','Proprietário Unidade 222 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(173,'223','A','Proprietário Unidade 223 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(174,'223','B','Proprietário Unidade 223 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(175,'224','A','Proprietário Unidade 224 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(176,'224','B','Proprietário Unidade 224 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(177,'231','A','Proprietário Unidade 231 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(178,'231','B','Proprietário Unidade 231 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(179,'232','A','Proprietário Unidade 232 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(180,'233','A','Proprietário Unidade 233 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(181,'233','B','Proprietário Unidade 233 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(182,'234','A','Proprietário Unidade 234 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(183,'234','B','Proprietário Unidade 234 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(184,'241','A','Proprietário Unidade 241 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(185,'241','B','Proprietário Unidade 241 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(186,'242','A','Proprietário Unidade 242 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(187,'242','B','Proprietário Unidade 242 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(188,'243','A','Proprietário Unidade 243 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(189,'243','B','Proprietário Unidade 243 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(190,'244','A','Proprietário Unidade 244 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(191,'244','B','Proprietário Unidade 244 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(192,'251','A','Proprietário Unidade 251 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(193,'251','B','Proprietário Unidade 251 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(194,'252','A','Proprietário Unidade 252 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(195,'253','A','Proprietário Unidade 253 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(196,'253','B','Proprietário Unidade 253 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(197,'254','A','Proprietário Unidade 254 A',0,'2017-12-05 06:47:17','2017-12-05 06:47:17'),(198,'254','B','Proprietário Unidade 254 B',0,'2017-12-05 06:47:17','2017-12-05 06:47:17');
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veiculos_da_unidade`
--

DROP TABLE IF EXISTS `veiculos_da_unidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veiculos_da_unidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `veiculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `placa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vaga` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `moradores_id` int(10) unsigned DEFAULT NULL,
  `unidades_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `veiculos_da_unidade_moradores_id_foreign` (`moradores_id`),
  KEY `veiculos_da_unidade_unidades_id_foreign` (`unidades_id`),
  CONSTRAINT `veiculos_da_unidade_moradores_id_foreign` FOREIGN KEY (`moradores_id`) REFERENCES `moradores` (`id`) ON DELETE CASCADE,
  CONSTRAINT `veiculos_da_unidade_unidades_id_foreign` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculos_da_unidade`
--

LOCK TABLES `veiculos_da_unidade` WRITE;
/*!40000 ALTER TABLE `veiculos_da_unidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `veiculos_da_unidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `veiculos_da_unidade_privacidade`
--

DROP TABLE IF EXISTS `veiculos_da_unidade_privacidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `veiculos_da_unidade_privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `veiculos_da_unidade_id` int(10) unsigned NOT NULL,
  `visibilidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `veiculos_da_unidade_privacidade_veiculos_da_unidade_id_foreign` (`veiculos_da_unidade_id`),
  CONSTRAINT `veiculos_da_unidade_privacidade_veiculos_da_unidade_id_foreign` FOREIGN KEY (`veiculos_da_unidade_id`) REFERENCES `veiculos_da_unidade` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `veiculos_da_unidade_privacidade`
--

LOCK TABLES `veiculos_da_unidade_privacidade` WRITE;
/*!40000 ALTER TABLE `veiculos_da_unidade_privacidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `veiculos_da_unidade_privacidade` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-05  5:53:16
