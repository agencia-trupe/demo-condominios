var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;
elixir.config.publicDir  = 'public_html';
elixir.config.publicPath = 'public_html';

var paths = {
	src : {
		less   : 'resources/assets/less/',
		js     : 'resources/assets/js/',
		vendor : './resources/assets/vendor/',
	},
	output : {
		less  : 'public_html/assets/css/',
		js    : 'public_html/assets/js/',
		fonts : 'public_html/assets/css/fonts/',
	},
};

elixir(function(mix) {
    mix
    	// --------------------------------//
    	// Cópia de arquivos originais da
    	// pasta Vendor para o acesso público
    	// --------------------------------//

      .copy( paths.src.vendor + 'modernizr/modernizr.js', paths.output.js + 'modernizr.js', './')
    	.copy( paths.src.vendor + 'jquery/dist/jquery.min.js', paths.output.js + 'jquery.js', './')
    	.copy( paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js', paths.output.js + 'bootstrap.js', './')
      .copy( paths.src.vendor + 'bootstrap/dist/fonts/', paths.output.fonts)
      .copy( paths.src.vendor + 'ckeditor/', paths.output.js + 'ckeditor/')

      /* Executar somente 1 vez \/  */
      //.copy( paths.src.vendor + 'ckeditor/config.js', paths.src.js + 'ckeditor_config.js')
      /* Executar somente 1 vez /\ */

      .scripts( paths.src.js + 'ckeditor_config.js', paths.output.js + 'ckeditor_config.js', './')

      // --------------------------------//



      // --------------------------------//
    	// Estilos e Scripts - Módulo Moradores
    	// --------------------------------//

      .styles([
        paths.src.vendor + 'css-reset/reset.min.css',
        paths.src.vendor + 'tooltipster/css/tooltipster.css',
        paths.src.vendor + 'featherlight/src/featherlight.css',
        paths.src.vendor + 'featherlight/src/featherlight.gallery.css',
        paths.src.vendor + 'pikaday/css/pikaday.css'
      ], paths.output.less + 'moradores/vendor.css', './')

      .less( paths.src.less + 'moradores/*.less', paths.output.less + 'moradores/moradores.css')

    	.scripts([

      	paths.src.vendor + 'tooltipster/js/jquery.tooltipster.min.js',

        paths.src.vendor + 'featherlight/src/featherlight.js',
        paths.src.vendor + 'featherlight/src/featherlight.gallery.js',

        paths.src.vendor + 'blueimp-file-upload/js/vendor/jquery.ui.widget.js',
        paths.src.vendor + 'blueimp-file-upload/js/jquery.iframe-transport.js',
        paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',

        paths.src.vendor + 'moment/min/moment.min.js',
        paths.src.vendor + 'pikaday/pikaday.js',

        paths.src.vendor + 'readmore/readmore.js',

        paths.src.vendor + 'jQuery-Mask-Plugin/dist/jquery.mask.min.js',

				// paths.src.vendor + 'jquery.lazyload/jquery.lazyload.js',

        // paths.src.vendor + 'jquery-textext/src/textext.core.js',

        paths.src.js + 'moradores/src/base.js',
        paths.src.js + 'moradores/src/formularios.js',
        paths.src.js + 'moradores/src/auth.js',
        paths.src.js + 'moradores/src/dashboard.js',
        paths.src.js + 'moradores/src/ocorrencias.js',
        paths.src.js + 'moradores/src/moradores-da-unidade.js',
        paths.src.js + 'moradores/src/veiculos-da-unidade.js',
        paths.src.js + 'moradores/src/animais-de-estimacao.js',
        paths.src.js + 'moradores/src/fale-com.js',
        paths.src.js + 'moradores/src/faq.js',
        paths.src.js + 'moradores/src/agendamento-de-mudanca.js',
        paths.src.js + 'moradores/src/chamados-de-manutencao.js',
        paths.src.js + 'moradores/src/reserva-de-espacos.js',
        paths.src.js + 'moradores/src/pessoas-autorizadas.js',
        paths.src.js + 'moradores/src/pessoas-nao-autorizadas.js',
        paths.src.js + 'moradores/src/prestadores-de-servico.js',
        paths.src.js + 'moradores/src/meu-perfil.js',
        paths.src.js + 'moradores/src/alterar-senha.js',
        paths.src.js + 'moradores/src/instrucoes-de-uso.js',
        paths.src.js + 'moradores/src/festas-particulares.js',
				paths.src.js + 'moradores/src/amizades.js',
				paths.src.js + 'moradores/src/classificados.js',
				paths.src.js + 'moradores/src/fornecedores.js',

        paths.src.js + 'moradores/moradores.js',

      ], paths.output.js + 'moradores.js', './')




      // --------------------------------//
      // Estilos e Scripts - Módulo Admin
      // --------------------------------//

      .styles([
        paths.src.vendor + 'css-reset/reset.min.css',
        paths.src.vendor + 'jquery-ui/themes/base/jquery-ui.css',
        paths.src.vendor + 'bootstrap/dist/css/bootstrap.min.css',
        paths.src.vendor + 'featherlight/src/featherlight.css',
        paths.src.vendor + 'featherlight/src/featherlight.gallery.css',
        paths.src.vendor + 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
      ], paths.output.less + 'admin/vendor.css', './')

      .less( paths.src.less + 'admin/*.less', paths.output.less + 'admin/admin.css')

      .scripts([
        paths.src.vendor + 'jquery-ui/jquery-ui.min.js',
        paths.src.vendor + 'moment/min/moment-with-locales.min.js',
				paths.src.vendor + 'bootbox/bootbox.js',
        paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js',
        paths.src.vendor + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',
        paths.src.vendor + 'featherlight/src/featherlight.js',
        paths.src.vendor + 'featherlight/src/featherlight.gallery.js',
        paths.src.vendor + 'jQuery-Mask-Plugin/dist/jquery.mask.min.js',
        paths.src.js     + 'admin.js'
      ], paths.output.js  + 'admin.js', './')


      // --------------------------------//
      // Estilos e Scripts - Módulo Portaria
      // --------------------------------//

      .styles([
        paths.src.vendor + 'css-reset/reset.min.css',
        paths.src.vendor + 'featherlight/src/featherlight.css',
      ], paths.output.less + 'portaria/vendor.css', './')

      .less( paths.src.less + 'portaria/*.less', paths.output.less + 'portaria/portaria.css')

      .scripts([

        paths.src.vendor + 'featherlight/src/featherlight.js',
        paths.src.vendor + 'featherlight/src/featherlight.gallery.js',
				paths.src.vendor + 'jQuery-Mask-Plugin/dist/jquery.mask.min.js',

        paths.src.js + 'portaria/src/base.js',
        paths.src.js + 'portaria/src/auth.js',
        paths.src.js + 'portaria/src/correspondencias.js',
        paths.src.js + 'portaria/src/encomendas.js',
        paths.src.js + 'portaria/src/busca.js',
        paths.src.js + 'portaria/src/unidades.js',
        paths.src.js + 'portaria/src/dashboard.js',
				paths.src.js + 'portaria/src/comunicacao.js',
				paths.src.js + 'portaria/src/lavanderia.js',
				paths.src.js + 'portaria/src/veiculos.js',

        paths.src.js + 'portaria/portaria.js',

      ], paths.output.js + 'portaria.js', './');

});
