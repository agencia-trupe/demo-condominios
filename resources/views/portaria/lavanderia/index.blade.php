@extends('portaria.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('portaria.dashboard.partials.header')<!--

	--><div class="main" id="lavanderia-main">

	  	<div class="main-conteudo">

				<h1>
          LAVANDERIA
        </h1>

        <div class="busca-lavanderia">
					<label>
          	<input type="text" id="consultar-unidade-lavanderia" placeholder="CONSULTAR: número do apartamento + bloco">
					</label>

					<div id="loader">
						<div class="spinner">
						  <div class="rect1"></div>
						  <div class="rect2"></div>
						  <div class="rect3"></div>
						  <div class="rect4"></div>
						  <div class="rect5"></div>
						</div>
					</div>

					<div id="resultados-lavanderia">
						<h3>Exibindo registros ainda não faturados do: <strong>Apartamento <span id='resultados-unidade-placeholder'></span></strong></h3>
						<div class="contem-tabela"></div>
					</div>

        </div>

				@include('portaria.lavanderia.partials.mensagens')

				<div id="input-motivo-remocao">
          <label>
						<h3>Informe o motivo do pedido de retirada de fichas:</h3>
            <textarea class="motivo-remocao"></textarea>
          </label>
					<input type="button" value="ENVIAR" class="btn btn-success enviar-solicitacao-remocao">
        </div>

				<form action="{{ route('portaria.lavanderia.solicitar-fichas') }}" method="post" novalidate>

          <h1>
            SOLICITAR FICHAS
          </h1>

					<div class="data">{{ Carbon\Carbon::now()->format('d/m/Y H:i') }}</div>

          <input type="hidden" id="listaUnidadesJson" value="{{ $listaUnidadesJson }}">

					<fieldset>

						{!! csrf_field() !!}

						<label class="lavanderia-form-input
									 @if($errors->has('lavanderia.quantidade')) input-erro obrigatorio @endif ">
							<input type="text"
	                   name="lavanderia[quantidade]"
	                   id="input-lavanderia-quantidade"
	                   placeholder="Quantidade de fichas"
	                   value="{{ old('lavanderia.quantidade') }}"
										 required>
						</label>

            <label class="lavanderia-form-input
									 @if($errors->has('lavanderia.unidade')) input-erro obrigatorio @endif ">
							<input type="text"
	                   name="lavanderia[unidade]"
	                   id="input-lavanderia-unidade"
	                   placeholder="Número e Bloco do apartamento"
	                   value="{{ old('lavanderia.unidade') }}"
										 required>
						</label>

            <label class="lavanderia-form-input
                   lavanderia-form-input-login
                   espacado
									 @if($errors->has('login')) input-erro obrigatorio @endif ">
							<input type="email"
	                   name="email"
	                   id="input-lavanderia-login"
	                   placeholder="login"
	                   value="{{ old('email') }}"
										 required>
						</label>

            <label class="lavanderia-form-input
                   lavanderia-form-input-login
                   @if($errors->has('senha')) input-erro obrigatorio @endif ">
							<input type="password"
	                   name="password"
	                   id="input-lavanderia-senha"
	                   placeholder="senha"
	                   required>
						</label>

					</fieldset>

					<input type="submit" value="CADASTRAR" class="btn btn-info">

				</form>

			</div>

			@include('portaria.busca.resultados')

		</div>

	</section>

@stop
