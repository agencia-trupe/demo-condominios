@if(Session::has('sucesso_fichas'))
	<div id="retorno_formulario_shadow">
		<h3>REGISTRADO COM SUCESSO</h3>
		@if(Session::has('cadastro_fichas'))
			<p>
				data: {{ Session::get('cadastro_fichas.data') }}<br>
				apartamento: <strong>{{ Session::get('cadastro_fichas.apartamento') }}</strong> <br>
				número de fichas: <strong>{{ Session::get('cadastro_fichas.quantidade') }}</strong>
			</p>
		@endif
	</div>
@endif

@if($errors->any())
	<p class="retorno_formulario erro">
		{{ $errors->first() }}
	</p>
@endif