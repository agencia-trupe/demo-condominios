@extends('portaria.templates.auth')

@section('conteudo')

	<section id="login-section" class="centro">

		<div class="login-banner">
			<div class="login-branding">
				<div class="login-marca">
					<img src="assets/images/layout/logo.png" alt="Sistema de Comunicação Trupe Condomínios">
				</div>
				<p>Sistema de Comunicação Trupe Condomínios</p>
			</div>
		</div>

		<div class="login-inferior">

			<div class="login-mostra-areas">
				<div class="area area-admin">
					<p>administração e comunicação</p>
				</div>
				<div class="area area-unidade">
					<p>minha unidade e garagem</p>
				</div>
				<div class="area area-areascomuns">
					<p>áreas comuns e lazer</p>
				</div>
				<div class="area area-acesso">
					<p>controle de acesso</p>
				</div>
			</div>

			<div class="login-form">

				<form action="{{ route('portaria.auth.login') }}" method="post">

					{!! csrf_field() !!}

					<fieldset>
						<label @if($errors->has('login')) {{ $errors->first('login') }} @endif class="
							login-form-input
							@if($errors->has('login')) erro @endif
							@if(str_is('*obrigatório*' ,$errors->first('login'))) obrigatorio @endif
							@if(str_is('*endereço de e-mail válido*' ,$errors->first('login'))) login-invalido @endif
							@if(str_is('*inválido*' ,$errors->first('login'))) login-invalido @endif
							@if(str_is('*informações de login*' ,$errors->first('login'))) login-fail @endif
							">
							<input type="text" name="login" placeholder="login (nome de usuário)" id="login-form-login" value="{{ old('login') }}">
						</label>
						<label class="
							login-form-input
							inline
							@if($errors->has('senha')) erro @endif
							@if(str_is('*obrigatório*' ,$errors->first('senha'))) obrigatorio @endif
							">
							<input type="password" name="password" placeholder="senha" id="login-form-senha" >
						</label>
						<input type="submit" value="OK">
					</fieldset>

				</form>

			</div>

		</div>

	</section>

@stop