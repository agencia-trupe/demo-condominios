@extends('moradores.templates.auth')

@section('conteudo')

    <section id="login-section" class="centro">

        <div class="login-banner">
            <div class="login-branding">
                <div class="login-marca">
                    <img src="assets/images/layout/logo.png" alt="Sistema de Comunicação Trupe Condomínios">
                </div>
                <p>Sistema de Comunicação Trupe Condomínios</p>
            </div>
        </div>

        <div class="login-inferior">

            <div class="login-mostra-areas">
                <div class="area area-admin">
                    <p>administração e comunicação</p>
                </div>
                <div class="area area-unidade">
                    <p>minha unidade e garagem</p>
                </div>
                <div class="area area-areascomuns">
                    <p>áreas comuns e lazer</p>
                </div>
                <div class="area area-acesso">
                    <p>controle de acesso</p>
                </div>
            </div>

            <div class="login-form">

                <div class="pagina-nao-encontrada">
                    <h1 class="erro">Ocorreu um erro na requisição.</h1>
                </div>

                <div class="login-disclaimer">
                    <p class='ativacao-resposta erro'>
                        {{ $exception->getMessage() }}
                    </p>
                </div>

            </div>

        </div>

    </section>

@stop