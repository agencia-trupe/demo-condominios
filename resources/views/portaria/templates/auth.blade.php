<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2015 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Trupe Condomínios - Acesso Portaria</title>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link rel="stylesheet" href="assets/css/portaria/vendor.css">
	<link rel="stylesheet" href="assets/css/portaria/portaria.css?cache={{time()}}">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>

	<script src='assets/js/modernizr.js'></script>

</head>
	<body>

		@yield('conteudo')

		<footer>
			&copy; {{ date('Y') }} Trupe Agência Criativa - Todos os direitos reservados.
		</footer>

		<script src='assets/js/portaria.js?cache={{time()}}'></script>

	</body>
</html>
