@if(Session::has('sucesso'))
	<p class="retorno_formulario sucesso">
		{{ Session::get('sucesso') }}
	</p>
@endif

@if($errors->any())
	<p class="retorno_formulario erro">
		{{ $errors->first() }}
	</p>
@endif