@extends('portaria.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('portaria.dashboard.partials.header')<!--

	 --><div class="main" id="dashboard-main">

			<div class="main-conteudo">

				<h1>INFORMAÇÕES DO DIA &bull; <strong>{{ $hoje->formatLocalized("%e %B %Y") }}</strong> </h1>

				@include('portaria.dashboard.partials.mudancas')

				@include('portaria.dashboard.partials.salao-de-festas')

				@include('portaria.dashboard.partials.churrasqueira')

				@include('portaria.dashboard.partials.festas-particulares')

	 		</div>

	 		@include('portaria.busca.resultados')

		</div>

	</section>

@stop