<header>

	<div class="dashboard-branding">
		<a href="{{ route('portaria.dashboard') }}" title="Página Inicial">
			<div class="dashboard-marca">
				<img src="assets/images/layout/logo.png" alt="Sistema de Comunicação Trupe Condomínios">
			</div>

			<p>Sistema de Comunicação Trupe Condomínios</p>
		</a>
	</div>

	<div class="dashboard-topo">

		<div class="dashboard-busca">

			<h1>PORTARIA &middot; CONSULTAS E SERVIÇOS <a href="{{ route('portaria.auth.logout') }}" title="Sair">(sair)</a></h1>

			<form action="" method="POST" id="form-busca">

				{!! csrf_field() !!}

				<input type="text" name="termo_busca" id="busca-principal" placeholder="BUSCA GERAL · apartamento ou nome">

				<input type="submit" value="Buscar" title="Buscar">

			</form>

		</div>

		<div class="dashboard-cadastros">

			<a href="{{ route('portaria.correspondencias.index') }}" title="Cadastro de Correspondências" @if(str_is('portaria.correspondencias*', Route::currentRouteName())) class="ativo" @endif >
				<img src="assets/images/layout/portaria/icone-notificacao-correspondencia.png" alt="Cadastro de Correspondências">
				<span>CADASTRO DE CORRESPONDÊNCIAS</span>
			</a>

			<a href="{{ route('portaria.encomendas.index') }}" title="Cadastro de Encomendas" @if(str_is('portaria.encomendas*', Route::currentRouteName())) class="ativo" @endif >
				<img src="assets/images/layout/portaria/icone-notificacao-encomenda.png" alt="Cadastro de Encomendas">
				<span>CADASTRO DE ENCOMENDAS</span>
			</a>

		</div>
	</div>

</header>

<aside>

	@include('portaria.dashboard.partials.menu')

</aside>