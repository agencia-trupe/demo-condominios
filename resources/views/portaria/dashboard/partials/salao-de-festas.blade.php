<div id="listaSalaoDeFestas">
	@if(sizeof($reservas_salao) > 0)
		<li>
			<h2>Festas &bull; Salão de Festas</h2>
		</li>
		@foreach($reservas_salao as $reserva)

			<li>
				<div class="unidade">{{ $reserva->morador->getUnidadeAbrev() }}</div>
				<div class="descricao">

					@if(sizeof($reserva->convidados))
						<a href="#" data-featherlight-abrir="#lista-convidados-salao-popup-{{ $reserva->id }}" class="btn btn-mini btn-danger btn-convidados" title="Consultar lista de convidados">CONSULTAR LISTA DE CONVIDADOS</a>
					@else
						<div class="lista-convidados-vazia">LISTA DE CONVIDADOS VAZIA</div>
					@endif

					<div class="titulo">
						{{ $reserva->titulo }}
					</div>

					<p>
						{{ $reserva->horario }} | {{ $reserva->morador->getNomeCompleto() }}
					</p>

				</div>
			</li>

			<div class="lista-convidados-popup" id="lista-convidados-salao-popup-{{ $reserva->id }}">
				<div class="popup-convidados">
					<div class="unidade">{{ $reserva->morador->getUnidadeAbrev() }}</div>

					<div class="topo-lista-convidados">

						<input type="text" id="filtro-lista-convidados" placeholder="buscar nome">

						<h3>{{ $reserva->titulo }}</h3>

						<div class="data">{{ $reserva->reserva->format('d/m/Y') }} | {{ $reserva->titulo }}</div>

					</div>

					<h4>LISTA DE CONVIDADOS <a href="#" class="toggle-visibilidade-lista-convidados"><span>mostrar</span> convidados presentes</a></h4>

					<ul class="lista-convidados" id="lista-convidados-salao">
						@foreach($reserva->convidados as $convidado)
							<li><a href="#" class="btn-presenca @if($convidado->presenca) {{'presente oculto'}} @endif" data-convidado="{{$convidado->id}}" data-lista="reservas">{{ $convidado->nome }} <span>@if($convidado->presenca) {{'presente'}} @else {{'marcar como presente'}} @endif</span></a></li>
						@endforeach
						<li><strong>NOME NÃO ENCONTRADO</strong></li>
					</ul>
				</div>
			</div>

		@endforeach
	@else
		<li class='lista-vazia'>
			<h2>Festas &bull; Salão de Festas</h2>
		</li>
	@endif
</div>
