@extends('portaria.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('portaria.dashboard.partials.header')<!--

	 --><div class="main" id="unidades-main">

	 		<div class="main-conteudo">

				<div class="barra-titulo">
					<h1>
						apartamento &bull; <strong>{{ $unidade->getResumo() }}</strong>
					</h1>
					<div class="btn-group">
						<a href="#" data-featherlight="#lista-moradores" title="MORADORES DA UNIDADE">MORADORES DA UNIDADE</a>
						<a href="#" data-featherlight="#lista-veiculos-da-unidade" title="VEÍCULOS">VEÍCULOS</a>
						<a href="#" data-featherlight="#lista-animais-de-estimacao" title="ANIMAIS DE ESTIMAÇÃO">ANIMAIS DE ESTIMAÇÃO</a>
					</div>
				</div>

				@include('portaria.unidades.partials.moradores-da-unidade')

				@include('portaria.unidades.partials.veiculos-da-unidade')

				@include('portaria.unidades.partials.animais-de-estimacao')

				@include('portaria.unidades.partials.pessoas-nao-autorizadas')

				@include('portaria.unidades.partials.pessoas-autorizadas')

				@include('portaria.unidades.partials.prestadores-de-servico')

				@include('portaria.unidades.partials.agendamento-de-mudancas')

				@include('portaria.unidades.partials.salao-de-festas')

				@include('portaria.unidades.partials.churrasqueira')

				@include('portaria.unidades.partials.festas-particulares')

				@include('portaria.unidades.partials.sala-squash')

			</div>

			@include('portaria.busca.resultados')

		</div>

	</section>

@stop