<div class="lista-item-popup" id="lista-veiculos-da-unidade">
	<div class="popup-unidades">
		<h1>Veículos da Unidade <strong>&bull; {{ $unidade->getResumo() }}</strong></h1>
		<ul>

			@forelse($unidade->veiculos_da_unidade as $veiculo)
				<li>
					<div class="veiculo">
						{{ $veiculo->veiculo }} | {{ $veiculo->cor }}
						<br>
						Vaga: {{$veiculo->vaga}}
						<br>
						<strong>Placa: {{ $veiculo->placa }}</strong>
					</div>
				</li>
			@empty
				<li><div class="veiculo">Nenhum veículo</div></li>
			@endforelse

		</ul>
	</div>
</div>
