<div class="lista-item-popup" id="lista-moradores">
	<div class="popup-unidades">
		<h1>Moradores da Unidade <strong>&bull; {{ $unidade->getResumo() }}</strong></h1>
		<ul>
			@if(sizeof($unidade->moradores) || sizeof($unidade->moradores_da_unidade))

				@foreach($unidade->moradores as $morador)
					@if($morador->status == 1)
						<li>

							@if($morador->foto)
								<img src="assets/images/moradores/fotos/thumbs/{{ $morador->foto }}" alt="{{ $morador->nome }}">
							@else
								<img src="moradores/assets/images/avatar-default/160/160/sem foto" alt="{{ $morador->nome }}">
							@endif

							<div class="nome">
								{{ $morador->nome }}
								@if($morador->data_nascimento)
									<span>idade aproximada: {{ $morador->data_nascimento->diffInYears() }} anos</span>
								@endif
							</div>
						</li>
					@endif
				@endforeach

				@foreach($unidade->moradores_da_unidade as $morador)
					<li>
						<img src="assets/images/moradores/moradores-da-unidade/thumbs/{{ $morador->foto }}" alt="{{ $morador->nome }}">
						<div class="nome">
							{{ $morador->nome }}
							@if($morador->data_nascimento)
								<span>idade aproximada: {{ $morador->data_nascimento->diffInYears() }} anos</span>
							@endif
						</div>
					</li>
				@endforeach

			@else

				<li><div class="nome">Nenhum Morador</div></li>

			@endif
		</ul>
	</div>
</div>
