<div class="lista-item-popup" id="lista-animais-de-estimacao">
	<div class="popup-unidades">
		<h1>Animais de Estimação da Unidade <strong>&bull; {{ $unidade->getResumo() }}</strong></h1>
		<ul>
			@if(sizeof($unidade->animais_de_estimacao))

				@foreach($unidade->animais_de_estimacao as $animal)
					<li>
						<img src="assets/images/moradores/animais-de-estimacao/thumbs/{{ $animal->foto }}" alt="{{ $animal->nome }}">
						<div class="nome">
							nome: {{ $animal->nome }}<br>
							{{ $animal->especie }}<br>
							{{ $animal->sexo }}
						</div>
					</li>
				@endforeach

			@else

				<li><div class="nome">Nenhum Animal de Estimação</div></li>

			@endif
		</ul>
	</div>
</div>