@if(sizeof($unidade->pessoas_autorizadas ))
	<div class="lista-item lista-pessoas-autorizadas">
		<h2>Pessoas AUTORIZADAS PERMANENTEMENTE</h2>
		<ul>
			@foreach($unidade->pessoas_autorizadas as $pessoa_autorizada)
				<li>
					<img src="assets/images/moradores/pessoas-autorizadas/thumbs/{{ $pessoa_autorizada->foto }}" alt="{{ $pessoa_autorizada->nome }}">
					<div class="nome">
						{{ $pessoa_autorizada->nome }}
						@if($pessoa_autorizada->data_nascimento)
							<span>idade aproximada: {{ $pessoa_autorizada->data_nascimento->diffInYears() }} anos</span>
						@endif
						<span>{{ $pessoa_autorizada->parentesco }}</span>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endif