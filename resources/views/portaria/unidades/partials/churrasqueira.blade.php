@if(sizeof($unidade->reservas_churrasqueira ))
	<div class="lista-item lista-festas-particulares">
		<h2>Festas &middot; Churrasqueira</h2>
		<ul>
			@foreach($unidade->reservas_churrasqueira as $reserva_churrasqueira)

				<li>
					<div class="descricao">

						@if(sizeof($reserva_churrasqueira->convidados))
							<a href="#" data-featherlight-abrir="#lista-convidados-festas-popup-{{ $reserva_churrasqueira->id }}" class="btn btn-mini btn-danger btn-convidados" title="Consultar lista de convidados">CONSULTAR LISTA DE CONVIDADOS</a>
						@else
							<div class="lista-convidados-vazia">LISTA DE CONVIDADOS VAZIA</div>
						@endif

						<div class="titulo">
							{{ $reserva_churrasqueira->titulo }}
						</div>

						<p>
							{{ $reserva_churrasqueira->reserva->format('d/m/Y') }} &middot; {{ $reserva_churrasqueira->horario }}
						</p>

					</div>
				</li>

				<div class="lista-convidados-popup" id="lista-convidados-festas-popup-{{ $reserva_churrasqueira->id }}">
					<div class="popup-convidados">
						<div class="unidade">{{ $reserva_churrasqueira->morador->getUnidadeAbrev() }}</div>

						<div class="topo-lista-convidados">

							<input type="text" id="filtro-lista-convidados" placeholder="buscar nome">

							<h3>{{ $reserva_churrasqueira->titulo }}</h3>

							<div class="data">{{ $reserva_churrasqueira->reserva->format('d/m/Y') }} | {{ $reserva_churrasqueira->morador->nome }}</div>

						</div>

						<h4>LISTA DE CONVIDADOS</h4>

						<ul id="lista-convidados">
							@foreach($reserva_churrasqueira->convidados as $convidado)
								<li>{{ $convidado->nome }}</li>
							@endforeach
							<li><strong>NOME NÃO ENCONTRADO</strong></li>
						</ul>
					</div>
				</div>

			@endforeach
		</ul>
	</div>
@endif