@if(sizeof($unidade->festas_particulares ))
	<div class="lista-item lista-festas-particulares">
		<h2>Festas particulares</h2>
		<ul>
			@foreach($unidade->festas_particulares as $festa)

				<li>
					<div class="descricao">

						@if(sizeof($festa->convidados))
							<a href="#" data-featherlight-abrir="#lista-convidados-festas-popup-{{ $festa->id }}" class="btn btn-mini btn-danger btn-convidados" title="Consultar lista de convidados">CONSULTAR LISTA DE CONVIDADOS</a>
						@else
							<div class="lista-convidados-vazia">LISTA DE CONVIDADOS VAZIA</div>
						@endif

						<div class="titulo">
							{{ $festa->titulo }}
						</div>

						<p>
							{{ $festa->reserva->format('d/m/Y') }} &middot; {{ $festa->horario }}
						</p>

					</div>
				</li>

				<div class="lista-convidados-popup" id="lista-convidados-festas-popup-{{ $festa->id }}">
					<div class="popup-convidados">
						<div class="unidade">{{ $festa->morador->getUnidadeAbrev() }}</div>

						<div class="topo-lista-convidados">

							<input type="text" id="filtro-lista-convidados" placeholder="buscar nome">

							<h3>{{ $festa->titulo }}</h3>

							<div class="data">{{ $festa->reserva->format('d/m/Y') }} | {{ $festa->morador->nome }}</div>

						</div>

						<h4>LISTA DE CONVIDADOS</h4>

						<ul id="lista-convidados">
							@foreach($festa->convidados as $convidado)
								<li>{{ $convidado->nome }}</li>
							@endforeach
							<li><strong>NOME NÃO ENCONTRADO</strong></li>
						</ul>
					</div>
				</div>

			@endforeach
		</ul>
	</div>
@endif