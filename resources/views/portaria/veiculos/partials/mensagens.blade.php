@if(Session::has('veiculo_da_unidade_create') && session('veiculo_da_unidade_create') == true)
	<!--
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Veículo da Unidade registrado com sucesso.</strong>
	</p>
	-->
	<div id="retorno_formulario_shadow">
		<h3>REGISTRADO COM SUCESSO</h3>
		@if(Session::has('cadastro_veiculo'))
			<p>
				unidade: <strong>{{ Session::get('cadastro_veiculo.unidade') }}</strong> <br>
				veiculo: {{ Session::get('cadastro_veiculo.veiculo') }} <br>
				cor: {{ Session::get('cadastro_veiculo.cor') }} <br>
				placa: <strong>{{ Session::get('cadastro_veiculo.placa') }} </strong><br>
				vaga: {{ Session::get('cadastro_veiculo.vaga') }} <br>
			</p>
		@endif
	</div>
@endif

@if(Session::has('veiculo_da_unidade_create') && session('veiculo_da_unidade_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Veículo da Unidade.</strong>
	</p>
@endif

@if($errors->any())
	<p class="retorno_formulario erro">
		{{ $errors->first() }}
	</p>
@endif
