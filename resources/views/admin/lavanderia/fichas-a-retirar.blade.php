@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Lavanderia - Fichas
        </h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <div class="btn-group">

          <a href="{{route('admin.fichas-lavanderia.index', ['filtro' => 'em_aberto'])}}"
             class="btn btn-default"
             title="Fichas Em Aberto ({{ Carbon\Carbon::now()->formatLocalized('%B') }})">
             Fichas Em Aberto ({{ Carbon\Carbon::now()->formatLocalized('%B') }})
          </a>

          <a href="{{route('admin.fichas-lavanderia.index', ['filtro' => 'fechadas'])}}"
             class="btn btn-default"
             title="Fichas de meses anteriores">
             Fichas de meses anteriores
          </a>

          <a href="{{route('admin.fichas-lavanderia.index', ['filtro' => 'retirada_solicitada'])}}"
             class="btn btn-default btn-info"
             title="Solicitações de Retirada">
             Solicitações de Retirada
          </a>

        </div>

        <hr>

        <table class="table table-striped table-bordered table-hover ">

      		<thead>
        		<tr>
              <th>Unidade</th>
              <th>Quantidade</th>
              <th>Morador</th>
              <th>Data</th>
              <th>Motivo da solicitação</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
          </thead>

          <tbody>
            @forelse ($registros as $registro)

            	<tr class="tr-row">
                <td>{{ $registro->morador->unidade->resumo }}</td>
                <td>{{ $registro->quantidade > 1 ? $registro->quantidade." fichas" : $registro->quantidade." ficha" }}</td>
                <td>{{ $registro->morador->nome }}</td>
                <td>{{ $registro->data_formatada }}</td>
                <td>
                  {!! nl2br($registro->motivo_cancelamento) !!}
                </td>
              	<td class="crud-actions" style="width: 300px;">

                  <a href="{{ route('admin.fichas-lavanderia.negar-retirada', $registro->id) }}" class="btn btn-info btn-sm">negar solicitação</a>

                  <form action="{{ URL::route('admin.fichas-lavanderia.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>

              	</td>
            	</tr>

            @empty

              <tr>
                <td colspan="6" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse

          </tbody>

        </table>

        {!! $registros->appends(['filtro' => $filtro])->render() !!}

      </div>
    </div>
  </div>

@endsection
