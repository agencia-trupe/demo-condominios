@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Lavanderia - Fichas
        </h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <div class="btn-group">

          <a href="{{route('admin.fichas-lavanderia.index', ['filtro' => 'em_aberto'])}}"
             class="btn btn-default"
             title="Fichas Em Aberto ({{ Carbon\Carbon::now()->formatLocalized('%B') }})">
             Fichas Em Aberto ({{ Carbon\Carbon::now()->formatLocalized('%B') }})
          </a>

          <a href="{{route('admin.fichas-lavanderia.index', ['filtro' => 'fechadas'])}}"
             class="btn btn-default btn-info"
             title="Fichas de meses anteriores">
             Fichas de meses anteriores
          </a>

          <a href="{{route('admin.fichas-lavanderia.index', ['filtro' => 'retirada_solicitada'])}}"
             class="btn btn-default"
             title="Solicitações de Retirada">
             Solicitações de Retirada
          </a>

        </div>

      </div>
    </div>

    <hr>

    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

        <div class="alert alert-warning">
          Informe o período desejado e o formato de arquivo para efetuar o download do cadastro de fichas correspondentes.
        </div>

        <form action="{{ route('admin.fichas-lavanderia.download') }}" method="post">

    			{!! csrf_field() !!}

          <div class="well">

  			    <div class="form-group">
  						<label for="inputPeriodo">Período</label>
  						<select name="periodo" class="form-control" id="inputPeriodo" required>
                <option value="">Selecione o período</option>
                @forelse($datas_disponiveis as $d)
                  <option value="{{$d->mes.'/'.$d->ano}}">{{Carbon\Carbon::createFromFormat('m', $d->mes)->formatLocalized('%B').' de '.$d->ano}}</option>
                @empty
                  <option value="">Não há lista de faturamento disponível até o momento.</option>
                @endforelse
              </select>
  					</div>

            <hr>

  					<div class="form-group">

              <p>Selecione o formato do arquivo:</p>

              <label style="width: 32%;">
                <input type="radio" name="formato" required value="pdf"> PDF
              </label>

              <label style="width: 32%;">
                <input type="radio" name="formato" value="csv"> CSV
              </label>

              <label style="width: 32%;">
                <input type="radio" name="formato" value="xls"> XLS
              </label>
  					</div>

            <button type="submit" class="btn btn-lg btn-success btn-block">
              <span class="glyphicon glyphicon-download"></span> download
            </button>

          </div>

    		</form>

      </div>
    </div>

  </div>

@endsection
