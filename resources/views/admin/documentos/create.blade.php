@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Adicionar Documento
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.documentos.store') }}" method="post" enctype="multipart/form-data">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

					<div class="form-group">
						<label for="inputCategoria">Categoria</label>
						<select class="form-control" id="inputCategoria" name="documentos_categoria_id" required>
							<option value="">Selecione</option>
							@if(sizeof($categorias))
								@foreach($categorias as $categoria)
									<option value="{{ $categoria->id }}" @if(old('documentos_categoria_id') == $categoria->id) selected @endif >{{ $categoria->titulo }}</option>
								@endforeach
							@endif
						</select>
					</div>

			    	<div class="form-group">
						<label for="inputTitulo">Título</label>
						<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ old('titulo') }}" required>
					</div>

					<div class="form-group">
						<label for="inputDescricao">Descrição</label>
						<textarea class="form-control textarea-simples" id="inputDescricao" name="texto" required>{{ old('texto') }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputArquivo">Arquivo</label>
						<input type="file" class="form-control" id="inputArquivo" name="arquivo" required>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{ route('admin.documentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection