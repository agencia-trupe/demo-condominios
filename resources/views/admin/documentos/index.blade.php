@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Documentos
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                 <a href="{{ route('admin.documentos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Documento</a>

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                            <th>Título</th>
                            <th>Texto</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)

                        	<tr class="tr-row">
                              	<td>
                                    {{ $registro->titulo }}
                                </td>
                                <td>
                                    {!! str_words(strip_tags($registro->texto), 10) !!}
                                </td>
                          		<td class="crud-actions">
                            		<a href="{{ route('admin.documentos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                                    <form action="{{ URL::route('admin.documentos.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                    </form>
                          		</td>
                        	</tr>

                    	@empty

                            <tr>
                                <td colspan="3" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                      @endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection