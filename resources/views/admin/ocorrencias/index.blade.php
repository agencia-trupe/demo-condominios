@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Ocorrências
        </h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <a href="{{ route('admin.ocorrencias.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Registrar Nova Ocorrência</a>

        <table class="table table-striped table-bordered table-hover ">

      		<thead>
        		<tr>
              <th>Data</th>
              <th>Autor</th>
              <th>Texto</th>
        			<th><span class="glyphicon glyphicon-cog"></span></th>
        		</tr>
        	</thead>
    		  <tbody>
          	@forelse ($registros as $registro)

            	<tr class="tr-row">
                <td style="width:15%;">
                  @if(\Gate::forUser(\Auth::admin()->get())->allows('verInedita', $registro))
                    <span class="label label-success">nova</span>
                  @endif
                  @if(\Gate::forUser(\Auth::admin()->get())->allows('verRespostaInedita', $registro))
                    <span class="label label-primary">nova resposta</span>
                  @endif
                  @if($registro->finalizada)
                    <span class="label label-default">finalizada</span>
                  @endif
                  {{ $registro->created_at->format('d/m/y · H:i \h') }}
                </td>
                <td>
                  {{ $registro->getNomeCompletoAutor() }}
                </td>
                <td>
                  {{ str_words(strip_tags($registro->texto), 15) }}
                </td>
                <td class="crud-actions visualizar">
                  <a href="{{ route('admin.ocorrencias.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>
                  @if(\Auth::admin()->get()->tipo == 'master')
                    <form action="{{ URL::route('admin.ocorrencias.destroy', $registro->id) }}" method="post">
                      {!! csrf_field() !!}
                      <input type="hidden" name="_method" value="DELETE">
                      <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                    </form>
                  @endif
                </td>
              </tr>

            @empty

              <tr>
                <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse
          </tbody>
        </table>

      </div>
    </div>
  </div>
@endsection
