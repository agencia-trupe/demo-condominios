@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Visualizar Ocorrência
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	    		<div class="well">
	    			<p>
	    				Ocorrência registrada por: <strong>{{ $registro->getNomeCompletoAutor() }} - {{ $registro->getUnidadeAutor() }}</strong>
	    				<br>
	    				<small>{{ $registro->created_at->format('d/m/Y H:i \h') }}</small>
	    			</p>
	    			{!! nl2br($registro->texto) !!}
	    			<hr>

	    			@if(sizeof($registro->respostas))
	    				@foreach($registro->respostas as $resposta)
	    					<p>
			    				Resposta de: <strong>{{ $resposta->getNomeCompletoAutor() }} - {{ $resposta->getUnidadeAutor() }}</strong>
			    				<br>
			    				<small>{{ $resposta->created_at->format('d/m/Y H:i \h') }}</small>
			    			</p>
			    			{!! nl2br($resposta->texto) !!}
			    			<hr>
	    				@endforeach
	    			@endif

	    		</div>
			</div>
		</div>

		@if(!$registro->finalizada)

			<form action="{{ route('admin.ocorrencias.update', $registro->id) }}" method="post">

				<input type="hidden" name="_method" value="PUT">

				{!! csrf_field() !!}

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

				    	<div class="form-group">
							<label for="inputTexto">Resposta</label>
							<textarea class="form-control textarea-simples textarea-simples-livre" id="inputTexto" name="texto" required>{{ old('texto') }}</textarea>
						</div>

						<input type="hidden" name="moradores_id" value="{{$registro->autor_id}}">

					</div>
				</div>

				<button type="submit" title="Responder" class="btn btn-success">Responder</button>

			</form>

			<hr>

			<a href="{{ route('admin.ocorrencias.finalizar', $registro->id) }}" title='Finalizar esta ocorrência' class="btn btn-danger">Finalizar Ocorrência</a>

		@else
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
					<div class="well">Ocorrência <strong>Finalizada</strong> em {{ $registro->finalizada_em->format('d/m/Y H:i \h') }} por {{ $registro->getNomeFinalizador() }}</div>
				</div>
			</div>
		@endif

		<hr>

		<a href="{{ route('admin.ocorrencias.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>


    </div>

@endsection