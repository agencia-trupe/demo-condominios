@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Reservas de Espaços <small><span class='label label-danger'>Canceladas com Multa</span></small>
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <div class="btn-group">
                    @foreach($listaEspacos as $espaco)
                        <a href="{{ route('admin.cancelamentos-com-multa.index', ['espaco_id' => $espaco->id]) }}" class="btn btn-default @if($espaco_id == $espaco->id) btn-info @endif " title="{{ $espaco->titulo }}"> &nbsp;{{ $espaco->titulo }}</a>
                    @endforeach
                    <a href="{{ route('admin.cancelamentos-com-multa.index') }}" class="btn btn-default @if(!$espaco_id) btn-info @endif " title="Todos os espaços" >Todos os espaços</a>
                </div>

                <table class="table table-striped table-bordered table-hover table-condensed">

              		<thead>
                		<tr>
                            <th>Data</th>
                            <th>Local Reservado</th>
                            <th>Morador</th>
                            <th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                        @forelse ($registros as $registro)

                            <tr class="tr-row">
                              	<td>
                                    {{ $registro->reserva->format('d/m/Y') }}

                                    @if($registro->getNaoLido()) <span class="label label-success">nova</span> @endif <br>
                                </td>

                                <td>
                                    {{ $registro->espacoReservado->titulo }}
                                </td>

                                <td>
                                    {{ $registro->morador->getNomeCompleto() }}
                                    <br>
                                    {{ $registro->morador->getUnidade() }}
                                </td>

                          		<td class="crud-actions visualizar">
                            		<a href="{{ route('admin.cancelamentos-com-multa.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>

                                    <form action="{{ URL::route('admin.cancelamentos-com-multa.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_tipo_reserva" value="por_periodo">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                    </form>
                          		</td>
                        	</tr>

                        @empty

                            <tr>
                                <td colspan="4" style='text-align:center; font-weight:bold;'>Selecione um Espaço</td>
                            </tr>

                    	@endforelse
              		</tbody>

            	</table>



            </div>
        </div>
    </div>

@endsection