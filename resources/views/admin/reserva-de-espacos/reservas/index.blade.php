@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Próximas Reservas de Espaços
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <div class="btn-group">
                    <a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => 'por_periodo']) }}" class="btn btn-default @if($filtro == 'por_periodo') btn-info @endif " title="Reservas por período" >Reservas por período @if(sizeof($notificacoes_areas_comuns['reservas_periodo']) > 0) <span class='label label-success'>{{ sizeof($notificacoes_areas_comuns['reservas_periodo']) }}</span> @endif </a>
                    <a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => 'diaria']) }}" class="btn btn-default @if($filtro == 'diaria') btn-info @endif " title="Reservas diárias" >Reservas diárias @if(sizeof($notificacoes_areas_comuns['reservas_diarias']) > 0) <span class='label label-success'>{{ sizeof($notificacoes_areas_comuns['reservas_diarias']) }}</span> @endif </a>
                    <a href="{{ route('admin.reservas-consultar.index') }}" class="btn btn-default @if($filtro == 'consulta') btn-info @endif " title="Consultar e extrair relatórios" >Consultar e extrair relatórios</a>
                </div>

                @if($filtro)

                    <hr>
                    <div class="btn-group">
                        @foreach($listaEspacos as $espaco)
                            <a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => $filtro, 'espaco_id' => $espaco->id]) }}" class="btn btn-default @if($espaco_id == $espaco->id) btn-info @endif " title="{{ $espaco->titulo }}">{{ $espaco->titulo }}</a>
                        @endforeach
                        <a href="{{ route('admin.reservas-de-espacos.index', ['filtro' => $filtro]) }}" class="btn btn-default @if(!$espaco_id) btn-info @endif " title="Todos os espaços" >Todos os espaços</a>
                    </div>

                    @if($espaco_id)

                        <hr>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            Reservas à partir de
                                        </span>

                                        @if($filtro == 'por_periodo')
                                            <input type="text" class="datetimepicker form-control" name="data" value="{{ $data->format('d/m/Y H:i') }}" placeholder="Data de reserva">
                                        @elseif($filtro == 'diaria')
                                            <input type="text" class="datepicker form-control" name="data" value="{{ $data->format('d/m/Y') }}" placeholder="Data de reserva">
                                        @endif

                                        <span class="input-group-btn">
                                            <a href="{!! str_replace('%3A', ':', route('admin.reservas-de-espacos.index', ['filtro' => $filtro, 'espaco_id' => $espaco_id, 'data' => ($filtro == 'por_periodo') ? $data->format('Y-m-d_H:i') : $data->format('Y-m-d') ])) !!}"
                                               href-original="{{ route('admin.reservas-de-espacos.index', ['filtro' => $filtro, 'espaco_id' => $espaco_id]) }}"
                                               title="Filtrar por Data"
                                               class="btn btn-success"
                                               id="link-filtrar"><span class="glyphicon glyphicon-search"></span></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif

                @endif

                <table class="table table-striped table-bordered table-hover table-condensed">

              		<thead>
                		<tr>
                            <th>Data</th>
                            <th>Local Reservado</th>
                            <th>Morador</th>
                            <th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                        @forelse ($registros as $registro)
                            <tr class="tr-row">
                              	<td>
                                    @if($filtro == 'por_periodo')
                                        {{ $registro->reserva->format('d/m/Y H:i') }}
                                    @elseif($filtro == 'diaria')
                                        {{ $registro->reserva->format('d/m/Y') }}
                                    @endif

                                    @if($registro->getNaoLido()) <span class="label label-success">nova</span> @endif <br>
                                </td>

                                <td>
                                    {{ $registro->espacoReservado->titulo }}
                                </td>

                                <td>
                                    {{ $registro->morador->getNomeCompleto() }}
                                    <br>
                                    {{ $registro->morador->getUnidade() }}
                                </td>

                          		<td class="crud-actions visualizar">
                            		<a href="{{ route('admin.reservas-de-espacos.show', ['id' => $registro->id, 'tipo' => $filtro] ) }}" class="btn btn-primary btn-sm">visualizar</a>

                          		</td>
                        	</tr>

                        @empty

                            @if($filtro)

                                <tr>
                                    <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                                </tr>

                            @else

                                <tr>
                                    <td colspan="4" style='text-align:center; font-weight:bold;'>Selecione um tipo de Reserva</td>
                                </tr>

                            @endif


                    	@endforelse
              		</tbody>

            	</table>



            </div>
        </div>
    </div>

@endsection
