@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Reservas de Espaços - Bloqueio de Datas
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <a href="#form-bloqueio" class="btn btn-success btn-sm" data-toggle="collapse"><span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Bloqueio</a>

                <div class="row collapse" id="form-bloqueio" style="margin-top:10px;">
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
                        <form action="{{ route('admin.reservas-bloqueios.store') }}" method="post" class="form-horizontal">
                            {!! csrf_field() !!}

                            <div class="well">

                                <div class="form-group">
                                    <label for="data_espaco" class="col-sm-2 control-label">Espaço</label>
                                    <div class="col-sm-10">
                                        <select name="bloqueio[espacos_id]" id="inputEspacoABloquear" class="form-control" id="data_espaco" required>
                                            <option value=""></option>
                                            @foreach($listaEspacosSelect as $espaco)
                                                <option value="{{ $espaco->id }}" data-tipo="{{ $espaco->tipo }}">{{ $espaco->titulo }} ({{ $espaco->tipo == 'por_periodo' ? 'período' : 'diária' }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="data_de" class="col-sm-2 control-label">De</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="bloqueio[data_inicio]" class="bloqueiopicker datetimepicker form-control" id="data_de" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="data_ate" class="col-sm-2 control-label">Até</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="bloqueio[data_termino]" class="bloqueiopicker datetimepicker form-control" id="data_ate" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-10">
                                        <div class="alert alert-block alert-warning">
                                            <p>
                                                Ao cadastrar um Bloqueio, todas as Reservas já feitas para
                                                o Espaço informado no período a ser bloqueado serão <strong>canceladas</strong>.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" id="hiddenTipo" name="tipo_espaco">

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success btn-sm">Cadastrar</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

                <hr>


                <div class="btn-group">
                    <a href="{{ route('admin.reservas-bloqueios.index', ['filtro' => 'por_periodo']) }}" class="btn btn-default @if($filtro == 'por_periodo') btn-info @endif " title="Bloqueios por período" >Bloqueios por período</a>
                    <a href="{{ route('admin.reservas-bloqueios.index', ['filtro' => 'diaria']) }}" class="btn btn-default @if($filtro == 'diaria') btn-info @endif " title="Bloqueios por diária" >Bloqueios por diária</a>
                </div>

                @if($filtro)

                    <hr>
                    <div class="btn-group">
                        @foreach($listaEspacos as $espaco)
                            <a href="{{ route('admin.reservas-bloqueios.index', ['filtro' => $filtro, 'espaco_id' => $espaco->id]) }}" class="btn btn-default @if($espaco_id == $espaco->id) btn-info @endif " title="{{ $espaco->titulo }}">{{ $espaco->titulo }}</a>
                        @endforeach
                        <a href="{{ route('admin.reservas-bloqueios.index', ['filtro' => $filtro]) }}" class="btn btn-default @if(!$espaco_id) btn-info @endif " title="Todos os espaços" >Todos os espaços</a>
                    </div>

                    @if($espaco_id)

                        <hr>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            Bloqueios à partir de
                                        </span>
                                        <input type="text" class="datetimepicker form-control" name="data" value="{{ $data->format('d/m/Y H:i') }}" placeholder="Data de reserva">
                                        <span class="input-group-btn">
                                            <a href="{!! str_replace('%3A', ':', route('admin.reservas-bloqueios.index', ['filtro' => $filtro, 'espaco_id' => $espaco_id, 'data' => $data->format('Y-m-d_H:i') ])) !!}"
                                               href-original="{{ route('admin.reservas-bloqueios.index', ['filtro' => $filtro, 'espaco_id' => $espaco_id]) }}"
                                               title="Filtrar por Data"
                                               class="btn btn-success"
                                               id="link-filtrar"><span class="glyphicon glyphicon-search"></span></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endif

                @endif

                <table class="table table-striped table-bordered table-hover table-condensed">

              		<thead>
                		<tr>
                            <th>Data</th>
                            <th>Local Bloqueado</th>
                            <th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                        @forelse ($registros as $registro)
                            <tr class="tr-row">
                              	<td>
                                    {{ $filtro == 'por_periodo' ? $registro->reserva->format('d/m/Y H:i') : $registro->reserva->format('d/m/Y') }}
                                </td>

                                <td>
                                    {{ $registro->espacoReservado->titulo }}
                                </td>

                                <td class="crud-actions visualizar">
                            		<form action="{{ URL::route('admin.reservas-bloqueios.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_tipo_reserva" value="{{ $filtro }}">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                    </form>
                          		</td>
                        	</tr>

                        @empty

                            @if($filtro)

                                <tr>
                                    <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                                </tr>

                            @else

                                <tr>
                                    <td colspan="4" style='text-align:center; font-weight:bold;'>Selecione um tipo de Reserva</td>
                                </tr>

                            @endif


                    	@endforelse
              		</tbody>

            	</table>



            </div>
        </div>
    </div>

@endsection