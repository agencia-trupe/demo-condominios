@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Reserva de Espaços - Alterar Espaço
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.espacos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

			    <div class="form-group">
						<label for="inputTitulo">Título</label>
						<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ $registro->titulo }}" disabled>
					</div>

					<div class="form-group">
						<label for="inputTipoDeReserva">Tipo de reserva</label>
						<select class="form-control" id="inputTipoDeReserva" name="tipo" disabled>
							<option value=""></option>
							<option value="diaria" @if($registro->tipo == 'diaria') selected @endif >diária</option>
							<option value="por_periodo" @if($registro->tipo == 'por_periodo') selected @endif >por período</option>
						</select>
					</div>

          <div class="well">
            <div class="form-group">
              @if($registro->regras_de_uso)
                Arquivo atual :
                <a href="assets/documentos/{{ $registro->regras_de_uso }}" class='btn btn-sm btn-default' target='_blank'>{{ $registro->regras_de_uso }}</a>
                <hr>
                <label><input type="checkbox" name="remover_arquivo" value="1"> remover arquivo</label>
                <hr>
              @endif
  						<label for="inputRegras">Termos de Uso</label>
  						<input type="file" class="form-control" id="inputRegras" name="regras_de_uso">
  					</div>
          </div>

          <input type="hidden" name="tipo" value="{{$registro->tipo}}">

					<hr>

					<div class="formgroupPorPeriodo" style="display:none;">

						<div class="form-group">
							<label for="inputHorarioInicio">Horário de Início para Reservas</label>
							<input type="text" class="form-control timepicker" id="inputHorarioInicio" name="periodo[horario_abertura]" value="{{ $registro->informacoes->horario_abertura }}">
						</div>

						<div class="form-group">
							<label for="inputHorarioTermino">Horário de Término para Reservas</label>
							<input type="text" class="form-control timepicker" id="inputHorarioTermino" name="periodo[horario_encerramento]" value="{{ $registro->informacoes->horario_encerramento }}">
						</div>

						<div class="form-group">
							<label for="inputIntervalo">Tempo de cada reserva</label>
							<select class="form-control" id="inputIntervalo" name="periodo[tempo_de_reserva]">
								<option value="15" @if($registro->informacoes->tempo_de_reserva == '15') selected @endif >15 minutos</option>
								<option value="30" @if($registro->informacoes->tempo_de_reserva == '30') selected @endif >30 minutos</option>
								<option value="45" @if($registro->informacoes->tempo_de_reserva == '45') selected @endif >45 minutos</option>
								<option value="60" @if($registro->informacoes->tempo_de_reserva == '60') selected @endif >60 minutos</option>
							</select>
						</div>

						<div class="form-group">
							<label for="inputMaxPorDia">Número Máximo de Reservas por Dia por Morador</label>
							<select class="form-control" id="inputMaxPorDia" name="periodo[max_reservas_dia]">
								<option value="1" @if($registro->informacoes->max_reservas_dia == '1') selected @endif >1</option>
								<option value="2" @if($registro->informacoes->max_reservas_dia == '2') selected @endif >2</option>
								<option value="3" @if($registro->informacoes->max_reservas_dia == '3') selected @endif >3</option>
								<option value="4" @if($registro->informacoes->max_reservas_dia == '4') selected @endif >4</option>
								<option value="5" @if($registro->informacoes->max_reservas_dia == '5') selected @endif >5</option>
								<option value="6" @if($registro->informacoes->max_reservas_dia == '6') selected @endif >6</option>
							</select>
						</div>

						<div class="alert alert-block alert-warning">
							<p>
								Ao alterar as informações de um Espaço com reservas <strong>por período</strong>, <strong>todas as reservas futuras</strong> para esse Espaço serão canceladas.
							</p>
						</div>
					</div>

					<div class="formgroupPorDiaria" style="display:none;">
						<div class="form-group">
							<label for="inputValor">Valor</label>
							<div class="input-group">
								<div class="input-group-addon">R$</div>
								<input type="text" class="form-control" id="inputValor" name="diaria[valor]" value="{{ $registro->informacoes->valor }}">
							</div>
						</div>

						<div class="form-group">
							<label for="inputDiasCarencia">Prazo de cancelamento sem multa</label>
							<div class="input-group">
								<div class="input-group-addon">Até</div>
								<select name="diaria[dias_carencia]" id="inputDiasCarencia" class="form-control">
									<option value="1" @if($registro->informacoes->dias_carencia == '1') selected @endif >1</option>
									<option value="2" @if($registro->informacoes->dias_carencia == '2') selected @endif >2</option>
									<option value="3" @if($registro->informacoes->dias_carencia == '3') selected @endif >3</option>
									<option value="4" @if($registro->informacoes->dias_carencia == '4') selected @endif >4</option>
									<option value="5" @if($registro->informacoes->dias_carencia == '5') selected @endif >5</option>
									<option value="6" @if($registro->informacoes->dias_carencia == '6') selected @endif >6</option>
									<option value="7" @if($registro->informacoes->dias_carencia == '7') selected @endif >7</option>
									<option value="8" @if($registro->informacoes->dias_carencia == '8') selected @endif >8</option>
									<option value="9" @if($registro->informacoes->dias_carencia == '9') selected @endif >9</option>
									<option value="10" @if($registro->informacoes->dias_carencia == '10') selected @endif >10</option>
									<option value="11" @if($registro->informacoes->dias_carencia == '11') selected @endif >11</option>
									<option value="12" @if($registro->informacoes->dias_carencia == '12') selected @endif >12</option>
									<option value="13" @if($registro->informacoes->dias_carencia == '13') selected @endif >13</option>
									<option value="14" @if($registro->informacoes->dias_carencia == '14') selected @endif >14</option>
									<option value="15" @if($registro->informacoes->dias_carencia == '15') selected @endif >15</option>
								</select>
								<div class="input-group-addon">dias antes da data da reserva</div>
							</div>
						</div>

            <!--
						<div class="form-group">
							<label for="inputTexto">Texto</label>
							<textarea name="diaria[texto]" class="form-control" id="inputTexto">{{ $registro->informacoes->texto }}</textarea>
						</div>
            -->
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ route('admin.espacos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
