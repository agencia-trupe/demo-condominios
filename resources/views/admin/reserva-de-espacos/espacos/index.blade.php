@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Reserva de Espaços - Espaços
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                            <th>Título</th>
                            <th>Reservas</th>
                            <th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                        @forelse ($registros as $registro)
                            <tr class="tr-row">
                              	<td>
                                    {{ $registro->titulo }}
                                </td>

                                <td>

                                    @if($registro->tipo == 'diaria')

                                        <strong>Reservas Diárias</strong><br>
                                        valor: <strong>R$ {{ $registro->informacoes->valor }}</strong><br>
                                        prazo de cancelamento sem multa: até <strong>{{ $registro->informacoes->dias_carencia }} dias antes</strong> da data da reserva


                                    @elseif($registro->tipo == 'por_periodo')

                                        <strong>Reservas por Período</strong><br>
                                        horário: <strong>{{ $registro->informacoes->horario_abertura }}</strong> até <strong>{{ $registro->informacoes->horario_encerramento }}</strong><br>
                                        reservas de: <strong>{{ $registro->informacoes->tempo_de_reserva }} minutos</strong><br>
                          		        máximo de: <strong>{{ $registro->informacoes->max_reservas_dia }} reservas por dia</strong> por morador.<br>

                                    @endif
                                </td>

                          		<td class="crud-actions">
                            		<a href="{{ route('admin.espacos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
                          		</td>
                        	</tr>

                        @empty

                            <tr>
                                <td colspan="3" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                    	@endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection