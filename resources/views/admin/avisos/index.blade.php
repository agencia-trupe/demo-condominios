@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Avisos
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <a href="{{ route('admin.avisos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Aviso</a>

                <table class="table table-striped table-bordered table-hover ">

              		<thead>
                		<tr>
                            <th>Data</th>
                            <th>Título</th>
                            <th>Chamada</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)

                        	<tr class="tr-row">
                              <td style="width:15%;">
                                  {{ $registro->created_at->format('d/m/y · H:i \h') }}
                              </td>
                          		<td>
                                    {{ $registro->titulo }}
                              </td>
                          		<td>
                                    {{ str_words(strip_tags($registro->olho), 15) }}
                              </td>
                          		<td class="crud-actions">
                            		<a href="{{ route('admin.avisos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                                    <form action="{{ URL::route('admin.avisos.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                    </form>
                          		</td>
                        	</tr>

                        @empty

                            <tr>
                                <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                    	@endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection