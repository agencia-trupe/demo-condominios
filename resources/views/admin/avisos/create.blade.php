@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Adicionar Aviso
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.avisos.store') }}" method="post">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

			    	<div class="form-group">
						<label for="inputTitulo">Título</label>
						<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ old('titulo') }}" required>
					</div>

					<div class="form-group">
						<label for="inputChamada">Chamada</label>
						<textarea name="olho" class="form-control textarea-simples" id="inputChamada" required>{{ old('olho') }}</textarea>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

					<div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control" id="inputTexto">{{ old('texto') }}</textarea>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

					<div class="form-group">
						<label for="inputAssinatura">Assinatura</label>
						<input type="text" class="form-control" id="inputAssinatura" name="assinatura" value="{{ old('assinatura') }}" required>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{ route('admin.avisos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection