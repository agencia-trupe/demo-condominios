@extends('admin.templates.auth')

@section('conteudo')

	<div class="main-painel-login">

		<div class="login">

			<form action="{{ route('admin.auth.login') }}" method="post">

				{!! csrf_field() !!}

                <h3>Trupe Condomínios <small>Painel Administrativo</small></h3>

                @if(count($errors) > 0)
                    @foreach ($errors->all() as $error)
		            	<div class="alert alert-danger">{{ $error }}</div>
		            @endforeach
					@endif

				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<input type="text" class="form-control" placeholder="Usuário" value="{{ old('login') }}" required name="login" autofocus>
				</div>

				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<input type="password" class="form-control" placeholder="Senha" required name="password">
				</div>

				<div class="submit-placeholder">
					<label>
						<input type="checkbox" value="1" name="remember_me"> Lembrar de mim
					</label>
		   			<input type="submit" class="btn btn-primary" value="Login">
				</div>

			</form>

		</div>

	</div>

@stop
