@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	FAQ - Adicionar Questão
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    		    @include('admin.templates.partials.mensagens')
    		</div>
    	</div>

		<form action="{{ route('admin.faq.store') }}" method="post">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

			    	<div class="form-group">
						<label for="inputQuestao">Questão</label>
						<textarea name="questao" class="form-control textarea-simples" id="inputQuestao" required>{{ old('questao') }}</textarea>
					</div>

					<div class="form-group">
						<label for="inputResposta">Resposta</label>
						<textarea name="resposta" class="form-control" id="inputResposta" required>{{ old('resposta') }}</textarea>
					</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{ route('admin.faq.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
