@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>FAQ - Texto de Entrada</h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="faq">

        	<thead>
        		<tr>
              <th>Texto</th>
            	<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>
        	<tbody>
          	@forelse ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ str_words(strip_tags($registro->texto), 15) }}</td>
              	<td class="crud-actions">
                	<a href="{{ route('admin.faq-info.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
              	</td>
              </tr>

            @empty

              <tr>
                <td colspan="2" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
