@extends('admin.templates.dashboard')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

      <h2>Estatísticas</h2>

      <hr>

      <ul class="list-group">
        @foreach($estatisticas as $item)
          <li class="list-group-item">
            <span class="badge">{{$item['valor']}}</span>
            {{$item['titulo']}}
          </li>
        @endforeach
    </ul>

		</div>
	</div>
</div>

@stop
