@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		      	<h2>
		        	Visualizar Agendamento de Mudança
		        </h2>

		        <hr>

		    </div>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	    		<div class="well">
	    			<p>
	    				Agendamento registrado por: <strong>{{ $registro->morador->getNomeCompleto() }} - {{ $registro->morador->getUnidade() }}</strong>
	    				<br>
	    				<small>agendamento feito em: {{ $registro->created_at->format('d/m/Y H:i \h') }}</small>
	    			</p>
	    			<p>
	    				<strong>Data : </strong> {{ $registro->data->format('d/m/Y') }} <br>
	    				<strong>Período : </strong> {{ $registro->getPeriodoFormatado() }} <br>
	    			</p>

	    		</div>
			</div>
		</div>

		<hr>

		<a href="{{ route('admin.agendamento-de-mudanca.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>


    </div>

@endsection