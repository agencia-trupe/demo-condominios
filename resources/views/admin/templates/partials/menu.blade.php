<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('admin.dashboard') }}">Trupe Condomínios</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('admin.unidades*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('admin.unidades.index')}}" title="Unidades">Unidades</a>
				</li>

				<li class="dropdown @if(preg_match('~admin\.(ocorrencias|documentos|documentos-categorias|avisos|agenda-administracao|fale-com|comunicacao-administracao|faq|informacoes-mudancas|agendamento-de-mudanca|fornecedores)\.*~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Administração e comunicação">
						Administração e comunicação
						@if($has_notificacoes_administracao_e_comunicacao)
							<span class="badge badge-notificacao">!</span>
						@endif
						<b class="caret"></b>
					</a>

					<ul class="dropdown-menu">

						<li @if(preg_match('~admin.(ocorrencias*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('admin.ocorrencias.index')}}" title="Ocorrências">
								Ocorrências
								@if(sizeof($notificacoes_administracao_e_comunicacao['ocorrencias']) > 0)
									<span class="badge badge-notificacao">{{ sizeof($notificacoes_administracao_e_comunicacao['ocorrencias']) }}</span>
								@endif
							</a>
						</li>

						<li class="menu-item dropdown dropdown-submenu @if(preg_match('~admin.(documentos*)~', Route::currentRouteName())) active @endif ">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Documentos</a>

							<ul class="dropdown-menu">

								<li @if(preg_match('~admin.(documentos-categorias*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{URL::route('admin.documentos-categorias.index')}}" title="Categorias">Categorias</a>
								</li>

								<li @if(preg_match('~admin.documentos.(index|create|edit)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.documentos.index') }}" title="Documentos">Documentos</a>
								</li>

							</ul>

						</li>

						<li @if(str_is('admin.avisos*', Route::currentRouteName())) class="active" @endif>
							<a href="{{ route('admin.avisos.index') }}" title="Avisos">Avisos</a>
						</li>

						<li @if(str_is('admin.agenda-administracao*', Route::currentRouteName())) class="active" @endif>
							<a href="{{ route('admin.agenda-administracao.index') }}" title="Agenda">Agenda</a>
						</li>

						<li class="menu-item dropdown dropdown-submenu @if(preg_match('~admin.fale-com|comunicacao-administracao*~', Route::currentRouteName())) active @endif ">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								Fale Com&nbsp;
								@if(sizeof($notificacoes_administracao_e_comunicacao['mensagens']) > 0 || sizeof($notificacoes_administracao_e_comunicacao['mensagens_portaria']) > 0)
									<span class="badge badge-notificacao">{{ sizeof($notificacoes_administracao_e_comunicacao['mensagens']) + sizeof($notificacoes_administracao_e_comunicacao['mensagens_portaria']) }}</span>
								@endif
							</a>

							<ul class="dropdown-menu">

								<li @if(preg_match('~admin.fale-com.(index|edit)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.fale-com.index') }}" title="Informações de Contato">Informações de Contato</a>
								</li>

								<li @if(str_is('admin.fale-com-mensagens*', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.fale-com-mensagens.index') }}" title="Mensagens de Moradores">
										Mensagens de Moradores
										@if(sizeof($notificacoes_administracao_e_comunicacao['mensagens']) > 0)
											<span class="badge badge-notificacao">{{ sizeof($notificacoes_administracao_e_comunicacao['mensagens']) }}</span>
										@endif
									</a>
								</li>

								@if(Gate::allows('receber-msg-portaria'))
									<li @if(str_is('admin.comunicacao-administracao*', Route::currentRouteName())) class="active" @endif >
										<a href="{{ URL::route('admin.comunicacao-administracao.index') }}" title="Mensagens da Portaria">
											Mensagens da Portaria
											@if(sizeof($notificacoes_administracao_e_comunicacao['mensagens_portaria']) > 0)
												<span class="badge badge-notificacao">{{ sizeof($notificacoes_administracao_e_comunicacao['mensagens_portaria']) }}</span>
											@endif
										</a>
									</li>
								@endif

							</ul>
						</li>

						<li class="menu-item dropdown dropdown-submenu  @if(preg_match('~admin.(faq|faq-info).*~', Route::currentRouteName())) active @endif ">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">FAQ</a>

							<ul class="dropdown-menu">

								<li @if(preg_match('~admin.faq.(index|edit)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.faq.index') }}" title="FAQ">FAQ</a>
								</li>

								<li @if(preg_match('~admin.(faq-info*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.faq-info.index') }}" title="Texto de Entrada">Texto de Entrada</a>
								</li>

							</ul>
						</li>

						<li class="menu-item dropdown dropdown-submenu  @if(preg_match('~admin.(informacoes-mudancas*|agendamento-de-mudanca*)~', Route::currentRouteName())) active @endif ">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								Mudanças
								@if(sizeof($notificacoes_administracao_e_comunicacao['mudancas']) > 0)
									<span class="badge badge-notificacao">{{ sizeof($notificacoes_administracao_e_comunicacao['mudancas']) }}</span>
								@endif
							</a>

							<ul class="dropdown-menu">

								<li @if(preg_match('~admin.(informacoes-mudancas*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.informacoes-mudancas.index') }}" title="Informações">Informações</a>
								</li>

								<li @if(preg_match('~admin.(agendamento-de-mudanca*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.agendamento-de-mudanca.index') }}" title="Agendamentos">
										Agendamentos
										@if(sizeof($notificacoes_administracao_e_comunicacao['mudancas']) > 0)
											<span class="badge badge-notificacao">{{ sizeof($notificacoes_administracao_e_comunicacao['mudancas']) }}</span>
										@endif
									</a>
								</li>

							</ul>
						</li>

						<li class="menu-item dropdown dropdown-submenu  @if(preg_match('~admin.(fornecedores*)~', Route::currentRouteName())) active @endif ">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Fornecedores</a>

							<ul class="dropdown-menu">

								<li @if(preg_match('~admin.fornecedores-categorias.*~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.fornecedores-categorias.index') }}" title="Categorias">Categorias</a>
								</li>

								<li @if(preg_match('~admin.fornecedores.(index|create|edit)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.fornecedores.index') }}" title="Fornecedores">Fornecedores</a>
								</li>

							</ul>
						</li>

					</ul>

				</li>

				<li class="dropdown @if(preg_match('~admin.(chamados-de-manutencao|espacos|reservas-consultar|reservas-de-espacos|reservas-bloqueios|cancelamentos-com-multa|instrucoes-de-uso|festas-particulares|fichas-lavanderia).*~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Áreas comuns e lazer">
						Áreas comuns e lazer
						@if($has_notificacoes_areas_comuns)
							<span class="badge badge-notificacao">!</span>
						@endif
						<b class="caret"></b>
					</a>

					<ul class="dropdown-menu">

						<li @if(str_is('admin.chamados-de-manutencao*', Route::currentRouteName())) class="active" @endif>
							<a href="{{ route('admin.chamados-de-manutencao.index') }}" title="Chamados de Manutencao">
								Chamados De Manutenção
								@if(sizeof($notificacoes_areas_comuns['chamados_manutencao']) > 0)
									<span class="badge badge-notificacao">{{ sizeof($notificacoes_areas_comuns['chamados_manutencao']) }}</span>
								@endif
							</a>
						</li>

						<li class="menu-item dropdown dropdown-submenu @if(preg_match('~admin.(espacos*|reservas-de-espacos*|reservas-consultar*|reservas-bloqueios*|cancelamentos-com-multa*)~', Route::currentRouteName())) active @endif ">

							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								Reserva de Espaços
								@if(sizeof($notificacoes_areas_comuns['reservas_periodo']) + sizeof($notificacoes_areas_comuns['reservas_diarias']) + sizeof($notificacoes_areas_comuns['cancelamentos_com_multa']) > 0)
									<span class="badge badge-notificacao">{{ sizeof($notificacoes_areas_comuns['reservas_periodo']) + sizeof($notificacoes_areas_comuns['reservas_diarias']) + sizeof($notificacoes_areas_comuns['cancelamentos_com_multa']) }}</span>
								@endif
							</a>

							<ul class="dropdown-menu">

								<li @if(preg_match('~admin.(espacos*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.espacos.index') }}" title="Espaços">Espaços</a>
								</li>

								<li @if(preg_match('~admin.(reservas-de-espacos*|reservas-consultar*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.reservas-de-espacos.index') }}" title="Reservas">
										Reservas
										@if(sizeof($notificacoes_areas_comuns['reservas_periodo']) + sizeof($notificacoes_areas_comuns['reservas_diarias']) > 0)
											<span class="badge badge-notificacao">{{ sizeof($notificacoes_areas_comuns['reservas_periodo']) + sizeof($notificacoes_areas_comuns['reservas_diarias']) }}</span>
										@endif
									</a>
								</li>

								<li @if(preg_match('~admin.(cancelamentos-com-multa*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.cancelamentos-com-multa.index') }}" title="Reservas Canceladas com Multa">
										Reservas Canceladas com Multa
										@if(sizeof($notificacoes_areas_comuns['cancelamentos_com_multa']) > 0)
											<span class="badge badge-notificacao">{{ sizeof($notificacoes_areas_comuns['cancelamentos_com_multa']) }}</span>
										@endif
									</a>
								</li>

								<li class="divider"></li>

								<li @if(preg_match('~admin.(reservas-bloqueios*)~', Route::currentRouteName())) class="active" @endif >
									<a href="{{ URL::route('admin.reservas-bloqueios.index') }}" title="Bloquear Datas">Bloquear Datas</a>
								</li>

							</ul>
						</li>

						<li @if(str_is('admin.festas-particulares*', Route::currentRouteName())) class="active" @endif>
							<a href="{{ route('admin.festas-particulares.index') }}" title="Festas Particulares">
								Festas Particulares
								@if(sizeof($notificacoes_areas_comuns['festas_particulares']) > 0)
									<span class="badge badge-notificacao">{{ sizeof($notificacoes_areas_comuns['festas_particulares']) }}</span>
								@endif
							</a>
						</li>

						<li @if(str_is('admin.instrucoes-de-uso*', Route::currentRouteName())) class="active" @endif>
							<a href="{{ route('admin.instrucoes-de-uso.index') }}" title="Instruções de Uso">Instruções de Uso</a>
						</li>

						<li @if(str_is('admin.fichas-lavanderia*', Route::currentRouteName())) class="active" @endif>
							<a href="{{ route('admin.fichas-lavanderia.index') }}" title="Lavanderia">Lavanderia</a>
						</li>

					</ul>
				</li>

				<li @if(str_is('admin.regulamentos*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('admin.regulamentos.index')}}" title="Regulamentos">Regulamentos</a>
				</li>

				<li @if(str_is('admin.enquetes*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('admin.enquetes.index')}}" title="Enquetes">Enquetes</a>
				</li>

				@if(Gate::allows('gerenciar-administradores'))

					<li class="dropdown @if(preg_match('~admin.(usuarios|estatisticas).*~', Route::currentRouteName())) active @endif ">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>

						<ul class="dropdown-menu">
							<li @if(preg_match('~admin.(estatisticas*)~', Route::currentRouteName())) class="active" @endif >
								<a href="{{ URL::route('admin.estatisticas.index') }}" title="Estatísticas">Estatísticas</a>
							</li>
							<li class="divider"></li>
							<li @if(preg_match('~admin.(usuarios-portaria*)~', Route::currentRouteName())) class="active" @endif >
								<a href="{{ URL::route('admin.usuarios-portaria.index') }}" title="Usuários - Portaria">Usuários - Portaria</a>
							</li>
							<li @if(preg_match('~admin.(usuarios\.(index|create|edit))~', Route::currentRouteName())) class="active" @endif >
								<a href="{{ URL::route('admin.usuarios.index') }}" title="Usuários - Administração">Usuários - Administração</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="{{URL::route('admin.auth.logout')}}" title="Sair">Logout ({{ Auth::admin()->get()->login }})</a>
							</li>
						</ul>
					</li>

				@else

					<li class="dropdown @if(preg_match('~admin.(alterar-senha*)~', Route::currentRouteName())) active @endif ">

						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>

						<ul class="dropdown-menu">
							<li @if(preg_match('~admin.(alterar-senha*)~', Route::currentRouteName())) class="active" @endif >
								<a href="{{ URL::route('admin.alterar-senha.index') }}" title="Alterar Senha">Alterar Senha</a>
							</li>

							<li class="divider"></li>

							<li>
								<a href="{{URL::route('admin.auth.logout')}}" title="Sair">Logout ({{ Auth::admin()->get()->login }})</a>
							</li>
						</ul>
					</li>

				@endif


			</ul>
		</div>

	</div>
</nav>
