<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2015 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Trupe Condomínios - Acesso Administração</title>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link rel="stylesheet" href="assets/css/admin/vendor.css">
	<link rel="stylesheet" href="assets/css/admin/admin.css?cache={{time()}}">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/js/jquery.js"><\/script>')</script>

	<script src='assets/js/modernizr.js'></script>

</head>
	<body class="body-painel-login">

		@yield('conteudo')

		<footer>
			<div class="container-fluid">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		<script src='assets/js/bootstrap.js'></script>

	</body>
</html>
