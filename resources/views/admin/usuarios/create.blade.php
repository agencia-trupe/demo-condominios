@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		      	<h2>
		        	Adicionar Usuário do Painel Administrativo
		        </h2>

		        <hr>
		    </div>

    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">


		        @include('admin.templates.partials.mensagens')

				<form action="{{ route('admin.usuarios.store') }}" method="post">

					{!! csrf_field() !!}

					<div class="form-group">
						<label for="inputTipo">Tipo de Usuário</label>
						<select name="tipo" id="inputTipo" class="form-control" required>
							<option value=""></option>
							<option value="master" @if(old('tipo') == 'master') selected @endif >Master</option>
							<option value="sindico" @if(old('tipo') == 'sindico') selected @endif >Síndico</option>
							<option value="conselho" @if(old('tipo') == 'conselho') selected @endif >Conselho</option>
							<option value="zelador" @if(old('tipo') == 'zelador') selected @endif >Zelador</option>
						</select>
					</div>

			    	<div class="form-group">
						<label for="inputUsuario">Usuário</label>
						<input type="text" class="form-control" id="inputUsuario" name="login"  value="{{ old('login') }}" required>
					</div>

					<div class="form-group">
						<label for="inputEmail">E-mail</label>
						<input type="email" class="form-control" id="inputEmail" name="email" value="{{ old('email') }}">
					</div>

					<div class="form-group">
						<label for="inputSenha">Senha</label>
						<input type="password" class="form-control" id="inputSenha" name="password" required>
					</div>

					<div class="form-group">
						<label for="inputConfSenha">Digite novamente a Senha</label>
						<input type="password" class="form-control" id="inputConfSenha" name="password_confirm" required>
					</div>

					<hr>

					<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

					<a href="{{ route('admin.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection