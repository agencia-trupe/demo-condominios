@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Mensagens da Portaria
        </h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <table class="table table-striped table-bordered table-hover">

      		<thead>
        		<tr>
              <th>Data de Envio</th>
              <th>Enviado por</th>
              <th>Mensagem</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
          </thead>

          <tbody>
          	@forelse ($registros as $registro)

            	<tr class="tr-row">
                <td style="width:15%;">
                  {{ $registro->created_at->format('d/m/y H:i \h') }} @if($registro->getNaoLido()) <span class="label label-success">nova</span> @endif
                </td>
              	<td>
                  {{ $registro->remetente_formatado }}
                </td>
                <td>
                  {{ str_words(strip_tags($registro->descricao), 10) }}
                </td>
              	<td class="crud-actions visualizar">
                	<a href="{{ route('admin.comunicacao-administracao.show', $registro->id ) }}" class="btn btn-primary btn-sm">visualizar</a>

                  <form action="{{ URL::route('admin.comunicacao-administracao.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
              	</td>
            	</tr>

            @empty

              <tr>
                <td colspan="5" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse
          </tbody>

        </table>

      </div>
    </div>
  </div>

@endsection