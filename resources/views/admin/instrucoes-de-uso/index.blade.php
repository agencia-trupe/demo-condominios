@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Instruções de Uso
                </h2>

                <hr>

            	@include('admin.templates.partials.mensagens')

                <a href="{{ route('admin.instrucoes-de-uso.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Instrução</a>

                <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="instrucoes_de_uso">

              		<thead>
                		<tr>
                            <th>Ordenar</th>
                            <th>Título</th>
                            <th>Texto</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@forelse ($registros as $registro)

                        	<tr class="tr-row" id="row_{{ $registro->id }}">
                                <td class='move-actions'>
                                    <a href="#" class="btn btn-info btn-move btn-sm">mover</a>
                                </td>
                          		<td>
                                    {{ str_words(strip_tags($registro->questao), 15) }}
                                </td>
                          		<td>
                                    {{ str_words(strip_tags($registro->resposta), 15) }}
                                </td>
                          		<td class="crud-actions">
                            		<a href="{{ route('admin.instrucoes-de-uso.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                                    <form action="{{ URL::route('admin.instrucoes-de-uso.destroy', $registro->id) }}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                    </form>
                          		</td>
                        	</tr>

                        @empty

                            <tr>
                                <td colspan="4" style='text-align:center'>Nenhum cadastro</td>
                            </tr>

                    	@endforelse
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection