@extends('admin.templates.dashboard')

@section('conteudo')

    <div class="container-fluid padded-bottom">

      <div class="row">
  			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        	<h2>
          	Alterar Regulamentos
          </h2>

          <hr>

  		  </div>
  		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
    	  @include('admin.templates.partials.mensagens')
    	</div>
    </div>

		<form action="{{ route('admin.regulamentos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

          <div class="well">
            <div class="form-group">
              @if($registro->completo)
                Arquivo atual :
                <a href="assets/documentos/{{ $registro->completo }}" class='btn btn-sm btn-default' target='_blank'>{{ $registro->completo }}</a>
                <hr>
              @endif
  						<label for="inputCompleto">Regulamento Completo</label>
  						<input type="file" class="form-control" id="inputCompleto" name="completo">
  					</div>
          </div>

          <div class="well">
            <div class="form-group">
              @if($registro->reduzido_reservas)
                Arquivo atual :
                <a href="assets/documentos/{{ $registro->reduzido_reservas }}" class='btn btn-sm btn-default' target='_blank'>{{ $registro->reduzido_reservas }}</a>
                <hr>
              @endif
  						<label for="inputReduzidoReservas">Regulamento Reduzido - Reservas</label>
  						<input type="file" class="form-control" id="inputReduzidoReservas" name="reduzido_reservas">
  					</div>
          </div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ route('admin.regulamentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
