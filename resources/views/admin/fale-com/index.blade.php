@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>
          Fale Com - Informações de Contato
        </h2>

        <hr>

      	@include('admin.templates.partials.mensagens')

        <table class="table table-striped table-bordered table-hover ">

      		<thead>
        		<tr>
              <th>Síndico</th>
              <th>Conselho</th>
              <th>Zelador</th>
              <th>Portaria</th>
              <th>Administradora</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
          </thead>

          <tbody>
            @forelse ($registros as $registro)

            	<tr class="tr-row">
                <td>{!! str_words(strip_tags($registro->contato_sindico), 10) !!}</td>
                <td>{!! str_words(strip_tags($registro->contato_conselho), 10) !!}</td>
                <td>{!! str_words(strip_tags($registro->contato_zelador), 10) !!}</td>
                <td>{!! str_words(strip_tags($registro->contato_portaria), 10) !!}</td>
                <td>{!! str_words(strip_tags($registro->contato_administradora), 10) !!}</td>
              	<td class="crud-actions">
                	<a href="{{ route('admin.fale-com.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
              	</td>
            	</tr>

            @empty

              <tr>
                <td colspan="6" style='text-align:center'>Nenhum cadastro</td>
              </tr>

            @endforelse

          </tbody>

        </table>

      </div>
    </div>
  </div>

@endsection