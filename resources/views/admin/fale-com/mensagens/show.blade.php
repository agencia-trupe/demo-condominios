@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
		  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    	  <h2>
      	  Fale Com - Visualizar Mensagem de Morador
        </h2>
        <hr>
	    </div>
	  </div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
        @include('admin.templates.partials.mensagens')
      </div>
    </div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	    	<div class="well">
	    		<h4>{{ $registro->assunto }}</h4>
	    		<p>
	    			Mensagem enviada por: <strong>{{ $registro->getNomeCompletoAutor() }} - {{ $registro->getUnidadeAutor() }}</strong>
	    			<br>
	    			<small>{{ $registro->created_at->format('d/m/Y H:i \h') }}</small>
	    		</p>
	    		{!! nl2br($registro->mensagem) !!}
	    		<hr>

	    		@if(sizeof($registro->respostas))
	    			@foreach($registro->respostas as $resposta)
	    				<h4>{{ $resposta->assunto }}</h4>
	    				<p>
			  				Resposta de: <strong>{{ $resposta->getNomeCompletoAutor() }} - {{ $resposta->getUnidadeAutor() }}</strong>
			  				<br>
			  				<small>{{ $resposta->created_at->format('d/m/Y H:i \h') }}</small>
			  			</p>
			  			{!! nl2br($resposta->mensagem) !!}
			  			<hr>
	    			@endforeach
	    		@endif

	    	</div>
			</div>
		</div>

		@if(sizeof($registro->respostas) == 0)
			<form action="{{ route('admin.fale-com-mensagens.store') }}" method="post">

				{!! csrf_field() !!}

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

				    <div class="form-group">
							<label for="inputTexto">Resposta</label>
							<textarea class="form-control textarea-simples textarea-simples-livre" id="inputTexto" name="mensagem" required>{{ old('mensagem') }}</textarea>
						</div>

					</div>
				</div>

				<input type="hidden" name="em_resposta_a" value="{{ $registro->id }}">

				<button type="submit" title="Responder" class="btn btn-success">Responder</button>

			</form>
		@endif

		<hr>

		<a href="{{ route('admin.fale-com-mensagens.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

    </div>

@endsection