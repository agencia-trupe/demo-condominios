@extends('admin.templates.dashboard')

@section('conteudo')

  <div class="container-fluid padded-bottom">

  	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      	<h2>
        	Alterar Enquete
        </h2>

        <hr>

	    </div>
	  </div>

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
        @include('admin.templates.partials.mensagens')
    	</div>
    </div>

		<form action="{{ route('admin.enquetes.update', $registro->id) }}" method="post">

      <input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

          <div class="form-group">
						<label for="inputTitulo">Título da Enquete</label>
						<input type="text" class="form-control" id="inputTitulo" name="titulo" value="{{ $registro->titulo }}" required>
					</div>

					<div class="form-group">
						<label for="inputQuestao">Questão</label>
						<input type="text" class="form-control" id="inputQuestao" name="texto" value="{{ $registro->texto }}" required>
					</div>

					<hr>

          <div class="clearfix opcoes-control">
            <button type="button" class="btn btn-success btn-sm pull-right" title="Adicionar opção de resposta"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar opção de resposta</button>
            <label class="pull-left">Respostas</label>
          </div>

  				<div id="listaOpcoes" style="margin: 15px 0;">

            @foreach($registro->opcoes as $opcao)
              <div class="input-group" style='margin-bottom:3px;'>
                <input type="text" class="form-control opcao-enquete" name="enquete_opcao[]" value="{{$opcao->texto}}" placeholder="Opção de Resposta">
                <span class="input-group-btn">
                  <button class="btn btn-default btn-danger" type="button"><span class="glyphicon glyphicon-remove-circle"></span></button>
                </span>
              </div>
            @endforeach

  				</div>

				</div>
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ route('admin.enquetes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection