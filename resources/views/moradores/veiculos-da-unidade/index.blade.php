@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="veiculos-da-unidade-main">

			<h1>CADASTRO DE VEÍCULOS (DOS MORADORES)</h1>

			<p>Veículos cadastrados para usar a(s) vaga(s) da unidade.</p>

			@include('moradores.veiculos-da-unidade.partials.mensagens')

			<div id="controle-veiculos-da-unidade">

				<a href="#" title="Acrescentar um veículo" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="veiculos-da-unidade-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ACRESCENTAR UM VEÍCULO</a>

				<form action="{{ route('moradores.veiculos-da-unidade.cadastrar') }}" id="veiculos-da-unidade-form" method="post">

					<fieldset>

						{!! csrf_field() !!}

						<label class="
							veiculos-da-unidade-form-input
							@if($errors->has('veiculos_da_unidade.veiculo')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-veiculo" name="veiculos_da_unidade[veiculo]" placeholder="veículo (modelo)" value="{{ old('veiculos_da_unidade.veiculo') }}">
							<div class="
								veiculos-da-unidade-input-addon
								@if($errors->has('veiculos_da_unidade.visibilidade')) input-erro obrigatorio @endif
								">
								<a href="#" class="veiculos-da-unidade-form-set_visibilidade addon-tooltip-visib off" title="Definir Visibilidade do registro" data-shadow-title="Cadastro de Veículo da Unidade" data-shadow-storage="veiculos-da-unidade-form-visibilidade"></a>
								<input type="hidden" name="veiculos_da_unidade[visibilidade]" id="veiculos-da-unidade-form-visibilidade" value="{{ old('veiculos_da_unidade.visibilidade') }}">
							</div>
						</label>

						<label class="
							veiculos-da-unidade-form-input
							@if($errors->has('veiculos_da_unidade.cor')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-cor" name="veiculos_da_unidade[cor]" placeholder="cor" value="{{ old('veiculos_da_unidade.cor') }}">
						</label>

						<label class="
							veiculos-da-unidade-form-input
							input-inline
							com-margem
							@if($errors->has('veiculos_da_unidade.placa')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-placa" name="veiculos_da_unidade[placa]" placeholder="placa" value="{{ old('veiculos_da_unidade.placa') }}">
						</label>

						<label class="
							veiculos-da-unidade-form-input
							input-inline
							@if($errors->has('veiculos_da_unidade.vaga')) input-erro obrigatorio @endif
						">
							<input type="text" id="veiculos-da-unidade-form-vaga" name="veiculos_da_unidade[vaga]" placeholder="número da vaga" value="{{ old('veiculos_da_unidade.vaga') }}">
						</label>

					</fieldset>

					<input type="submit" value="CADASTRAR" class="btn btn-info">

				</form>

			</div>

			@include('moradores.veiculos-da-unidade.partials.lista-veiculos')

		</div>

	</section>

@stop
