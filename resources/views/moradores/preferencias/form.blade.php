@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="preferencias-main">

	 		<h1>PREFERÊNCIAS</h1>

	 		@include('moradores.preferencias.partials.mensagens')

			<form action="{{ route('moradores.preferencias.alterar') }}" method="post">
				<fieldset>

					{!! csrf_field() !!}

					<label class="register-form-input input-espacado">
						<input
              type="checkbox"
              name="preferencias[enviar_email_nova_correspondencia]"
              @if(sizeof($morador->preferencias) && $morador->preferencias->enviar_email_nova_correspondencia == 1) checked @endif
              value="1">&nbsp;
              Quero receber e-mail quando chegar <strong>nova correspondência ou encomenda</strong>
					</label>

          <label class="register-form-input input-espacado">
						<input
              type="checkbox"
              name="preferencias[enviar_email_novo_aviso]"
              @if(sizeof($morador->preferencias) && $morador->preferencias->enviar_email_novo_aviso == 1) checked @endif
              value="1">&nbsp;
              Quero receber e-mail quando houver qualquer <strong>novo aviso</strong> publicado no sistema
					</label>

          <label class="register-form-input input-espacado">
						<input
            type="checkbox"
            name="preferencias[enviar_email_novo_documento]"
            @if(sizeof($morador->preferencias) && $morador->preferencias->enviar_email_novo_documento == 1) checked @endif
            value="1">&nbsp;
            Quero receber e-mail quando <strong>novos documentos</strong> forem inseridos no sistema
					</label>

				</fieldset>

				<input type="submit" value="ATUALIZAR">
			</form>

	 	</div>

	</section>

@stop