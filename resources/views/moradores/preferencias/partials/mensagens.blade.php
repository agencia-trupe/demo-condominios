@if(Session::has('preferencias_atualizadas'))
	<p class="retorno_formulario sucesso">
		<strong>Suas preferências foram atualizadas com sucesso!</strong>
	</p>
@endif

@if(Session::has('preferencias_atualizadas_fail'))
	<p class="retorno_formulario erro">
		<strong>Erro ao atualizar suas preferências.</strong>
	</p>
@endif