<!DOCTYPE html>
<html lang="en-US">
    <head>
      <meta charset="utf-8">
    </head>
    <body>
      <h2>Nova Correspondência/Encomenda</h2>

      <p>
        Uma nova <strong>Correspondência/Encomenda</strong> chegou para você na Portaria na data de hoje. Você
        já pode dirigir-se à portaria para retirá-la.
      </p>

      <p>
        Trupe Condomínios<br>
        <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
      </p>

    </body>
</html>
