<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>

    <h2>Ative seu cadastro</h2>

    <p>
  		Obrigado por ser cadastrar no Sistema Trupe Condomínios.
  	</p>

  	<p>
      Para ativar seu cadastro e confirmar o e-mail de utilização do sistema, por favor clique no link abaixo: <br>
      <a href="{{ route('moradores.auth.ativar', ['email' => $morador->email, 'activation_code' => $morador->activation_code]) }}">ATIVAR CADASTRO</a>
  	</p>

    <p>
      Trupe Condomínios<br>
      <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
    </p>

  </body>
</html>
