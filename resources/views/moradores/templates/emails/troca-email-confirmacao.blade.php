<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Ative sua alteração de e-mail</h2>

    <p>
      Você realizou alteração do seu e-mail no nosso sistema.
    </p>

    <p>
      Para ativar o novo e-mail do seu cadastro, por favor clique no link abaixo: <br>
      <a href="{{ route('moradores.meu-perfil.ativar', ['email' => $morador->novo_email, 'activation_code' => $morador->novo_email_activation_code]) }}">CONFIRMAR ALTERAÇÃO DE E-MAIL</a>
  	</p>

    <p>
      Caso não tenha sido você que realizou essa alteração entre em contato com <a href='mailto:contato@trupe.net'>contato@trupe.net</a> informando o ocorrido.
    </p>

    <p>
      Gratos
    </p>

    <p>
      Trupe Condomínios<br>
      <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
    </p>

  </body>
</html>
