<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Redefinicão de Senha - Trupe Condomínios</h2>

    <p>
      Você solicitou a redefinição da sua senha em nosso sistema.
    </p>

    <p>
      Utilize o link abaixo para redefinir a sua senha de acesso. <br>
      <a href="{{ url('moradores/auth/redefinir/'.$type.'/'.$token) }}">Redefinir Senha - Trupe Condomínios</a>
    </p>

    <p>
      Trupe Condomínios<br>
      <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
    </p>

  </body>
</html>
