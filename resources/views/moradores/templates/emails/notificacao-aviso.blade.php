<!DOCTYPE html>
<html lang="en-US">
    <head>
      <meta charset="utf-8">
    </head>
    <body>
      <h2>Novo Aviso</h2>

      <p>
        Um novo <strong>Aviso</strong> foi publicado no Sistema Trupe Condomínios.
      </p>

      <p>
        Acesse o sistema para acompanhar e dar sua ciência à mensagem: <a href="http://condominios.trupe.net/moradores/avisos" target="_blank" title="Clique aqui para acessar o sistema">condominios.trupe.net</a>
      </p>

      <p>
        Trupe Condomínios<br>
        <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
      </p>

    </body>
</html>
