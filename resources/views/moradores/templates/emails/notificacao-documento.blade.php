<!DOCTYPE html>
<html lang="en-US">
    <head>
      <meta charset="utf-8">
    </head>
    <body>
      <h2>Novo Documento</h2>

      <p>
        Um novo <strong>Documento</strong> foi publicado no Sistema Trupe Condomínios.<br>
        Acesse o sistema para acompanhar todos os assuntos publicados: <a href="http://condominios.trupe.net/moradores/documentos-oficiais" target="_blank" title="Clique aqui para acessar o sistema">condominios.trupe.net</a>
      </p>

      <p>
        Trupe Condomínios<br>
        <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
      </p>

    </body>
</html>
