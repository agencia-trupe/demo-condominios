<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Nova Mensagem de Morador - Sistema Trupe Condomínios</h2>

    <h3>{{$mensagem->assunto}}</h3>
    <p>{!! nl2br($mensagem->mensagem) !!}</p>

    <p>
      Mensagem enviada por: <strong>{{ $mensagem->getNomeCompletoAutor() }} - {{ $mensagem->getUnidadeAutor() }}</strong>
      <br>
      <small>{{ $mensagem->created_at->format('d/m/Y H:i \h') }}</small>
    </p>

    <p>
      Trupe Condomínios<br>
      <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
    </p>

  </body>
</html>
