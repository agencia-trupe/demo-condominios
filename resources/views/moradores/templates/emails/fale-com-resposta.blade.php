<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    
    <h4>Sua mensagem:</h4>
    <p><strong>{{ $original->assunto }}</strong></p>
    <p>{!! nl2br($original->mensagem) !!}</p>

    <p>
      Mensagem enviada por: <strong>{{ $original->getNomeCompletoAutor() }} - {{ $original->getUnidadeAutor() }}</strong>
      <br>
      <small>{{ $original->created_at->format('d/m/Y H:i \h') }}</small>
    </p>

    <hr>

    <h4>Resposta da administração:</h4>
    <p><strong>{{ $resposta->assunto }}</strong></p>
    <p>{!! nl2br($resposta->mensagem) !!}</p>

    <p>
      Trupe Condomínios<br>
      <a href="http://condominios.trupe.net" title="Sistema Trupe Condomínios">condominios.trupe.net</a>
    </p>

  </body>
</html>
