@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="faq-main">

	 		<h1>FAQ &middot; PERGUNTAS FREQUENTES</h1>

	 		<p>
				{!! $faqinfo->texto !!}
	 		</p>

	 		<ul id="lista-faq">
	 			@foreach($questoes as $questao)
	 				<li>
	 					<a href="#" title="{{ $questao->questao }}" class="questao conteudo-cke">
	 						{!! $questao->questao !!}

	 						<img src="assets/images/layout/moradores/sinal-mais.png" alt="+">
	 					</a>
	 					<div class="resposta conteudo-cke">
	 						{!! $questao->resposta !!}
	 					</div>
	 				</li>
	 			@endforeach
	 		</ul>

	 	</div>

	</section>

@stop
