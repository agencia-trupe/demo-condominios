@extends('moradores.templates.auth')

@section('conteudo')

	<section id="register-section" class="centro">

		<aside>
			<div class="register-branding">
				<div class="register-marca">
					<img src="assets/images/layout/logo.png" alt="Sistema de Comunicação para Condomínios">
				</div>
				<p>Sistema de Comunicação para Condomínios</p>
			</div>

			<div class="register-mostra-areas">
				<div class="area area-admin">
					<p>administração e comunicação</p>
				</div>
				<div class="area area-unidade">
					<p>minha unidade e garagem</p>
				</div>
				<div class="area area-areascomuns">
					<p>áreas comuns e lazer</p>
				</div>
				<div class="area area-acesso">
					<p>controle de acesso</p>
				</div>
			</div>
		</aside>

		<div id="register-form">
			<div class="register-titulo">
				<h1>CADASTRO INICIAL</h1>
			</div>

			@if(Session::has('cadastro_realizado'))

				<p class="register-form-resposta sucesso">
					<strong>Cadastro realizado com sucesso!</strong>
					Uma mensagem com o link para ativação da sua conta
					foi enviada para o endereço de e-mail informado.
				</p>

			@else

				@if(Session::has('cadastro_fail'))
					<p class="register-form-resposta erro">
						<strong>Não foi possível realizar seu cadastro.</strong>
						{{ Session::get('cadastro_fail') }}
					</p>
				@endif

				<form action="{{ route('moradores.auth.register') }}" method="post">
					<fieldset>

						{!! csrf_field() !!}

						<label class="
								modulo-moradores-moradores-da-unidade
								register-form-input
								input-inline
								inline-med
								@if($errors->has('unidade.numero_unidade')) input-erro obrigatorio @endif
							">
							<input type="text" name="unidade[numero_unidade]" placeholder="número da unidade" id="register-form-numero_unidade"  value={{ old('unidade.numero_unidade') }}>
						</label>

						<label class="
								register-form-input
								input-inline
								input-peq
								@if($errors->has('unidade.bloco')) input-erro obrigatorio @endif
							">
							<input type="text" name="unidade[bloco]" placeholder="bloco" id="register-form-bloco"  value={{ old('unidade.bloco') }}>
						</label>

						<label class="
								register-form-input
								input-espacado
								@if($errors->has('unidade.nome_proprietario')) input-erro obrigatorio @endif
							">
							<input type="text" name="unidade[nome_proprietario]" placeholder="nome completo do proprietário da unidade" id="register-form-nome_proprietario"  value={{ old('unidade.nome_proprietario') }}>
							<div class="register-input-addon">
								<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip-nome" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?">
							</div>
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.nome')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[nome]" placeholder="nome completo do morador" id="register-form-nome"  value="{{ old('moradores.nome') }}">
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.apelido')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[apelido]" placeholder="como prefere ser chamado" id="register-form-apelido"  value="{{ old('moradores.apelido') }}">
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.relacao_unidade')) input-erro obrigatorio @endif
							">
							<select name="moradores[relacao_unidade]" id="register-form-relacao_unidade" required>
								<option value="" disabled selected>tipo de morador</option>
								<option value="locatario"    @if(old('moradores.relacao_unidade') == 'locatario')    selected @endif >Locatário</option>
								<option value="proprietario" @if(old('moradores.relacao_unidade') == 'proprietario') selected @endif >Proprietário</option>
							</select>
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.data_nascimento')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[data_nascimento]" placeholder="data de nascimento" id="register-form-data_nascimento"  value="{{ old('moradores.data_nascimento') }}">
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.data_mudanca')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[data_mudanca]" placeholder="data de mudança" id="register-form-data_mudanca"  value="{{ old('moradores.data_mudanca') }}">
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.email')) input-erro @endif
								@if(str_is('*obrigatório*' ,$errors->first('moradores.email'))) obrigatorio @endif
								@if(str_is('*e-mail válido*' ,$errors->first('moradores.email'))) email-invalido @endif
								@if(str_is('*está em uso*' ,$errors->first('moradores.email'))) email-em-uso @endif
							">
							<input type="email" name="moradores[email]" placeholder="e-mail" id="register-form-email"  value="{{ old('moradores.email') }}">
							<div class="
								register-input-addon
								@if($errors->has('moradores.visibilidade_email')) input-erro obrigatorio @endif
								">

								<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-email off" title="Definir Visibilidade do e-mail"></a>
								<input type="hidden" name="moradores[visibilidade_email]" id="register-form-visibilidade_email" value="{{ old('moradores.visibilidade_email') }}">

							</div>
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.senha')) input-erro @endif
								@if(str_is('*obrigatório*' ,$errors->first('moradores.senha'))) obrigatorio @endif
								@if(str_is('*devem ser iguais*' ,$errors->first('moradores.senha'))) confirmacao-senha-invalida @endif
							">
							<input type="password" name="moradores[senha]" placeholder="senha" id="register-form-senha" >
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.confirmacao_senha')) input-erro obrigatorio @endif
							">
							<input type="password" name="moradores[confirmacao_senha]" placeholder="confirmar senha" id="register-form-confirmacao_senha" >
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.telefone_fixo')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[telefone_fixo]" placeholder="telefone fixo" id="register-form-telefone_fixo"  value="{{ old('moradores.telefone_fixo') }}">
							<div class="
								register-input-addon
								@if($errors->has('moradores.visibilidade_telefone_fixo')) input-erro obrigatorio @endif
								">

								<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-tel_fixo off" title="Definir Visibilidade do número de telefone fixo"></a>
								<input type="hidden" name="moradores[visibilidade_telefone_fixo]" id="register-form-visibilidade_tel-fixo" value="{{ old('moradores.visibilidade_telefone_fixo') }}">

							</div>
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.telefone_celular')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[telefone_celular]" placeholder="telefone celular" id="register-form-telefone_celular"  value="{{ old('moradores.telefone_celular') }}">
							<div class="
								register-input-addon
								@if($errors->has('moradores.visibilidade_telefone_celular')) input-erro obrigatorio @endif
								">

								<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-tel_cel off" title="Definir Visibilidade do número de telefone celular"></a>
								<input type="hidden" name="moradores[visibilidade_telefone_celular]" id="register-form-visibilidade_tel-cel" value="{{ old('moradores.visibilidade_telefone_celular') }}">

							</div>
						</label>

						<label class="
								register-form-input
								@if($errors->has('moradores.telefone_comercial')) input-erro obrigatorio @endif
							">
							<input type="text" name="moradores[telefone_comercial]" placeholder="telefone comercial" id="register-form-telefone_comercial"  value="{{ old('moradores.telefone_comercial') }}">
							<div class="
								register-input-addon
								@if($errors->has('moradores.visibilidade_telefone_comercial')) input-erro obrigatorio @endif
								">

								<a href="#" class="register-form-set_visibilidade addon-tooltip-visib visib-tel_com off" title="Definir Visibilidade do número de telefone comercial"></a>
								<input type="hidden" name="moradores[visibilidade_telefone_comercial]" id="register-form-visibilidade_tel-com" value="{{ old('moradores.visibilidade_telefone_comercial') }}">

							</div>
						</label>
					</fieldset>

					<div class="input-foto">

						<div class="register-foto-placeholder">
							<label title="Adicionar Foto" class="
									@if($errors->has('moradores.foto')) input-erro @endif
								">
								<p data-html-original="ADICIONAR FOTO">
									@if($errors->has('moradores.foto'))
										{{ $errors->first('moradores.foto') }}
									@else
										ADICIONAR FOTO
									@endif
								</p>
								<div id="upload-progresso"><div class="barra"></div></div>
								<input type="hidden" name="moradores[foto]" id="register-form-foto" value="{{ old('moradores.foto') }}">
								<input type="file"   name="foto" id="register-form-fileupload">
							</label>
						</div>

						<div class="register-input-addon">
							<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip-foto" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?">
						</div>
					</div>

					<input type="submit" value="CADASTRAR"> <a href="{{ route('moradores.inicial') }}" class="btn btn-info btn-voltar" title="Voltar">VOLTAR</a>
				</form>

			@endif
		</div>

	</section>

@stop
