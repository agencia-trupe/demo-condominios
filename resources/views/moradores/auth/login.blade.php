@extends('moradores.templates.auth')

@section('conteudo')

	<section id="login-section" class="centro">

		<div class="login-banner">
			<div class="login-branding">
				<div class="login-marca">
					<img src="assets/images/layout/logo.png" alt="Sistema de Comunicação para Condomínios">
				</div>
				<p>Sistema de Comunicação para Condomínios</p>
			</div>
		</div>

		<div class="login-inferior">

			<div class="login-mostra-areas">
				<div class="area area-admin">
					<p>administração e comunicação</p>
				</div>
				<div class="area area-unidade">
					<p>minha unidade e garagem</p>
				</div>
				<div class="area area-areascomuns">
					<p>áreas comuns e lazer</p>
				</div>
				<div class="area area-acesso">
					<p>controle de acesso</p>
				</div>
			</div>

			<div class="login-form">

				<form action="{{ route('moradores.auth.login') }}" method="post">

					{!! csrf_field() !!}

					<fieldset>
						<label class="
							login-form-input
							@if($errors->has('email')) erro @endif
							@if(str_is('*obrigatório*' ,$errors->first('email'))) obrigatorio @endif
							@if(str_is('*endereço de e-mail válido*' ,$errors->first('email'))) email-invalido @endif
							@if(str_is('*inválido*' ,$errors->first('email'))) email-invalido @endif
							@if(str_is('*informações de login*' ,$errors->first('email'))) login-fail @endif
							">
							<input type="email" name="email" placeholder="login (e-mail)" id="login-form-email" value="{{ old('email') }}">
						</label>
						<label class="
							login-form-input
							inline
							@if($errors->has('senha')) erro @endif
							@if(str_is('*obrigatório*' ,$errors->first('senha'))) obrigatorio @endif
							">
							<input type="password" name="password" placeholder="senha" id="login-form-senha" >
						</label>
						<input type="submit" value="OK">
					</fieldset>
					<a href="{{ route('moradores.auth.recuperar') }}" title="esqueci minha senha" class="login-recovery-link">esqueci minha senha &raquo;</a>
				</form>

				<div class="login-disclaimer">
					@if(session('ativacao_realizada'))
						<p class='ativacao-resposta sucesso'>
							<strong>Seu cadastro agora está ativo!</strong> Efetue o login para acessar o sistema.
						</p>
					@elseif(session('ativacao_fail'))
						<p class='ativacao-resposta erro'>
							<strong>Não foi possível ativar seu cadastro.</strong>
						</p>
					@elseif(session('redefinicao_realizada'))
						<p class='ativacao-resposta sucesso'>
							<strong>Sua senha foi redefinida!</strong> Efetue o login para acessar o sistema.
						</p>
					@else
						<a href="{{ route('moradores.auth.register') }}" title="Primeiro Cadastro" class="login-register-link">PRIMEIRO CADASTRO</a>
					@endif
					<p>
						Este é um sistema para comunicação interna de Condomínios desenvolvido por: <a href='http://www.trupe.net' target='_blank'>Trupe Agência Criativa</a>.<br>
						Para utilizar o sistema é preciso ser morador do condomínio e estar previamente cadastrado.<br>
						Os administradores se reservam o direito de não aceitar cadastros de acordo com seus critérios.
					</p>
				</div>
			</div>

		</div>

	</section>

@stop
