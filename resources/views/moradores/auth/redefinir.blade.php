@extends('moradores.templates.auth')

@section('conteudo')

	<section id="login-section" class="centro">

		<div class="login-banner">
			<div class="login-branding">
				<div class="login-marca">
					<img src="assets/images/layout/logo.png" alt="Sistema de Comunicação para Condomínios">
				</div>
				<p>Sistema de Comunicação para Condomínios</p>
			</div>
		</div>

		<div class="login-inferior">

			<div class="login-mostra-areas">
				<div class="area area-admin">
					<p>administração e comunicação</p>
				</div>
				<div class="area area-unidade">
					<p>minha unidade e garagem</p>
				</div>
				<div class="area area-areascomuns">
					<p>áreas comuns e lazer</p>
				</div>
				<div class="area area-acesso">
					<p>controle de acesso</p>
				</div>
			</div>

			<div class="redefinicao-form">

				<form action="{{ route('moradores.auth.redefinir') }}" method="post">

					{!! csrf_field() !!}

					<input type="hidden" name="token" value="{{ $token }}">

					<fieldset>
						<h2>Nova Senha</h2>

							<label class="
								login-form-input
								@if($errors->has('email')) erro @endif
								@if(str_is('*obrigatório*' ,$errors->first('email'))) obrigatorio @endif
								@if(str_is('*endereço de e-mail válido*' ,$errors->first('email'))) email-invalido @endif
								@if(str_is('*inválido*' ,$errors->first('email'))) email-invalido @endif
								@if(str_is('*nenhum usuário*' ,$errors->first('email'))) email-invalido @endif
								">
								<input type="email" name="email" placeholder="Informe seu e-mail" id="recovery-form-email" value="{{ old('email') }}">
							</label>

							<label class="
								login-form-input
								inline
								@if($errors->has('password')) erro @endif
								@if(str_is('*obrigatório*' ,$errors->first('password'))) obrigatorio @endif
								@if(str_is('*não confere*' ,$errors->first('password'))) confirmacao-senha-invalida @endif
								">
								<input type="password" name="password" placeholder="Informe sua nova senha" id="recovery-form-password">
							</label>

							<label class="
								login-form-input
								inline
								@if($errors->has('password_confirmation')) erro @endif
								@if(str_is('*obrigatório*' ,$errors->first('password_confirmation'))) obrigatorio @endif
								">
								<input type="password" name="password_confirmation" placeholder="Confirme a nova senha" id="recovery-form-confirm_password">
							</label>

							<input type="submit" value="OK">

					</fieldset>
				</form>

				<div class="login-disclaimer">
					@if(Session::has('ativacao_realizada'))
						<p class='ativacao-resposta sucesso'>
							<strong>Seu cadastro agora está ativo!</strong> Efetue o login para acessar o sistema.
						</p>
					@elseif(Session::has('ativacao_fail'))
						<p class='ativacao-resposta erro'>
							<strong>Não foi possível ativar seu cadastro.</strong>
						</p>
					@else
						<a href="{{ route('moradores.auth.login') }}" title="Voltar" class="login-register-link">&laquo; VOLTAR</a>
					@endif
					<p>
						Este é um sistema para comunicação interna de Condomínios desenvolvido por: <a href='http://www.trupe.net' target='_blank'>Trupe Agência Criativa</a>.<br>
						Para utilizar o sistema é preciso ser morador do condomínio e estar previamente cadastrado.<br>
						Os administradores se reservam o direito de não aceitar cadastros de acordo com seus critérios.
					</p>
				</div>
			</div>

		</div>

	</section>

@stop