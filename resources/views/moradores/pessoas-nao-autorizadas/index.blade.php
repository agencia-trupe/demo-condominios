@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="pessoas-nao-autorizadas-main">

			<h1>PESSOAS NÃO AUTORIZADAS</h1>

			<p>Registre as pessoas que NÃO podem acessar sua Unidade.</p>

			@include('moradores.pessoas-nao-autorizadas.partials.mensagens')

			@include('moradores.pessoas-nao-autorizadas.partials.form')

			@include('moradores.pessoas-nao-autorizadas.partials.lista-nao-autorizados')

		</div>

	</section>

@stop