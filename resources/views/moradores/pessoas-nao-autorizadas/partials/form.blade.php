<div id="controle-pessoas-nao-autorizadas">

@if(isset($pessoa_nao_autorizada))
	<a href="#" title="Alterar uma pessoa não autorizada" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="pessoas-nao-autorizadas-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ALTERAR PESSOA AUTORIZADA</a>
	<form action="{{ route('moradores.pessoas-nao-autorizadas.atualizar', $pessoa_nao_autorizada->id) }}" id="pessoas-nao-autorizadas-form" class="form-unidade-edicao" method="post">
@else
	<a href="#" title="Acrescentar uma pessoa não autorizada" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="pessoas-nao-autorizadas-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ACRESCENTAR UMA PESSOA</a>
	<form action="{{ route('moradores.pessoas-nao-autorizadas.cadastrar') }}" id="pessoas-nao-autorizadas-form" method="post">
@endif
		<fieldset>

			{!! csrf_field() !!}

			<label class="
				pessoas-nao-autorizadas-form-input
				@if($errors->has('pessoas_nao_autorizadas.nome')) input-erro obrigatorio @endif
			">
				<input type="text" id="pessoas-nao-autorizadas-form-nome" name="pessoas_nao_autorizadas[nome]" placeholder="nome completo" @if(isset($pessoa_nao_autorizada)) value="{{ $pessoa_nao_autorizada->nome }}" @else value="{{ old('pessoas_nao_autorizadas.nome') }}" @endif>
				<div class="
					pessoas-nao-autorizadas-input-addon
					@if($errors->has('pessoas_nao_autorizadas.visibilidade')) input-erro obrigatorio @endif
					">
					<a href="#" class="pessoas-nao-autorizadas-form-set_visibilidade addon-tooltip-visib off" title="Definir Visibilidade do registro" data-shadow-title="Cadastro de pessoa não autorizada" data-shadow-storage="pessoas-nao-autorizadas-form-visibilidade"></a>
					<input type="hidden" name="pessoas_nao_autorizadas[visibilidade]" id="pessoas-nao-autorizadas-form-visibilidade" @if(isset($pessoa_nao_autorizada)) value="{{ $pessoa_nao_autorizada->visibilidades }}" @else value="{{ old('pessoas_nao_autorizadas.visibilidade') }}" @endif>
				</div>
			</label>

			<label class="
				pessoas-nao-autorizadas-form-input
				@if($errors->has('pessoas_nao_autorizadas.data_nascimento')) input-erro obrigatorio @endif
			">
				<input type="text" id="pessoas-nao-autorizadas-form-data" class="datepicker" name="pessoas_nao_autorizadas[data_nascimento]" placeholder="data de nascimento" @if(isset($pessoa_nao_autorizada)) value="{{ $pessoa_nao_autorizada->data_nascimento->format('d/m/Y') }}" @else value="{{ old('pessoas_nao_autorizadas.data_nascimento') }}" @endif>
				<div class="pessoas-nao-autorizadas-input-addon">
					<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-data-nascimento" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A data de nascimento ajuda a estimarmos a idade da pessoa, confirmando assim que é aquela que não poderá acessar o condomínio.">
				</div>
			</label>

			<label class="
				pessoas-nao-autorizadas-form-input
				@if($errors->has('pessoas_nao_autorizadas.parentesco')) input-erro obrigatorio @endif
			">
				<select name="pessoas_nao_autorizadas[parentesco]" id="pessoas-nao-autorizadas-form-parentesco">
					<option value="">parentesco/relação [selecione]</option>
					@if(isset($pessoa_nao_autorizada))
						<option value="Avô"      @if($pessoa_nao_autorizada->parentesco == 'Avô')      selected @endif >Avô</option>
						<option value="Avó"      @if($pessoa_nao_autorizada->parentesco == 'Avó')      selected @endif >Avó</option>
						<option value="Pai"      @if($pessoa_nao_autorizada->parentesco == 'Pai')      selected @endif >Pai</option>
						<option value="Mãe"      @if($pessoa_nao_autorizada->parentesco == 'Mãe')      selected @endif >Mãe</option>
						<option value="Marido"   @if($pessoa_nao_autorizada->parentesco == 'Marido')   selected @endif >Marido</option>
						<option value="Esposa"   @if($pessoa_nao_autorizada->parentesco == 'Esposa')   selected @endif >Esposa</option>
						<option value="Filho"    @if($pessoa_nao_autorizada->parentesco == 'Filho')    selected @endif >Filho</option>
						<option value="Filha"    @if($pessoa_nao_autorizada->parentesco == 'Filha')    selected @endif >Filha</option>
						<option value="Irmão"    @if($pessoa_nao_autorizada->parentesco == 'Irmão')    selected @endif >Irmão</option>
						<option value="Irmã"     @if($pessoa_nao_autorizada->parentesco == 'Irmã')     selected @endif >Irmã</option>
						<option value="Tio"      @if($pessoa_nao_autorizada->parentesco == 'Tio')      selected @endif >Tio</option>
						<option value="Tia"      @if($pessoa_nao_autorizada->parentesco == 'Tia')      selected @endif >Tia</option>
						<option value="Sobrinho" @if($pessoa_nao_autorizada->parentesco == 'Sobrinho') selected @endif >Sobrinho</option>
						<option value="Sobrinha" @if($pessoa_nao_autorizada->parentesco == 'Sobrinha') selected @endif >Sobrinha</option>
						<option value="Neto"     @if($pessoa_nao_autorizada->parentesco == 'Neto')     selected @endif >Neto</option>
						<option value="Neta"     @if($pessoa_nao_autorizada->parentesco == 'Neta')     selected @endif >Neta</option>
						<option value="Genro"    @if($pessoa_nao_autorizada->parentesco == 'Genro')    selected @endif >Genro</option>
						<option value="Nora"     @if($pessoa_nao_autorizada->parentesco == 'Nora')     selected @endif >Nora</option>
						<option value="Sogro"    @if($pessoa_nao_autorizada->parentesco == 'Sogro')    selected @endif >Sogro</option>
						<option value="Sogra"    @if($pessoa_nao_autorizada->parentesco == 'Sogra')    selected @endif >Sogra</option>
						<option value="Outro"    @if($pessoa_nao_autorizada->parentesco == 'Outro')    selected @endif >Outro</option>
					@else
						<option value="Avô"      @if(old('pessoas_nao_autorizadas.parentesco') == 'Avô')      selected @endif >Avô</option>
						<option value="Avó"      @if(old('pessoas_nao_autorizadas.parentesco') == 'Avó')      selected @endif >Avó</option>
						<option value="Pai"      @if(old('pessoas_nao_autorizadas.parentesco') == 'Pai')      selected @endif >Pai</option>
						<option value="Mãe"      @if(old('pessoas_nao_autorizadas.parentesco') == 'Mãe')      selected @endif >Mãe</option>
						<option value="Marido"   @if(old('pessoas_nao_autorizadas.parentesco') == 'Marido')   selected @endif >Marido</option>
						<option value="Esposa"   @if(old('pessoas_nao_autorizadas.parentesco') == 'Esposa')   selected @endif >Esposa</option>
						<option value="Filho"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Filho')    selected @endif >Filho</option>
						<option value="Filha"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Filha')    selected @endif >Filha</option>
						<option value="Irmão"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Irmão')    selected @endif >Irmão</option>
						<option value="Irmã"     @if(old('pessoas_nao_autorizadas.parentesco') == 'Irmã')     selected @endif >Irmã</option>
						<option value="Tio"      @if(old('pessoas_nao_autorizadas.parentesco') == 'Tio')      selected @endif >Tio</option>
						<option value="Tia"      @if(old('pessoas_nao_autorizadas.parentesco') == 'Tia')      selected @endif >Tia</option>
						<option value="Sobrinho" @if(old('pessoas_nao_autorizadas.parentesco') == 'Sobrinho') selected @endif >Sobrinho</option>
						<option value="Sobrinha" @if(old('pessoas_nao_autorizadas.parentesco') == 'Sobrinha') selected @endif >Sobrinha</option>
						<option value="Neto"     @if(old('pessoas_nao_autorizadas.parentesco') == 'Neto')     selected @endif >Neto</option>
						<option value="Neta"     @if(old('pessoas_nao_autorizadas.parentesco') == 'Neta')     selected @endif >Neta</option>
						<option value="Genro"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Genro')    selected @endif >Genro</option>
						<option value="Nora"     @if(old('pessoas_nao_autorizadas.parentesco') == 'Nora')     selected @endif >Nora</option>
						<option value="Sogro"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Sogro')    selected @endif >Sogro</option>
						<option value="Sogra"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Sogra')    selected @endif >Sogra</option>
						<option value="Outro"    @if(old('pessoas_nao_autorizadas.parentesco') == 'Outro')    selected @endif >Outro</option>
					@endif
				</select>
				<div class="pessoas-nao-autorizadas-input-addon">
					<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-parentesco" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="O parentesco também ajuda na identificação da pessoa que está tentando adentrar o condomínio e facilita o trabalho da portaria.">
				</div>
			</label>

		</fieldset>

		<div class="input-foto">

			<div class="pessoas-nao-autorizadas-foto-placeholder">
				<label title="Alterar Foto" class="
						@if($errors->has('pessoas_nao_autorizadas.foto')) input-erro @endif
					">
					<p data-html-original="ADICIONAR FOTO">
						@if($errors->has('pessoas_nao_autorizadas.foto'))
							{{ $errors->first('pessoas_nao_autorizadas.foto') }}
						@else
							ADICIONAR FOTO
						@endif
					</p>
					@if(isset($pessoa_nao_autorizada))
						<img src="assets/images/moradores/pessoas-nao-autorizadas/thumbs/{{ $pessoa_nao_autorizada->foto }}">
					@endif
					<div id="upload-progresso"><div class="barra"></div></div>
					<input type="hidden" name="pessoas_nao_autorizadas[foto]" class="form-foto-hidden" id="pessoas-nao-autorizadas-form-foto" @if(isset($pessoa_nao_autorizada)) value="{{ $pessoa_nao_autorizada->foto }}" @else value="{{ old('pessoas_nao_autorizadas.foto') }}" @endif>
					<input type="file"   name="foto" id="pessoas-nao-autorizadas-form-fileupload">
				</label>
			</div>

			<div class="pessoas-nao-autorizadas-input-addon">
				<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-morador-unidade-foto" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="Para o caso de pessoas NÃO AUTORIZADAS a foto é essencial. Atente-se para postar uma foto recente da pessoa e que facilite a identificação por parte dos funcionários.">
			</div>
		</div>

		<input type="submit" @if(isset($pessoa_nao_autorizada)) value="ALTERAR" @else value="CADASTRAR" @endif class="btn btn-info">

	</form>

</div>
