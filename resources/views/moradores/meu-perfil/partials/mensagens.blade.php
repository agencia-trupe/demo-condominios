@if(Session::has('cadastro_realizado'))
	<p class="retorno_formulario sucesso">
		<strong>Os dados de seu Perfil foram atualizados com sucesso!</strong>
	</p>
@endif

@if(Session::has('cadastro_fail'))
	<p class="retorno_formulario erro">
		<strong>Os dados de seu Perfil não puderam ser atualizados!</strong>
	</p>
@endif

@if(Session::has('ativacao_realizada'))
	<p class="retorno_formulario sucesso">
		<strong>O seu e-mail de cadastro foi atualizado com sucesso!</strong>
	</p>
@endif

@if(Session::has('ativacao_fail'))
	<p class="retorno_formulario erro">
		<strong>O seu e-mail de cadastro não pode ser atualizado!</strong>
	</p>
@endif

@if(Session::has('confirmar_email'))
	<p class="retorno_formulario sucesso">
		Para finalizar a troca de e-mail do seu cadastro, é necessário
		validar o endereço <br> <strong>clicando no link do e-mail que foi enviado
		ao novo endereço informado</strong>.
	</p>
@endif