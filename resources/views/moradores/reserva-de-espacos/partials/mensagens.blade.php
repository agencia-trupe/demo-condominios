@if(Session::has('data_ja_reservada') && session('data_ja_reservada') == true)
	<p class="retorno_formulario erro">
		Data indisponível
	</p>
@endif

@if(Session::has('reserva_de_espaco_feita') && session('reserva_de_espaco_feita') == true)
	<p class="retorno_formulario sucesso">
		Sua Reserva foi <strong>realizada</strong> com sucesso!
	</p>
@endif

@if(Session::has('reserva_de_espaco_feita') && session('reserva_de_espaco_feita') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao realizar Reserva.
	</p>
@endif

@if(Session::has('reserva_de_espaco_cancelada') && session('reserva_de_espaco_cancelada') == true)
	<p class="retorno_formulario sucesso">
		Sua Reserva foi <strong>cancelada</strong> com sucesso!
	</p>
@endif

@if(Session::has('reserva_de_espaco_cancelada') && session('reserva_de_espaco_cancelada') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao cancelar Reserva.
	</p>
@endif

@if(Session::has('lista_convidados_inserida') && session('lista_convidados_inserida') == true)
	<p class="retorno_formulario sucesso">
		Sua Lista de Convidados foi <strong>registrada</strong> com sucesso!
	</p>
@endif