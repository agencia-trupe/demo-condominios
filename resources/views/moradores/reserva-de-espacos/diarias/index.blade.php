@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>RESERVA DE ESPAÇOS &middot; {{ $espaco->titulo }}</h1>

	 		<p>Para a locação de áreas comuns selecione a data e faça sua reserva.</p>

	 		@include('moradores.reserva-de-espacos.partials.mensagens')

	 		<div id="mensagens-erro"></div>

	 		<div id="grid-por-diaria" class="grid-reserva">

				<div id="seleciona-data">

					<form action="{{ route('moradores.reserva-de-espacos.reservar', $espaco->slug) }}" method="POST" id="form-selecao-data-diaria">

						{!! csrf_field() !!}

						<input type="hidden" id="diarias_bloqueadas_json" value="{{ $timestamp_reservas }}">

						<div id="calendario">

						</div>

						<div id="input-data">

							<label>
								Ou informe a data desejada:<br>
								<input type="text" id="datepicker" name="reserva" value="{{ Carbon\Carbon::createFromFormat('Y-m-d', $data_inicial)->format('d/m/Y') }}">
							</label>

							<label>
								Nome da festa:<br>
								<input type="text" id="input-titulo-festa" name="titulo_festa" placeholder="exemplo: Aniversário da Ana, Chá de Bebê da Carolina, etc..." value="{{ old('titulo_festa') }}">
							</label>

							<label>
								Horário aproximado da festa:<br>
								<input type="text" id="input-horario-festa" name="horario_festa" value="{{ old('horario_festa') }}" maxlength="15">
							</label>

							<input type="submit" value="RESERVAR ESTA DATA" class="btn btn-info">

						</div>

					</form>

				</div>

				@if(sizeof($reservas_do_morador) > 0)
					<div id="lista-reservas">
						<h2>RESERVAS REALIZADAS</h2>
						<ul>
							@foreach($reservas_do_morador as $reserva)
								<li>
									<div class="info-festa">
										{{ $reserva->reserva->formatLocalized("%e de %B de %Y") }} &middot; {{ $reserva->horario }} | <span class="titulo_festa">{{ $reserva->titulo }}</span>
									</div>
									<a href="{{ route('moradores.reservas-de-espacos.detalhes', ['slug_espaco' => $espaco->slug, 'reserva_id' => $reserva->id]) }}" class="btn btn-info">INSTRUÇÕES DE USO E FORMULÁRIOS</a>
									<a href="{{ route('moradores.reservas-de-espacos.convidados', ['slug_espaco' => $espaco->slug, 'reserva_id' => $reserva->id]) }}" class="btn btn-info">LISTA DE CONVIDADOS</a>
									<a href="{{ route('moradores.reserva-de-espacos.remover', ['slug_espaco' => $espaco->slug, 'timestamp_reserva' => $reserva->reserva->format('Y-m-d')]) }}" class="btn btn-info btn-cancelar-diaria" data-rid="{{ $reserva->id }}">CANCELAR RESERVA</a>
								</li>
							@endforeach
						</ul>
					</div>
				@endif

	 		</div>

	 	</div>

	</section>

@stop