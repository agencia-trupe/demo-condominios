@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>RESERVA DE ESPAÇOS &middot; {{ $espaco->titulo }}</h1>

	 		<p>Informe a sua lista de convidados:</p>

			<div id="informacoes-reserva">
				{{ $reserva->reserva->formatLocalized("%e de %B de %Y") }} | {{ $reserva->titulo }}
			</div>

			<form action="{{ route('moradores.reservas-de-espacos.salvar-convidados', ['slug_espaco' => $espaco->slug, 'reserva_id' => $reserva->id]) }}" id="armazena-convidados" method="post">

				{{ csrf_field() }}

				<textarea id="convidados-textarea" placeholder="Cole sua lista de convidados ou insira um nome por vez separados por Enter."></textarea>

				<ul id="convidados-lista">
					@if($reserva->convidados)
						@foreach($reserva->convidados as $convidado)
							<li>{{ $convidado->nome }} <a href='#' class='btn btn-danger btn-mini'>x</a><input type="hidden" name="lista_convidados[]" value="{{ $convidado->nome }}"></li>
						@endforeach
					@endif
				</ul>

				<input type="submit" value="SALVAR LISTA DE CONVIDADOS" class="btn btn-success"> <a href="{{ route('moradores.reserva-de-espacos.reservar', [$espaco->slug]) }}" class="btn btn-info">VOLTAR</a>

			</form>

	 	</div>

	</section>

@stop