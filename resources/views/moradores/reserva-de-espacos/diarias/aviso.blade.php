@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>RESERVA DE ESPAÇOS &middot; {{ $espaco->titulo }}</h1>

	 		<p>Para a locação de áreas comuns selecione a data e faça sua reserva.</p>

			<div id="aviso-diaria">

				<h2>VOCÊ ESTÁ RESERVANDO: <strong>{{ $espaco->titulo }}</strong></h2>

				<h2>DATA SELECIONADA: <strong>{{ $reserva->formatLocalized("%e de %B de %Y") }}</strong></h2>

				<h3>NOME DO EVENTO: <strong>{{ $titulo }}</strong></h3>

				<h3>HORÁRIO APROXIMADO: <strong>{{ $horario }}</strong></h3>

				<p>
					O valor da locação do espaço: {{ $espaco->titulo }} é de R$ {{ $espaco->informacoes->valor }}.<br>
					Esse valor será acrescentado no seu boleto do condomínio para pagamento.
				</p>

				<hr>

				<form action="{{ route('moradores.reserva-de-espacos.confirmar-reserva', $espaco->slug) }}" method="post" id="form-confirmar-reserva" novalidate>

					{!! csrf_field() !!}

					<div class="swap-texto">

						<div class="inicial">

							<p class="reduzido">Antes de confirmar sua reserva confira os <a href="assets/documentos/{{$espaco->regras_de_uso}}" target="_blank">Termos de Uso</a> e o <a href="assets/documentos/{{$regulamentos->reduzido_reservas}}" target="_blank" title="Regulamento Interno Resumido">Regulamento Interno</a>.</p>
							<input type="button" class="btn btn-warning" value='PROSSEGUIR COM A RESERVA'>

						</div>

						<div class="final">

							<p class="reduzido texto-azul">A locação deste espaço implica na aceitação de todas as regras estabelecidas no Regulamento Interno e nas instruções de uso abaixo disponibilizadas:</p>

							@if($espaco->regras_de_uso != '' && file_exists(public_path('assets/documentos/'.$espaco->regras_de_uso)))
								<a href="{{route('moradores.reserva-de-espacos.regras-de-uso', $espaco->id)}}" class="btn btn-vazado">DOWNLOAD DOS TERMOS DE USO E FORMULÁRIOS</a>
							@endif

							<div class="resumo conteudo-cke">
								<p>
									IMPORTANTE: Não deixe de ler o <a href="assets/documentos/{{$regulamentos->completo}}" target="_blank" title="REGULAMENTO COMPLETO">REGULAMENTO COMPLETO</a>
								</p>
							</div>

							<div id="erros-formulario"></div>

							<label>
								<input type="checkbox" id="aceite_input" name="aceite_termos" value="1" required> Li, entendi e estou de acordo com todas os termos e instruções relacionadas ao uso do Espaço <strong>{{ $espaco->titulo }}</strong>.
							</label>

							<input type="hidden" name="reserva" value="{{ $reserva->format('d/m/Y') }}">
							<input type="hidden" name="titulo" value="{{ $titulo }}">
							<input type="hidden" name="horario" value="{{ $horario }}">

							<input type="submit" class="btn btn-warning" value='PROSSEGUIR COM A RESERVA'>
						</div>
					</div>

				</form>

				<hr>

				<p class="reduzido">{!! $data_maxima_cancelamento !!}</p>

				<a href="{{ route('moradores.reserva-de-espacos.grid', $espaco->slug) }}" class="btn btn-info" title="VOLTAR">VOLTAR</a>

			</div>

	 	</div>

	</section>

@stop
