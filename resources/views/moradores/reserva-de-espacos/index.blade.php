@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>RESERVA DE ESPAÇOS</h1>

	 		<p>Para a locação de áreas comuns selecione a data e faça sua reserva.</p>

	 		<ul id="lista-espaco">
	 			@foreach($espacos as $espaco)
	 				<li>
	 					<a href="{{ route('moradores.reserva-de-espacos.grid', $espaco->slug) }}" title="Reservar {{ $espaco->titulo }}" class="btn btn-info btn-xtra-radius btn-ucase">
	 						{{ $espaco->titulo }}
	 					</a>
	 				</li>
	 			@endforeach
	 		</ul>

	 	</div>

	</section>

@stop