@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="ocorrencias-main">

	 		<h1>LIVRO DE OCORRÊNCIAS</h1>

	 		<p>
				O Livro de Ocorrências Digital tem a mesma função do livro de ocorrências físico com a facilidade de você ser avisado quando houver resposta à sua postagem. Todos os registros são visíveis a todos os usuários (moradores) do prédio - assim como no livro físico - e todos são obrigatoriamente e automaticamente assinados pelo sistema quando criados.
	 		</p>

	 		<p>
				<strong>Tente ser sempre objetivo e claro sobre a questão no texto postado.</strong> O síndico responderá a todas as questões que demandem alguma ação dele ou do corpo diretivo. O sistema avisará você quando houver resposta para ser lida.
	 		</p>

	 		<p>
	 			<small>OBSERVAÇÃO: Somente o responsável pelo registro inicial pode respoder às respostas do Síndico. Morador ou Síndico podem encerraro tópico a qualquer momento.</small>
	 		</p>

	 		@if(session('registrar_ocorrencia_sucesso'))
				<p class="retorno_formulario sucesso">
					<strong>Sua ocorrência foi registrada com sucesso.</strong>
				</p>
	 		@endif

	 		@if(session('registrar_ocorrencia_resposta_sucesso'))
				<p class="retorno_formulario sucesso">
					<strong>Sua resposta foi registrada com sucesso.</strong>
				</p>
	 		@endif

	 		@if(session('registrar_ocorrencia_erro'))
				<p class="retorno_formulario erro">
					{{ session('registrar_ocorrencia_erro') }}
				</p>
	 		@endif

	 		<ul class="lista-ocorrencias">
	 			<li>
	 				<div class="coluna-esquerda">
						<h2>REGISTRAR NOVA OCORRÊNCIA</h2>
						<small>
							{{ \Auth::moradores()->get()->getNomeCompleto() }}
							<br>
							{{ \Auth::moradores()->get()->getUnidade() }}
						</small>
	 				</div>
	 				<div class="coluna-direita">
						<form action="{{ route('moradores.ocorrencias.registrar') }}" method="post" id='ocorrencias-registrar-form'>
							{!! csrf_field() !!}
							<label class="
								@if($errors->first('ocorrencias.texto')) erro @endif
								@if(str_is('*obriga*', $errors->first('ocorrencias.texto'))) obrigatorio @endif
								@if(str_is('*mínimo*', $errors->first('ocorrencias.texto'))) minimo @endif ">
								<textarea name="ocorrencias[texto]" id="ocorrencia-texto" placeholder="Escrever mensagem...">{{ old('ocorrencias.texto') }}</textarea>
							</label>

							<div class="ocorrencia-visibilidade">
								<label><input type="radio" name="ocorrencias[ocorrencia_publica]" value="0" checked> Ocorrência Privada (visível somente para Administração)</label>
								<label><input type="radio" name="ocorrencias[ocorrencia_publica]" value="1"> Ocorrência Pública (visível para todos Moradores)</label>
							</div>

							<input type="submit" value="ENVIAR">
						</form>
	 				</div>
	 			</li>
	 			@if(sizeof($ocorrencias))
	 				@foreach($ocorrencias as $ocorrencia)

						<li>
			 				<div class="coluna-esquerda">
								<div class="ocorrencia-timestamp">
									{{ $ocorrencia->created_at->format('d/m/Y H:i \h') }}
								</div>
								<small>
									{{ $ocorrencia->getNomeCompletoAutor() }}
									<br>
									{{ $ocorrencia->getUnidadeAutor() }}
								</small>
			 				</div>
			 				<div class="coluna-direita">
								<div class="ocorrencia-texto">

									<div class="ocorrencia-original">
										<p>
											{!! nl2br($ocorrencia->texto) !!}
										</p>
										@if(sizeof($ocorrencia->respostas))
											<span class="ocorrencia-com-resposta">HÁ RESPOSTA(S) PARA ESSE REGISTRO.</span>
										@endif
									</div>

									@if(sizeof($ocorrencia->respostas))
										@foreach($ocorrencia->respostas as $resposta)
											<div class="ocorrencia-resposta">
												<p>
													{!! nl2br($resposta->texto) !!}
												</p>
												<div class="resposta-data">{{ $resposta->created_at->format('d/m/Y H:i \h') }}</div>
												@if($resposta->origem == 'admin')
													<div class="resposta-autor">| Administração</div>
												@else
													<div class="resposta-autor">| {{ $resposta->autor->getNomeCompleto() . ' - ' . $resposta->autor->getUnidade() }}</div>
												@endif
											</div>
										@endforeach
									@endif

									@if(\Gate::forUser(\Auth::moradores()->get())->allows('responder', $ocorrencia))

										<div class="responder-form-container">
											<form action="{{ route('moradores.ocorrencias.responder', $ocorrencia->id) }}" method="post" class="ocorrencia-separador ocorrencia-responder-form">
												{!! csrf_field() !!}
												<label class="
													@if($errors->first('resposta.'.$ocorrencia->id.'.texto')) erro erro-resposta-ocorrencia @endif
													@if(str_is('*obriga*', $errors->first('resposta.'.$ocorrencia->id.'.texto'))) obrigatorio @endif
													@if(str_is('*mínimo*', $errors->first('resposta.'.$ocorrencia->id.'.texto'))) minimo @endif ">
													<textarea name="resposta[{{ $ocorrencia->id }}][texto]" placeholder="Escrever mensagem..."></textarea>
												</label>
												<input type="submit" value="ENVIAR">
												<label class="finalizar-ocorrencia"><input type="checkbox" name="finalizar_ocorrencia[{{ $ocorrencia->id }}]" value="{{ $ocorrencia->id }}"> Finalizar assunto</a>
											</form>
										</div>

									@else
										@if($ocorrencia->finalizada == 1)
											<span class='ocorrencia-finalizada'>Assunto finalizado em: {{ $ocorrencia->finalizada_em->format('d/m/Y H:i \h') }} por {{ $ocorrencia->getNomeFinalizador() }}</span>
										@endif
									@endcan
								</div>
			 				</div>
			 			</li>
	 				@endforeach
	 			@endif
	 		</ul>

	 	</div>

	</section>

@stop
