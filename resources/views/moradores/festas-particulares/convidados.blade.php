@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="reserva-de-espacos-main">

	 		<h1>FESTA PARTICULARES (DENTRO DA UNIDADE)</h1>

	 		<p>
	 			As festas particulares dentro dos apartamentos não tem obrigação de serem cadastradas.
	 		</p>
	 		<p>
				Porém, se você desejar que seus convidados sejam autorizados diretamente pela portaria
				sem a necessidade da sua confirmação via interfone, informe data e horário da festa
				para disponibilizar sua lista de convidados. Evite cadastrar convidados com apenas
				o primeiro nome, por questão de segurança. Quem não constar na lista será liberado com
				a consulta via interfone normalmente.
	 		</p>

			<div id="informacoes-reserva">
				{{ $reserva->reserva->formatLocalized("%e de %B de %Y") }} | {{ $reserva->titulo }}
			</div>

			<form action="{{ route('moradores.festas-particulares.salvar-convidados', ['reserva_id' => $reserva->id]) }}" id="armazena-convidados" method="post">

				{{ csrf_field() }}

				<textarea id="convidados-textarea" placeholder="Cole sua lista de convidados ou insira um nome por vez separados por Enter."></textarea>

				<ul id="convidados-lista">
					@if($reserva->convidados)
						@foreach($reserva->convidados as $convidado)
							<li>{{ $convidado->nome }} <a href='#' class='btn btn-danger btn-mini'>x</a><input type="hidden" name="lista_convidados[]" value="{{ $convidado->nome }}"></li>
						@endforeach
					@endif
				</ul>

				<input type="submit" value="SALVAR LISTA DE CONVIDADOS" class="btn btn-success"> <a href="{{ route('moradores.festas-particulares.index') }}" class="btn btn-info">VOLTAR</a>

			</form>

	 	</div>

	</section>

@stop