@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="festas-particulares-main">

	 		<h1>FESTAS PARTICULARES</h1>

	 		<p>
	 			As festas particulares dentro dos apartamentos não tem obrigação de serem cadastradas.
	 		</p>
	 		<p>
				Porém, se você desejar que seus convidados sejam autorizados diretamente pela portaria
				sem a necessidade da sua confirmação via interfone, informe data e horário da festa
				para disponibilizar sua lista de convidados. Evite cadastrar convidados com apenas
				o primeiro nome, por questão de segurança. Quem não constar na lista será liberado com
				a consulta via interfone normalmente.
	 		</p>

	 		@include('moradores.festas-particulares.partials.mensagens')

	 		<div id="mensagens-erro"></div>

	 		<div id="grid-particulares" class="grid-reserva">

				<div id="seleciona-data">

					<form action="{{ route('moradores.festas-particulares.reservar') }}" method="POST" id="form-selecao-data-festa-particular">

						{!! csrf_field() !!}

						<input type="hidden" id="diarias_bloqueadas_json" value="{{ print_r($timestamp_reservas) }}">

						<div id="calendario">

						</div>

						<div id="input-data">

							<label class="
								@if($errors->has('reserva')) input-erro @endif
								@if(str_is('*obrigatório*', $errors->first('reserva'))) obrigatorio @endif
								@if(str_is('*confere com o formato*', $errors->first('reserva'))) data-invalida @endif
							" >
								Ou informe a data desejada:<br>
								<input type="text" id="datepicker" name="reserva" value="{{ old('reserva', Carbon\Carbon::now()->format('d/m/Y')) }}">
							</label>

							<label @if($errors->has('titulo_festa')) class="input-erro obrigatorio" @endif >
								Nome do evento:<br>
								<input type="text" id="input-titulo-festa" name="titulo_festa" placeholder="exemplo: Aniversário da Ana, Chá de Bebê da Carolina, etc..." value="{{ old('titulo_festa') }}">
							</label>

							<label>
								Horário aproximado da festa:<br>
								<input type="text" id="input-horario-festa" name="horario_festa" value="{{ old('horario_festa') }}" maxlength="15">
							</label>

							<input type="submit" value="AGENDAR EVENTO" class="btn btn-info">

						</div>

					</form>

				</div>

				@if(sizeof($festas) > 0)
					<div id="lista-reservas">
						<h2>RESERVAS REALIZADAS</h2>
						<ul>
							@foreach($festas as $festa)
								<li>
									<div class="info-festa">
										{{ $festa->reserva->formatLocalized("%e de %B de %Y") }} &middot; {{ $festa->horario }} | <span class="titulo_festa">{{ $festa->titulo }}</span>
									</div>
									<a href="{{ route('moradores.festas-particulares.convidados', [ 'festa_id' => $festa->id ]) }}" class="btn btn-info">LISTA DE CONVIDADOS</a>
									<a href="{{ route('moradores.festas-particulares.remover', [ 'festa_id' => $festa->id ]) }}" class="btn btn-info btn-cancelar-diaria">CANCELAR RESERVA</a>
								</li>
							@endforeach
						</ul>
					</div>
				@endif

	 		</div>


	 	</div>

	</section>

@stop