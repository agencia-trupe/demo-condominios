@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')

    <div class="main" id="amizades-main">

  	 	<h1>AMIGOS MORADORES</h1>

      <div class="amizades-subnav">
        <a href="{{ route('moradores.amizades.index') }}" class="btn @if(str_is('moradores.amizades.index', Route::currentRouteName())) btn-success @else btn-info @endif" title="+ Vizinhos">+ VIZINHOS</a>
        <a href="{{ route('moradores.amizades.autorizados') }}" class="btn @if(str_is('moradores.amizades.autorizados', Route::currentRouteName())) btn-success @else btn-info @endif" title="Autorizados a ver minhas informações">Autorizados a ver minhas informações</a>
      </div>

      <p class="espacamento-extra">
  			Marque as pessoas que podem ver informações pessoais quando você selecionar
        apenas &laquo;amigos moradores&raquo;.
   		</p>

      @if(Session::has('amizades_definidas') && session('amizades_definidas') == true)
      	<p class="retorno_formulario sucesso">
      		<strong>Amizades armazenadas com sucesso!</strong>
      	</p>
      @endif

      <form action="{{ route('moradores.amizades.salvar-autorizados') }}" method="post">

        {!! csrf_field() !!}

        <ul id="lista-moradores">

					<li>
						<label>

							<div class="input">
								<input type="checkbox" id="marcar-todos">
							</div>

							<div class="nome" id="marcar-todos-label">
								Marcar todos
							</div>

						</label>
					</li>

          @foreach($moradores as $morador)
            <li>
              <label>

                <div class="input">
                  <input type="checkbox" name="amigo_id[]" value="{{ $morador->id }}" @if(Auth::moradores()->get()->getTemUsuarioComoAmigo($morador)) checked @endif >
                </div>

                <div class="imagem">
                  @if($morador->foto && file_exists( "assets/images/moradores/fotos/thumbs/" . $morador->foto ))
            				<img src="assets/images/moradores/fotos/thumbs/{{ $morador->foto }}">
            			@else
            				<img src="{{ route('moradores.avatar-default') }}">
            			@endif
                </div>

                <div class="nome">
                  {{ $morador->nome }} <strong>| unidade {{ $morador->unidade->resumo }}</strong>
                </div>

              </label>
            </li>
          @endforeach
        </ul>

        <input type="submit" value="SALVAR" class="btn btn-success btn-info">

      </form>

	  </div>

  </section>

@stop
