@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="fornecedores-main">

      <h1>INDICAÇÕES DE FORNECEDORES</h1>

      	<p>
      		Aqui ficam as indicações de vizinhos sobre fornecedores
					de produtos e serviços que possam ser úteis.<br>
					Além de encontrar uma boa indicação você também pode - e deve - avaliar
					os serviços dos quais se utilizou.<br>
					Indique você também um fornecedor ou deixe sua avaliação de um serviço
					prestado!
      	</p>

	    @include('moradores.fornecedores.partials.mensagens')


			@if(isset($fornecedor))

				@include('moradores.fornecedores.partials.form-edicao-indicacao')

			@else

				@include('moradores.fornecedores.partials.form-indicacao')

			@endif


			@include('moradores.fornecedores.partials.form-busca')


			@if(isset($resultadoBusca))

				@include('moradores.fornecedores.partials.resultados-busca')

			@else

				@include('moradores.fornecedores.partials.indicacoes-recentes')

			@endif

	  </div>

	</section>

@stop
