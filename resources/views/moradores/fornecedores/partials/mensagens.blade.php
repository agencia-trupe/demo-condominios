@if(Session::has('fornecedores_create') && session('fornecedores_create') == true)
	<p class="retorno_formulario sucesso">
		Sua Indicação de Fornecedor foi <strong>registrada</strong> com sucesso!
	</p>
@endif

@if(Session::has('fornecedores_create') && session('fornecedores_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao registrar Indicação de Fornecedor.
	</p>
@endif

@if(Session::has('fornecedores_update') && session('fornecedores_update') == true)
	<p class="retorno_formulario sucesso">
		Sua Indicação de Fornecedor foi <strong>atualizada</strong> com sucesso!
	</p>
@endif

@if(Session::has('fornecedores_update') && session('fornecedores_update') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao atualizar Indicação de Fornecedor.
	</p>
@endif

@if(Session::has('fornecedor_destroy') && session('fornecedor_destroy') == true)
	<p class="retorno_formulario sucesso">
		Sua Indicação de Fornecedor foi <strong>removida</strong> com sucesso!
	</p>
@endif

@if(Session::has('fornecedor_destroy') && session('fornecedor_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao remover Indicação de Fornecedor.
	</p>
@endif

@if(Session::has('fornecedor_avaliado') && session('fornecedor_avaliado') == true)
	<p class="retorno_formulario sucesso">
		Sua Avaliação foi <strong>publicada</strong> com sucesso!
	</p>
@endif

@if(Session::has('fornecedor_avaliado') && session('fornecedor_avaliado') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao publicar sua Avaliação.
	</p>
@endif

@if(Session::has('avaliacao_destroy') && session('avaliacao_destroy') == true)
	<p class="retorno_formulario sucesso">
		Sua Avaliação foi <strong>removida</strong> com sucesso!
	</p>
@endif

@if(Session::has('avaliacao_destroy') && session('avaliacao_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao remover sua Avaliação.
	</p>
@endif
