<div id="controle-fornecedores">

	<a href="#" title="Atualizar Fornecedor" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="fornecedores-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ATUALIZAR FORNECEDOR</a>

	<form action="{{ route('moradores.fornecedores.atualizar', $fornecedor->id) }}" id="fornecedores-form" method="post">

		<fieldset>

			{!! csrf_field() !!}

			<label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.categoria')) input-erro obrigatorio @endif
			">
				<select name="fornecedores[fornecedores_categorias_id]" id="fornecedores-form-categoria">
					<option value="">Selecione o tipo de Fornecedor</option>
					@foreach($listaCategorias as $categoria)
						<option value="{{$categoria->id}}" @if($fornecedor->fornecedores_categorias_id == $categoria->id) selected @endif >{{$categoria->titulo}}</option>
					@endforeach
					<option value="outros" @if($fornecedor->fornecedores_categorias_id == null) selected @endif >Outros</option>
				</select>
			</label>

			<label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.outra_categoria')) input-erro obrigatorio @endif
			"
			id="fornecedores-form-label-outra_categoria">
				<input type="text" id="fornecedores-form-outra_categoria" name="fornecedores[outra_categoria]" placeholder="Especifique" value="{{ $fornecedor->outra_categoria }}">
			</label>

			<label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.nome')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-nome" name="fornecedores[nome]" placeholder="Nome" value="{{ $fornecedor->nome }}">
			</label>

	    <label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.telefone')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-telefone" name="fornecedores[telefone]" placeholder="Telefone" value="{{ $fornecedor->telefone }}">
			</label>

	    <label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.email')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-email" name="fornecedores[email]" placeholder="E-mail" value="{{ $fornecedor->email }}">
			</label>

	    <label class="
				fornecedores-form-input
				@if($errors->has('fornecedores.site')) input-erro obrigatorio @endif
			">
				<input type="text" id="fornecedores-form-site" name="fornecedores[site]" placeholder="Site" value="{{ $fornecedor->site }}">
			</label>

			<label class="
				fornecedores-form-input
				contem-textarea
				@if($errors->has('fornecedores.descritivo')) input-erro obrigatorio @endif
			">
				<textarea name="fornecedores[descritivo]" id="fornecedores-form-descritivo" placeholder="Descritivo">{{ $fornecedor->descritivo }}</textarea>
			</label>

		</fieldset>

		<div class="input-foto">

			<div class="fornecedores-foto-placeholder">
				<label title="Enviar Fotos">
					<p data-html-original="ENVIAR FOTOS (se houver)">
						ENVIAR FOTOS (se houver)
					</p>
					<div id="upload-progresso"><div class="barra"></div></div>

					<input type="file" multiple name="foto" id="fornecedores-form-fileupload">
				</label>
			</div>

			<ul class="lista-imagens">
				@if($fornecedor->imagens)
					@foreach($fornecedor->imagens as $foto)
						<li>
							<a href='#'
								data-featherlight-gallery
								data-featherlight='assets/images/moradores/fornecedores/redimensionadas/{{ $foto->imagem }}'
								data-featherlight-type='image'
								title='Ampliar'
								class='btn-ampliar-imagem'
							>
								<img src="assets/images/moradores/fornecedores/thumbs/{{ $foto->imagem }}">
							</a>
							<a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>
							<input type='hidden' name='fornecedores[fotos][]' class='form-foto-hidden' value='{{ $foto->imagem }}'>
						</li>
					@endforeach
				@endif
			</ul>
		</div>

		<input type="submit" value="ATUALIZAR FORNECEDOR" class="btn btn-info">

    <a href="{{ route('moradores.fornecedores.index') }}" class="btn btn-default" style="margin-left: 5px;">CANCELAR</a>

	</form>

</div>
