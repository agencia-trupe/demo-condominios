<div id="fornecedores-avaliacoes">

  @if($avaliacao_recomendante)
    <div id="avaliacao-recomendante">
      <p class="titulo">
        ESTA É UMA INDICAÇÃO DE:
      </p>
      <div class="avaliacao">

        @if($avaliacao_recomendante->moradores_id == Auth::moradores()->get()->id)
          <a href="{{ route('moradores.fornecedores.remover-avaliacao', $avaliacao_recomendante->id) }}" class="btn btn-danger btn-remover-avaliacao btn-mini" title="Remover Avaliação">X</a>
        @endif

        <div class="texto">
          <p>
            {{$avaliacao_recomendante->texto}}
          </p>
          <span class="data">{{$avaliacao_recomendante->created_at->formatLocalized('%e %B %Y')}}</span>
        </div>
        <div class="nota">
          <div class="rating alinhado-esquerda">
            @for($r = 1; $r <= 5; $r++)
              <span class="estrela @if($r <= $avaliacao_recomendante->nota) marcada @endif ">★</span>
            @endfor
          </div>
          <div class="morador">
            <div class="foto">
              @if($avaliacao_recomendante->morador->foto && file_exists( "assets/images/moradores/fotos/thumbs/" . $avaliacao_recomendante->morador->foto ))
                <img src="assets/images/moradores/fotos/thumbs/{{ $avaliacao_recomendante->morador->foto }}" alt="">
              @else
                <img src="{{ route('moradores.avatar-default') }}">
              @endif
            </div>
            <div class="nome-unidade">
              <div class="nome-morador">{{$avaliacao_recomendante->morador->nome}}</div>
              <div class="unidade-morador">apto {{$avaliacao_recomendante->morador->unidade->resumo}}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif

  <div id="outras-avaliacoes">
    <p class="titulo">
      AVALIAÇÕES:
    </p>

    <ul id="lista-outras-avaliacoes">
      @foreach($outras_avaliacoes as $avaliacao)
        <li class="avaliacao">

          @if($avaliacao->moradores_id == Auth::moradores()->get()->id)
            <a href="{{ route('moradores.fornecedores.remover-avaliacao', $avaliacao->id) }}" class="btn btn-danger btn-remover-avaliacao btn-mini" title="Remover Avaliação">X</a>
          @endif

          <div class="texto">
            <p>
              {{$avaliacao->texto}}
            </p>
            <span class="data">{{$avaliacao->created_at->formatLocalized('%e %B %Y')}}</span>
          </div>
          <div class="nota">
            <div class="rating alinhado-esquerda">
              @for($r = 1; $r <= 5; $r++)
                <span class="estrela @if($r <= $avaliacao->nota) marcada @endif ">★</span>
              @endfor
            </div>
            <div class="morador">
              <div class="foto">
                @if($avaliacao->morador->foto && file_exists( "assets/images/moradores/fotos/thumbs/" . $avaliacao->morador->foto ))
                  <img src="assets/images/moradores/fotos/thumbs/{{ $avaliacao->morador->foto }}" alt="">
                @else
                  <img src="{{ route('moradores.avatar-default') }}">
                @endif
              </div>
              <div class="nome-unidade">
                <div class="nome-morador">{{$avaliacao->morador->nome}}</div>
                <div class="unidade-morador">apto {{$avaliacao->morador->unidade->resumo}}</div>
              </div>
            </div>
          </div>
        </li>
      @endforeach
    </ul>

  </div>

  <div id="avaliar" @if(count($errors) > 0) class="aberto" @endif>

    <a href="#" id="toggle-avaliacao-form" class="btn btn-info">COMENTE</a>

    <div id="avaliacao-form">

      <p class="titulo">DEIXE SEU COMENTÁRIO SOBRE ESSE FORNECEDOR</p>

      <form action="{{ route('moradores.fornecedores.avaliar', $fornecedor->id) }}" method="post" id="avaliar-form">

        {{ csrf_field() }}

        <label class="avalicao-form-input @if($errors->has('avaliacao.texto')) input-erro obrigatorio @endif">
          <textarea name="avaliacao[texto]" id="avaliacao-form-texto" placeholder="Comente">{{old('avaliacao.texto')}}</textarea>
        </label>

        <div class="nota">
          Nota de avaliação geral:
          <div class="rating @if($errors->has('avaliacao.nota')) obrigatorio @endif">
            <input type="radio" id="star5" name="avaliacao[nota]" value="5"  @if(old('avaliacao.nota') == '5') checked @endif /><label for="star5" title="5 estrelas">5 estrelas</label>
  			    <input type="radio" id="star4" name="avaliacao[nota]" value="4"  @if(old('avaliacao.nota') == '4') checked @endif /><label for="star4" title="4 estrelas">4 estrelas</label>
  			    <input type="radio" id="star3" name="avaliacao[nota]" value="3"  @if(old('avaliacao.nota') == '3') checked @endif /><label for="star3" title="3 estrelas">3 estrelas</label>
  			    <input type="radio" id="star2" name="avaliacao[nota]" value="2"  @if(old('avaliacao.nota') == '2') checked @endif /><label for="star2" title="2 estrelas">2 estrelas</label>
  			    <input type="radio" id="star1" name="avaliacao[nota]" value="1"  @if(old('avaliacao.nota') == '1') checked @endif /><label for="star1" title="1 estrela">1 estrela</label>
          </div>
        </div>

        <p class="observacao"><strong>Observação:</strong> a data, seu nome, foto e unidade serão adicionados automaticamente.</p>

        <input type="submit" value="PUBLICAR" class="btn btn-success">

      </form>

    </div>

  </div>

</div>
