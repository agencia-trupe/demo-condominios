<div id="busca-fornecedor">

  <h3>BUSQUE UM FORNECEDOR</h3>

  <form class="form-ajax"
        action="{{route('moradores.fornecedores.busca')}}"
        method="post">

    {!! csrf_field() !!}

    <label>
      <select name="busca_categoria" id="busca-fornecedor-input-categoria">
        <option value="">Encontrar Fornecedor - por Categoria</option>
        @foreach($listaCategorias as $key => $value)
          <option value="{{$value->id}}" @if(isset($busca_categoria) && $busca_categoria == $value->id) selected @endif >{{$value->titulo}}</option>
        @endforeach
      </select>
    </label>

    <label class="label-inline">
      <input type="text" name="busca_nome" placeholder="Encontrar Fornecedor - por Nome" id="busca-fornecedor-input-nome" @if(isset($busca_nome)) value="{{$busca_nome}}" @endif >
    </label>

    <label class="label-inline reduzido">
      <input type="submit" value="BUSCAR" class="btn btn-info">
    </label>

  </form>

</div>
