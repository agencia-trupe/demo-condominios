@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="fornecedores-main">

      <h1>INDICAÇÕES DE FORNECEDORES</h1>

      <a href="{{ route('moradores.fornecedores.index') }}" class="btn btn-info" title="Voltar">&laquo; VOLTAR</a>

	    @include('moradores.fornecedores.partials.mensagens')

      @include('moradores.fornecedores.partials.detalhes')

      @include('moradores.fornecedores.partials.avaliacoes')

      <a href="{{ route('moradores.fornecedores.index') }}" class="btn btn-info" title="Voltar">&laquo; VOLTAR</a>

	  </div>

	</section>

@stop
