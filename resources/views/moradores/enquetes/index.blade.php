@extends('moradores.templates.dashboard')

@section('conteudo')

<section class="centro" id="dashboard-section">

  @include('moradores.dashboard.partials.header')<!--

  --><div class="main" id="enquetes-main">

    <h1>ENQUETES</h1>

    @include('moradores.enquetes.partials.mensagens')

    @if($enquete_ativa || sizeof($historico))
      <ul id="lista-enquetes">

        @if($enquete_ativa)
          <li id="enquete-ativa">

            <div class="titulo">
              <h2>{{ $enquete_ativa->titulo }}</h2>
              <div class="data">{{ $enquete_ativa->created_at->formatLocalized('%B %Y') }}</div>
            </div>

            <div class="texto">

              <div class="questao">{{ $enquete_ativa->texto }}</div>

              <form action="{{ route('moradores.enquetes.votar') }}" method="post">

                {!! csrf_field() !!}

                <input type="hidden" name="enquetes_id" value="{{$enquete_ativa->id}}">

                <ul class="lista-opcoes">
                  @foreach( $enquete_ativa->opcoes as $opcao )
                    <li>
                      <label>
                        <input type="radio" required name="voto_enquete" value="{{ $opcao->id }}"> {{ $opcao->texto }}
                      </label>
                    </li>
                  @endforeach
                </ul>

                <div class="submit-box">
                  <input type="submit" value="VOTAR" class="btn btn-info">
                  <a href="{{ route('moradores.enquetes.resultados', $enquete_ativa->id) }}" title="ver resultados até o momento">ver resultados até o momento &raquo;</a>
                </div>

              </form>
            </div>

          </li>
        @endif

        @foreach($historico as $enquete)
          <li>
            <a href="{{ route('moradores.enquetes.resultados', $enquete->id) }}" title="Ver resultados da Enquete">
              <div class="titulo">
                <h2>{{ $enquete->titulo }}</h2>
                <div class="data">{{ $enquete->created_at->formatLocalized('%B %Y') }}</div>
              </div>
              <div class="texto">
                <p>
                  {{ $enquete->texto }}
                </p>
              </div>
            </a>
          </li>
        @endforeach

      </ul>
    @else
      <p>Ainda não há nenhuma enquete cadastrada.</p>
    @endif

  </div>

</section>

@stop