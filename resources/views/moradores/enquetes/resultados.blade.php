@extends('moradores.templates.dashboard')

@section('conteudo')

<section class="centro" id="dashboard-section">

  @include('moradores.dashboard.partials.header')<!--

  --><div class="main" id="enquetes-main">

    <h1>ENQUETES</h1>

    @include('moradores.enquetes.partials.mensagens')

    <div id="resultado-enquetes">

      <div class="titulo">
        <h2>{{ $enquete->titulo }}</h2>
        <div class="data">{{ $enquete->created_at->formatLocalized('%B %Y') }}</div>
      </div>

      <div class="texto">

        <div class="questao">{{ $enquete->texto }}</div>

        <form action="{{ route('moradores.enquetes.votar') }}" method="post">

          {!! csrf_field() !!}

          <ul class="lista-opcoes">
            @foreach( $enquete->opcoes as $opcao )
              <li>
                <div class="porcentagem">
                  <div class="barra" style="width: {{ $opcao->porcentagem }}%"></div>
                </div>
                <label><strong>{{ $opcao->porcentagem }}% |</strong> {{ $opcao->texto }}</label>
              </li>
            @endforeach
          </ul>

        </form>
      </div>

    </div>

    <a href="{{ route('moradores.enquetes.index') }}" class="btn btn-info">VOLTAR</a>

  </div>

</section>

@stop