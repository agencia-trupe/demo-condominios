@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="animais-de-estimacao-main">

			<h1>CADASTRO DE ANIMAIS DE ESTIMAÇÃO</h1>

			<p>O Cadastro de Animais de Estimação serve para ajudar a Portaria e funcionários caso alguma ocorrência com animais de estimação necessite de auxílio para identificação do animal. O cadastro não é obrigatório, mas desejável.</p>

			@include('moradores.animais-de-estimacao.partials.mensagens')

			@include('moradores.animais-de-estimacao.partials.form', $animal_de_estimacao)

			@include('moradores.animais-de-estimacao.partials.lista-animais')

		</div>

	</section>

@stop