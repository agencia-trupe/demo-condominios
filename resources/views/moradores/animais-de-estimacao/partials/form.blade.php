<div id="controle-animais-de-estimacao">

@if(isset($animal_de_estimacao))
	<a href="#" title="Alterar animal de estimação" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="animais-de-estimacao-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ALTERAR ANIMAL DE ESTIMAÇÃO</a>
	<form action="{{ route('moradores.animais-de-estimacao.atualizar', $animal_de_estimacao->id) }}" id="animais-de-estimacao-form" class="form-unidade-edicao" method="post">
@else
	<a href="#" title="Acrescentar um animal de estimação" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="animais-de-estimacao-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ACRESCENTAR UM ANIMAL DE ESTIMAÇÃO</a>
	<form action="{{ route('moradores.animais-de-estimacao.cadastrar') }}" id="animais-de-estimacao-form" method="post">
@endif
		<fieldset>

			{!! csrf_field() !!}

			<label class="
				animais-de-estimacao-form-input
				@if($errors->has('animais_de_estimacao.nome')) input-erro obrigatorio @endif
			">
				<input type="text" id="animais-de-estimacao-form-nome" name="animais_de_estimacao[nome]" placeholder="nome do animal" @if(isset($animal_de_estimacao)) value="{{ $animal_de_estimacao->nome }}" @else value="{{ old('animais_de_estimacao.nome') }}" @endif>
				<div class="
					animais-de-estimacao-input-addon
					recuoo-extra
					@if($errors->has('animais_de_estimacao.visibilidade')) input-erro obrigatorio @endif
					">
					<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-data-nascimento" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="O cadastro de animais pode ajudar no caso do seu bichinho se perder no condomínio, ou para identificação das unidades que possuem pets no caso de uma emergência ou avisos direcionados.">
					<a href="#" class="animais-de-estimacao-form-set_visibilidade addon-tooltip-visib off" title="Definir visibilidade do registro" data-shadow-title="Cadastro de Animal de Estimação" data-shadow-storage="animais-de-estimacao-form-visibilidade"></a>
					<input type="hidden" name="animais_de_estimacao[visibilidade]" id="animais-de-estimacao-form-visibilidade" @if(isset($animal_de_estimacao)) value="{{ $animal_de_estimacao->visibilidades }}" @else value="{{ old('animais_de_estimacao.visibilidade') }}" @endif>
				</div>
			</label>

			<label class="
				animais-de-estimacao-form-input
				@if($errors->has('animais_de_estimacao.especie')) input-erro obrigatorio @endif
			">
				<input type="text" id="animais-de-estimacao-form-especie" name="animais_de_estimacao[especie]" placeholder="espécie / raça" @if(isset($animal_de_estimacao)) value="{{ $animal_de_estimacao->especie }}" @else value="{{ old('animais_de_estimacao.especie') }}" @endif>
			</label>

			<label class="
				animais-de-estimacao-form-input
				@if($errors->has('animais_de_estimacao.sexo')) input-erro obrigatorio @endif
			">
				<select name="animais_de_estimacao[sexo]" id="animais-de-estimacao-form-sexo">
					<option value="">sexo</option>
					@if(isset($animal_de_estimacao))
						<option value="Macho" @if($animal_de_estimacao->sexo == 'Macho') selected @endif >Macho</option>
						<option value="Fêmea" @if($animal_de_estimacao->sexo == 'Fêmea') selected @endif >Fêmea</option>
					@else
						<option value="Macho" @if(old('animais_de_estimacao.sexo') == 'Macho') selected @endif >Macho</option>
						<option value="Fêmea" @if(old('animais_de_estimacao.sexo') == 'Fêmea') selected @endif >Fêmea</option>
					@endif
				</select>
			</label>

		</fieldset>

		<div class="input-foto">

			<div class="animais-de-estimacao-foto-placeholder">
				<label title="Alterar Foto" class="
						@if($errors->has('animais_de_estimacao.foto')) input-erro @endif
					">
					<p data-html-original="ADICIONAR FOTO">
						@if($errors->has('animais_de_estimacao.foto'))
							{{ $errors->first('animais_de_estimacao.foto') }}
						@else
							ADICIONAR FOTO
						@endif
					</p>
					@if(isset($animal_de_estimacao))
						<img src="assets/images/moradores/animais-de-estimacao/thumbs/{{ $animal_de_estimacao->foto }}">
					@endif
					<div id="upload-progresso"><div class="barra"></div></div>
					<input type="hidden" name="animais_de_estimacao[foto]" class="form-foto-hidden" id="animais-de-estimacao-form-foto" @if(isset($animal_de_estimacao)) value="{{ $animal_de_estimacao->foto }}" @else value="{{ old('animais_de_estimacao.foto') }}" @endif>
					<input type="file"   name="foto" id="animais-de-estimacao-form-fileupload">
				</label>
			</div>

			<div class="animais-de-estimacao-input-addon recuoo-normal">
				<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-animal-de-estimacao-foto" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A foto do seu animal de estimação ajuda os funcionários do prédio na identificação caso seu bichinho se perca pelo condomínio e seja encontrado por outra pessoa.">
			</div>
		</div>

		<input type="submit" @if(isset($animal_de_estimacao)) value="ALTERAR" @else value="CADASTRAR" @endif class="btn btn-info">

	</form>

</div>
