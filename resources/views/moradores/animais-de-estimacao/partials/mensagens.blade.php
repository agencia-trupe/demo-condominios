@if(Session::has('animais_de_estimacao_create') && session('animais_de_estimacao_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Animal de Estimação registrado com sucesso.</strong>
	</p>
@endif

@if(Session::has('animais_de_estimacao_create') && session('animais_de_estimacao_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Animal de Estimação.</strong>
	</p>
@endif

@if(Session::has('animais_de_estimacao_destroy') && session('animais_de_estimacao_destroy') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Animal de Estimação removido com sucesso.</strong>
	</p>
@endif

@if(Session::has('animais_de_estimacao_destroy') && session('animais_de_estimacao_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Remover Animal de Estimação.</strong>
	</p>
@endif

@if(Session::has('animais_de_estimacao_update') && session('animais_de_estimacao_update') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Animal de Estimação atualizado com sucesso.</strong>
	</p>
@endif

@if(Session::has('animais_de_estimacao_update') && session('animais_de_estimacao_update') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Atualizar Animal de Estimação.</strong>
	</p>
@endif
