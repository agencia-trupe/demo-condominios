<div id="controle-classificados">

  <a href="#" title="Publicar anúncio" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="anuncio-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> PUBLICAR ANÚNCIO</a>

  <form action="{{ route('moradores.classificados.publicar') }}" id="anuncio-form" method="post">

    <fieldset>

      {!! csrf_field() !!}

      <label class="
        classificados-form-input
        @if($errors->has('anuncio.categoria')) input-erro obrigatorio @endif
      ">
        <select class="" id="classificados-form-categoria" name="anuncio[categoria]">
          <option value="">categoria (selecione)</option>
          <option value="venda_e_troca" @if(old('anuncio.categoria') == 'venda_e_troca') selected @endif >Venda & Troca</option>
          <option value="servico" @if(old('anuncio.categoria') == 'servico') selected @endif >Serviços</option>
          <option value="recado" @if(old('anuncio.categoria') == 'recado') selected @endif >Recados</option>
        </select>
      </label>

      <label class="
        classificados-form-input
        @if($errors->has('anuncio.titulo')) input-erro obrigatorio @endif
      ">
        <input type="text" id="classificados-form-titulo" name="anuncio[titulo]" placeholder="título do anúncio" value="{{ old('anuncio.titulo') }}">
      </label>

      <label class="classificados-form-input">
        <textarea name="anuncio[descricao]" id="classificados-form-titulo" placeholder="descrição do anúncio">{{old('anuncio.descricao')}}</textarea>
      </label>

      <label class="classificados-form-input">
        <textarea name="anuncio[contato]" id="classificados-form-contato" placeholder="informações de contato">{{old('anuncio.contato')}}</textarea>
      </label>

    </fieldset>

    <div class="input-foto">

      <div class="classificados-foto-placeholder">
        <label title="Enviar Fotos">
          <p data-html-original="ADICIONAR IMAGENS">
            ADICIONAR IMAGENS
          </p>
          <div id="upload-progresso"><div class="barra"></div></div>

          <input type="file" multiple name="foto" id="classificados-form-fileupload">
        </label>
      </div>

      <ul class="lista-imagens">
        @if(old('chamados_de_manutencao.fotos'))
          @foreach(old('chamados_de_manutencao.fotos') as $foto)
            <li>
              <a href='#'
                data-featherlight-gallery
                data-featherlight='assets/images/moradores/classificados/redimensionadas/{{ $foto }}'
                data-featherlight-type='image'
                title='Ampliar'
                class='btn-ampliar-imagem'
              >
                <img src="assets/images/moradores/classificados/thumbs/{{ $foto }}">
              </a>
              <a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>
              <input type='hidden' name='anuncio[fotos][]' class='form-foto-hidden' value='{{ $foto }}'>
            </li>
          @endforeach
        @endif
      </ul>
    </div>

    <p>
      <small><strong>Observação:</strong> seu nome, foto e unidade serão adicionados automaticamente.</small>
    </p>


    <input type="submit" value="CADASTRAR" class="btn btn-info">

  </form>

</div>