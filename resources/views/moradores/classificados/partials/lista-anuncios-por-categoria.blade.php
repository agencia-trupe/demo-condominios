<ul class="lista-anuncios">
	@if(sizeof($anuncios))
		@foreach($anuncios as $anuncio)
			<li>

				@if($anuncio->fotos)
					<div class="galeria-anuncio" data-featherlight-gallery data-featherlight-filter="a">
						@foreach($anuncio->fotos as $key => $value)
							<a href="assets/images/moradores/classificados/redimensionadas/{{$value->imagem}}" class='abre-galeria-anuncio' title="Abrir galeria do anúncio">
								<img src="assets/images/moradores/classificados/thumbs/{{$value->imagem}}" alt="Abrir galeria do anúncio" />
							</a>
						@endforeach
					</div>
				@endif

			  <h2>{{ $anuncio->titulo }}</h2>
	      <div class="texto">
	        {!! nl2br($anuncio->descricao) !!}
	      </div>
	      <div class="contato">
	        {!! nl2br(parse_emails($anuncio->contato)) !!}
	      </div>
	      <div class="assinatura">
	        <div class="avatar">
	          @if($anuncio->morador->foto && file_exists( "assets/images/moradores/fotos/thumbs/" . $anuncio->morador->foto ))
	    				<img src="assets/images/moradores/fotos/thumbs/{{ $anuncio->morador->foto }}" alt="">
	    			@else
	    				<img src="{{ route('moradores.avatar-default') }}">
	    			@endif
	        </div>
	        <div class="dados">
	          <div class="data-publicacao">Publicado em: {{$anuncio->created_at->format('d/m/Y')}}</div>
	          <div class="morador">{!! $anuncio->morador->nome . ' | <strong>unidade ' . $anuncio->morador->unidade->resumo . '</strong>' !!}</div>
	        </div>
	      </div>
			</li>
		@endforeach
	@else
		<li>
			<p class="nenhum-anuncio">Não há anúncios publicados para esta categoria.</p>
		</li>
	@endif
</ul>
