<div id="controle-classificados">

  <a href="#" title="Publicar anúncio" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="anuncio-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ALTERAR ANÚNCIO</a>

  <form action="{{ route('moradores.classificados.editar', $anuncio->id) }}" class="form-unidade-edicao" id="anuncio-form" method="post">

    <fieldset>

      {!! csrf_field() !!}

      <label class="
        classificados-form-input
        @if($errors->has('anuncio.categoria')) input-erro obrigatorio @endif
      ">
        <select class="" id="classificados-form-categoria" name="anuncio[categoria]">
          <option value="">categoria (selecione)</option>
          <option value="venda_e_troca" @if($anuncio->categoria == 'venda_e_troca') selected @endif >Venda & Troca</option>
          <option value="servico" @if($anuncio->categoria == 'servico') selected @endif >Serviços</option>
          <option value="recado" @if($anuncio->categoria == 'recado') selected @endif >Recados</option>
        </select>
      </label>

      <label class="
        classificados-form-input
        @if($errors->has('anuncio.titulo')) input-erro obrigatorio @endif
      ">
        <input type="text" id="classificados-form-titulo" name="anuncio[titulo]" placeholder="título do anúncio" value="{{ $anuncio->titulo }}">
      </label>

      <label class="classificados-form-input">
        <textarea name="anuncio[descricao]" id="classificados-form-titulo" placeholder="descrição do anúncio">{{$anuncio->descricao}}</textarea>
      </label>

      <label class="classificados-form-input">
        <textarea name="anuncio[contato]" id="classificados-form-contato" placeholder="informações de contato">{{$anuncio->contato}}</textarea>
      </label>

    </fieldset>

    <div class="input-foto">

      <div class="classificados-foto-placeholder">
        <label title="Enviar Fotos">
          <p data-html-original="ADICIONAR IMAGENS">
            ADICIONAR IMAGENS
          </p>
          <div id="upload-progresso"><div class="barra"></div></div>

          <input type="file" multiple name="foto" id="classificados-form-fileupload">
        </label>
      </div>

      <ul class="lista-imagens">
        @if($anuncio->fotos)
          @foreach($anuncio->fotos as $foto)
            <li>
              <a href='#'
                data-featherlight-gallery
                data-featherlight='assets/images/moradores/classificados/redimensionadas/{{ $foto->imagem }}'
                data-featherlight-type='image'
                title='Ampliar'
                class='btn-ampliar-imagem'
              >
                <img src="assets/images/moradores/classificados/thumbs/{{ $foto->imagem }}">
              </a>
              <a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>
              <input type='hidden' name='anuncio[fotos][]' class='form-foto-hidden' value='{{ $foto->imagem }}'>
            </li>
          @endforeach
        @endif
      </ul>
    </div>

    <p>
      <small><strong>Observação:</strong> seu nome, foto e unidade serão adicionados automaticamente.</small>
    </p>


    <input type="submit" value="GRAVAR" class="btn btn-info"> <a href="{{ route('moradores.classificados.meus-anuncios') }}" class="btn btn-default">CANCELAR</a>

  </form>

</div>