@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="classificados-main">

			<h1>MURAL & CLASSIFICADOS</h1>

      @include('moradores.classificados.partials.submenu')

			@include('moradores.classificados.partials.mensagens')

      <p>
        Publique anúncios de venda, promoção de serviços (seus e indicações) e
        recados gerais. Para todos os formatos é possível adicionar imagens.
      </p>

      @include('moradores.classificados.partials.form')

			@include('moradores.classificados.partials.lista-meus-anuncios')		

		</div>

	</section>

@stop
