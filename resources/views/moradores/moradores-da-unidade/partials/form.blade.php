<div id="controle-moradores-da-unidade">

@if(isset($morador_da_unidade))
	<a href="#" title="Alterar morador" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="moradores-da-unidade-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ALTERAR MORADOR</a>
	<form action="{{ route('moradores.moradores-da-unidade.atualizar', $morador_da_unidade->id) }}" id="moradores-da-unidade-form" class="form-unidade-edicao" method="post">
@else
	<a href="#" title="Acrescentar um morador" id="form-toggle" class="btn btn-success btn-com-icone" data-toggle-target="moradores-da-unidade-form"><img src="assets/images/layout/moradores/sinal-mais.png" alt="+"> ACRESCENTAR UM MORADOR</a>
	<form action="{{ route('moradores.moradores-da-unidade.cadastrar') }}" id="moradores-da-unidade-form" method="post">
@endif
		<fieldset>

			{!! csrf_field() !!}

			<label class="
				moradores-da-unidade-form-input
				@if($errors->has('moradores_da_unidade.nome')) input-erro obrigatorio @endif
			">
				<input type="text" id="moradores-da-unidade-form-nome" name="moradores_da_unidade[nome]" placeholder="nome completo" @if(isset($morador_da_unidade)) value="{{ $morador_da_unidade->nome }}" @else value="{{ old('moradores_da_unidade.nome') }}" @endif>
				<div class="
					moradores-da-unidade-input-addon
					@if($errors->has('moradores_da_unidade.visibilidade')) input-erro obrigatorio @endif
					">
					<a href="#" class="moradores-da-unidade-form-set_visibilidade addon-tooltip-visib off" title="Definir Visibilidade do registro" data-shadow-title="Cadastro de Morador da Unidade" data-shadow-storage="moradores-da-unidade-form-visibilidade"></a>
					<input type="hidden" name="moradores_da_unidade[visibilidade]" id="moradores-da-unidade-form-visibilidade" @if(isset($morador_da_unidade)) value="{{ $morador_da_unidade->visibilidades }}" @else value="{{ old('moradores_da_unidade.visibilidade') }}" @endif>
				</div>
			</label>

			<label class="
				moradores-da-unidade-form-input
				@if($errors->has('moradores_da_unidade.data_nascimento')) input-erro obrigatorio @endif
			">
				<input type="text" id="moradores-da-unidade-form-data" class="datepicker" name="moradores_da_unidade[data_nascimento]" placeholder="data de nascimento" @if(isset($morador_da_unidade)) value="{{ $morador_da_unidade->data_nascimento->format('d/m/Y') }}" @else value="{{ old('moradores_da_unidade.data_nascimento') }}" @endif>
				<div class="moradores-da-unidade-input-addon">
					<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-data-nascimento" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A data de nascimento ajuda a estimarmos a idade da pessoa auxiliando na identificação de possíveis tentativas de fraude no acesso ao condomínio.">
				</div>
			</label>

			<label class="
				moradores-da-unidade-form-input
				@if($errors->has('moradores_da_unidade.parentesco')) input-erro obrigatorio @endif
			">
				<select name="moradores_da_unidade[parentesco]" id="moradores-da-unidade-form-parentesco">
					<option value="">parentesco [selecione]</option>
					@if(isset($morador_da_unidade))
						<option value="Avô"      @if($morador_da_unidade->parentesco == 'Avô')      selected @endif >Avô</option>
						<option value="Avó"      @if($morador_da_unidade->parentesco == 'Avó')      selected @endif >Avó</option>
						<option value="Pai"      @if($morador_da_unidade->parentesco == 'Pai')      selected @endif >Pai</option>
						<option value="Mãe"      @if($morador_da_unidade->parentesco == 'Mãe')      selected @endif >Mãe</option>
						<option value="Marido"   @if($morador_da_unidade->parentesco == 'Marido')   selected @endif >Marido</option>
						<option value="Esposa"   @if($morador_da_unidade->parentesco == 'Esposa')   selected @endif >Esposa</option>
						<option value="Filho"    @if($morador_da_unidade->parentesco == 'Filho')    selected @endif >Filho</option>
						<option value="Filha"    @if($morador_da_unidade->parentesco == 'Filha')    selected @endif >Filha</option>
						<option value="Irmão"    @if($morador_da_unidade->parentesco == 'Irmão')    selected @endif >Irmão</option>
						<option value="Irmã"     @if($morador_da_unidade->parentesco == 'Irmã')     selected @endif >Irmã</option>
						<option value="Tio"      @if($morador_da_unidade->parentesco == 'Tio')      selected @endif >Tio</option>
						<option value="Tia"      @if($morador_da_unidade->parentesco == 'Tia')      selected @endif >Tia</option>
						<option value="Sobrinho" @if($morador_da_unidade->parentesco == 'Sobrinho') selected @endif >Sobrinho</option>
						<option value="Sobrinha" @if($morador_da_unidade->parentesco == 'Sobrinha') selected @endif >Sobrinha</option>
						<option value="Neto"     @if($morador_da_unidade->parentesco == 'Neto')     selected @endif >Neto</option>
						<option value="Neta"     @if($morador_da_unidade->parentesco == 'Neta')     selected @endif >Neta</option>
						<option value="Genro"    @if($morador_da_unidade->parentesco == 'Genro')    selected @endif >Genro</option>
						<option value="Nora"     @if($morador_da_unidade->parentesco == 'Nora')     selected @endif >Nora</option>
						<option value="Sogro"    @if($morador_da_unidade->parentesco == 'Sogro')    selected @endif >Sogro</option>
						<option value="Sogra"    @if($morador_da_unidade->parentesco == 'Sogra')    selected @endif >Sogra</option>
						<option value="Outro"    @if($morador_da_unidade->parentesco == 'Outro')    selected @endif >Outro</option>
					@else
						<option value="Avô"      @if(old('moradores_da_unidade.parentesco') == 'Avô')      selected @endif >Avô</option>
						<option value="Avó"      @if(old('moradores_da_unidade.parentesco') == 'Avó')      selected @endif >Avó</option>
						<option value="Pai"      @if(old('moradores_da_unidade.parentesco') == 'Pai')      selected @endif >Pai</option>
						<option value="Mãe"      @if(old('moradores_da_unidade.parentesco') == 'Mãe')      selected @endif >Mãe</option>
						<option value="Marido"   @if(old('moradores_da_unidade.parentesco') == 'Marido')   selected @endif >Marido</option>
						<option value="Esposa"   @if(old('moradores_da_unidade.parentesco') == 'Esposa')   selected @endif >Esposa</option>
						<option value="Filho"    @if(old('moradores_da_unidade.parentesco') == 'Filho')    selected @endif >Filho</option>
						<option value="Filha"    @if(old('moradores_da_unidade.parentesco') == 'Filha')    selected @endif >Filha</option>
						<option value="Irmão"    @if(old('moradores_da_unidade.parentesco') == 'Irmão')    selected @endif >Irmão</option>
						<option value="Irmã"     @if(old('moradores_da_unidade.parentesco') == 'Irmã')     selected @endif >Irmã</option>
						<option value="Tio"      @if(old('moradores_da_unidade.parentesco') == 'Tio')      selected @endif >Tio</option>
						<option value="Tia"      @if(old('moradores_da_unidade.parentesco') == 'Tia')      selected @endif >Tia</option>
						<option value="Sobrinho" @if(old('moradores_da_unidade.parentesco') == 'Sobrinho') selected @endif >Sobrinho</option>
						<option value="Sobrinha" @if(old('moradores_da_unidade.parentesco') == 'Sobrinha') selected @endif >Sobrinha</option>
						<option value="Neto"     @if(old('moradores_da_unidade.parentesco') == 'Neto')     selected @endif >Neto</option>
						<option value="Neta"     @if(old('moradores_da_unidade.parentesco') == 'Neta')     selected @endif >Neta</option>
						<option value="Genro"    @if(old('moradores_da_unidade.parentesco') == 'Genro')    selected @endif >Genro</option>
						<option value="Nora"     @if(old('moradores_da_unidade.parentesco') == 'Nora')     selected @endif >Nora</option>
						<option value="Sogro"    @if(old('moradores_da_unidade.parentesco') == 'Sogro')    selected @endif >Sogro</option>
						<option value="Sogra"    @if(old('moradores_da_unidade.parentesco') == 'Sogra')    selected @endif >Sogra</option>
						<option value="Outro"    @if(old('moradores_da_unidade.parentesco') == 'Outro')    selected @endif >Outro</option>
					@endif
				</select>
				<div class="moradores-da-unidade-input-addon">
					<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-parentesco" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="O parentesco também ajuda na identificação da pessoa que está tentando adentrar o condomínio. Se uma pessoa diz que é pai de alguém que não teria idade pra isso, os funcionários terão motivo para exigir mais detalhes na identificação e talvez barrar possíveis tentativas de fraude no acesso.">
				</div>
			</label>

		</fieldset>

		<div class="input-foto">

			<div class="moradores-da-unidade-foto-placeholder">
				<label title="Alterar Foto" class="
						@if($errors->has('moradores_da_unidade.foto')) input-erro @endif
					">
					<p data-html-original="ADICIONAR FOTO">
						@if($errors->has('moradores_da_unidade.foto'))
							{{ $errors->first('moradores_da_unidade.foto') }}
						@else
							ADICIONAR FOTO
						@endif
					</p>
					@if(isset($morador_da_unidade) && $morador_da_unidade->foto != '' && file_exists(public_path("assets/images/moradores/moradores-da-unidade/thumbs/{{ $morador_da_unidade->foto }}")))
						<img src="assets/images/moradores/moradores-da-unidade/thumbs/{{ $morador_da_unidade->foto }}">
					@endif
					<div id="upload-progresso"><div class="barra"></div></div>
					<input type="hidden" name="moradores_da_unidade[foto]" class="form-foto-hidden" id="moradores-da-unidade-form-foto" @if(isset($morador_da_unidade)) value="{{ $morador_da_unidade->foto }}" @else value="{{ old('moradores_da_unidade.foto') }}" @endif>
					<input type="file"   name="foto" id="moradores-da-unidade-form-fileupload">
				</label>
			</div>

			<div class="moradores-da-unidade-input-addon">
				<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-morador-unidade-foto" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A foto ajuda muito a garantir que a pessoa que vai acessar o condomínio é realmente alguém autorizado.">
			</div>
		</div>

		<input type="submit" @if(isset($morador_da_unidade)) value="ALTERAR" @else value="CADASTRAR" @endif class="btn btn-info">

	</form>

</div>
