@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="agenda-administracao-main">

	 		<h1>AGENDA</h1>

	 		<p>
				Acompanhe aqui as próximas reuniões, assembleias e eventos marcados pelo Condomínio.
	 		</p>

	 		<ul id="lista-agenda">
	 			@foreach($agenda as $evento)
	 				<li>
	 					<div class="data">
	 						<div class="dia">{{ $evento->data->format('d') }}</div>
	 						<div class="mes">{{ $evento->data->formatLocalized("%b") }}</div>
	 						<div class="ano">{{ $evento->data->format('Y') }}</div>
	 					</div>
	 					<div class="conteudo conteudo-cke">
	 						<h2>{{ $evento->titulo }}</h2>
	 						<div class="texto">
	 							{!! $evento->texto !!}
	 						</div>
	 						<div class="horario">
	 							{{ $evento->horario .' &middot; '. $evento->local }}
	 						</div>
	 					</div>
	 				</li>
	 			@endforeach
	 		</ul>

	 	</div>

	</section>

@stop
