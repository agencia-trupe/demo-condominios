@if(Session::has('senha_alterada'))
	<p class="retorno_formulario sucesso">
		<strong>Sua senha foi alterada com sucesso!</strong>
	</p>
@endif

@if(Session::has('senha_alterada_fail'))
	<p class="retorno_formulario erro">
		<strong>Erro ao alterar sua senha.</strong>
	</p>

	<p class="retorno_formulario erro">
		{{ session('senha_alterada_fail') }}
	</p>
@endif