@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="alterar-senha-main">

	 		<h1>ALTERAR SENHA</h1>

	 		@include('moradores.alterar-senha.partials.mensagens')

			<form action="{{ route('moradores.alterar-senha.alterar') }}" method="post">
				<fieldset>

					{!! csrf_field() !!}

					<label class="
							register-form-input
							input-espacado
							@if($errors->has('moradores.senha_atual')) input-erro @endif
							@if(str_is('*confere com a atual*' ,$errors->first('moradores.senha_atual'))) nao-confere @endif
							@if(str_is('*obrigatório*' ,$errors->first('moradores.senha_atual'))) obrigatorio @endif
							@if(str_is('*devem ser iguais*' ,$errors->first('moradores.senha_atual'))) confirmacao-senha-invalida @endif
						">
						<input type="password" name="moradores[senha_atual]" placeholder="senha atual" id="register-form-senha-atual" >
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.senha_nova')) input-erro @endif
							@if(str_is('*obrigatório*' ,$errors->first('moradores.senha_nova'))) obrigatorio @endif
							@if(str_is('*devem ser iguais*' ,$errors->first('moradores.senha_nova'))) confirmacao-senha-invalida @endif
						">
						<input type="password" name="moradores[senha_nova]" placeholder="senha nova" id="register-form-senha-nova" >
					</label>

					<label class="
							register-form-input
							@if($errors->has('moradores.confirmacao_senha')) input-erro obrigatorio @endif
						">
						<input type="password" name="moradores[confirmacao_senha]" placeholder="confirmar senha nova" id="register-form-confirmacao_senha" >
					</label>

				</fieldset>

				<input type="submit" value="ATUALIZAR">
			</form>

	 	</div>

	</section>

@stop