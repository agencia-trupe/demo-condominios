@if(Session::has('pessoa_autorizada_create') && session('pessoa_autorizada_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Pessoa Autorizada registrado com sucesso.</strong>
	</p>
@endif

@if(Session::has('pessoa_autorizada_create') && session('pessoa_autorizada_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Pessoa Autorizada.</strong>
	</p>
@endif

@if(Session::has('pessoa_autorizada_destroy') && session('pessoa_autorizada_destroy') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Pessoa Autorizada removido com sucesso.</strong>
	</p>
@endif

@if(Session::has('pessoa_autorizada_destroy') && session('pessoa_autorizada_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Remover Pessoa Autorizada.</strong>
	</p>
@endif

@if(Session::has('pessoa_autorizada_update') && session('pessoa_autorizada_update') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Pessoa Autorizada atualizado com sucesso.</strong>
	</p>
@endif

@if(Session::has('pessoa_autorizada_update') && session('pessoa_autorizada_update') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Atualizar Pessoa Autorizada.</strong>
	</p>
@endif

@if(Session::has('exception'))
	<p class="retorno_formulario erro">
		{{ session('exception') }}
	</p>
@endif