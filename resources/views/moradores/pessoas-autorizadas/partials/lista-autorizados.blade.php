@if(sizeof($pessoas))
	<ul id="lista-registros">
		@foreach($pessoas as $pessoa_aut)
			<li>
				<div class="pessoas-autorizadas-foto">
					@if($pessoa_aut->foto != '')
						<img src="assets/images/moradores/pessoas-autorizadas/thumbs/{{ $pessoa_aut->foto }}" alt="Pessoa autorizada">
					@else
						<img src="moradores/assets/images/avatar-default/122/122/sem foto" alt="Morador da unidade">
					@endif
				</div>
				<div class="pessoas-autorizadas-texto">
					<div class="pessoas-autorizadas-espacamento">
						<h2>{{ $pessoa_aut->nome }}</h2>
						<div class="data">{{ $pessoa_aut->data_nascimento->format('d/m/Y') }}</div>
						<div class="parentesco">{{ $pessoa_aut->parentesco }}</div>
						<div class="acoes">
							<a href="{{ route('moradores.pessoas-autorizadas.editar', $pessoa_aut->id) }}" class="btn btn-info" title="Editar">EDITAR</a>
							<a href="{{ route('moradores.pessoas-autorizadas.remover', $pessoa_aut->id) }}" class="btn btn-danger" title="Remover">X</a>
						</div>
					</div>
				</div>
			</li>
		@endforeach
	</ul>
@endif
