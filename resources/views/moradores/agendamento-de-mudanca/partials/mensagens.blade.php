@if(Session::has('agendamento_mudanca_create') && session('agendamento_mudanca_create') == true)
	<p class="retorno_formulario sucesso">
		Seu Agendamento de Mudança foi <strong>realizado</strong> com sucesso!
	</p>
@endif

@if(Session::has('agendamento_mudanca_create') && session('agendamento_mudanca_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao realizar Agendamento de Mudança.
	</p>
@endif

@if(Session::has('agendamento_mudanca_destroy') && session('agendamento_mudanca_destroy') == true)
	<p class="retorno_formulario sucesso">
		Seu Agendamento de Mudança foi <strong>cancelado</strong> com sucesso!
	</p>
@endif

@if(Session::has('agendamento_mudanca_destroy') && session('agendamento_mudanca_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro</strong> ao cancelar seu Agendamento de Mudança.
	</p>
@endif