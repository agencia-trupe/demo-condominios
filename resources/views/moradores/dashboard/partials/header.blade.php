<header>

	<div class="dashboard-branding">
		<a href="{{ route('moradores.dashboard') }}" title="Página Inicial">
			<div class="dashboard-marca">
				<img src="assets/images/layout/logo.png" alt="Sistema de Comunicação para Condomínios">
			</div>

			<p>Sistema de Comunicação para Condomínios</p>
		</a>
	</div>

	<div class="dashboard-topo">

		<div class="dashboard-avatar">
			@if(\Auth::moradores()->get()->foto && file_exists( "assets/images/moradores/fotos/thumbs/" . \Auth::moradores()->get()->foto ))
				<img src="assets/images/moradores/fotos/thumbs/{{ \Auth::moradores()->get()->foto }}" alt="">
			@else
				<img src="{{ route('moradores.avatar-default') }}">
			@endif
		</div>

		<div class="dashboard-perfil-menu">
			<h1>{{ \Auth::moradores()->get()->nome }}</h1>
			<ul>
				<li>
					<a href="{{ route('moradores.meu-perfil.index') }}" title="Meu Perfil" @if(str_is('moradores.meu-perfil*', Route::currentRouteName())) class='ativo' @endif >MEU PERFIL</a>
				</li>
				<li>
					<a href="{{ route('moradores.preferencias.index') }}" title="Preferências" @if(str_is('moradores.preferencias.index', Route::currentRouteName())) class="ativo" @endif>PREFERÊNCIAS</a>
				</li>
				<li>
					<a href="moradores/auth/logout" title="Sair">SAIR</a>
				</li>
			</ul>
		</div>

		<div class="dashboard-perfil-menu-lateral">
			<ul>
				<li><a href="{{ route('moradores.alterar-senha.index') }}" @if(str_is('moradores.alterar-senha.index', Route::currentRouteName())) class="ativo" @endif title="Alterar senha">&raquo; Alterar senha</a></li>
				<li><a href="{{ route('moradores.amizades.index') }}" @if(str_is('moradores.amizades.index', Route::currentRouteName())) class="ativo" @endif title="Amigos moradores">&raquo; Amigos moradores</a></li>
				<li><a href="{{ route('moradores.enquetes.index') }}" @if(str_is('moradores.enquetes.index', Route::currentRouteName())) class="ativo" @endif title="Enquetes">&raquo; Enquetes</a></li>
				<li><a href="{{ route('moradores.classificados.index', 'venda_e_troca') }}" @if(str_is('moradores.classificados*', Route::currentRouteName())) class="ativo" @endif title="Mural & Classificados">&raquo; Mural & Classificados</a></li>
				<li><a href="{{ route('moradores.fornecedores.index') }}" title="Indicações de fornecedores" @if(str_is('moradores.fornecedores.*', Route::currentRouteName())) class='ativo' @endif >&raquo; Indicações de fornecedores</a></li>
			</ul>
		</div>

	</div>

</header>

<aside>

	@include('moradores.dashboard.partials.menu')

</aside>
