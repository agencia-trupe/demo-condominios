@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	  --><div class="main" id="dashboard-main">

			@include('moradores.dashboard.partials.notificacoes')

			@if(sizeof($avisos))

				<div id="dashboard-avisos">

					<h2>AVISOS IMPORTANTES</h2>

					<ul class="lista-avisos">

						@foreach ($avisos as $aviso)

							<li>
								<div class="coluna-esquerda">
									<div class="aviso-timestamp">{{ $aviso->created_at->format('d/m/y · H:i \h') }}</div>
									<div class="aviso-marcar-lido">
										<label>
											<input type="checkbox" name="aviso_lido" value="{{ $aviso->id }}"> Li e entendi.
										</label>
									</div>
								</div><!--
								--><div class="coluna-direita">
									<div class="aviso-chamada">
										<h3>{{ $aviso->titulo }}</h3>
										<div class="aviso-chamada-texto">
											{{ $aviso->olho }}
										</div>
										<div class="aviso-assinatura">
											{{ $aviso->assinatura }}
										</div>
										<div class="aviso-link">
											<a href="{{ route('moradores.avisos.detalhes', $aviso->id) }}" title="Continuar lendo sobre este aviso">CONTINUAR LENDO SOBRE ESTE AVISO</a>
										</div>
									</div>
								</div>
							</li>

						@endforeach

					</ul>

				</div>

			@endif

			@if(sizeof($novidades))

				<div id="dashboard-novidade">

					<h2>TEM NOVIDADE! CONFIRA!</h2>

					<ul class="lista-novidades">
						@foreach($novidades as $novidade)
							<li>
								<a href="{{ route('moradores.documentos.download', $novidade->id) }}" title="{{ $novidade->titulo }}">
									<div class="novidades-origem">
										ADMINISTRAÇÃO E COMUNICAÇÃO | {{ $novidade->categoria->titulo }}
									</div>
									<div class="novidades-titulo">
										{{ $novidade->titulo }}
									</div>
									<div class="novidades-timestamp">
										ADICIONADO EM {{ $novidade->created_at->formatLocalized("%e DE %B DE %Y · %H:%I h") }}
									</div>
								</a>
							</li>
						@endforeach
					</ul>
				</div>
			@endif

		</div>

	</section>

@stop
