<form action="{{ route('moradores.chamados-de-manutencao.registrar') }}" id="chamados-de-manutencao-form" method="post">

	<fieldset>

		{!! csrf_field() !!}

		<label class="
			chamados-de-manutencao-form-input
			@if($errors->has('chamados_de_manutencao.data')) input-erro obrigatorio @endif
		">
			<input type="text" class="datepicker" id="chamados-de-manutencao-form-data" name="chamados_de_manutencao[data]" placeholder="data da ocorrência" value="{{ old('chamados_de_manutencao.data') }}">
			<div class="chamados-de-manutencao-input-addon">
				<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-data-nascimento" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A data, hora e localização da ocorrência dentro do condomínio são de extrema importância no registro de chamados para evitar que o zelador se preocupe em resolver um mesmo problema relatado por pessoas diferentes. Seja o mais preciso possível nas informações.">
			</div>
		</label>

		<label class="
			chamados-de-manutencao-form-input
			@if($errors->has('chamados_de_manutencao.horario')) input-erro obrigatorio @endif
		">
			<input type="text" id="chamados-de-manutencao-form-horario" name="chamados_de_manutencao[horario]" placeholder="horário aproximado" value="{{ old('chamados_de_manutencao.horario') }}">
			<div class="chamados-de-manutencao-input-addon">
				<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-data-nascimento" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A data, hora e localização da ocorrência dentro do condomínio são de extrema importância no registro de chamados para evitar que o zelador se preocupe em resolver um mesmo problema relatado por pessoas diferentes. Seja o mais preciso possível nas informações.">
			</div>
		</label>

		<label class="
			chamados-de-manutencao-form-input
			@if($errors->has('chamados_de_manutencao.localizacao')) input-erro obrigatorio @endif
		">
			<input type="text" id="chamados-de-manutencao-form-localizacao" name="chamados_de_manutencao[localizacao]" placeholder="localização (especifique bem)" value="{{ old('chamados_de_manutencao.localizacao') }}">
			<div class="chamados-de-manutencao-input-addon">
				<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-data-nascimento" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="A data, hora e localização da ocorrência dentro do condomínio são de extrema importância no registro de chamados para evitar que o zelador se preocupe em resolver um mesmo problema relatado por pessoas diferentes. Seja o mais preciso possível nas informações.">
			</div>
		</label>

		<label class="
			chamados-de-manutencao-form-input
			contem-textarea
			@if($errors->has('chamados_de_manutencao.descricao')) input-erro obrigatorio @endif
		">
			<textarea name="chamados_de_manutencao[descricao]" id="chamados-de-manutencao-form-descricao" placeholder="descrição">{{ old('chamados_de_manutencao.descricao') }}</textarea>
		</label>

	</fieldset>

	<div class="input-foto">

		<div class="chamados-de-manutencao-foto-placeholder">
			<label title="Enviar Fotos">
				<p data-html-original="ENVIAR FOTOS (se houver)">
					ENVIAR FOTOS (se houver)
				</p>
				<div id="upload-progresso"><div class="barra"></div></div>

				<input type="file" multiple name="foto" id="chamados-de-manutencao-form-fileupload">
			</label>
		</div>

		<div class="chamados-de-manutencao-input-addon recuoo-normal">
			<img src="assets/images/layout/moradores/icone-interrogacao.png" class="addon-tooltip addon-tooltip-animal-de-estimacao-foto" alt="Por que essa informação é solicitada?" title="Por que essa informação é solicitada?" data-tooltip-text="Uma foto do problema, juntamente com a descrição da localização, pode ajudar o zelador na identificação e resolução do problema com maior facilidade.">
		</div>

		<ul class="lista-imagens">
			@if(old('chamados_de_manutencao.fotos'))
				@foreach(old('chamados_de_manutencao.fotos') as $foto)
					<li>
						<a href='#'
							data-featherlight-gallery
							data-featherlight='assets/images/moradores/chamados-de-manutencao/redimensionadas/{{ $foto }}'
							data-featherlight-type='image'
							title='Ampliar'
							class='btn-ampliar-imagem'
						>
							<img src="assets/images/moradores/chamados-de-manutencao/thumbs/{{ $foto }}">
						</a>
						<a href='#' title='Remover Imagem' class='btn btn-danger btn-mini btn-remover-imagem'>x</a>
						<input type='hidden' name='chamados_de_manutencao[fotos][]' class='form-foto-hidden' value='{{ $foto }'>
					</li>
				@endforeach
			@endif
		</ul>
	</div>

	<input type="submit" value="REGISTRAR OCORRÊNCIA" class="btn btn-info">

</form>
