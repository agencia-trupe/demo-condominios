@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="pessoas-autorizadas-main">

			<h1>PRESTADORES DE SERVIÇO</h1>

			<p>Registre os prestadores de serviços que podem acessar sua Unidade.</p>

			@include('moradores.pessoas-autorizadas.partials.mensagens')

			@include('moradores.pessoas-autorizadas.partials.form', $prestador_de_servico)

			@include('moradores.pessoas-autorizadas.partials.lista-prestadores')

		</div>

	</section>

@stop