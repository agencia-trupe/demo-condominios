@if(Session::has('prestador_de_servico_create') && session('prestador_de_servico_create') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Prestador de Serviço registrado com sucesso.</strong>
	</p>
@endif

@if(Session::has('prestador_de_servico_create') && session('prestador_de_servico_create') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Cadastrar Prestador de Serviço.</strong>
	</p>
@endif

@if(Session::has('prestador_de_servico_destroy') && session('prestador_de_servico_destroy') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Prestador de Serviço removido com sucesso.</strong>
	</p>
@endif

@if(Session::has('prestador_de_servico_destroy') && session('prestador_de_servico_destroy') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Remover Prestador de Serviço.</strong>
	</p>
@endif

@if(Session::has('prestador_de_servico_update') && session('prestador_de_servico_update') == true)
	<p class="retorno_formulario sucesso">
		<strong>Cadastro de Prestador de Serviço atualizado com sucesso.</strong>
	</p>
@endif

@if(Session::has('prestador_de_servico_update') && session('prestador_de_servico_update') == false)
	<p class="retorno_formulario erro">
		<strong>Erro ao Atualizar Prestador de Serviço.</strong>
	</p>
@endif

@if(Session::has('exception'))
	<p class="retorno_formulario erro">
		{{ session('exception') }}
	</p>
@endif