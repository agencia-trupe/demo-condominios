@extends('moradores.templates.dashboard')

@section('conteudo')

	<section class="centro" id="dashboard-section">

		@include('moradores.dashboard.partials.header')<!--

	 --><div class="main" id="avisos-main">

			<h1>AVISOS</h1>

			<p>Todos os avisos postados ficam armazenados para consulta, por data de postagem (a mais recente exibida primeiro).</p>

			@if(sizeof($avisos_nao_lidos))
				<div id="dashboard-avisos">
					<h2>AINDA NÃO LIDOS</h2>
					<ul class="lista-avisos">
						@foreach ($avisos_nao_lidos as $aviso)
							<li>
								<div class="coluna-esquerda">
									<div class="aviso-timestamp">{{ $aviso->created_at->format('d/m/y · H:i \h') }}</div>
									<div class="aviso-marcar-lido">
										<label>
											<input type="checkbox" name="aviso_lido" value="{{ $aviso->id }}"> Li e entendi.
										</label>
									</div>
								</div><!--
								--><div class="coluna-direita">
									<div class="aviso-chamada">
										<h3>{{ $aviso->titulo }}</h3>
										<div class="aviso-chamada-texto">
											{{ $aviso->olho }}
										</div>
										<div class="aviso-assinatura">
											{{ $aviso->assinatura }}
										</div>
										<div class="aviso-link">
											<a href="{{ route('moradores.avisos.detalhes', $aviso->id) }}" title="Continuar lendo sobre este aviso">CONTINUAR LENDO SOBRE ESTE AVISO</a>
										</div>
									</div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if(sizeof($avisos_lidos))
				<div id="dashboard-avisos-lidos">
					<h2>AVISOS LIDOS</h2>
					<ul class="lista-avisos-lidos">
						@foreach ($avisos_lidos as $aviso)
							<li>
								<div class="coluna-esquerda">
									<div class="aviso-timestamp">{{ $aviso->created_at->format('d/m/y · H:i \h') }}</div>
								</div><!--
							 --><div class="coluna-direita">
									<div class="aviso-link">
										<a href="{{ route('moradores.avisos.detalhes', $aviso->id) }}" title="Reler este aviso">RELER</a>
									</div>
									<div class="aviso-titulo">
										<h3>{{ $aviso->titulo }}</h3>
									</div>
								</div>
							</li>
						@endforeach
					</ul>
					{!! $avisos_lidos->render() !!}
				</div>
			@endif

		</div>

	</section>

@stop