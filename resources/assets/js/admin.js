$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
})

jQuery(function($){

  $.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c;Anterior',
    nextText: 'Pr&oacute;ximo&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };

  $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function iconeUploadStart(scope){
  $('.multiUpload .icone .glyphicon-open', scope).css('opacity', 0);
  setTimeout( function(){
    $('.multiUpload .icone .glyphicon-refresh', scope).css({'display' : 'block', 'opacity': 1});
    $('.multiUpload .icone .glyphicon-open', scope).css('display', 'none');
  }, 350);
}

function iconeUploadStop(scope){
  $('.multiUpload .icone .glyphicon-refresh', scope).css('opacity', 0);
  setTimeout( function(){
    $('.multiUpload .icone .glyphicon-open', scope).css({'display' : 'block', 'opacity': 1});
    $('.multiUpload .icone .glyphicon-refresh', scope).css('display', 'none');
  }, 350);
}

function controlaUploadImagens(limite, scope){
  if(maximoImagensAtingido(limite, scope))
    esconderUploadImagens(0, scope);
  else
    mostrarUploadImagens(0, scope);
}

function maximoImagensAtingido(limite, scope){
  return $('.projetoImagem', scope).length >= limite;
}

function esconderUploadImagens(speed, scope){
  $('.multiUpload', scope).hide(speed);
  $('.limiteImagensAtingido', scope).show(speed);
}

function mostrarUploadImagens(speed, scope){
  $('.multiUpload', scope).show(speed);
  $('.limiteImagensAtingido', scope).hide(speed);
}

function busca(termo){
  $("table").find("tr").each(function(index) {
    if (!index) return;
    var id = $(this).find("td.celula-nome").first().text().toLowerCase();
    $(this).toggle(id.indexOf(termo.toLowerCase()) !== -1);
  });
}

$('document').ready( function(){

  $('.reenviar-email-confirmacao').click( function(e){
    e.preventDefault();
    var botao   = $(this);
    var morador = botao.attr('data-morador');
    $.post('admin/reenviar-email-confirmacao', {
      morador: morador
    }, function(resposta){
      botao.removeClass('btn-default')
           .addClass('btn-success')
           .html("e-mail reenviado <span class='glyphicon glyphicon-ok'></span>")
    });
  });

  // Botão de excluir registro
  $('.btn-delete').click( function(e){
  	e.preventDefault();
  	var form = $(this).closest('form');
  	bootbox.confirm('Deseja Excluir o Registro?', function(result){
    	if(result)
      	form.submit();
    	else
      	$(this).modal('hide');
  	});
	});

  // Botão Ativar Enquete
  $('.btn-ativar-enquete').click( function(e){
  	e.preventDefault();
  	var destino = $(this).attr('href');
  	bootbox.confirm("<h4>Deseja Ativar a Enquete?</h4><div class='alert alert-warning text-center' role='alert'>Ao ativar esta enquete, a enquete ativa anteriormente será desativa.<br>Caso ela já possua votos, será encerrada e arquivada no Histórico.</div>", function(result){
    	if(result)
      	window.location = destino;
    	else
      	$(this).modal('hide');
  	});
	});

  // Ordenação na tabela
  $('table.table-sortable tbody').sortable({
    update : function () {
      serial = [];
      tabela = $('table.table-sortable').attr('data-tabela');
      $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
        serial.push(elm.id.split('_')[1])
      });
      $.post('admin/ordernar-registros', {
        data : serial,
        tabela : tabela
      });
    },
    helper: function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    handle : $('.btn-move')
  }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

  $('.timepicker').datetimepicker({
    format: 'HH:mm',
    stepping: 15
  });

  $('.monthpicker').datetimepicker({
    format: 'M/YYYY'
  });

  $('.datetimepicker').datetimepicker({
    format: 'DD/MM/YYYY HH:mm',
    stepping: 15,
    locale: 'pt'
  });

  $('.datetimepicker').on('dp.change', function(e){
    var data    = e.date.format('YYYY-MM-DD_HH:mm');
    var destino = $('#link-filtrar').attr('href-original');
    $('#link-filtrar').attr('href', destino+'&data='+data);
  });

  if($('textarea').length)
    $('textarea').not('.textarea-simples').ckeditor({
      customConfig: '/assets/js/ckeditor_config.js'
    });

  $('.listaImagens').sortable({
    placeholder: "imagem-placeholder"
  }).disableSelection();


  if($('#inputTipoDeReserva').val() != ''){

    if($('#inputTipoDeReserva').val() == 'por_periodo')

      $('.formgroupPorPeriodo').show('normal');

    else if($('#inputTipoDeReserva').val() == 'diaria')

      $('.formgroupPorDiaria').show('normal');

    else

      $('.formgroupPorPeriodo').hide('normal');

  }

  $('#inputTipoDeReserva').change( function(){

    if($(this).val() == 'por_periodo'){

      $('.formgroupPorPeriodo').show('normal');
      $('.formgroupPorDiaria').hide('normal');

    }else if($(this).val() == 'diaria'){

      $('.formgroupPorPeriodo').hide('normal');
      $('.formgroupPorDiaria').show('normal');

    }else{

      $('.formgroupPorPeriodo').hide('normal');
      $('.formgroupPorDiaria').hide('normal');

    }

  });

  $('#inputEspacoABloquear').change( function(){

    var tipo = $('option:selected', this).attr('data-tipo');

    $('#data_de').data("DateTimePicker").destroy();
    $('#data_ate').data("DateTimePicker").destroy();

    if(tipo == 'por_periodo'){

      $('#data_de').datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        stepping: 15,
        locale: 'pt'
      });

      $('#data_ate').datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        stepping: 15,
        locale: 'pt'
      });

      $('#hiddenTipo').val('por_periodo');

    }else if(tipo == 'diaria'){

      $('#data_de').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'pt'
      });

      $('#data_ate').datetimepicker({
        format: 'DD/MM/YYYY',
        locale: 'pt'
      });

      $('#hiddenTipo').val('diaria');

    }else{
      $('#hiddenTipo').val('');
    }

  });

  $('#inputValor').mask('000.000.000.000.000,00', {reverse: true});


  /*
  *   EXEMPLO DE BUSCA PARA O PAINEL
  */
  var timer;
  $('#filtra-unidades').on('keyup', function(){
    var termo = $(this).val();

    clearTimeout(timer);

    timer = setTimeout( function(){
      busca(termo);
    }, 300);
  });

  var original = "<div class='input-group' style='margin-bottom:3px;'><input type='text' class='form-control opcao-enquete' name='enquete_opcao[]' placeholder='Opção de Resposta'><span class='input-group-btn'><button class='btn btn-default btn-danger' type='button'><span class='glyphicon glyphicon-remove-circle'></span></button></span></div>";
  var placeholder = $('#listaOpcoes');

  $('.opcoes-control button').click( function(){
      placeholder.append(original);
  });

  $('body #listaOpcoes').on('click', '.input-group button.btn-danger', function(){
    var elPai = $(this).parent().parent();
    elPai.fadeOut('normal', function(){
      elPai.remove();
    });
  });

  /*
  *   EXEMPLO DE UPLOAD EM AJAX PARA O PAINEL
  */
  if($('.fileupload-EXEMPLO').length){

    $.each($('.fileupload'), function(){
      var fieldname = $(this).attr('data-fieldname');
      var limite = $(this).attr('data-limite');
      var scope  = $(this).parent().parent();
      var path   = $(this).attr('data-path');

      controlaUploadImagens(limite, scope);

      $(this).fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
          name  : 'path',
          value : path
        }],
        start: function(e, data){
          iconeUploadStart(scope);
        },
        done: function (e, data) {
          var imagem = "<div class='projetoImagem'>";
          imagem += "<img src='"+data.result.thumb+"'>";
          imagem += "<input type='hidden' name='"+fieldname+"[]' value='"+data.result.filename+"'>";
          imagem += "<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>";
          imagem += "</div>";
          if(!maximoImagensAtingido(limite, scope)){
            $('.listaImagens', scope).append(imagem);
            $('.listaImagens', scope).sortable("refresh");
            controlaUploadImagens(limite, scope);
            iconeUploadStop(scope);
          }
        }
      });
    });

    $(document).on('click', '.projetoImagem a.btn-remover', function(e){
      e.preventDefault();
      var parent = $(this).parent();
      var scope  = $(this).parent().parent().parent();
      var limite = $('.fileupload', scope).attr('data-limite');
      parent.css('opacity', .35);
      setTimeout( function(){
        parent.remove();
        controlaUploadImagens(limite, scope);
      }, 350);
    });

  }

  $('#seleciona-acao input[type=radio]').change( function(){
    selecionado = $(this).val();

    if(selecionado == 'is_nova_categoria'){

      $('#categoria_existente').removeClass('in');
      $('#nova_categoria').addClass('in');
      $('#nova_categoria').find('input').select();

    }else if(selecionado == 'relacionar_categoria'){

      $('#nova_categoria').removeClass('in');
      $('#categoria_existente').addClass('in');
      
    }

  });

});
