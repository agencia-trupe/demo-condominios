$('document').ready( function(){

	Moradores.init();

	$('.modulo-inativo').click( function(e){

		e.preventDefault();

		var template = "";
		template += "<div id='modulo-inativo-shadow'>";
			template += "<h2>Módulo não implementado</h2>";
			template += "<p class='p-extra-espaco'>";
				template += "Este módulo ainda não está disponível.";
			template += "</p>";
			template += "<button id='fechar-shadow'>OK</button>";
		template += "</div>";

		$.featherlight(template, {
			otherClose : '#fechar-shadow'
		});
	});

});
