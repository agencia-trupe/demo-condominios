Moradores.PessoasNaoAutorizadas = {

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('pessoas-nao-autorizadas-form-data'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#pessoas-nao-autorizadas-form-data').parent()
													.removeClass('input-erro')
													.removeClass('obrigatorio');
			}
		});
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#pessoas-nao-autorizadas-form-nome',
				'#pessoas-nao-autorizadas-form-visibilidade',
				'#pessoas-nao-autorizadas-form-data',
				'#pessoas-nao-autorizadas-form-parentesco'
			],
			form_com_imagem   : true,
			multiplas_imagens : false,
			diretorio_imagens : 'pessoas-nao-autorizadas'
		});

		this.form_datepicker();

	}
};