Moradores.AlterarSenha = {

	alterar_senha_form_submit : function() {
		$('#alterar-senha-main form').submit( function(e) {

			var inputs_obrigatorios = [
				'#register-form-senha-atual',
				'#register-form-senha-nova',
				'#register-form-confirmacao_senha'
			];

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent().addClass('input-erro')
								  .addClass('obrigatorio');

					check.focus().on('keyup', function(){
								  	check.parent().removeClass('input-erro')
								  				  .removeClass('obrigatorio');
								  });

					e.preventDefault();
					return false;
				}
			};

			var input_senha = $('#register-form-senha-nova');
    		var input_conf_senha = $('#register-form-confirmacao_senha');
    		if(input_senha.val() != input_conf_senha.val()){

    			input_senha.parent().addClass('input-erro')
							  		.addClass('confirmacao-senha-invalida');

				input_senha.focus().on('keyup', function(){
							  		input_senha.parent().removeClass('input-erro')
							  				   .removeClass('confirmacao-senha-invalida');
							  	});

				e.preventDefault();
				return false;
    		}

		});
	},

	init : function() {
		this.alterar_senha_form_submit();
	}
};