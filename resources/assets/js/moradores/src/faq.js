Moradores.FAQ = {

	toggle_resposta : function(){
		$('#lista-faq .questao').click( function(e){
			e.preventDefault();

			$(this).parent().toggleClass('ativo');
		});
	},

	init : function() {
		this.toggle_resposta();
	}
};