Moradores.InstrucoesDeUso = {

	toggle_resposta : function(){
		$('#lista-instrucoes-de-uso .questao').click( function(e){
			e.preventDefault();

			$(this).parent().toggleClass('ativo');
		});
	},

	init : function() {
		this.toggle_resposta();
	}
};