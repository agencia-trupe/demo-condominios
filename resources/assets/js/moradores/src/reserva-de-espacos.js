Moradores.ReservaDeEspacos = {

	is_periodo : false,
	diarias_bloqueadas : [],
	is_convidados : false,

	form_datepicker : function(){
		var picker = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('datepicker'),
			format: 'DD/MM/YYYY',
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			minDate: new Date(),
			bound: false,
			container: document.getElementById('calendario'),
			onSelect: function() {

				// Lógica para buscar as reservas e retornar o grid
				// Só é válido para reservas por períodos

				if(Moradores.ReservaDeEspacos.is_periodo){

					var tabela_grid = $('table.tabela-horarios');

					var url = 'moradores/reserva-de-espacos/grid-por-periodo/';
					url += this.getMoment().format('YYYY-MM-DD');
					url += '/';
					url += tabela_grid.attr('data-slug-espaco');

					$.ajax(url, {
          	beforeSend : function(){

          		tabela_grid.animate({
          			'opacity': 0
          		}, 400);

          	},
          	success : function(resultado){

          		setTimeout( function(){

            		tabela_grid.html(resultado);

            		tabela_grid.animate({
            			'opacity': 1
            		}, 400);

          		}, 400);
          	}
          });
				}
	    },
      onOpen: function(){
      	if(Moradores.ReservaDeEspacos.is_periodo){

        	$('#navegacao-por-dia').click( function(e){

						var moment = picker.getMoment();

						e.preventDefault();
						moment.add(2, 'day');
						picker.setDate(moment.format('YYYY-MM-DD'));
						
					});

      	}
      },
      disableDayFn: function(data){
      	var data_moment = moment(data);
      	return Moradores.ReservaDeEspacos.diarias_bloqueadas.indexOf(data_moment.format('YYYY-MM-DD')) !== -1;
      }
		});
	},

	marcar_dia : function(){
		$(document).on('click', 'tr td label', function(){
			var input = $(this).find("input[type=checkbox]");
			if(input.is(':checked')){
				$(this).addClass('selecionado');

				var data = $(this).attr('data-formatada');
				var ini = $(this).attr('data-horario-inicio');
				var fim = $(this).attr('data-horario-fim');

				Moradores.ReservaDeEspacos.abrir_modal_anotacoes_form(data,ini,fim);
			}else{
				$(this).removeClass('selecionado');
			}
		});
	},

	abrir_modal_anotacoes_form: function(data,ini,fim){

		var template = "";
		template += "<div id='modal-reservar-espacos'>";
			template += "<div class='informacoes-reserva'>";
				template += "Confirmar agendamento para:<br>";
				template += "<strong>"+data+"</strong><br>";
				template += "<em>início: <strong>"+ini+"</strong> | fim: <strong>"+fim+"</strong></em>";
			template += "</div>";
			template += "<p>ANOTAÇÕES SOBRE O AGENDAMENTO:</p>";
			template += "<textarea id='textarea-anotacoes'></textarea>";
			template += "<div class='botoes'>";
				template += "<button id='enviar-form-reserva-espacos' class='btn btn-info'>SIM</button>";
				template += "<button id='fechar-shadow-anotacoes' class='btn btn-info'>NÃO</button>";
			template += "</div>";
		template += "</div>";

		$.featherlight(template, {
			otherClose : '#fechar-shadow-anotacoes',
			afterContent : function(e){

				$('textarea')[0].focus();

				$('#enviar-form-reserva-espacos').click( function(){

					// Salvar conteudo do textarea na página do form antes de enviar
					var anotacoes = $('#textarea-anotacoes').val();
					$('#input-anotacoes').val(anotacoes);

					Moradores.ReservaDeEspacos.enviar_formulario();
				});
			},
			afterClose : function(e){
				var marcado = $('label.selecionado');
				var checkbox = marcado.find('input');

				marcado.removeClass('selecionado');
				checkbox.attr('checked', false);
			}
		});

	},

	enviar_formulario: function(){
		$('#grid-por-periodo > form').submit();
	},

	abrir_modal_anotacoes_visualizacao: function(){
		$(document).on('click', '.btn-visualizar-reserva', function(e){
			e.preventDefault();

			var data = $(this).attr('data-formatada');
			var h_inicio = $(this).attr('data-horario-inicio');
			var h_termino = $(this).attr('data-horario-fim');
			var anotacoes = $(this).attr('data-anotacoes');

			var template = "";
			template += "<div id='modal-reservar-espacos'>";
				template += "<div class='informacoes-reserva'>";
					template += "Agendamento confirmado para:<br>";
					template += "<strong>"+data+"</strong><br>";
					template += "<em>início: <strong>"+h_inicio+"</strong> | fim: <strong>"+h_termino+"</strong></em>";
				template += "</div>";
				template += "<p>ANOTAÇÕES SOBRE O AGENDAMENTO:</p>";
				template += "<div class='anotacoes-gravadas'>"+anotacoes+"</div>";
				template += "<div class='botoes'>";
					template += "<button id='fechar-shadow-anotacoes' class='btn btn-info'>FECHAR</button>";
				template += "</div>";
			template += "</div>";

			$.featherlight(template, {
				afterContent : function(e){
					$('#fechar-shadow-anotacoes').click( function(){
						$.featherlight.current().close();
					});
				}
			});
		});
	},

	toggle_textos_confirmacao: function(){
		var botao = $(".swap-texto .inicial input[type='button']");
		var txt_inicial = $(".swap-texto .inicial");
		var txt_final = $(".swap-texto .final");

		if(botao.length > 0){

			botao.click( function(e){
				e.preventDefault();
				txt_inicial.fadeOut('normal', function(){
					txt_final.fadeIn('normal');
				});
			});

		}
	},

	enviar_form_confirmacao_reserva: function(){
		$('#form-confirmar-reserva').submit( function(e){

			var aceite = $('#aceite_input');

			if(!aceite.is(':checked')){
				var mensagem = "<p class='retorno_formulario erro'>";
				mensagem += "É necessário aceitar os termos para confirmar a reserva.";
				mensagem += "</p>";

				$('#erros-formulario').html(mensagem).show('normal');
				e.preventDefault();
				return false;
			}

		});
	},

	cancelar_diaria: function(){
		$('.btn-cancelar-diaria').click( function(e){
			e.preventDefault();

			var regras_cancelamento;
			var rid = $(this).attr('data-rid');

			$.ajax('moradores/reserva-de-espacos/verificar-multa/'+rid,
			{
				success : function(regras_cancelamento){
					var template = "";
					template += "<div id='modal-cancelar-diaria'>";

					if(regras_cancelamento.tem_multa){

						template += "<p class='p-espacado'>";
							template += "O cancelamento desta reserva nesta data acarreta cobrança de multa por estar dentro do ";
							template += "período inferior a " + regras_cancelamento.dias_carencia + " dias de antecedência.";
						template += "</p>";

						template += "<p>";
							template += "Se confirmado o cancelamento a multa constará em seu boleto do condomínio do próximo mês."
						template += "</p>";

					}else{

						template += "<p>";
							template += "O cancelamento desta reserva nesta data <strong>não</strong> acarreta cobrança de multa.";
						template += "</p>";

					}

					template += "<hr>";
					template += "<h2>CONFIRMAR O CANCELAMENTO DA RESERVA?</h2>";

					template += "<div class='botoes'>";
						template += "<a href='moradores/reserva-de-espacos/reservar/" + regras_cancelamento.slug_espaco + "/remover/" + regras_cancelamento.timestamp_reserva + "' title='Confirmar Cancelamento' class='btn btn-info'>SIM</a>";
						template += "<a href='#' title='Desistir do Cancelamento' class='btn btn-info' id='fechar-cancelar-reserva'>NÃO</a>";
					template += "</div>";

					template += "</div>";

					$.featherlight(template, {
						variant : 'cancelamento_diaria',
						otherClose : '#fechar-cancelar-reserva'
					});

				}
			});


		});
	},

	verificar_envio_form_diaria: function(){
		$('#form-selecao-data-diaria').submit( function(e){

			$('#mensagens-erro').html('');

			if($('#datepicker').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe a Data da Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

			if($('#input-titulo-festa').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe um Título para a Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

			if($('#input-horario-festa').val() == ''){

				var aviso = $("<p class='retorno_formulario erro'>Informe o Horário aproximado da Festa</p>");

				$('#mensagens-erro').html($(aviso));

				setTimeout( function(){
					aviso.fadeOut('fast', function(){
						aviso.remove();
					});
				}, 3000);

				e.preventDefault();
				return false;
			}

		});
	},

	gerenciar_convidados : function(){
		this.focar_input_convidados();
		this.inserir_lista_colada();
		this.inserir_no_enter();
		this.remover_convidado();
	},

	focar_input_convidados : function(){
		document.getElementById('convidados-textarea').focus();
	},

	inserir_lista_colada: function(){
		$("#convidados-textarea").on('paste', function (e) {
			setTimeout( function(){
				Moradores.ReservaDeEspacos.inserir_convidado(false);
			}, 100);
	    });
	},

	inserir_no_enter: function(){
		$("#convidados-textarea").on('keyup', function (e) {
			if(e.which == 13){
				Moradores.ReservaDeEspacos.inserir_convidado(true);
			}
	    });
	},

	inserir_convidado: function(prepend){
		var lista = $("#convidados-textarea").val().split('\n');
		var nro_nomes = lista.length;

		for (var i = 0; i < nro_nomes; i++) {
			if(lista[i] != ''){

				var item = $("<li style='display: none;'>"+lista[i]+" <a href='#' class='btn btn-danger btn-mini'>x</a><input type='hidden' name='lista_convidados[]' value='"+lista[i]+"'></li>");

				if(prepend)
					$('#convidados-lista').prepend(item);
				else
					$('#convidados-lista').append(item);

				item.fadeIn('normal');
			}
		};

		$("#convidados-textarea").val('');
	},

	remover_convidado: function(){
		$(document).on('click', '#convidados-lista > li > a', function(e){

			e.preventDefault();

			var convidado = $(this).parent();
			convidado.fadeOut('normal', function(){
				convidado.remove();
			});
		});
	},

	init: function(){

		this.is_periodo = $('#grid-por-periodo').length > 0;
		this.diarias_bloqueadas = $('#diarias_bloqueadas_json').length > 0 ? $('#diarias_bloqueadas_json').val() : [];
		this.is_convidados = $("#convidados-textarea").length;

		this.form_datepicker();

		if(this.is_convidados)
			this.gerenciar_convidados();

		if(this.is_periodo){

			this.marcar_dia();
			this.abrir_modal_anotacoes_visualizacao();

		}else{

			this.toggle_textos_confirmacao();
			this.enviar_form_confirmacao_reserva();
			this.cancelar_diaria();

			this.verificar_envio_form_diaria();

		}


	}
};