Moradores.VeiculosDaUnidade = {

	form_mascara_placa : function() {
		$('#veiculos-da-unidade-form-placa').mask('SSS-0000', {
			placeholder : 'placa',
			clearIfNotMatch: true,
			onKeyPress: function(cep, event, currentField, options){
			  $(currentField).val($(currentField).val().toUpperCase())
			}
		});
	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#veiculos-da-unidade-form-veiculo',
				'#veiculos-da-unidade-form-visibilidade',
				'#veiculos-da-unidade-form-cor',
				'#veiculos-da-unidade-form-placa'
			],
			form_com_imagem : false
		});

		this.form_mascara_placa();

	}
};
