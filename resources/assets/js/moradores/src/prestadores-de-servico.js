Moradores.PrestadoresDeServico = {

	form_datepickers : function(){

		var datas = [
			'prestadores-de-servico-form-data_nascimento',
			'prestadores-de-servico-form-permissao_inicio',
			'prestadores-de-servico-form-permissao_termino'
		];

		var pickers = [];

		for (var i = datas.length - 1; i >= 0; i--) {

			id = datas[i];

			pickers[i] = new Pikaday({
				theme: 'gallery-theme',
				field: document.getElementById(id),
				format: 'DD/MM/YYYY',
				yearRange: [1900, 2050],
				i18n: {
				    previousMonth : 'Mês Anterior',
				    nextMonth     : 'Próximo Mês',
				    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
				    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
				},
				onSelect : function(){
					$(id).parent()
						 .removeClass('input-erro')
						 .removeClass('obrigatorio');
				}
			});
		};

	},

	init : function(){

		Moradores.Formularios.init({
			campos_obrigatorios : [
				'#prestadores-de-servico-form-nome',
				'#prestadores-de-servico-form-visibilidade',
				'#prestadores-de-servico-form-documento',
				// '#prestadores-de-servico-form-empresa',
				// '#prestadores-de-servico-form-data_nascimento',
				'#prestadores-de-servico-form-permissao_inicio',
				'#prestadores-de-servico-form-permissao_termino'
			],
			form_com_imagem   : false
		});

		this.form_datepickers();

	}
};