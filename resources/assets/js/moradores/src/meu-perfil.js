Moradores.MeuPerfil = {

	register_tooltip_foto : function() {
		var tooltip_foto = $('.addon-tooltip-foto');
		var conteudo = "";
		conteudo += "<strong>POR QUE ESSA INFORMAÇÃO É SOLICITADA?</strong>";
		conteudo += "<p>";
			conteudo += "A foto é importante para que novos funcionários possam reconhecer você no acesso ao prédio. ";
			conteudo += "Atente-se para usar uma imagem que se pareça com ";
			conteudo += "você no dia a dia e facilitar o reconhecimento. Usar ";
			conteudo += "uma foto com muita maquiagem quando ";
			conteudo += "geralmente você não se maqueia, ou com chapéu ";
			conteudo += "ou boné podem dificultar a identificação.";
		conteudo += "</p>";
		$.each(tooltip_foto, function(){
			var title_original = $(this).attr('title');
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $(conteudo),
				maxWidth: 240,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	register_tooltip_visibilidade : function() {
		var tooltip_visib = $('.addon-tooltip-visib');
		$.each(tooltip_visib, function(){
			var title_original = $(this).attr('title');
			$(this).find('img').attr('title', '');
			$(this).find('img').attr('data-old-title', title_original);
			$(this).tooltipster({
				content: $("<strong>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA.</strong>"),
				maxWidth: 180,
				theme: 'tema-tooltipster-custom'
        	});
		});
	},

	register_shadow_email : function() {
		$('.register-form-set_visibilidade.visib-email').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Endereço de E-mail', $('#register-form-visibilidade_email'));
		});
	},

	register_shadow_telfixo : function() {
		$('.register-form-set_visibilidade.visib-tel_fixo').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Fixo', $('#register-form-visibilidade_tel-fixo'));
		});
	},

	register_shadow_telcel : function() {
		$('.register-form-set_visibilidade.visib-tel_cel').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Celular', $('#register-form-visibilidade_tel-cel'));
		});
	},

	register_shadow_telcom : function() {
		$('.register-form-set_visibilidade.visib-tel_com').click( function(e){
			e.preventDefault();
			Moradores.Auth.register_abrir_shadow_visibilidade('Telefone Comercial', $('#register-form-visibilidade_tel-com'));
		});
	},

	register_abrir_shadow_visibilidade : function(titulo, input) {

		var template = "";
		template += "<div id='register-visibilidade-shadow'>";
			template += "<h2>"+titulo+"</h2>";
			template += "<p>SELECIONAR PARA QUEM ESSA INFORMAÇÃO SERÁ EXIBIDA DENTRO DO SISTEMA:</p>";
			template += "<ul>";
				template += "<li><label><input type='checkbox' value='1' class='check-marcar-todos'> Todos (moradores, corpo diretivo, zelador)</label></li>";
				template += "<li><label class='marcado'><input type='checkbox' value='2' checked id='check-marcar-admin'> Somente síndico e conselho</label></li>";
				// template += "<li><label><input type='checkbox' value='3'> Portaria</label></li>";
				template += "<li><label><input type='checkbox' value='4'> Zelador</label></li>";
				template += "<li><label><input type='checkbox' value='5' id='check-marcar-moradores'> Todos os moradores</label></li>";
				template += "<li><label><input type='checkbox' value='6' id='check-marcar-amigos'> Somente moradores selecionados como amigos</label></li>";
			template += "</ul>";
			template += "<button id='fechar-visibilidade-shadow'>CONFIRMAR</button>";
		template += "</div>";

		$.featherlight(template, {
			afterContent : function(e){

				var inputs = $(".featherlight-inner input[type='checkbox']");
				var valor_atual = input.val();

				if(valor_atual.indexOf(',') !== -1){

					var valores_atuais = valor_atual.split(',')

					$.each(inputs, function(){
						if($.inArray($(this).val(), valores_atuais) !== -1){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});

				}else if(valor_atual != ''){
					$.each(inputs, function(){
						if($(this).val() == valor_atual){
							$(this).attr('checked', 'checked');
							$(this).parent().addClass('marcado');
						}
					});
				}

				$("#register-visibilidade-shadow .check-marcar-todos").click( function(){
					// Ao clicar em 'marcar todos', marcar ou desmarcar todos os checkboxes
					// EXCETO o de só amigos e o da administração (esse sempre ativo)
					$("#register-visibilidade-shadow input[type=checkbox]").not('#check-marcar-amigos')
																		   .not('#check-marcar-admin')
																		   .prop('checked', this.checked);

					// Sempre DESmarcar o de 'só amigos'
					$('#check-marcar-amigos').prop('checked', false);
					$('#check-marcar-amigos').parent().removeClass('marcado');

					if(this.checked){
						$("#register-visibilidade-shadow label input").not('#check-marcar-amigos')
																	  .not('#check-marcar-admin')
																	  .parent()
																	  .addClass('marcado');
					}else{
						$("#register-visibilidade-shadow label input").not('#check-marcar-amigos')
																	  .not('#check-marcar-admin')
																	  .parent()
																	  .removeClass('marcado');
					}
				});

				$("#register-visibilidade-shadow input[type=checkbox]").not(".check-marcar-todos").click( function(e){

					if($(this).val() == '2'){ // Sempre visível pra administração
						e.preventDefault();
						return false;
					}

					if(!this.checked){
						$("#register-visibilidade-shadow .check-marcar-todos").prop('checked', false);
						$("#register-visibilidade-shadow .check-marcar-todos").parent().removeClass('marcado');
						$(this).parent().removeClass('marcado');
					}else{
						$(this).parent().addClass('marcado');

						if($(this).val() == '5'){
							// Se value = 5 (todos os moradores)
							// Desmarca o 6 (somente amigos)
							$('#check-marcar-amigos').prop('checked', false).parent().removeClass('marcado');
						}else if($(this).val() == '6'){
							// Se value = 6 (somente amigos)
							// Desmarca o 5 (todos os moradores)
							$('#check-marcar-moradores').prop('checked', false).parent().removeClass('marcado');
						}
					}
				});

				$('#fechar-visibilidade-shadow').click( function(){
					Moradores.Auth.register_salvar_visibilidade(input);
					$.featherlight.current().close();
				});
			}
		});
	},

	register_salvar_visibilidade : function(input) {
		var marcados = $(".featherlight-inner input[type='checkbox']:checked");
		var icone = input.parent().find('.register-form-set_visibilidade');
		var valores = Array();

		if(marcados.length > 0){
			$.each(marcados, function(){
				valores.push($(this).val());
			});
			valores = valores.join(',');
		}else{
			valores = '';
		}

		input.val(valores);

		input.parent().removeClass('input-erro')
					  .removeClass('obrigatorio');

		if(input.val() == '')
			icone.removeClass('on').addClass('off');
		else
			icone.removeClass('off').addClass('on');
	},

	register_marcar_icone_visibilidade : function() {
		var inputs_visibilidade = [
			'#register-form-visibilidade_email',
			'#register-form-visibilidade_tel-fixo',
			'#register-form-visibilidade_tel-cel',
			'#register-form-visibilidade_tel-com'
		];
		for (var i = 0; i < inputs_visibilidade.length; i++) {
			input = $(inputs_visibilidade[i]);
			if(input.val() != ''){
				input.parent().find('.addon-tooltip-visib')
							  .removeClass('off')
							  .addClass('on');
			}
		};
	},

	register_remover_erro_keyup : function() {
		$('.input-erro input').keyup( function(){
			$(this).parent().removeClass('input-erro')
							.removeClass('obrigatorio')
							.removeClass('email-invalido')
							.removeClass('confirmacao-senha-invalida')
							.removeClass('email-em-uso');
		});
	},

	register_carregar_foto_onload : function() {
		var file = $('#register-form-foto').val();
		if($('#register-form-foto').length && file != ''){
			var foto = new Image;
    		var target = $('.register-foto-placeholder label');
    		foto.onload = function(){
    			target.append(foto);
    		};
    		foto.src = 'assets/images/moradores/fotos/thumbs/'+file;
		}
	},

	register_foto_upload : function() {
		$('#register-form-fileupload').fileupload({
			url: 'moradores/meu-perfil/upload-foto',
			type: 'post',
	        dataType: 'json',
	        send: function(){
	        	Moradores.Auth.register_foto_upload_esconder_erro();
	        	$('.register-foto-placeholder label').addClass('enviando');
	        },
	        progress: function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('#upload-progresso .barra').css(
		            'width',
		            progress + '%'
		        );
		    },
	        done: function (e, data) {
	        	$('.register-foto-placeholder label').removeClass('enviando');
	        	if(data.result.success == 1){
	        		var foto = new Image;
	        		var target = $('.register-foto-placeholder label');
	        		$('#register-form-foto').val(data.result.filename);
	        		foto.onload = function(){
	        			target.append(foto);
	        		};
	        		foto.src = data.result.thumb;
	        	}else{
	        		Moradores.Auth.register_foto_upload_mostar_erro(data.result.msg);
	        		setTimeout( function(){
	        			Moradores.Auth.register_foto_upload_esconder_erro();
	        		}, 2000);
	        	}
	        }
	    });
	},

	register_foto_upload_mostar_erro: function(msg) {
		$('.register-foto-placeholder label').addClass('input-erro');
	    $('.register-foto-placeholder label p').html(msg);
	    $('#register-form-foto').val('');
	    $('.register-foto-placeholder label img').fadeOut('normal', function(){
	    	$(this).remove();
	    })
	},

	register_foto_upload_esconder_erro: function() {
		$('.register-foto-placeholder label').removeClass('input-erro');
	    $('.register-foto-placeholder label p').html($('.register-foto-placeholder label p').attr('data-html-original'));
	},

	register_form_submit : function() {
		$('#register-form form').submit( function(e) {

			var inputs_obrigatorios = [
				'#register-form-numero_unidade',
				'#register-form-bloco',
				'#register-form-nome_proprietario',
				'#register-form-nome',
				'#register-form-apelido',
				'#register-form-relacao_unidade',
				'#register-form-data_mudanca',
				'#register-form-email',
				'#register-form-visibilidade_email',
				'#register-form-telefone_fixo',
				'#register-form-visibilidade_tel-fixo',
				'#register-form-telefone_celular',
				'#register-form-visibilidade_tel-cel',
				'#register-form-telefone_comercial',
				'#register-form-visibilidade_tel-com',
				'#register-form-foto'
			];

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent().addClass('input-erro')
								  .addClass('obrigatorio');

					check.focus().on('keyup', function(){
								  	check.parent().removeClass('input-erro')
								  				  .removeClass('obrigatorio');
								  });

					e.preventDefault();
					return false;
				}
			};

			var campo = $('#register-form-email');
			var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			var email = campo.val();
    		if(!regex.test(email)){

    			campo.parent().addClass('input-erro')
							  .addClass('email-invalido');

				campo.focus().on('keyup', function(){
							  	campo.parent().removeClass('input-erro')
							  				  .removeClass('email-invalido');
							  });

				e.preventDefault();
				return false;
    		}

		});
	},

	register_form_pikaday : function() {

		var picker1 = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('register-form-data_mudanca'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#register-form-data_mudanca').parent()
												.removeClass('input-erro')
												.removeClass('obrigatorio');
			}
		});

		var picker2 = new Pikaday({
			theme: 'gallery-theme',
			field: document.getElementById('register-form-data_nascimento'),
			format: 'DD/MM/YYYY',
			yearRange: [1900, 2050],
			i18n: {
			    previousMonth : 'Mês Anterior',
			    nextMonth     : 'Próximo Mês',
			    months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			    weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			    weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb']
			},
			onSelect : function(){
				$('#register-form-data_nascimento').parent()
												.removeClass('input-erro')
												.removeClass('obrigatorio');
			}
		});

	},

	mascara_telefones : function(){
		var telefones = '#register-form-telefone_fixo, #register-form-telefone_celular, #register-form-telefone_comercial';
		$(telefones).mask('(00) 00000-0000');
	},

	init : function() {
		this.register_tooltip_foto();
		this.register_tooltip_visibilidade();
		this.register_shadow_email();
		this.register_shadow_telfixo();
		this.register_shadow_telcel();
		this.register_shadow_telcom();
		this.register_foto_upload();
		this.register_form_submit();
		this.register_marcar_icone_visibilidade();
		this.register_remover_erro_keyup();
		this.register_carregar_foto_onload();
		this.register_form_pikaday();
		this.mascara_telefones();
	}
};
