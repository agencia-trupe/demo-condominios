Moradores.Ocorrencias = {

	registrar_form_submit : function(){
		$('#ocorrencias-registrar-form').submit( function(e){

			var campo_texto = $(this).find('textarea'); //$('#ocorrencias-registrar-form label textarea');

			if(campo_texto.val() == ''){

				campo_texto.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('textarea')
						   .focus();

				e.preventDefault();
				return false;
			}

			if(campo_texto.val().length < 10){

				campo_texto.parent()
						   .addClass('erro')
						   .addClass('minimo')
						   .find('textarea')
						   .focus();

				e.preventDefault();
				return false;
			}
		});
	},

	registrar_form_keyup : function() {
		$('#ocorrencias-registrar-form textarea').on('keyup', function(){
			$(this).parent().removeClass('erro')
							.removeClass('obrigatorio');
		});
	},

	expandir_texto : function() {
		$.each($('.lista-ocorrencias .ocorrencia-texto'), function(){

			var aberto = $(this).find('.erro-resposta-ocorrencia').length > 0 ? true : false;

			if(aberto)
				$(this).addClass('expandido');

			$(this).readmore({
				collapsedHeight: 70,
				speed: 300,
				startOpen : aberto,
				moreLink: "<a href='#' class='ocorrencia-toggle-texto abrir'>ABRIR PARA LER MAIS</a>",
				lessLink: "<a href='#' class='ocorrencia-toggle-texto fechar'>RECOLHER</a>",
				beforeToggle: function(trigger, element, expanded) {
				    if(!expanded) { // Texto expandido
				    	$(element).addClass('expandido');
				    }else{
				    	$(element).removeClass('expandido');
				    }
				}
			});
		});
	},

	finalizar_ocorrencia : function() {
		$('.finalizar-ocorrencia input[type=checkbox]').change( function(){

			var ocorrencia_id = $(this).val();
			var container_form = $(this).parent().parent();

			$(this).attr('disabled', true);

			$.post('moradores/livro-de-ocorrencias/finalizar', {
				ocorrencia_id : ocorrencia_id
			}, function(resposta){

				var resposta = "<span class='ocorrencia-finalizada apagado'>Assunto finalizado em: "+resposta.finalizada_em+" pelo "+resposta.finalizada_por+"</span>";

				// desativar form
				// esconder textarea
				// esconder submit
				// trocar conteúdo de .finalizar-ocorrencia
				// Reaplicar .readmore()

				container_form.attr('action', '');
				container_form.find('textarea').fadeOut('normal');
				container_form.find('input[type=submit]').fadeOut('normal', function(){
					container_form.find('.finalizar-ocorrencia').html(resposta);
					container_form.find('.apagado').removeClass('apagado');
					container_form.parent().parent().readmore({
						collapsedHeight: 70,
						speed: 300,
						startOpen : true,
						moreLink: "<a href='#' class='ocorrencia-toggle-texto abrir'>ABRIR PARA LER MAIS</a>",
						lessLink: "<a href='#' class='ocorrencia-toggle-texto fechar'>RECOLHER</a>",
						beforeToggle: function(trigger, element, expanded) {
						    if(!expanded) { // Texto expandido
						    	$(element).addClass('expandido');
						    }else{
						    	$(element).removeClass('expandido');
						    }
						}
					});
				});

			});
		});
	},

	init : function(){
		this.registrar_form_submit();
		this.registrar_form_keyup();
		this.expandir_texto();
		this.finalizar_ocorrencia();
	}
};