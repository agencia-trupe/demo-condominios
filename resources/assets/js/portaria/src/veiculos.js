Portaria.Veiculos = {

  listaUnidades : [],

  timer : null,

  iniciar_unidades : function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

  verificar_unidade : function(unidade)
	{
		var resumo_unidade = unidade.toUpperCase();
    var filtrado = this.listaUnidades.filter(
    	function(data){return data == resumo_unidade}
  	);

    return filtrado.length > 0;
	},

  mascara_placa_veiculos_busca : function(){
    $('#consultar-placa-veiculo').mask('SSS-0000', {
			placeholder : 'placa',
			clearIfNotMatch: true,
			onKeyPress: function(cep, event, currentField, options){
        $(currentField).val($(currentField).val().toUpperCase())
        Portaria.Veiculos.apagar_resultado_busca();
			},
      onComplete: function(cep, event, currentField, options) {
        var input = $(currentField);
        var parent = input.parent();
  			var termo = input.val();
        var loader = $('#loader');

        clearTimeout(Portaria.Veiculos.timer);

  			if(termo.length > 0){

          loader.fadeIn('fast', function(){
            Portaria.Veiculos.timer = setTimeout( function(){
              Portaria.Veiculos.buscar_placa(termo);
            }, 400);
          });

  			}else{

  				Portaria.Veiculos.apagar_resultado_busca();

  			}

      }
		});
  },

  mascara_placa_veiculos_form : function(){
    $('#veiculos-da-unidade-form-placa').mask('SSS-0000', {
			placeholder : 'placa',
			clearIfNotMatch: true,
			onKeyPress: function(cep, event, currentField, options){
			  $(currentField).val($(currentField).val().toUpperCase())
			}
		});
  },

  remover_erros : function()
  {
    $.each($('.input-erro input'), function(){

      var evento = 'click';

      $(this).on(evento, function(){
        $(this).parent()
               .removeClass('input-erro')
               .removeClass('obrigatorio');
      });

    });
  },

  mostra_retorno_em_shadow : function()
  {
    var retorno = $('#retorno_formulario_shadow');

    if(retorno.length){
      $.featherlight(retorno, {
        closeOnClick: 'anywhere',
        onKeyUp: function(){
          $.featherlight.current().close();
        }
      });
    }
  },

  validacao_on_submit : function(inputs_obrigatorios)
  {
		$('#form-cadastrar-veiculo-temporario').submit( function(e) {

			for (var i = 0; i < inputs_obrigatorios.length; i++) {

				var check = $(inputs_obrigatorios[i]);

				if(!check.val()){

					check.parent()
               .addClass('input-erro')
							 .addClass('obrigatorio');

					evento = check.prop('nodeName').toLowerCase() == 'select' ? 'click' : 'keyup';

					check.focus().on(evento, function(){
					  	check.parent()
                   .removeClass('input-erro')
					  			 .removeClass('obrigatorio');
					});

					e.preventDefault();
					return false;
				}
			};

      var check_unidade = $('#veiculos-da-unidade-form-unidade');

      if(!Portaria.Veiculos.verificar_unidade(check_unidade.val())) {
        check_unidade.parent()
                     .addClass('input-erro')
                     .addClass('unidade-invalida');

        check_unidade.focus().on('keyup', function(){
            check_unidade.parent()
                         .removeClass('input-erro')
                         .removeClass('unidade-invalida');
        });

        e.preventDefault();
        return false;
      }

		});
	},

  buscar_placa : function(termo)
  {
    $.post('portaria/veiculos/consulta', {
      'placa' : termo
    }, function(retorno){

      Portaria.Veiculos.mostrar_resultados(termo, retorno);

    });
  },

  mostrar_resultados : function(termo, retorno)
  {
    $('#resultados-placa-placeholder').html(termo.toUpperCase());

    if(retorno.length == 0)
      Portaria.Veiculos.mostrar_msg_sem_resultados();
    else
      Portaria.Veiculos.mostrar_tabela_veiculos(retorno);

    $('#loader').fadeOut('fast', function(){
      $('#resultados-veiculos').fadeIn('normal');
    });

  },

  mostrar_msg_sem_resultados : function()
  {
    $('#resultados-veiculos .contem-tabela').html('<h4>Nenhum resultado encontrado</h4>');
  },

  mostrar_tabela_veiculos : function(retorno)
  {
    var tabela = "<table>";

    tabela += "<thead>";
      tabela += "<tr>";
        tabela += "<th>";
        tabela += "Unidade";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Veículo";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Cor";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Placa";
        tabela += "</th>";
        tabela += "<th>";
        tabela += "Vaga";
        tabela += "</th>";
      tabela += "</tr>";
    tabela += "</thead>";

    tabela += "<tbody>";
    for (var i = 0; i < retorno.length; i++) {
      tabela += "<tr>";
        tabela += "<td>";
          tabela += retorno[i].unidade_resumo;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].veiculo;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].cor;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].placa;
        tabela += "</td>";
        tabela += "<td>";
          tabela += retorno[i].vaga;
        tabela += "</td>";
      tabela += "</tr>";
    }
    tabela += "</tbody>";

    tabela += "</table>";

    $('#resultados-veiculos .contem-tabela').html(tabela);
  },

  apagar_resultado_busca : function()
  {
    $('#resultados-veiculos').fadeOut('normal');
  },

  init: function(){
    var campos_obrigatorios = [
      '#veiculos-da-unidade-form-unidade',
      '#veiculos-da-unidade-form-veiculo',
      '#veiculos-da-unidade-form-cor',
      '#veiculos-da-unidade-form-placa',
      //'#veiculos-da-unidade-form-vaga'
		];

    this.iniciar_unidades();
    this.validacao_on_submit(campos_obrigatorios);
    this.remover_erros();
    this.mascara_placa_veiculos_busca();
    this.mascara_placa_veiculos_form();
    this.mostra_retorno_em_shadow();
  }
};
