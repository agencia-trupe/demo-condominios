Portaria.Dashboard = {

	abre_lista_convidados: function(){

		var botoes = $('.btn-convidados');

		$.each(botoes, function(){
			$(this).featherlight($(this).attr('data-featherlight-abrir'), {
				persist: true,
				afterOpen : function(){
					this.$content.addClass('featherlight-visible');
					Portaria.Dashboard.iniciarBusca();
					Portaria.Dashboard.toggle_presenca_convidado();
					Portaria.Dashboard.toggle_visibilidade_presentes();
				},
				afterClose: function(){
					Portaria.Dashboard.removerBinds();
					this.$content.removeClass('featherlight-visible');
				}
			});
		});

	},

	iniciarBusca: function(){

		var busca = $('.featherlight .featherlight-visible').find('input');

		busca.val('');
		Portaria.Dashboard.filtrar_lista_convidados('');

		busca.on('keyup', function(){
			Portaria.Dashboard.filtrar_lista_convidados($(this).val());
		});

		busca.focus();
	},

	filtrar_lista_convidados: function(termo){

		var resultados = 0;

		$(".featherlight .featherlight-visible .lista-convidados").find("li").each( function(index) {

			if($(this).find('a').length > 0){

				var nome = $(this).find('a').text().toLowerCase();

    		var check = nome.indexOf( termo.toLowerCase() ) !== -1;

      	$(this).toggle(check);

      	if(check){
      		resultados++;
      	}
			}

    });

    if(resultados == 0){
    	$(".featherlight .featherlight-visible .lista-convidados li:last-child").show();
    }else{
    	$(".featherlight .featherlight-visible .lista-convidados li:last-child").hide();
    }
	},

	toggle_presenca_convidado: function(){
		$('.lista-convidados .btn-presenca').on('click.toggle_presenca_convidado', function(e){
			e.preventDefault();

			var btn = $(this);
			var span = btn.find('span');

			var convidado = btn.attr('data-convidado');
			var lista = btn.attr('data-lista');

			if(btn.hasClass('oculto')){

				span.html('marcar como presente');
				btn.removeClass('presente');

				setTimeout( function(){
					$.post('portaria/toggle-presenca-convidado', {
						lista: lista,
						convidado: convidado,
						presenca: 0
					}, function(){
						btn.removeClass('oculto');
					});
				}, 300);

			}else{

				span.html('presente');
				btn.addClass('presente');

				setTimeout( function(){
					$.post('portaria/toggle-presenca-convidado', {
						lista: lista,
						convidado: convidado,
						presenca: 1
					}, function(){
						btn.addClass('oculto');
					});
				}, 300);

			}
		});
	},

	toggle_visibilidade_presentes: function(){
		$('.lista-convidados-popup .toggle-visibilidade-lista-convidados').on('click.toggle_presenca_convidado', function(e){
			e.preventDefault();

			var lista = $(this).parent().parent().find('ul.lista-convidados');
			var span = $(this).find('span');

			if(lista.hasClass('mostrar-ocultos')){

				span.html('mostrar');
				lista.removeClass('mostrar-ocultos');

			}else{

				span.html('ocultar');
				lista.addClass('mostrar-ocultos');

			}
		});
	},

	removerBinds: function(){
		$('.featherlight .featherlight-visible input').off('keyup');
		$('.lista-convidados .btn-presenca').off('click.toggle_presenca_convidado');
		$('.lista-convidados-popup .toggle-visibilidade-lista-convidados').off('click.toggle_presenca_convidado');
	},

	init: function(){

		this.abre_lista_convidados();

	}
};
