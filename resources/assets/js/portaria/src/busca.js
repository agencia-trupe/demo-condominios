Portaria.Busca = {

	timer : null,

	busca_on_keyup : function(){
		$('#busca-principal').on('keyup', function(e){

			var termo = $(this).val();

			clearTimeout(Portaria.Busca.timer);

			if(e.which != 13){

				if(termo.length > 0){

					if($('.main-conteudo').is(":visible"))
						Portaria.Busca.esconder_conteudo_anterior();

					Portaria.Busca.timer = setTimeout( function(){
						Portaria.Busca.esconder_aviso();
						Portaria.Busca.busca_ajax(termo);
					}, 400);

				}else{

					Portaria.Busca.mostrar_conteudo_anterior();

				}
			}

		});
	},

	bloquear_envio_form : function(){
		$('#form-busca').submit( function(e){

			e.preventDefault();
			e.stopPropagation();

			var resultados = $('#resultado-busca .resultado');
			var nro_resultados = resultados.length;
			var avisos_div = $('#avisos-busca');

			if(nro_resultados > 1){

				// Selecione um resultado
				avisos_div.html("<p class='retorno_busca erro'>Selecione um resultado abaixo</p>");

			}else if(nro_resultados == 1){

				// Entrar no 1o resultado
				window.location.href = resultados.find(':first').attr('href');

			}else{
				// nenhum resultado
			}

		});
	},

	busca_ajax: function(termo){

		$.post('portaria/busca', {
			termo : termo
		}, function(resposta){
			Portaria.Busca.trata_retorno_ajax(resposta);
		});

	},

	trata_retorno_ajax : function(resposta){

		if($.isEmptyObject(resposta))
			this.nenhumResultado();
		else
			this.mostrarResultados(resposta);

	},

	esconder_conteudo_anterior : function(){
		$('.main-conteudo').fadeOut('fast', function(){
			$('#resultado-busca').fadeIn('fast');
		});
	},

	mostrar_conteudo_anterior : function(){
		$('#resultado-busca').fadeOut('fast', function(){
			$('.main-conteudo').fadeIn('fast');
		});
	},

	mostrarResultados : function(resposta){

		var template   = '';
		var img_padrao = 'moradores/assets/images/avatar-default/70/70/sem foto';
		var nro_resultados = resposta.length;

		for (var unidade in resposta) {

			lista_moradores = resposta[unidade];
			numero_moradores = lista_moradores.length;

			template += "<div class='resultado'>";
				template += "<a href='portaria/unidade/"+unidade+"' title='"+unidade+"'>";

					template += "<div class='abreviacao-unidade'>";
						template += unidade;
					template += "</div>";

					template += "<div class='lista-moradores'>";

					if(numero_moradores > 0){
						for(var c = 0; c < numero_moradores; c++){

							template += "<div class='morador'>";

								template += !lista_moradores[c].foto ? "<img src='"+img_padrao+"'>" : "<img src='"+lista_moradores[c].foto_fullpath+"'>";

								template += "<div class='nome-morador'>";
									template += lista_moradores[c].nome;
									template += !lista_moradores[c].idade ? "<span>idade não informada</span>" : "<span>"+lista_moradores[c].idade+" anos</span>";
									template += " <span>e-mail: " + lista_moradores[c].email+"</span>";
								template += "</div>";

								template += "<div class='dados-morador'>";
									template += lista_moradores[c].telefone_fixo == '' ? "Tel. Fixo: --<br>" : "Tel. Fixo: "+lista_moradores[c].telefone_fixo + '<br>';
									template += lista_moradores[c].telefone_celular == '' ? "Tel. Celular: --<br>" : "Tel. Celular: "+lista_moradores[c].telefone_celular + '<br>';
									template += lista_moradores[c].telefone_comercial == '' ? "Tel. Comercial: --" : "Tel. Comercial: "+lista_moradores[c].telefone_comercial;
								template += "</div>";

							template += "</div>";

						}
					}else{
						template += "<div class='morador'>";

							template += "<img src='"+img_padrao+"'>";

							template += "<div class='nome-morador'>";
								template += 'nenhum morador';
							template += "</div>";

						template += "</div>";
					}

					template += "</div>";

				template += "</a>";
			template += "</div>";
		}

		$('#resultado-busca').html(template);
	},

	nenhumResultado : function(){
		$('#resultado-busca').html("<h2>Nenhum Resultado</h2>");
	},

	esconder_aviso : function(){
		$('#avisos-busca p').fadeOut('fast', function(){
			$('#avisos-busca').html('');
		})
	},

	init : function(){
		this.busca_on_keyup();
		this.bloquear_envio_form();
	}
};
