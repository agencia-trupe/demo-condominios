var Portaria = {
	init : function(){

		var modulo = location.pathname.split('/')[2];

		if(modulo == 'auth')
			this.Auth.init();

		if(modulo == 'correspondencias')
			this.Correspondencias.init();

		if(modulo == 'encomendas')
			this.Encomendas.init();

		if(modulo == 'unidade')
			this.Unidades.init();

		if(modulo == 'comunicacao')
			this.Comunicacao.init();

		if(modulo == 'lavanderia')
			this.Lavanderia.init();

		if(modulo == 'veiculos')
			this.Veiculos.init();

		this.Dashboard.init();
		this.Busca.init();

		if($('.retorno_formulario').length){
			setTimeout( function(){
				$('.retorno_formulario').addClass('removendo').fadeOut('normal');
			}, 3000);
		}

		$.ajaxSetup({
	    headers: {
	      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
		})
	}
};
