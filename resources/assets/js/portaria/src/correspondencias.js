Portaria.Correspondencias = {

	listaUnidades : [],

	iniciarUnidades: function()
	{
		this.listaUnidades = JSON.parse($('#listaUnidadesJson').val());
	},

	verificarUnidade: function()
	{
		$('#input-busca-unidade').on('keyup', function(e){

			var resumo = $(this).val().toUpperCase();

			$(this).parent().removeClass('bloqueado');

			if(e.which != 13){

				if(Portaria.Correspondencias.buscarUnidade(resumo).length > 0){
					Portaria.Correspondencias.liberarForm();
				}else{
					Portaria.Correspondencias.bloquearForm();
				}

			}

		});

	},

	removerClassesNoBlur: function()
	{
		$('#input-busca-unidade').on('blur', function(){
			$('#input-busca-unidade').parent().removeClass('bloqueado').removeClass('liberado');
		});
	},

	liberarForm: function()
	{
		$('#input-busca-unidade').parent().removeClass('bloqueado').addClass('liberado');
	},

	bloquearForm: function()
	{
		$('#input-busca-unidade').parent().removeClass('liberado').addClass('bloqueado');
	},

	enviarForm: function()
	{
		$('#form-inserir-correspondencia').submit('keyup', function(e){

			e.preventDefault();

			if($('#input-busca-unidade').parent().hasClass('liberado')){

				$.post('portaria/correspondencias', {
					'termo_busca_unidade' : $('#input-busca-unidade').val()
				}, function(resposta){

					$('#input-busca-unidade').val('');
					$('#input-busca-unidade').parent().removeClass('liberado')

					var lista = $('#lista-avisos ul');
					var item_str = "";

					item_str += "<li style='display:none;'>";
						item_str += "<div class='unidade'>" + resposta.resumo + "</div>";
						item_str += "<div class='timestamp'>" + resposta.data + "</div>";
						item_str += "<div class='marcacao'>";
							item_str += "<a href='#' title='Marcar como entregue' data-id-correspondencia='" + resposta.id + "'>entregue</a>";
						item_str += "</div>";
						item_str += "<div class='remover'>";
							item_str += "<a href='#' title='Remover aviso de Correspondência' class='btn btn-danger btn-circular' data-id-correspondencia='" + resposta.id + "'>X</a>";
						item_str += "</div>";
					item_str += "</li>";

					if(lista.find('li').length > 0)
					{
						lista.prepend(item_str);
					}
					else
					{
						Portaria.Encomendas.mostrarBusca();
						lista.html(item_str);
					}

					$('#lista-avisos ul li:first').fadeIn('slow');
				});
			}

		});
	},

	buscarUnidade: function(resumo)
	{
		return this.listaUnidades.filter(
	    	function(data){return data == resumo}
	  	);
	},

	marcarEntregue: function()
	{
		$(document).on('click', '.marcacao a', function(e){
			e.preventDefault();
			if(!$(this).hasClass('marcado')){

				var botao = $(this);
				var id = botao.attr('data-id-correspondencia');
				var linha_inteira = botao.parent().parent();
				var texto_sem_check = botao.html();

				botao.addClass('marcado').html(texto_sem_check + ' ✓');

				setTimeout( function(){
					linha_inteira.fadeOut('normal', function(){

						linha_inteira.remove();

						Portaria.Encomendas.esconderBuscaSeVazio();

						// MARCAR aviso de correspondencia COMO ENTREGUE
						// Remover notificações para todos os usuários
						$.ajax('portaria/correspondencias/marcarEntregue/'+id);

					});
				}, 1000);
			}
		});
	},

	removerCorrespondencia: function(){
		$(document).on('click', '.remover a', function(e){
			e.preventDefault();

			var botao = $(this);

			var id = botao.attr('data-id-correspondencia');
			var linha_inteira = botao.parent().parent();

			linha_inteira.fadeOut('normal', function(){
				linha_inteira.remove();

				// REMOVER aviso de correspondencia
				// Remover notificações para todos os usuários
				$.ajax('portaria/correspondencias/remover/'+id,{
					success : function(){
						Portaria.Encomendas.esconderBuscaSeVazio();
					}
				});

			});
		});
	},

	filtraTabela: function(){
		$('#input_filtra_tabela').on('keyup', function(){

	        var termo = $(this).val();

	        $("#lista-avisos ul").find("li").each( function(index) {
        		var campo_unidade = $(this).find(".unidade").first().text().toLowerCase();
	        	$(this).toggle(campo_unidade.indexOf( termo.toLowerCase() ) !== -1);
	    	});
    	});
	},

	mostrarBusca : function(){
		$('#input_filtra_tabela').is(':hidden')
			$('#input_filtra_tabela').fadeIn('normal');
	},

	esconderBuscaSeVazio : function(){
		if($('#lista-avisos ul li').length == 0)
			$('#input_filtra_tabela').fadeOut('normal');
	},

	init: function(){
		this.iniciarUnidades();
		this.verificarUnidade();
		this.removerClassesNoBlur();
		this.enviarForm();
		this.marcarEntregue();
		this.removerCorrespondencia();
		this.filtraTabela();
	}
};