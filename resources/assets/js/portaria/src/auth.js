Portaria.Auth = {

	login_form_submit : function() {
		$('.login-form form').submit( function(e){

			var campo_email = $('#login-form-login');
			var campo_passw = $('#login-form-senha');

			if(!campo_email.val()){

				campo_email.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('input')
						   .focus();

				e.preventDefault();
				return false;
			}

			if(!campo_passw.val()){

				campo_passw.parent()
						   .addClass('erro')
						   .addClass('obrigatorio')
						   .find('input')
						   .focus();

				e.preventDefault();
				return false;
			}
		});
	},

	login_form_keyup : function() {
		$('#login-form-login, #login-form-senha').on('keyup', function(){
			$(this).parent().removeClass('erro')
							.removeClass('obrigatorio');
		});
	},

	init : function() {
		this.login_form_submit();
		this.login_form_keyup();
	}
};