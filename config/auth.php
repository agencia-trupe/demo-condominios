<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Authentication Driver
    |--------------------------------------------------------------------------
    |
    | This option controls the authentication driver that will be utilized.
    | This driver manages the retrieval and authentication of the users
    | attempting to get access to protected areas of your application.
    |
    | Supported: "database", "eloquent"
    |
    */
    'multi-auth' => [
        'admin' => [
            'driver' => 'eloquent',
            'model'  => Gallery\Models\Admin::class
        ],
        'moradores' => [
            'driver' => 'eloquent',
            'model'  => Gallery\Models\Morador::class,
            'email' => 'moradores.templates.emails.redefinicao-senha',
        ],
        'portaria' => [
            'driver' => 'eloquent',
            'model'  => Gallery\Models\Porteiro::class,
        ]
    ],


    /*
    |--------------------------------------------------------------------------
    | Password Reset Settings
    |--------------------------------------------------------------------------
    |
    | Here you may set the options for resetting passwords including the view
    | that is your password reset e-mail. You can also set the name of the
    | table that maintains all of the reset tokens for your application.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    'password' => [
        'table' => 'recuperacao_senha',
        'expire' => 60,
    ],

];
