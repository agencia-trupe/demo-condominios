## Sistema Gallery Online

Sistema de gerenciamento de Condomínio desenvolvido inicialmente para o Condomínio In Gallery - Jardim Sul.

O sistema é dividido em 3 módulos principais:

- Módulo de ** Moradores **
- Módulo de ** Administração **
- Módulo de ** Portaria **

Cada um dos módulos tem o seu próprio sistema de autenticação independente.

# Composição dos Módulos

Os módulos são compostos por submódulos que podem funcionar (quase) de maneira independente.

O módulo de ** Moradores ** é composto pelos sub-módulos:

- Autenticação (cadastro, autenticação, recuperação de senha, etc)
- Dashboard (reune informações de todos os outros módulos)
- Perfil (alteração de dados, alteração de senha)
- Amigos (selecionar moradores amigos)
- Enquetes
- Classificados


- Administração e Comunicação
	- Livro de Ocorrências
	- Documentos Oficiais
	- Avisos
	- Agenda da Administração
	- Fale Com
	- FAQ
	- Agendamento de Mudança


- Minha Unidade e Garagem
	- Moradores da Unidade
	- Veículos da Unidade
	- Animais de Estimação


- Áreas Comuns e Lazer
	- Chamados de Manutenção
	- Reservas de Espaços
	- Instruções de Uso


- Controle de Acesso
	- Pessoas autorizadas
	- Prestadores de Serviço
	- Pessoas Não Autorizadas



O módulo de ** Administração ** é composto pelos sub-módulos (a serem reorganizados):

- Avisos
- Agenda
- Ocorrências
- Documentos
- Fale Com
- FAQ
- Mudanças
- Chamados de Manutenção
- Reservas de Espaços
- Administradores
- Dashboard
- Auth

O módulo de ** Portaria ** ainda não está completamente definido.