<?php

namespace Gallery\Libs;

use \Image, \Input;

/**
*	Classe wrapper para criação de Thumbs
*	@author Bruno Monteiro
*	@since 10/04/2014
*/
class Thumbs
{
	/* Vou receber o nome do input
	*  pegar o input via post
	*  checar dimensões
	*  redimensionar de acordo
	*  retornar o nome da imagem ou false
	*/

	/* MAKE FROM POST */
	public static function make($input = false, $largura = 0, $altura = 0, $destino = '', $customTimestamp = false, $bgcolor = '#FFFFFF', $cortarExcedente = true, $corte = 'center')
	{
		if(!$input || !$destino) return false;

		if(Input::hasFile($input)){

			$imagem   = Input::file($input);
			$filename = ($customTimestamp !== false) ? $customTimestamp.$imagem->getClientOriginalName() : date('YmdHis').$imagem->getClientOriginalName();
			$filename = str_replace(' ', '-', $filename);

			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());

			if($altura == null && $largura == null){
				$imgobj->save('assets/img/'.$destino.$filename, 100);
  			}elseif($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->save('assets/img/'.$destino.$filename, 100);
  			}else{

				$ratio = $imgobj->width() / $imgobj->height();
	  			$nratio = $largura / $altura;

	  			if($cortarExcedente){
	  				if ($ratio > $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save('assets/img/'.$destino.$filename, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save('assets/img/'.$destino.$filename, 100);
					}
				}else{
					if ($ratio < $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save('assets/img/'.$destino.$filename, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save('assets/img/'.$destino.$filename, 100);
					}
				}
			}
			$imgobj->destroy();
			return $filename;
		}else{
			return false;
		}
	}

	/* MAKE FROM FILE */
	public static function makeFromFile($dir = '', $arquivo = false, $largura = 0, $altura = 0, $destino = '', $bgcolor = '#FFFFFF', $cortarExcedente = true, $corte = 'center')
	{
		if(!$dir || !$arquivo || !$destino) return false;

		if(file_exists($dir.$arquivo)){

			$imgobj = Image::make(file_get_contents($dir.$arquivo));

  			if($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->save($destino.$arquivo, 100);
  			}else{

				$ratio = $imgobj->width() / $imgobj->height();
	  			$nratio = $largura / $altura;

	  			if($cortarExcedente){
					if ($ratio > $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save($destino.$arquivo, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save($destino.$arquivo, 100);
					}
				}else{
					if ($ratio < $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save($destino.$arquivo, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, $corte, false, $bgcolor)->save($destino.$arquivo, 100);
					}
				}
			}
			$imgobj->destroy();
			return $arquivo;
		}else{
			return false;
		}
	}
}
