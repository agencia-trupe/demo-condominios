<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Gallery\Models\FestaParticular;

class MoradorCriouFestaParticular extends Event
{
    use SerializesModels;

    public $festa;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(FestaParticular $festa)
    {
        $this->festa = $festa;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
