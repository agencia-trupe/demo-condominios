<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Gallery\Models\ChamadoDeManutencao;

class MoradorAbriuChamadoDeManutencao extends Event
{
    use SerializesModels;

    public $chamado;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ChamadoDeManutencao $chamado)
    {
        $this->chamado = $chamado;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
