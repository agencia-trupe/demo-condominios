<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Gallery\Models\Ocorrencia;

class AdminRespondeuOcorrencia extends Event
{
    use SerializesModels;

    public $ocorrencia;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Ocorrencia $ocorrencia)
    {
        $this->ocorrencia = $ocorrencia;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
