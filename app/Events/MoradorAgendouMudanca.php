<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Gallery\Models\AgendamentoDeMudanca;

class MoradorAgendouMudanca extends Event
{
    use SerializesModels;

    public $agendamento;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AgendamentoDeMudanca $agendamento)
    {
        $this->agendamento = $agendamento;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
