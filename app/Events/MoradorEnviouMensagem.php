<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Gallery\Models\FaleComMensagem;

class MoradorEnviouMensagem extends Event
{
    use SerializesModels;

    public $mensagem;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(FaleComMensagem $mensagem)
    {
        $this->mensagem = $mensagem;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
