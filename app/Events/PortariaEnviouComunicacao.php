<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Gallery\Models\ComunicacaoAdministracao;

class PortariaEnviouComunicacao extends Event
{
    use SerializesModels;

    public $comunicacao;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ComunicacaoAdministracao $comunicacao)
    {
        $this->comunicacao = $comunicacao;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
