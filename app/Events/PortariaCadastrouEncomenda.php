<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Gallery\Models\Encomenda;

class PortariaCadastrouEncomenda extends Event
{
    use SerializesModels;

    public $encomenda;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Encomenda $encomenda)
    {
        $this->encomenda = $encomenda;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
