<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Gallery\Models\EspacoReservaPorDiaria;

class AdminVisualizouReservaComMulta extends Event
{
    use SerializesModels;

    public $reserva;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EspacoReservaPorDiaria $reserva)
    {
        $this->reserva = $reserva;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
