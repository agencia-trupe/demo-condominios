<?php

namespace Gallery\Events;

use Gallery\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Gallery\Models\EspacoReservaPorPeriodo;

class MoradorReservouEspacoPorPeriodo extends Event
{
    use SerializesModels;

    public $reserva;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(EspacoReservaPorPeriodo $reserva)
    {
        $this->reserva = $reserva;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
