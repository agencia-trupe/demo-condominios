<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class ClassificadoFoto extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'classificados_imagens';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'classificados_id',
    'imagem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  
}