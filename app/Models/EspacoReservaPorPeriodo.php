<?php

namespace Gallery\Models;

use Auth;
use DB;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;
use Gallery\Models\EspacoReservaPorPeriodoLido;

class EspacoReservaPorPeriodo extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos_reservas_por_periodo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'espacos_id',
        'moradores_id',
        'unidades_id',
        'reserva',
        'anotacoes',
        'is_bloqueio'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['reserva', 'created_at', 'updated_at'];

    protected $casts = [
        'is_bloqueio' => 'boolean',
    ];

    public function setReservaAttribute($value){
        $this->attributes['reserva'] = $value.':00';
    }

    /*
    *   Método para verificar se o Chamado já  foi visto
    *   pelo usuário do painel logado para mostrar a tag
    *   de 'nova' na listagem no painel de administração
    */
    public function getNaoLido()
    {
        $check = EspacoReservaPorPeriodoLido::where('espacos_reservas_id', $this->id)
                                            ->where('administradores_id', Auth::admin()->get()->id)
                                            ->get();

        return sizeof($check) == 0 ? true : false;
    }

    public function removerNotificacoes()
    {
        $destino_notificacao = 'admin/reservas-de-espacos/'.$this->id.'?tipo=por_periodo';

        Notificacao::reservasDeEspacos('por_periodo')
                   ->where('link', $destino_notificacao)
                   ->delete();
    }

    public function scopeBloqueios($query)
    {
        return $query->where('is_bloqueio', 1);
    }

    public function scopeSemBloqueios($query)
    {
        return $query->where('is_bloqueio', 0);
    }

    public function scopeAcharPorHorario($query, $value)
    {
        return $query->where('reserva', $value);
    }

    public function scopeHorario($query, $value)
    {
        return $query->where('reserva', $value);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('reserva', 'asc');
    }

    public function scopeEspaco($query, $id)
    {
        return $id ? $query->where('espacos_id', $id) : $query;
    }

    public function scopeAPartirDeData($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where('reserva', '>=', $data);
    }

    public function scopeProximosTresDias($query, $data = null)
    {
        if(is_null($data))
            $data = Carbon::now();
        else
            $data = Carbon::createFromFormat('Y-m-d', $data);

        $data->addDays(3);

        return $query->where('reserva', '<=', $data->format('Y-m-d'));
    }

    public function scopeAntesDeData($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where('reserva', '<', $data);
    }

    public function scopeAPartirDeDataEHora($query, $timestamp = null)
    {
        if(is_null($timestamp)) $timestamp = date('Y-m-d H:i');

        return $query->where('reserva', '>=', $timestamp);
    }

    public function scopeAteDataEHora($query, $timestamp = null)
    {
        if(is_null($timestamp)) $timestamp = date('Y-m-d H:i');

        return $query->where('reserva', '<=', $timestamp);
    }

    public function scopeMoradorLogado($query)
    {
        return $query->where('moradores_id', '=', Auth::moradores()->get()->id);
    }

    public function getNomeCompletoAutor()
    {
        if(is_null($this->moradores_id))
            return 'Administração';
        else
            return $this->morador->getNomeCompleto();
    }

    public function getUnidadeAutor()
    {
        if(is_null($this->moradores_id))
            return '';
        else
            return $this->morador->getUnidade();
    }


    public function espacoReservado()
    {
        return $this->belongsTo('Gallery\Models\Espaco', 'espacos_id');
    }

    public function morador()
    {
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

}
