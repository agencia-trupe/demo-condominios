<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class PrestadorDeServicoPrivacidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'prestadores_de_servico_privacidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prestador_id',
        'visibilidade'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    /**
    * Relação Privacidade - Morador
    *
    */
    public function prestador(){
        return $this->belongsTo('Gallery\Models\PrestadorDeServico', 'prestador_id');
    }
}