<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Gallery\Models\FaleComMensagemLida;

class FaleComMensagem extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fale_com_mensagens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'moradores_id',
    	'em_resposta_a',
    	'assunto',
    	'mensagem',
    	'destino'
    ];


    public function getDestinatarioFormatado(){

        switch ($this->destino) {
            case 'sindico':
                $retorno = "Síndico";
                break;

            default:
                $retorno = ucfirst($this->destino);
                break;
        }

        return $retorno;
    }

    public function scopeOrdenado($query){
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeMensagensEnviadas($query){
        return $query->whereNull('em_resposta_a');
    }

    public function scopePerfil($query, $perfil){

        if($perfil == 'master') return $query;

        return $query->where('destino', $perfil);
    }

    public function getNomeCompletoAutor()
    {
        if(is_null($this->moradores_id))
            return 'Administração';
        else
            return $this->autor->getNomeCompleto();
    }

    public function getUnidadeAutor()
    {
      if(is_null($this->moradores_id))
          return 'sem unidade';
      else
          return $this->autor->getUnidade();
    }

    public function getNaoLido()
    {
      $check = FaleComMensagemLida::where('fale_com_mensagens_id', $this->id)
                                  ->where('administradores_id', Auth::admin()->get()->id)
                                  ->get();

      return sizeof($check) == 0 ? true : false;
    }

    public function autor()
    {
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function respostas($ordem_data = 'asc')
    {
        return $this->hasMany('Gallery\Models\FaleComMensagem', 'em_resposta_a')->orderBy('created_at', $ordem_data);
    }
}
