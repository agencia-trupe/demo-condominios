<?php

namespace Gallery\Models;

use DB;
use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class MoradorDaUnidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moradores_da_unidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'data_nascimento',
        'parentesco',
        'foto',
        'moradores_id',
        'unidades_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['data_nascimento', 'created_at', 'updated_at'];

    public function setDataNascimentoAttribute($value)
    {
        $this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getVisibilidadesAttribute()
    {
        $visibilidades_array = $this->privacidade()->lists('visibilidade')->all();
        return implode(',', $visibilidades_array);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('nome', 'asc');
    }

    public function scopeDaUnidade($query)
    {
        return $query->where('unidades_id', '=', Auth::moradores()->get()->unidade->id);
    }

    /**
    * Relação Morador da Unidade - Privacidade
    */
    public function privacidade(){
        return $this->hasMany('Gallery\Models\MoradorDaUnidadePrivacidade', 'moradores_da_unidade_id');
    }

}