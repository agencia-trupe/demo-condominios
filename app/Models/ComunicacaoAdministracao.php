<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

use Gallery\Models\ComunicacaoAdministracaoLido;

class ComunicacaoAdministracao extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'comunicacao_administracao';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'prioridade',
    'descricao',
    'remetente',
    'destinatario',
    'is_resposta',
    'em_resposta_a'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $casts = [
    'is_resposta' => 'boolean'
  ];

  public function getDestinatarioFormatadoAttribute(){
    return $this->formataNomeDePerfis($this->remetente);
  }

  public function getRemetenteFormatadoAttribute(){
    return $this->formataNomeDePerfis($this->destinatario);
  }

  private function formataNomeDePerfis($nome_de_perfil){
    switch ($nome_de_perfil) {
      case 'sindico':
        $retorno = "Síndico";
        break;

      default:
        $retorno = ucfirst($nome_de_perfil);
        break;
    }
    return $retorno;
  }

  public function scopeOrdenado($query){
    return $query->orderBy('created_at', 'desc');
  }

  public function scopeParaPerfil($query, $perfil){
    return $query->where('destinatario', $perfil);
  }

  public function getNaoLido()
  {
    $check = ComunicacaoAdministracaoLido::where('comunicacao_adm_id', $this->id)
                                         ->where('administradores_id', Auth::admin()->get()->id)
                                         ->get();

    return sizeof($check) == 0 ? true : false;
  }

}