<?php

namespace Gallery\Models;

use Auth;
use DB;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;
use Gallery\Models\EspacoReservaPorDiariaLido;
use Gallery\Models\NotificacaoAdmin;

use Illuminate\Database\Eloquent\SoftDeletes;

class EspacoReservaPorDiaria extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos_reservas_por_diaria';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'espacos_id',
        'moradores_id',
        'unidades_id',
        'reserva',
        'titulo',
        'horario',
        'is_bloqueio'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    protected $dates = ['reserva', 'deleted_at', 'created_at', 'updated_at'];

    protected $casts = [
        'is_bloqueio' => 'boolean',
    ];

    public function setReservaAttribute($value){
        $this->attributes['reserva'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    /*
    *   Método para verificar se o Chamado já  foi visto
    *   pelo usuário do painel logado para mostrar a tag
    *   de 'nova' na listagem no painel de administração
    */
    public function getNaoLido()
    {
        $check = EspacoReservaPorDiariaLido::where('espacos_reservas_id', $this->id)
                                           ->where('administradores_id', Auth::admin()->get()->id)
                                           ->get();

        return sizeof($check) == 0 ? true : false;
    }

    public function removerNotificacoes()
    {
        NotificacaoAdmin::reservaDeEspacoDiaria()
                        ->where('espacos_reservas_por_diaria_id', $this->id)
                        ->delete();
    }

    public function removerMarcacaoDeLido()
    {
        EspacoReservaPorDiariaLido::where('espacos_reservas_id', $this->id)->delete();
    }

    public function scopeBloqueios($query)
    {
        return $query->where('is_bloqueio', '=', 1);
    }

    public function scopeSemBloqueios($query)
    {
        return $query->where('is_bloqueio', '=', 0);
    }

    public function scopeAcharPorData($query, $value)
    {
        $data = Carbon::createFromFormat('d/m/Y', $value);

        return $query->where('reserva', '=', $data->format('Y-m-d'));
    }

    public function scopeData($query, $value)
    {
        return $query->where('reserva', '=', $value);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('reserva', 'asc');
    }

    public function scopeEspaco($query, $id)
    {
        return $id ? $query->where('espacos_id', '=', $id) : $query;
    }

    public function scopeAgendadasParaHoje($query)
    {
        return $query->where('reserva', '=', Carbon::now()->format('Y-m-d'));
    }

    public function scopeRangeTrintaDias($query, $data = null)
    {
        if(is_null($data))
            $data = Carbon::now();
        else
            $data = Carbon::createFromFormat('Y-m-d', $data);

        $data_inferior = clone $data;
        $data_superior = clone $data;

        $data_inferior->subDays(30);
        $data_superior->addDays(30);

        return $query->where(\DB::raw('DATE(reserva)'), '>=', $data_inferior->format('Y-m-d'))
                     ->where(\DB::raw('DATE(reserva)'), '<=', $data_superior->format('Y-m-d'));
    }

    public function scopeProximosDias($query, $data = null)
    {
        if(is_null($data))
            $data = Carbon::now();
        else
            $data = Carbon::createFromFormat('Y-m-d', $data);

        $data->addDays(3);

        return $query->where(\DB::raw('DATE(reserva)'), '<=', $data->format('Y-m-d'));
    }

    public function scopeAntesDeData($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where(\DB::raw('DATE(reserva)'), '<', $data);
    }

    public function scopeDeData($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where('reserva', '>=', $data);
    }

    public function scopeAteData($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where('reserva', '<=', $data);
    }

    public function scopeDatasFuturas($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where('reserva', '>=', $data);
    }

    public function scopeMoradorLogado($query)
    {
        return $query->where('moradores_id', '=', Auth::moradores()->get()->id);
    }

    public function getNomeCompletoAutor()
    {
        if(is_null($this->moradores_id))
            return 'Administração';
        else
            return $this->morador->getNomeCompleto();
    }

    public function getUnidadeAutor()
    {
        if(is_null($this->moradores_id))
            return '';
        else
            return $this->morador->getUnidade();
    }


    public function espacoReservado()
    {
        return $this->belongsTo('Gallery\Models\Espaco', 'espacos_id');
    }

    public function morador()
    {
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function convidados()
    {
        return $this->hasMany('Gallery\Models\EspacoReservaPorDiariaConvidados', 'reservas')->orderBy('nome', 'asc');
    }

}
