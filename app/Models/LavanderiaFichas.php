<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LavanderiaFichas extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'lavanderia_fichas';

  

  protected $fillable = [
    'quantidade',
    'moradores_id',
    'is_faturado',
    'is_retirada_solicitada'
  ];

  protected $casts = [
    'is_faturado'            => 'boolean',
    'is_retirada_solicitada' => 'boolean'
  ];

  protected $appends = [
    'data_formatada',
    'unidade_resumo'
  ];

  public function getDataFormatadaAttribute()
  {
    return $this->created_at->format('d/m/Y • H:i \h');
  }

  public function getUnidadeResumoAttribute()
  {
    return $this->morador->unidade->resumo;
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }

  public function scopeEmAberto($query)
  {
    return $query->whereRaw("MONTH(created_at) = ".date('m'));
  }

  public function scopeFechadas($query)
  {
    return $query->whereRaw("MONTH(created_at) < ".date('m'));
  }

  public function scopeNaoFaturados($query)
  {
    return $query->where('is_faturado', 0);
  }

  public function scopeFaturados($query)
  {
    return $query->where('is_faturado', 1);
  }

  public function scopeNaoRemovidos($query)
  {
    return $query->where('is_retirada_solicitada', 0);
  }

  public function scopeRemocaoSolicitada($query)
  {
    return $query->where('is_retirada_solicitada', 1);
  }

  public function scopePorPeriodo($query, $mes, $ano)
  {
    return $query->whereRaw("MONTH(created_at) = ".$mes)
                 ->whereRaw("YEAR(created_at) = ".$ano);
  }

  public function morador()
  {
    return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
  }

}