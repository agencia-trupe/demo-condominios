<?php

namespace Gallery\Models;

use DB;
use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class VeiculoDaUnidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'veiculos_da_unidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'veiculo',
      'cor',
      'placa',
      'vaga'
    ];

    protected $appends = [
      'unidade_resumo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['created_at', 'updated_at'];

    public function getToTextAttribute()
    {
      $toText = "";
      $toText .= "<strong>Veículo:</strong> {$this->veiculo} <br>";
      $toText .= "<strong>Cor:</strong> {$this->cor} <br>";
      $toText .= "<strong>Placa:</strong> {$this->placa} <br>";
      $toText .= $this->vaga != '' ? "<strong>Vaga:</strong> {$this->vaga}" : "<strong>Vaga:</strong> --";
      return $toText;
    }

    public function getVisibilidadesAttribute()
    {
        $visibilidades_array = $this->privacidade()->lists('visibilidade')->all();
        return implode(',', $visibilidades_array);
    }

    public function getUnidadeResumoAttribute()
    {
      return $this->unidade->resumo;
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('veiculo', 'asc');
    }

    public function scopeDaUnidade($query)
    {
        return $query->where('unidades_id', '=', Auth::moradores()->get()->unidade->id);
    }

    public function scopeAcharPorPlaca($query, $placa)
    {
      return $query->where('placa', '=', $placa);
    }

    /**
    * Relação Morador da Unidade - Privacidade
    */
    public function privacidade(){
        return $this->hasMany('Gallery\Models\VeiculoDaUnidadePrivacidade', 'veiculos_da_unidade_id');
    }

    public function unidade(){
        return $this->belongsTo('Gallery\Models\Unidade', 'unidades_id')->incluirTestes();
    }

}
