<?php

namespace Gallery\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Unidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unidades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'unidade',
      'bloco',
      'proprietario',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['created_at', 'updated_at'];

    protected $appends = ['resumo'];

    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery()->where('is_teste', '=', 0);
    }

    public function scopeIncluirTestes($query)
    {
        return $query->orWhere('is_teste', '=', 1);
    }

    public function getResumo()
    {
      return strtoupper($this->unidade.$this->bloco);
    }

    public function getResumoAttribute()
    {
      return $this->getResumo();
    }

    public function scopeAcharPorResumo($query, $resumo)
    {
      return $query->whereRaw("CONCAT_WS('', `unidade`, `bloco`) = '{$resumo}'");
    }

    public function scopeOrdenado($query)
    {
      return $query->orderBy('unidade', 'asc');
    }

    public function scopeOrdenacaoNatural($query)
    {
      return $query->orderBy(DB::raw('LENGTH(unidade)'))
                   ->orderBy('unidade');
    }

    public function scopeListarResumos($query)
    {
      return $query->select(DB::raw("CONCAT_WS('', `unidade`, `bloco`) as resumo"));
    }

    public function scopeChecar($query, $dados_unidade)
    {
      $dados_unidade['bloco'] = prepara_dados_unidade($dados_unidade['bloco']);
      $dados_unidade['nome_proprietario'] = prepara_dados_unidade($dados_unidade['nome_proprietario']);

      return $query->where('unidade', $dados_unidade['numero_unidade'])
                   ->where('bloco', $dados_unidade['bloco'])
                   ->where('proprietario', $dados_unidade['nome_proprietario']);
    }

    public function scopeEagerLoadAll($query){
      return $query->with('moradores', 'moradores_da_unidade', 'correspondencias', 'encomendas',
                          'pessoas_nao_autorizadas', 'pessoas_autorizadas', 'veiculos_da_unidade',
                          'animais_de_estimacao', 'prestadores_de_servico', 'agendamentos_de_mudanca',
                          'festas_particulares');
    }


    public function moradores()
    {
      return $this->hasMany('Gallery\Models\Morador', 'unidades_id');
    }

    public function moradores_da_unidade()
    {
      return $this->hasMany('Gallery\Models\MoradorDaUnidade', 'unidades_id');
    }

    public function correspondencias()
    {
      return $this->hasMany('Gallery\Models\Correspondencia', 'unidades_id');
    }

    public function encomendas()
    {
      return $this->hasMany('Gallery\Models\Encomenda', 'unidades_id');
    }

    public function pessoas_nao_autorizadas()
    {
      return $this->hasMany('Gallery\Models\PessoaNaoAutorizada', 'unidades_id');
    }

    public function pessoas_autorizadas()
    {
      return $this->hasMany('Gallery\Models\PessoaAutorizada', 'unidades_id');
    }

    public function veiculos_da_unidade()
    {
      return $this->hasMany('Gallery\Models\VeiculoDaUnidade', 'unidades_id');
    }

    public function animais_de_estimacao()
    {
      return $this->hasMany('Gallery\Models\AnimalDeEstimacao', 'unidades_id');
    }

    public function prestadores_de_servico()
    {
      return $this->hasMany('Gallery\Models\PrestadorDeServico', 'unidades_id');
    }

    public function agendamentos_de_mudanca()
    {
      return $this->hasMany('Gallery\Models\AgendamentoDeMudanca', 'unidades_id')->where('data', '>=', date('Y-m-d'));
    }

    public function festas_particulares()
    {
      return $this->hasMany('Gallery\Models\FestaParticular', 'unidades_id')->where('reserva', '>=', date('Y-m-d'));
    }

    public function reservas_salao_festas()
    {
      $espaco = Espaco::where('slug', '=', 'salao_de_festas')->first();

      if(!$espaco) dd('Unidade não encontrada');

      return $this->hasMany('Gallery\Models\EspacoReservaPorDiaria', 'unidades_id')->where('reserva', '>=', date('Y-m-d'))
                                                                                   ->where('espacos_id', '=', $espaco->id);
    }

    public function reservas_churrasqueira()
    {
      $espaco = Espaco::where('slug', '=', 'churrasqueira')->first();

      if(!$espaco) dd('Unidade não encontrada');

      return $this->hasMany('Gallery\Models\EspacoReservaPorDiaria', 'unidades_id')->where('reserva', '>=', date('Y-m-d'))
                                                                                   ->where('espacos_id', '=', $espaco->id);
    }

    public function reservas_sala_de_squash()
    {
      $espaco = Espaco::where('slug', '=', 'sala_de_squash')->first();

      if(!$espaco) dd('Unidade não encontrada');

      return $this->hasMany('Gallery\Models\EspacoReservaPorPeriodo', 'unidades_id')->where('reserva', '>=', date('Y-m-d'))
                                                                                    ->where('espacos_id', '=', $espaco->id);
    }
}
