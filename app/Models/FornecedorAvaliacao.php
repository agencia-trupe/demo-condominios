<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FornecedorAvaliacao extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'fornecedores_avaliacoes';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'fornecedores_id',
    'moradores_id',
    'texto',
    'nota'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  public function scopeOrdenado($query)
  {
    return $query->orderBy('nota', 'desc');
  }

  public function fornecedor()
  {
    return $this->belongsTo('Gallery\Models\Fornecedor', 'fornecedores_id');
  }

  public function morador()
  {
    return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
  }

}
