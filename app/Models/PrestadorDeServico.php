<?php

namespace Gallery\Models;

use DB;
use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class PrestadorDeServico extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'prestadores_de_servico';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'documento',
        'data_nascimento',
        'empresa',
        'permissao_inicio',
        'permissao_termino',
        'moradores_id',
        'unidades_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['permissao_inicio', 'permissao_termino', 'data_nascimento', 'created_at', 'updated_at'];

    public function setPermissaoInicioAttribute($value)
    {
        $this->attributes['permissao_inicio'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function setPermissaoTerminoAttribute($value)
    {
        $this->attributes['permissao_termino'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function setDataNascimentoAttribute($value)
    {
        if(is_null($value))
            $this->attributes['data_nascimento'] = null;
        else
            $this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getVisibilidadesAttribute()
    {
        $visibilidades_array = $this->privacidade()->lists('visibilidade')->all();
        return implode(',', $visibilidades_array);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('nome', 'asc');
    }

    public function scopeDaUnidade($query)
    {
        return $query->where('unidades_id', '=', Auth::moradores()->get()->unidade->id);
    }

    /**
    * Relação Morador da Unidade - Privacidade
    */
    public function privacidade(){
        return $this->hasMany('Gallery\Models\PrestadorDeServicoPrivacidade', 'prestador_id');
    }

}