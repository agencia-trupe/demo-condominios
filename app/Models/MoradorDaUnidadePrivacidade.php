<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class MoradorDaUnidadePrivacidade extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'moradores_da_unidade_privacidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'moradores_da_unidade_id',
        'visibilidade'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    /**
    * Relação Privacidade - Morador
    *
    */
    public function morador(){
        return $this->belongsTo('Gallery\Models\MoradorDaUnidade', 'moradores_da_unidade_id');
    }
}