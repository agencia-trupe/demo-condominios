<?php

namespace Gallery\Models;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class AgendaAdministracao extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agenda_administracao';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'data',
		'titulo',
		'texto',
		'horario',
		'local'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['data', 'created_at', 'updated_at'];

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('data', 'desc');
    }

}