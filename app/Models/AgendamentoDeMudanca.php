<?php

namespace Gallery\Models;

use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class AgendamentoDeMudanca extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mudancas_agendamento';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		  'data',
		  'periodo',
      'lido',
      'lido_em'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['data', 'lido_em', 'created_at', 'updated_at'];
    protected $casts = [
      'lido' => 'boolean',
    ];

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getPeriodoFormatado()
    {
        return ($this->periodo == 'manha') ? 'Manhã' : ucfirst($this->periodo);
    }

    public function getNaoLido()
    {
        $check = AgendamentoDeMudancaLido::where('mudancas_agendamento_id', $this->id)
                                         ->where('administradores_id', Auth::admin()->get()->id)
                                         ->get();

        return sizeof($check) == 0 ? true : false;
    }

    public function unidade()
    {
        return $this->belongsTo('Gallery\Models\Unidade', 'unidades_id')->incluirTestes();
    }

    public function morador()
    {
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }


    public function scopeAgendadoPor($query, $value)
    {
        return $query->where('moradores_id', $value);
    }

    public function scopePeriodo($query, $value)
    {
        return $query->where('periodo', $value);
    }

    public function scopeProximasDatas($query)
    {
        return $query->where('data', '>=', date('Y-m-d'));
    }

    public function scopeAgendadasParaHoje($query)
    {
        return $query->where('data', '=', date('Y-m-d'));
    }

    public function scopeBloco($query, $bloco)
    {
        return $query->whereHas('unidade', function ($query) use ($bloco) {
                        $query->where('bloco', $bloco);
                    });
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('data', 'asc');
    }

    public function scopeDatasComAgendamento($query)
    {
        return $query->ordenado()
                     ->proximasDatas()
                     ->bloco(Auth::moradores()->user()->unidade->bloco)
                     ->select('data')
                     ->distinct()
                     ->get();
    }

    public function scopeDatasAgendadasPorPeriodoArray($query, $periodo, $somente_usuario = false)
    {
        if($somente_usuario)
            $query->agendadoPor(Auth::moradores()->user()->id);

        return $query->ordenado()
                     ->proximasDatas()
                     ->bloco(Auth::moradores()->user()->unidade->bloco)
                     ->periodo($periodo)
                     ->select('data')
                     ->get()
                     ->pluck('data')
                     ->toArray();
    }

}
