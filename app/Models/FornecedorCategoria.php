<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FornecedorCategoria extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'fornecedores_categorias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'slug'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  public function scopeOrdenado($query)
  {
    return $query->orderBy('titulo', 'asc');
  }

}
