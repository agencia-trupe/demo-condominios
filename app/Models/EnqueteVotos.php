<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class EnqueteVotos extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'enquete_votos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'enquete_questoes_id',
    'enquete_opcoes_id',
    'moradores_id'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $dates = ['created_at', 'updated_at'];

  public function questao()
  {
    return $this->belongsTo('Gallery\Models\Enquete', 'enquete_questoes_id');
  }

  public function opcao()
  {
    return $this->belongsTo('Gallery\Models\EnqueteOpcoes', 'enquete_opcoes_id');
  }

  public function scopeMoradorLogado($query)
  {
    return $query->where('moradores_id', Auth::moradores()->get()->id);
  }

  public function scopeEnquete($query, $enquete_id)
  {
    return $query->where('enquete_questoes_id', $enquete_id);
  }

}