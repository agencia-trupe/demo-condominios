<?php

namespace Gallery\Models;

use DB;
use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class AnimalDeEstimacao extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'animais_de_estimacao';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'especie',
        'sexo',
        'foto',
        'moradores_id',
        'unidades_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['created_at', 'updated_at'];

    public function getVisibilidadesAttribute()
    {
        $visibilidades_array = $this->privacidade()->lists('visibilidade')->all();
        return implode(',', $visibilidades_array);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('nome', 'asc');
    }

    public function scopeDaUnidade($query)
    {
        return $query->where('unidades_id', '=', Auth::moradores()->get()->unidade->id);
    }

    /**
    * Relação Morador da Unidade - Privacidade
    */
    public function privacidade(){
        return $this->hasMany('Gallery\Models\AnimalDeEstimacaoPrivacidade', 'animais_de_estimacao_id');
    }

}