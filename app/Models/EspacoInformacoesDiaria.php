<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class EspacoInformacoesDiaria extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos_informacoes_diaria';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'espacos_id',
        'valor',
        'dias_carencia',
        'texto'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function getValorAttribute($valor)
    {
        $valor = $valor > 0 ? $valor/100 : 0;
        return money_format('%!.2i', $valor);
    }

    public function setValorAttribute($valor)
    {
        $this->attributes['valor'] = preg_replace("/([^0-9])/i", "", $valor);
    }

}