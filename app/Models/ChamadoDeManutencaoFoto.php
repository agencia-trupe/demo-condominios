<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class ChamadoDeManutencaoFoto extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chamados_de_manutencao_fotos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'chamados_de_manutencao_id',
        'foto'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
}