<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Encomenda extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'encomendas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'unidades_id',
    'is_entregue',
    'entregue_em'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $dates = ['entregue_em', 'created_at', 'updated_at'];
  protected $casts = [
    'is_entregue' => 'boolean',
  ];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }

  public function scopeNaoEntregues($query)
  {
    return $query->where('is_entregue', '=', 0);
  }

  public function unidade()
  {
    return $this->belongsTo('Gallery\Models\Unidade', 'unidades_id')->incluirTestes();
  }

}
