<?php

namespace Gallery\Models;

use Auth;
use DB;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;
use Gallery\Models\FestaParticularLido;

class FestaParticular extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'festas_particulares';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unidades_id',
        'moradores_id',
        'reserva',
        'horario',
        'titulo'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */


    protected $dates = ['reserva', 'created_at', 'updated_at'];

    public function setReservaAttribute($value){
        $this->attributes['reserva'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function scopeData($query, $value)
    {
        return $query->where('reserva', '=', $value);
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('reserva', 'asc');
    }

    public function scopeDaUnidade($query, $unidade_id = false)
    {
        return $unidade_id ? $query->where('unidades_id', '=', $unidade_id) : $query->where('unidades_id', '=', Auth::moradores()->get()->unidades_id);
    }

    public function scopeAgendadasParaHoje($query)
    {
        return $query->where('reserva', '=', Carbon::now()->format('Y-m-d'));
    }

    public function scopeDatasFuturas($query, $data = null)
    {
        if(is_null($data)) $data = date('Y-m-d');

        return $query->where('reserva', '>=', $data);
    }

    public function scopeHoje($query)
    {
        return $query->where('reserva', '=', date('Y-m-d'));
    }

    public function scopeMoradorLogado($query)
    {
        return $query->where('moradores_id', '=', Auth::moradores()->get()->id);
    }

    public function getNomeCompletoAutor()
    {
        if(is_null($this->moradores_id))
            return 'Administração';
        else
            return $this->morador->getNomeCompleto();
    }

    public function getUnidadeAutor()
    {
        if(is_null($this->moradores_id))
            return '';
        else
            return $this->morador->getUnidade();
    }

    public function getNaoLido()
    {
        $check = FestaParticularLido::where('festas_particulares_id', $this->id)
                                    ->where('administradores_id', Auth::admin()->get()->id)
                                    ->get();

        return sizeof($check) == 0 ? true : false;
    }


    public function unidade()
    {
        return $this->belongsTo('Gallery\Models\Unidade', 'unidades_id')->incluirTestes();
    }

    public function morador()
    {
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function convidados()
    {
        return $this->hasMany('Gallery\Models\FestaParticularConvidados', 'festas_particulares_id')->orderBy('nome', 'asc');
    }

}
