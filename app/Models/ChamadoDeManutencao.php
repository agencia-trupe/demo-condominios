<?php

namespace Gallery\Models;

use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

use Gallery\Models\ChamadoDeManutencaoLido;

class ChamadoDeManutencao extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chamados_de_manutencao';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'moradores_id',
        'data',
        'horario',
        'localizacao',
        'descricao'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    protected $dates = ['data', 'visto_pela_adm_em', 'created_at', 'updated_at'];

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    /*
    *   Método para verificar se o Chamado já  foi visto
    *   pelo usuário do painel logado para mostrar a tag
    *   de 'nova' na listagem no painel de administração
    */
    public function getNaoLido()
    {
        $check = ChamadoDeManutencaoLido::where('chamados_de_manutencao_id', $this->id)
                                        ->where('administradores_id', Auth::admin()->get()->id)
                                        ->get();

        return sizeof($check) == 0 ? true : false;
    }

    /*
    *   Método para verificar se o Chamado já  foi marcado
    *   como Visto por algum usuário da administração. Vai
    *   mostrar o timestamp da marcação no painel dos moradores
    */
    public function getVistoPelaAdm()
    {
        return is_null($this->visto_pela_adm_em) ? false : true;
    }

    public function getLoginVistoPor()
    {
        if(is_null($this->visto_pela_adm_id)){
            return 'login não encontrado';
        }else{
            return $this->vistoPor->login;
        }
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('data', 'desc');
    }

    public function scopeDoUsuarioLogado($query)
    {
        return $query->where('moradores_id', Auth::moradores()->get()->id);
    }

    public function autor()
    {
        return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
    }

    public function vistoPor()
    {
        return $this->belongsTo('Gallery\Models\Admin', 'visto_pela_adm_id');
    }

    public function fotos()
    {
        return $this->hasMany('Gallery\Models\ChamadoDeManutencaoFoto', 'chamados_de_manutencao_id');
    }
}