<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class FaleCom extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fale_com';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contato_sindico',
        'contato_conselho',
        'contato_zelador',
        'contato_portaria',
        'contato_administradora'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

}