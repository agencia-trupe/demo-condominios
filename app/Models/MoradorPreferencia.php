<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class MoradorPreferencia extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'moradores_preferencias';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'moradores_id',
    'enviar_email_nova_correspondencia',
    'enviar_email_novo_aviso',
    'enviar_email_novo_documento'
  ];

  protected $casts = [
    'enviar_email_nova_correspondencia',
    'enviar_email_novo_aviso',
    'enviar_email_novo_documento'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  /**
  * Relação Privacidade - Morador
  *
  */
  public function morador(){
      return $this->belongsTo('Gallery\Models\Morador', 'moradores_id');
  }
}