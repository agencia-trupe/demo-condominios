<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'documentos';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'documentos_categoria_id',
    'titulo',
    'slug',
    'arquivo'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $dates = ['created_at', 'updated_at'];

  public function scopeOrdenado($query)
  {
      return $query->orderBy('created_at', 'desc');
  }

  public function categoria()
  {
    return $this->belongsTo('Gallery\Models\DocumentoCategoria', 'documentos_categoria_id');
  }

}