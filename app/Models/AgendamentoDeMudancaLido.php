<?php

namespace Gallery\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class AgendamentoDeMudancaLido extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mudancas_agendamento_lidas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mudancas_agendamento_id',
        'administradores_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function lidaPor(){
        return $this->belongsTo('Gallery\Models\Admin', 'administradores_id');
    }

    public function agendamento(){
        return $this->belongsTo('Gallery\Models\AgendamentoDeMudanca', 'mudancas_agendamento_id');
    }
}