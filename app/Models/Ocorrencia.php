<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ocorrencia extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ocorrencias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
      'moradores_id',
      'texto',
      'em_resposta_a',
      'origem',
      'finalizada'
    ];

    protected $casts = [
      'finalizada' => 'boolean',
    ];

    protected $dates = [
      'visto_em',
      'created_at',
      'updated_at',
      'deleted_at',
      'finalizada_em'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function getNomeFinalizador()
    {
      if(is_null($this->finalizada_por))
        return 'Administração';
      else
        return $this->finalizador->getNomeCompleto();
    }

    public function scopeOrdenado($query)
    {
      return $query->orderBy('created_at', 'desc');
    }

    public function scopePublicas($query, $id_morador)
    {
      return $query->where( function($query) use($id_morador){
        $query->where('ocorrencia_publica', 1)
              ->orWhere('autor_id', $id_morador);
      });
    }

    public function scopeMensagensIniciais($query)
    {
      return $query->whereNull('em_resposta_a');
    }

    public function getNomeCompletoAutor()
    {
      if(is_null($this->autor_id))
        return 'Administração';
      else
        return $this->autor->getNomeCompleto();
    }

    public function getUnidadeAutor()
    {
      if(is_null($this->autor_id))
        return '';
      else
        return $this->autor->getUnidade();
    }

    public function vistos(){
      return $this->hasMany('Gallery\Models\OcorrenciaLida', 'ocorrencias_id');
    }

    public function autor()
    {
      return $this->belongsTo('Gallery\Models\Morador', 'autor_id');
    }

    public function finalizador()
    {
      return $this->belongsTo('Gallery\Models\Morador', 'finalizada_por');
    }

    public function mensagemInicial()
    {
      return $this->belongsTo('Gallery\Models\Ocorrencia', 'em_resposta_a');
    }

    public function respostas($ordem_data = 'asc')
    {
      return $this->hasMany('Gallery\Models\Ocorrencia', 'em_resposta_a')->orderBy('created_at', $ordem_data);
    }
}
