<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class EspacoInformacoesPeriodo extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos_informacoes_periodo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'espacos_id',
        'horario_abertura',
        'horario_encerramento',
        'tempo_de_reserva',
        'max_reservas_dia'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function getHorarioAberturaAttribute($horario){
        list($horas,$minutos,$segundos) = explode(':', $horario);
        return $horas.':'.$minutos;
    }

    public function getHorarioEncerramentoAttribute($horario){
        list($horas,$minutos,$segundos) = explode(':', $horario);
        return $horas.':'.$minutos;
    }

    public function getTempoDeReservaAttribute($horario){
        list($horas,$minutos,$segundos) = explode(':', $horario);
        return $horas == '01' ? 60 : $minutos;
    }

    public function setTempoDeReservaAttribute($value){
        $this->attributes['tempo_de_reserva'] = $value == '60' ? '01:00:00' : '00:'.$value.':00';
    }
}