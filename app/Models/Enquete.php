<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class Enquete extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'enquete_questoes';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'titulo',
    'texto',
    'ativa'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  

  protected $dates = ['created_at', 'updated_at'];
  protected $casts = [
    'ativa'     => 'boolean',
    'historico' => 'boolean'
  ];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }

  public function scopeAtiva($query)
  {
    return $query->where('ativa', '=', 1);
  }

  public function scopeAtuais($query)
  {
    return $query->where('historico', '=', 0);
  }

  public function scopeHistorico($query)
  {
    return $query->where('historico', '=', 1);
  }

  public function opcoes()
  {
    return $this->hasMany('Gallery\Models\EnqueteOpcoes', 'enquete_questoes_id');
  }

  public function votos()
  {
    return $this->hasMany('Gallery\Models\EnqueteVotos', 'enquete_questoes_id');
  }

}