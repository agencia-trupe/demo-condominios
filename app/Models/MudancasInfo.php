<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Model;

class MudancasInfo extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mudancas_informacoes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'horario_manha',
        'horario_tarde',
        'regulamento'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    


}