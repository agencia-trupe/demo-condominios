<?php

namespace Gallery\Models;

use Auth;
use DB;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class EspacoReservaPorDiariaConvidados extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'espacos_reservas_por_diaria_convidados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reservas',
        'nome'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    public function scopeOrdenado($query)
    {
        return $query->orderBy('nome', 'asc');
    }

    public function reserva()
    {
        return $this->belongsTo('Gallery\Models\EspacoReservaPorDiaria', 'reserva');
    }

}