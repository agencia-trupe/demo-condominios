<?php

namespace Gallery\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Gallery\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Gallery\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [

        'auth.moradores' => \Gallery\Http\Middleware\Moradores\Authenticate::class,
        'guest.moradores' => \Gallery\Http\Middleware\Moradores\RedirectIfAuthenticated::class,

        'auth.admin' => \Gallery\Http\Middleware\Admin\Authenticate::class,
        'guest.admin' => \Gallery\Http\Middleware\Admin\RedirectIfAuthenticated::class,

        'auth.portaria' => \Gallery\Http\Middleware\Portaria\Authenticate::class,
        'guest.portaria' => \Gallery\Http\Middleware\Portaria\RedirectIfAuthenticated::class,

        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    ];
}
