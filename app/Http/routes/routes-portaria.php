<?php

Route::group(['namespace' => 'Portaria', 'prefix' => 'portaria'], function(){

	Route::get('', ['as' => 'portaria.dashboard', 'uses' => 'Dashboard\DashboardController@getDashboard']);

	Route::post('busca',  ['as' => 'portaria.busca',  'uses' => 'Busca\BuscaController@postBusca']);

	Route::post('toggle-presenca-convidado',  ['as' => 'portaria.toggle-presenca-convidado',  'uses' => 'Dashboard\DashboardController@postTogglePresencaConvidados']);

	// Autenticação
	Route::get ('auth/login',  ['as' => 'portaria.auth.login',  'uses' => 'Auth\AuthController@getLogin']);
	Route::post('auth/login',  ['as' => 'portaria.auth.login',  'uses' => 'Auth\AuthController@postLogin']);
	Route::get ('auth/logout', ['as' => 'portaria.auth.logout', 'uses' => 'Auth\AuthController@getLogout']);

	Route::get('correspondencias', ['as' => 'portaria.correspondencias.index', 'uses' => 'Correspondencias\CorrespondenciasController@getIndex']);
	Route::post('correspondencias', ['as' => 'portaria.correspondencias.inserir', 'uses' => 'Correspondencias\CorrespondenciasController@postArmazenar']);
	Route::get('correspondencias/marcarEntregue/{correspondencias_id}', ['as' => 'portaria.correspondencias.marcar-entregue', 'uses' => 'Correspondencias\CorrespondenciasController@getMarcarEntregue']);
	Route::get('correspondencias/remover/{correspondencias_id}', ['as' => 'portaria.correspondencias.remover', 'uses' => 'Correspondencias\CorrespondenciasController@getRemover']);

	Route::get('encomendas', ['as' => 'portaria.encomendas.index', 'uses' => 'Encomendas\EncomendasController@getIndex']);
	Route::post('encomendas', ['as' => 'portaria.encomendas.inserir', 'uses' => 'Encomendas\EncomendasController@postArmazenar']);
	Route::get('encomendas/marcarEntregue/{encomendas_id}', ['as' => 'portaria.encomendas.marcar-entregue', 'uses' => 'Encomendas\EncomendasController@getMarcarEntregue']);
	Route::get('encomendas/remover/{encomendas_id}', ['as' => 'portaria.encomendas.remover', 'uses' => 'Encomendas\EncomendasController@getRemover']);

	Route::get('unidade/{id_unidade}', ['as' => 'portaria.unidade.detalhes', 'uses' => 'Unidade\UnidadeController@getDetalhes']);

	Route::get('comunicacao/{slug_destinatario}', ['as' => 'portaria.comunicacao.index', 'uses' => 'Comunicacao\ComunicacaoController@getIndex'])
			 ->where('slug_destinatario', 'sindico|zelador');

	Route::post('comunicacao/{slug_destinatario}', ['as' => 'portaria.comunicacao.enviar', 'uses' => 'Comunicacao\ComunicacaoController@postEnviar'])
			 ->where('slug_destinatario', 'sindico|zelador');

	Route::get ('lavanderia', ['as' => 'portaria.lavanderia.index', 'uses' => 'Lavanderia\LavanderiaController@getIndex']);
	Route::post('lavanderia/solicitar-fichas', ['as' => 'portaria.lavanderia.solicitar-fichas', 'uses' => 'Lavanderia\LavanderiaController@postSolicitar']);
	Route::post('lavanderia/consulta', ['as' => 'portaria.lavanderia.consultar', 'uses' => 'Lavanderia\LavanderiaController@postConsultar']);
	Route::post('lavanderia/remover', ['as' => 'portaria.lavanderia.remover', 'uses' => 'Lavanderia\LavanderiaController@postRemover']);

	Route::get('veiculos', ['as' => 'portaria.veiculos.index', 'uses' => 'Veiculos\VeiculosController@getIndex']);
	Route::post('veiculos/cadastrar', ['as' => 'portaria.veiculos.cadastrar-veiculo-temporario', 'uses' => 'Veiculos\VeiculosController@postStore']);
	Route::post('veiculos/consulta', ['as' => 'portaria.veiculos.consulta-placa', 'uses' => 'Veiculos\VeiculosController@postConsulta']);
});
