<?php

Route::group(['namespace' => 'Moradores', 'prefix' => 'moradores'], function(){

	Route::get ('teste', function(){
		dd("nenhum teste");
	});

	// Dashboard
	Route::get ('', ['as' => 'moradores.dashboard', 'uses' => 'Dashboard\DashboardController@getDashboard']);
	Route::post('avisos/lido', ['as' => 'moradores.avisos.lido', 'uses' => 'Avisos\AvisosController@postLido']);

	// Autenticação
	Route::get ('auth/login',  ['as' => 'moradores.auth.login',  'uses' => 'Auth\AuthController@getLogin']);
	Route::post('auth/login', ['as' => 'moradores.auth.login',  'uses' => 'Auth\AuthController@postLogin']);
	Route::get ('auth/logout', ['as' => 'moradores.auth.logout', 'uses' => 'Auth\AuthController@getLogout']);

	// Recuperação de Senha
	Route::get ('auth/recuperar',  ['as' => 'moradores.auth.recuperar', 'uses' => 'Auth\PasswordController@getEmail']);
	Route::post('auth/recuperar', ['as' => 'moradores.auth.recuperar', 'uses' => 'Auth\PasswordController@postEmail']);
	Route::get ('auth/redefinir/{type}/{token}', ['as' => 'moradores.auth.redefinir', 'uses' => 'Auth\PasswordController@getReset']);
	Route::post('auth/redefinir',        ['as' => 'moradores.auth.redefinir', 'uses' => 'Auth\PasswordController@postReset']);

	// Cadastro
	Route::get ('auth/register',  ['as' => 'moradores.auth.register', 'uses' => 'Auth\AuthController@getRegister']);
	Route::post('auth/register', ['as' => 'moradores.auth.register', 'uses' => 'Auth\AuthController@postRegister']);
	Route::get ('auth/ativar/{email}/{activation_code}', ['as' => 'moradores.auth.ativar', 'uses' => 'Auth\AuthController@getAtivar']);
	Route::post('auth/register/upload-foto', ['as' => 'moradores.auth.register.upload', 'uses' => 'Auth\AuthController@postUploadFoto']);

	// Avisos
	Route::get ('avisos', ['as' => 'moradores.avisos.index', 'uses' => 'Avisos\AvisosController@getIndex']);
	Route::get ('avisos/{avisos_id}', ['as' => 'moradores.avisos.detalhes', 'uses' => 'Avisos\AvisosController@getDetalhes']);

	// Ocorrências
	Route::get ( 'livro-de-ocorrencias', ['as' => 'moradores.ocorrencias.index', 'uses' => 'Ocorrencias\OcorrenciasController@getIndex']);
	Route::post('livro-de-ocorrencias', ['as' => 'moradores.ocorrencias.registrar', 'uses' => 'Ocorrencias\OcorrenciasController@postRegistrar']);
	Route::post('livro-de-ocorrencias/responder/{ocorrencia_id}', ['as' => 'moradores.ocorrencias.responder', 'uses' => 'Ocorrencias\OcorrenciasController@postReponder']);
	Route::post('livro-de-ocorrencias/finalizar', ['as' => 'moradores.ocorrencias.finalizar', 'uses' => 'Ocorrencias\OcorrenciasController@postFinalizar']);

	// Documentos Oficias
	Route::get ('documentos-oficiais/{slug_categoria?}', ['as' => 'moradores.documentos.index', 'uses' => 'Documentos\DocumentosController@getIndex']);
	Route::get ('documentos-oficiais/download/{documento_id}', ['as' => 'moradores.documentos.download', 'uses' => 'Documentos\DocumentosController@getDownload']);

	// Moradores da Unidade
	Route::get ( 'moradores-da-unidade', ['as' => 'moradores.moradores-da-unidade.index', 'uses' => 'MoradoresDaUnidade\MoradoresDaUnidadeController@getIndex']);
	Route::post('moradores-da-unidade/cadastrar', ['as' => 'moradores.moradores-da-unidade.cadastrar', 'uses' => 'MoradoresDaUnidade\MoradoresDaUnidadeController@postStore']);
	Route::get ( 'moradores-da-unidade/editar/{morador_da_unidade_id}', ['as' => 'moradores.moradores-da-unidade.editar', 'uses' => 'MoradoresDaUnidade\MoradoresDaUnidadeController@getEdit']);
	Route::post('moradores-da-unidade/atualizar/{morador_da_unidade_id}', ['as' => 'moradores.moradores-da-unidade.atualizar', 'uses' => 'MoradoresDaUnidade\MoradoresDaUnidadeController@postUpdate']);
	Route::get ( 'moradores-da-unidade/remover/{morador_da_unidade_id}', ['as' => 'moradores.moradores-da-unidade.remover', 'uses' => 'MoradoresDaUnidade\MoradoresDaUnidadeController@getDestroy']);
	Route::post('moradores-da-unidade/upload-foto', ['as' => 'moradores.moradores-da-unidade.upload', 'uses' => 'MoradoresDaUnidade\MoradoresDaUnidadeController@postUploadFoto']);

	// Veículos da Unidade
	Route::get ( 'veiculos-da-unidade', ['as' => 'moradores.veiculos-da-unidade.index', 'uses' => 'VeiculosDaUnidade\VeiculosDaUnidadeController@getIndex']);
	Route::post('veiculos-da-unidade/cadastrar', ['as' => 'moradores.veiculos-da-unidade.cadastrar', 'uses' => 'VeiculosDaUnidade\VeiculosDaUnidadeController@postStore']);
	Route::get ( 'veiculos-da-unidade/remover/{veiculo_da_unidade_id}', ['as' => 'moradores.veiculos-da-unidade.remover', 'uses' => 'VeiculosDaUnidade\VeiculosDaUnidadeController@getDestroy']);

	// Animais de estimação
	Route::get ( 'animais-de-estimacao', ['as' => 'moradores.animais-de-estimacao.index', 'uses' => 'AnimaisDeEstimacao\AnimaisDeEstimacaoController@getIndex']);
	Route::post('animais-de-estimacao/cadastrar', ['as' => 'moradores.animais-de-estimacao.cadastrar', 'uses' => 'AnimaisDeEstimacao\AnimaisDeEstimacaoController@postStore']);
	Route::get ( 'animais-de-estimacao/editar/{animais_de_estimacao_id}', ['as' => 'moradores.animais-de-estimacao.editar', 'uses' => 'AnimaisDeEstimacao\AnimaisDeEstimacaoController@getEdit']);
	Route::post('animais-de-estimacao/atualizar/{animais_de_estimacao_id}', ['as' => 'moradores.animais-de-estimacao.atualizar', 'uses' => 'AnimaisDeEstimacao\AnimaisDeEstimacaoController@postUpdate']);
	Route::get ( 'animais-de-estimacao/remover/{animais_de_estimacao_id}', ['as' => 'moradores.animais-de-estimacao.remover', 'uses' => 'AnimaisDeEstimacao\AnimaisDeEstimacaoController@getDestroy']);
	Route::post('animais-de-estimacao/upload-foto', ['as' => 'moradores.animais-de-estimacao.upload', 'uses' => 'AnimaisDeEstimacao\AnimaisDeEstimacaoController@postUploadFoto']);

	// Agenda
	Route::get ('agenda', ['as' => 'moradores.agenda.index', 'uses' => 'AgendasAdministracao\AgendasAdministracaoController@getIndex']);

	// Fale Com
	Route::get ('fale-com', ['as' => 'moradores.fale-com.index', 'uses' => 'FaleCom\FaleComController@getIndex']);
	Route::get ('fale-com/mensagem/{destinatario}', ['as' => 'moradores.fale-com.mensagem', 'uses' => 'FaleCom\FaleComController@getMensagem'])->where('destinatario', '(sindico|conselho|zelador|portaria)');
	Route::post('fale-com/mensagem/{destinatario}', ['as' => 'moradores.fale-com.enviar', 'uses' => 'FaleCom\FaleComController@postMensagem'])->where('destinatario', '(sindico|conselho|zelador|portaria)');

	// FAQ
	Route::get ('faq', ['as' => 'moradores.faq.index', 'uses' => 'FAQ\FAQController@getIndex']);

	// Agendamento de Mudança
	Route::get ('agendamento-de-mudanca', ['as' => 'moradores.agendamento-de-mudanca.index', 'uses' => 'AgendamentoDeMudanca\AgendamentoDeMudancaController@getIndex']);
	Route::post('agendamento-de-mudanca', ['as' => 'moradores.agendamento-de-mudanca.agendar', 'uses' => 'AgendamentoDeMudanca\AgendamentoDeMudancaController@postAgendar']);
	Route::get ('agendamento-de-mudanca/regulamento', ['as' => 'moradores.agendamento-de-mudanca.regulamento', 'uses' => 'AgendamentoDeMudanca\AgendamentoDeMudancaController@getRegulamento']);
	Route::get ('agendamento-de-mudanca/cancelar/{data}/{periodo}', ['as' => 'moradores.agendamento-de-mudanca.cancelar', 'uses' => 'AgendamentoDeMudanca\AgendamentoDeMudancaController@getCancelar']);

	// Chamados de Manutenção
	Route::get ('chamados-de-manutencao', ['as' => 'moradores.chamados-de-manutencao.index', 'uses' => 'ChamadosDeManutencao\ChamadosDeManutencaoController@getIndex']);
	Route::post('chamados-de-manutencao', ['as' => 'moradores.chamados-de-manutencao.registrar', 'uses' => 'ChamadosDeManutencao\ChamadosDeManutencaoController@postRegistrar']);
	Route::post('chamados-de-manutencao/upload-foto', ['as' => 'moradores.chamados-de-manutencao.upload', 'uses' => 'ChamadosDeManutencao\ChamadosDeManutencaoController@postUploadFoto']);

	// Reserva de Espaços
	Route::get('reserva-de-espacos', ['as' => 'moradores.reserva-de-espacos.index', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getIndex']);
	Route::get('reserva-de-espacos/grid-por-periodo/{data_inicial}/{slug_espaco}', ['as' => 'moradores.reserva-de-espacos.grid-por-periodo', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getTabelaReservasPorPeriodo']);
	Route::get('reserva-de-espacos/verificar-multa/{id_diaria}', ['as' => 'moradores.reserva-de-espacos.verificar-multa', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@postVerificarMulta']);
	Route::get('reserva-de-espacos/regras-de-uso/{espaco}', ['as' => 'moradores.reserva-de-espacos.regras-de-uso', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getRegrasDeUso']);

	Route::get ('reserva-de-espacos/reservar/{slug_espaco}/{data_inicial?}', ['as' => 'moradores.reserva-de-espacos.grid', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getGrid']);
	Route::post('reserva-de-espacos/reservar/{slug_espaco}', ['as' => 'moradores.reserva-de-espacos.reservar', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@postReservar']);
	Route::post('reserva-de-espacos/reservar/{slug_espaco}/confirmar-reserva', ['as' => 'moradores.reserva-de-espacos.confirmar-reserva', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@postConfirmarDiaria']);
	Route::get ('reserva-de-espacos/reservar/{slug_espaco}/detalhes/{reserva_id}', ['as' => 'moradores.reservas-de-espacos.detalhes', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getDetalhes']);
	Route::get ('reserva-de-espacos/reservar/{slug_espaco}/remover/{timestamp_reserva}', ['as' => 'moradores.reserva-de-espacos.remover', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getDestroy']);
	Route::get ('reserva-de-espacos/reservar/{slug_espaco}/convidados/{reserva_id}', ['as' => 'moradores.reservas-de-espacos.convidados', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@getConvidados']);
	Route::post('reserva-de-espacos/reservar/{slug_espaco}/convidados/{reserva_id}', ['as' => 'moradores.reservas-de-espacos.salvar-convidados', 'uses' => 'ReservaDeEspacos\ReservaDeEspacosController@postConvidados']);

	// Pessoas Autorizadas
	Route::get ('pessoas-autorizadas', ['as' => 'moradores.pessoas-autorizadas.index', 'uses' => 'PessoasAutorizadas\PessoasAutorizadasController@getIndex']);
	Route::post('pessoas-autorizadas/cadastrar', ['as' => 'moradores.pessoas-autorizadas.cadastrar', 'uses' => 'PessoasAutorizadas\PessoasAutorizadasController@postStore']);
	Route::get ('pessoas-autorizadas/editar/{pessoa_autorizada_id}', ['as' => 'moradores.pessoas-autorizadas.editar', 'uses' => 'PessoasAutorizadas\PessoasAutorizadasController@getEdit']);
	Route::post('pessoas-autorizadas/atualizar/{pessoa_autorizada_id}', ['as' => 'moradores.pessoas-autorizadas.atualizar', 'uses' => 'PessoasAutorizadas\PessoasAutorizadasController@postUpdate']);
	Route::get ('pessoas-autorizadas/remover/{pessoa_autorizada_id}', ['as' => 'moradores.pessoas-autorizadas.remover', 'uses' => 'PessoasAutorizadas\PessoasAutorizadasController@getDestroy']);
	Route::post('pessoas-autorizadas/upload-foto', ['as' => 'moradores.pessoas-autorizadas.upload', 'uses' => 'PessoasAutorizadas\PessoasAutorizadasController@postUploadFoto']);

	// Pessoas Não Autorizadas
	Route::get ('pessoas-nao-autorizadas', ['as' => 'moradores.pessoas-nao-autorizadas.index', 'uses' => 'PessoasNaoAutorizadas\PessoasNaoAutorizadasController@getIndex']);
	Route::post('pessoas-nao-autorizadas/cadastrar', ['as' => 'moradores.pessoas-nao-autorizadas.cadastrar', 'uses' => 'PessoasNaoAutorizadas\PessoasNaoAutorizadasController@postStore']);
	Route::get ('pessoas-nao-autorizadas/editar/{pessoa_nao_autorizada_id}', ['as' => 'moradores.pessoas-nao-autorizadas.editar', 'uses' => 'PessoasNaoAutorizadas\PessoasNaoAutorizadasController@getEdit']);
	Route::post('pessoas-nao-autorizadas/atualizar/{pessoa_nao_autorizada_id}', ['as' => 'moradores.pessoas-nao-autorizadas.atualizar', 'uses' => 'PessoasNaoAutorizadas\PessoasNaoAutorizadasController@postUpdate']);
	Route::get ('pessoas-nao-autorizadas/remover/{pessoa_nao_autorizada_id}', ['as' => 'moradores.pessoas-nao-autorizadas.remover', 'uses' => 'PessoasNaoAutorizadas\PessoasNaoAutorizadasController@getDestroy']);
	Route::post('pessoas-nao-autorizadas/upload-foto', ['as' => 'moradores.pessoas-nao-autorizadas.upload', 'uses' => 'PessoasNaoAutorizadas\PessoasNaoAutorizadasController@postUploadFoto']);

	// Prestadores de Serviço
	Route::get ('prestadores-de-servico', ['as' => 'moradores.prestadores-de-servico.index', 'uses' => 'PrestadoresDeServico\PrestadoresDeServicoController@getIndex']);
	Route::post('prestadores-de-servico/cadastrar', ['as' => 'moradores.prestadores-de-servico.cadastrar', 'uses' => 'PrestadoresDeServico\PrestadoresDeServicoController@postStore']);
	Route::get ('prestadores-de-servico/editar/{prestador_id}', ['as' => 'moradores.prestadores-de-servico.editar', 'uses' => 'PrestadoresDeServico\PrestadoresDeServicoController@getEdit']);
	Route::post('prestadores-de-servico/atualizar/{prestador_id}', ['as' => 'moradores.prestadores-de-servico.atualizar', 'uses' => 'PrestadoresDeServico\PrestadoresDeServicoController@postUpdate']);
	Route::get ('prestadores-de-servico/remover/{prestador_id}', ['as' => 'moradores.prestadores-de-servico.remover', 'uses' => 'PrestadoresDeServico\PrestadoresDeServicoController@getDestroy']);

	// Meu Perfil
	Route::get ('meu-perfil', ['as' => 'moradores.meu-perfil.index', 'uses' => 'MeuPerfil\MeuPerfilController@getIndex']);
	Route::post('meu-perfil', ['as' => 'moradores.meu-perfil.atualizar', 'uses' => 'MeuPerfil\MeuPerfilController@postUpdate']);
	Route::post('meu-perfil/upload-foto', ['as' => 'moradores.meu-perfil.upload-foto', 'uses' => 'MeuPerfil\MeuPerfilController@postUploadFoto']);
	Route::get ('meu-perfil/ativar/{email}/{activation_code}', ['as' => 'moradores.meu-perfil.ativar', 'uses' => 'MeuPerfil\MeuPerfilController@getAtivar']);

	// Alterar Senha
	Route::get ('alterar-senha', ['as' => 'moradores.alterar-senha.index', 'uses' => 'AlterarSenha\AlterarSenhaController@getIndex']);
	Route::post('alterar-senha', ['as' => 'moradores.alterar-senha.alterar', 'uses' => 'AlterarSenha\AlterarSenhaController@postAlterar']);

	// Instruçẽos de Uso
	Route::get ('instrucoes-de-uso', ['as' => 'moradores.instrucoes-de-uso.index', 'uses' => 'InstrucoesDeUso\InstrucoesDeUsoController@getIndex']);

	// Festas Particulares
	Route::get ('festas-particulares', ['as' => 'moradores.festas-particulares.index', 'uses' => 'FestasParticulares\FestasParticularesController@getIndex']);
	Route::post('festas-particulares', ['as' => 'moradores.festas-particulares.reservar', 'uses' => 'FestasParticulares\FestasParticularesController@postReservar']);
	Route::get ('festas-particulares/remover/{festa_id}', ['as' => 'moradores.festas-particulares.remover', 'uses' => 'FestasParticulares\FestasParticularesController@getDestroy']);

	Route::get ('festas-particulares/convidados/{festa_id}', ['as' => 'moradores.festas-particulares.convidados', 'uses' => 'FestasParticulares\FestasParticularesController@getConvidados']);
	Route::post('festas-particulares/convidados/{festa_id}', ['as' => 'moradores.festas-particulares.salvar-convidados', 'uses' => 'FestasParticulares\FestasParticularesController@postConvidados']);

	Route::get ('amizades', ['as' => 'moradores.amizades.index', 'uses' => 'Amizades\AmizadesController@getIndex']);
	Route::post('amizades/buscar', ['as' => 'moradores.amizades.buscar', 'uses' => 'Amizades\AmizadesController@postBuscar']);

	Route::get ('amizades/autorizados', ['as' => 'moradores.amizades.autorizados', 'uses' => 'Amizades\AmizadesController@getAutorizados']);
	Route::post('amizades/salvar-autorizados', ['as' => 'moradores.amizades.salvar-autorizados', 'uses' => 'Amizades\AmizadesController@postAutorizar']);

	Route::get ('enquetes', ['as' => 'moradores.enquetes.index', 'uses' => 'Enquetes\EnquetesController@getIndex']);
	Route::post('enquetes/votar', ['as' => 'moradores.enquetes.votar', 'uses' => 'Enquetes\EnquetesController@postVotar']);
	Route::get ('enquetes/resultados/{enquete}', ['as' => 'moradores.enquetes.resultados', 'uses' => 'Enquetes\EnquetesController@getResultados']);

	Route::get ('classificados/meus_anuncios', ['as' => 'moradores.classificados.meus-anuncios', 'uses' => 'Classificados\ClassificadosController@getMeusAnuncios']);
	Route::post('classificados/meus_anuncios', ['as' => 'moradores.classificados.publicar', 'uses' => 'Classificados\ClassificadosController@postPublicar']);
	Route::post('classificados/upload-foto', ['as' => 'moradores.classificados.upload', 'uses' => 'Classificados\ClassificadosController@postUploadFoto']);
	Route::get ('classificados/remover/{anuncio}', ['as' => 'moradores.classificados.remover', 'uses' => 'Classificados\ClassificadosController@getRemoverAnuncio']);
	Route::get ('classificados/editar/{anuncio}', ['as' => 'moradores.classificados.editar', 'uses' => 'Classificados\ClassificadosController@getEditarAnuncio']);
	Route::post('classificados/editar/{anuncio}', ['as' => 'moradores.classificados.editar', 'uses' => 'Classificados\ClassificadosController@postEditarAnuncio']);

	Route::get('classificados/{slug_categoria?}', ['as' => 'moradores.classificados.index', 'uses' => 'Classificados\ClassificadosController@getIndex'])
	->where('slug_categoria', 'venda_e_troca|servico|recado');

	Route::get ('preferencias', ['as' => 'moradores.preferencias.index', 'uses' => 'Preferencias\PreferenciasController@getIndex']);
	Route::post('preferencias', ['as' => 'moradores.preferencias.alterar', 'uses' => 'Preferencias\PreferenciasController@postAlterar']);

	Route::get ('fornecedores', ['as' => 'moradores.fornecedores.index', 'uses' => 'Fornecedores\FornecedoresController@getIndex']);
	Route::post('fornecedores/busca', ['as' => 'moradores.fornecedores.busca', 'uses' => 'Fornecedores\FornecedoresController@postBusca']);
	Route::get ('fornecedores/detalhes/{fornecedor}', ['as' => 'moradores.fornecedores.detalhes', 'uses' => 'Fornecedores\FornecedoresController@getShow']);

	Route::post('fornecedores/indicar', ['as' => 'moradores.fornecedores.indicar', 'uses' => 'Fornecedores\FornecedoresController@postStore']);
	Route::post('fornecedores/upload-foto', ['as' => 'moradores.fornecedores.upload', 'uses' => 'Fornecedores\FornecedoresController@postUploadFoto']);
	Route::get ('fornecedores/editar/{fornecedor}', ['as' => 'moradores.fornecedores.editar', 'uses' => 'Fornecedores\FornecedoresController@getEdit']);
	Route::post('fornecedores/atualizar/{fornecedor}', ['as' => 'moradores.fornecedores.atualizar', 'uses' => 'Fornecedores\FornecedoresController@postUpdate']);
	Route::get ('fornecedores/remover/{fornecedor}', ['as' => 'moradores.fornecedores.remover', 'uses' => 'Fornecedores\FornecedoresController@getDestroy']);

	Route::post('fornecedores/avaliar/{fornecedor}', ['as' => 'moradores.fornecedores.avaliar', 'uses' => 'Fornecedores\FornecedoresController@postAvaliar']);
	Route::get('fornecedores/remover-avaliacao/{avaliacao}', ['as' => 'moradores.fornecedores.remover-avaliacao', 'uses' => 'Fornecedores\FornecedoresController@getDestroyAvaliacao']);
});
