<?php

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function(){

	Route::get('', ['as' => 'admin.dashboard', 'uses' => 'Dashboard\DashboardController@getDashboard']);

	// Autenticação
	Route::get('auth/login',  ['as' => 'admin.auth.login',  'uses' => 'Auth\AuthController@getLogin']);
	Route::post('auth/login', ['as' => 'admin.auth.login',  'uses' => 'Auth\AuthController@postLogin']);
	Route::get('auth/logout', ['as' => 'admin.auth.logout', 'uses' => 'Auth\AuthController@getLogout']);

	// Gerenciamento de Usuários
	Route::resource('usuarios', 'Usuarios\UsuariosController', ['except' => ['show']]);
	Route::resource('usuarios-portaria', 'Usuarios\UsuariosPortariaController', ['except' => ['show']]);

	Route::resource('avisos', 'Avisos\AvisosController', ['except' => ['show']]);
	Route::resource('regulamentos', 'Regulamentos\RegulamentosController', ['except' => ['create', 'store', 'show', 'destroy']]);

	Route::get('ocorrencias/finalizar/{ocorrencia_id}', ['as' => 'admin.ocorrencias.finalizar', 'uses' => 'Ocorrencias\OcorrenciasController@getFinalizar']);
	Route::resource('ocorrencias', 'Ocorrencias\OcorrenciasController', ['except' => ['edit']]);

	Route::resource('documentos-categorias', 'Documentos\DocumentosCategoriasController', ['only' => ['index', 'edit', 'update']]);
	Route::resource('documentos', 'Documentos\DocumentosController', ['except' => ['show']]);
	Route::resource('agenda-administracao', 'AgendasAdministracao\AgendasAdministracaoController', ['except' => ['show']]);

	Route::resource('fale-com', 'FaleCom\FaleComController', ['only' => ['index', 'edit', 'update']]);
	Route::resource('fale-com-mensagens', 'FaleCom\FaleComMensagensController', ['only' => ['index', 'show', 'store', 'destroy']]);

	Route::resource('faq', 'FAQ\FAQController', ['except' => ['show']]);
	Route::resource('faq-info', 'FAQ\InfoController', ['except' => ['create', 'store', 'destroy', 'show']]);

	Route::post('ordernar-registros', ['as' => 'admin.ordenar-registros', 'uses' => 'BaseAdminController@postOrdenar']);
	Route::post('reenviar-email-confirmacao', ['as' => 'admin.reenviar-email-confirmacao', 'uses' => 'BaseAdminController@getReenviarEmail']);

	Route::resource('informacoes-mudancas', 'Mudancas\MudancasInfoController', ['only' => ['index', 'edit', 'update']]);
	Route::resource('agendamento-de-mudanca', 'Mudancas\AgendamentoDeMudancaController', ['only' => ['index', 'show', 'destroy']]);
	Route::get('chamados-de-manutencao/vistar/{id}', ['as' => 'admin.chamados-de-manutencao.vistar', 'uses' => 'ChamadosDeManutencao\ChamadosDeManutencaoController@getVistar']);
	Route::resource('chamados-de-manutencao', 'ChamadosDeManutencao\ChamadosDeManutencaoController', ['only' => ['index', 'show', 'destroy']]);
	Route::resource('espacos', 'ReservaDeEspacos\EspacosController', ['only' => ['index', 'edit', 'update']]);
	Route::any('reservas-consultar', ['as' => 'admin.reservas-consultar.index', 'uses' => 'ReservaDeEspacos\ConsultaController@getIndex']);
	Route::any('reservas-download', ['as' => 'admin.reservas-consultar.download', 'uses' => 'ReservaDeEspacos\ConsultaController@postDownload']);
	Route::resource('reservas-de-espacos', 'ReservaDeEspacos\ReservasController', ['only' => ['index', 'show', 'destroy']]);
	Route::resource('cancelamentos-com-multa', 'ReservaDeEspacos\CanceladasComMultaController', ['only' => ['index', 'show', 'destroy']]);
	Route::resource('reservas-bloqueios', 'ReservaDeEspacos\BloqueiosController', ['only' => ['index', 'store', 'destroy']]);
	Route::resource('instrucoes-de-uso', 'InstrucoesDeUso\InstrucoesDeUsoController', ['except' => ['show']]);
	Route::resource('festas-particulares', 'FestasParticulares\FestasParticularesController', ['only' => ['index', 'show']]);

	Route::resource('unidades', 'Unidades\UnidadesController', ['only' => ['index', 'show']]);
	Route::get('enquetes/ativar/{enquete}', ['as' => 'admin.enquetes.ativar', 'uses' => 'Enquetes\EnquetesController@getAtivar']);
	Route::resource('enquetes', 'Enquetes\EnquetesController');

	Route::resource('comunicacao-administracao', 'Comunicacao\ComunicacaoController', ['only' => ['index', 'show', 'destroy']]);

	Route::post('fichas-lavanderia/download', ['as' => 'admin.fichas-lavanderia.download', 'uses' => 'Lavanderia\FichasLavanderiaController@postDownload']);
	Route::get('fichas-lavanderia/negar-retirada/{id}', ['as' => 'admin.fichas-lavanderia.negar-retirada', 'uses' => 'Lavanderia\FichasLavanderiaController@getNegarRetirada']);

	Route::resource('fichas-lavanderia', 'Lavanderia\FichasLavanderiaController', ['only' => ['index', 'destroy']]);

	Route::resource('fornecedores-categorias', 'Fornecedores\CategoriasController');
	Route::resource('fornecedores', 'Fornecedores\FornecedoresController', ['only' => ['index', 'edit', 'update', 'destroy']]);
	Route::delete('fornecedores/avaliacoes/remover/{avaliacao}', ['as' => 'admin.fornecedores.avaliacoes.destroy', 'uses' => 'Fornecedores\AvaliacoesController@deleteDestroy']);
	Route::get('fornecedores/avaliacoes/{fornecedor}', ['as' => 'admin.fornecedores.avaliacoes.index', 'uses' => 'Fornecedores\AvaliacoesController@getIndex']);

	Route::get('alterar-senha', ['as' => 'admin.alterar-senha.index', 'uses' => 'Usuarios\AlterarSenhaController@getIndex']);
	Route::post('alterar-senha', ['as' => 'admin.alterar-senha.update', 'uses' => 'Usuarios\AlterarSenhaController@postUpdate']);

	Route::get('estatisticas', ['as' => 'admin.estatisticas.index', 'uses' => 'Estatisticas\EstatisticasController@getIndex']);
});
