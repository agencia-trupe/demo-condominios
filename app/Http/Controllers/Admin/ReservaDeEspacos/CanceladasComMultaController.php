<?php

namespace Gallery\Http\Controllers\Admin\ReservaDeEspacos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\AdminVisualizouReservaComMulta;
use Carbon\Carbon as Carbon;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorDiaria;

class CanceladasComMultaController extends BaseAdminController
{

  public function index(Request $request)
  {
    $espaco_id = $request->has('espaco_id') ? $request->input('espaco_id') : false;

    $listaEspacos = Espaco::porTipo('diaria')
                          ->ordenado()
                          ->get();

  	$registros = EspacoReservaPorDiaria::onlyTrashed()
                                        ->semBloqueios()
                                        ->espaco($espaco_id)
                                        ->orderBy('reserva', 'asc')
                                        ->get();

    return view('admin.reserva-de-espacos.cancelada-com-multa.index', compact(
      'registros',
      'listaEspacos',
      'espaco_id'
    ));
  }

  public function show(Request $request, $id)
  {
    $tipo = $request->input('tipo');

    $registro = EspacoReservaPorDiaria::withtrashed()->find($id);
    Event::fire( new AdminVisualizouReservaComMulta($registro) );

    $this->carregarNotificacoes();

    return view('admin.reserva-de-espacos.cancelada-com-multa.show', compact('registro'));
  }

  public function destroy(Request $request, $id)
  {
  	$object = EspacoReservaPorDiaria::withtrashed()->find($id);
 		$espaco_id = $object->espacos_id;
    $object->removerNotificacoes();
    $object->forceDelete();

    $request->session()->flash('sucesso', 'Reserva de espaço removida com sucesso.');

    Event::fire( new AtividadeRastreavel('reserva_espaco_com_multa_removida', $object, Auth::admin()->get()));

    return redirect()->route('admin.cancelamentos-com-multa.index', [
		  'espaco_id' => $espaco_id
    ]);
  }
}
