<?php

namespace Gallery\Http\Controllers\Admin\ReservaDeEspacos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Espaco;
use Gallery\Models\EspacoReservaPorPeriodo;

use Gallery\Models\EspacoInformacoesPeriodo;
use Gallery\Models\EspacoInformacoesDiaria;

class EspacosController extends BaseAdminController
{

  protected $uploadPath = 'assets/documentos';

  public function index()
  {
    $registros = Espaco::ordenado()->get();
    return view('admin.reserva-de-espacos.espacos.index', compact('registros'));
  }

  public function edit($id)
  {
    return view('admin.reserva-de-espacos.espacos.edit', ['registro' => Espaco::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'tipo'   => 'required|in:diaria,por_periodo',

      'periodo.horario_abertura'      => 'required_if:tipo,por_periodo|date_format:H:i',
      'periodo.horario_encerramento'  => 'required_if:tipo,por_periodo|date_format:H:i',
      'periodo.tempo_de_reserva'      => 'required_if:tipo,por_periodo|in:15,30,45,60',
      'periodo.max_reservas_dia'      => 'required_if:tipo,por_periodo|in:1,2,3,4,5,6',

      'diaria.valor'         => 'required_if:tipo,diaria',
      'diaria.dias_carencia' => 'required_if:tipo,diaria|in:1,2,3,4,5,6,7,8,9,10,11,12,13,14,15',

      'regras_de_uso'   => 'sometimes|mimes:pdf'
    ]);

    $object = Espaco::find($id);

    if($request->file('regras_de_uso')){

      $arquivo = $request->file('regras_de_uso');
      $filename = 'regras_de_uso_'.$object->slug.'_'.date('d-m-cd www/Y').'.'.$arquivo->getClientOriginalExtension();

      if($arquivo->move($this->uploadPath, $filename))
        $object->regras_de_uso = $filename;

    }

    if($request->has('remover_arquivo') && $request->input('remover_arquivo') == '1'){
      $object->regras_de_uso = '';
    }

    $object->fill($request->except('tipo', 'regras_de_uso', 'remover_arquivo'));

    if($object->tipo == 'diaria'){

      $info = new EspacoInformacoesDiaria([
        'valor'         => $request->input('diaria.valor'),
        'dias_carencia' => $request->input('diaria.dias_carencia'),
        'texto'         => ''
      ]);

    }elseif($object->tipo == 'por_periodo'){

      $info = new EspacoInformacoesPeriodo([
        'horario_abertura'     => $request->input('periodo.horario_abertura'),
        'horario_encerramento' => $request->input('periodo.horario_encerramento'),
        'tempo_de_reserva'     => $request->input('periodo.tempo_de_reserva'),
        'max_reservas_dia'     => $request->input('periodo.max_reservas_dia')
      ]);
    }

    $this->removerInformacoesAntigas($object);

    $object->informacoes()->save($info);

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('espaco_alterado', $object, Auth::admin()->get()));

      // apagar todas a reservas futuras para esse espaço
      $this->removerReservas($object);

      $request->session()->flash('sucesso', 'Informações do Espaço alteradas com sucesso.');

      return redirect()->route('admin.espacos.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar Informações do Espaço! ('.$e->getMessage().')'));

    }
  }

  private function removerInformacoesAntigas($espaco)
  {
    EspacoInformacoesDiaria::where('espacos_id', $espaco->id)->delete();
    EspacoInformacoesPeriodo::where('espacos_id', $espaco->id)->delete();
  }

  private function removerReservas($espaco)
  {
    EspacoReservaPorPeriodo::espaco($espaco->id)
                           ->aPartirDeData()
                           ->delete();
  }

}
