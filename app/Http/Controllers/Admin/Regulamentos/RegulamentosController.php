<?php

namespace Gallery\Http\Controllers\Admin\Regulamentos;

use Event;
use Auth;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Regulamento;

class RegulamentosController extends BaseAdminController
{

    protected $uploadPath = 'assets/documentos/';

    public function index()
    {
      $registros = Regulamento::all();
      return view('admin.regulamento.index', compact('registros'));
    }

    public function edit($id)
    {
      return view('admin.regulamento.edit', ['registro' => Regulamento::find($id)]);
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'completo'           => 'sometimes|mimes:pdf',
        'reduzido_reservas'  => 'sometimes|mimes:pdf'
      ]);

      $object = Regulamento::find($id);

      $arquivos = ['completo', 'reduzido_reservas'];

      foreach ($arquivos as $key => $input) {
        if($request->file($input)){

          $arquivo = $request->file($input);
          $filename = 'regulamento_'.$input.'.'.$arquivo->getClientOriginalExtension();

          if($arquivo->move($this->uploadPath, $filename))
            $object->{$input} = $filename;

        }
      }

      try {

        $object->save();

        Event::fire( new AtividadeRastreavel('regulamento_atualizado', $object, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Regulamentos alterados com sucesso.');

        return redirect()->route('admin.regulamentos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar regulamentos! ('.$e->getMessage().')'));

      }
    }

}
