<?php

namespace Gallery\Http\Controllers\Admin\FAQ;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\FAQ;

class FAQController extends BaseAdminController
{

  public function index()
  {
    $registros = FAQ::ordenado()->get();
    return view('admin.faq.index', compact('registros'));
  }

  public function create()
  {
    return view('admin.faq.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'questao'  => 'required',
      'resposta' => 'required'
    ]);

    $object = FAQ::create($request->input());

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('faq_criado', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Questão do FAQ criada com sucesso.');

      return redirect()->route('admin.faq.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar questão! ('.$e->getMessage().')'));

    }
  }

  public function edit($id)
  {
    return view('admin.faq.edit', ['registro' => FAQ::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'questao'  => 'required',
      'resposta' => 'required'
    ]);

    $object = FAQ::find($id);

    $object->fill($request->input());

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('faq_alterado', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Questão do FAQ alterada com sucesso.');

      return redirect()->route('admin.faq.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar questão! ('.$e->getMessage().')'));

    }
  }

  public function destroy(Request $request, $id)
  {
    $object = FAQ::find($id);

    Event::fire( new AtividadeRastreavel('faq_removido', $object, Auth::admin()->get()));

    $object->delete();

    $request->session()->flash('sucesso', 'Questão removida com sucesso.');

    return redirect()->route('admin.faq.index');
  }

}
