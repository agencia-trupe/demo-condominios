<?php

namespace Gallery\Http\Controllers\Admin\FAQ;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\FAQInfo;

class InfoController extends BaseAdminController
{

    public function index()
    {
      $registros = FAQInfo::all();
      return view('admin.faq.info.index', compact('registros'));
    }

    public function edit($id)
    {
      return view('admin.faq.info.edit', ['registro' => FAQInfo::find($id)]);
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'texto'  => 'required'
      ]);

      $object = FAQInfo::find($id);

      $object->fill($request->input());

      try {

        $object->save();

        Event::fire( new AtividadeRastreavel('faq_info_alterada', $object, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Texto de Entrada do FAQ alterado com sucesso.');

        return redirect()->route('admin.faq-info.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar questão! ('.$e->getMessage().')'));

      }
    }

}
