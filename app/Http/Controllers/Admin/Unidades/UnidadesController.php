<?php

namespace Gallery\Http\Controllers\Admin\Unidades;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\Unidade;

class UnidadesController extends BaseAdminController
{

  public function index()
  {
  	$registros = Unidade::ordenacaoNatural()->get();

    return view('admin.unidades.index', compact('registros'));
  }

  public function show($id_unidade)
  {
    $registro = Unidade::findOrFail($id_unidade);

    return view('admin.unidades.show', compact('registro'));
  }

}
