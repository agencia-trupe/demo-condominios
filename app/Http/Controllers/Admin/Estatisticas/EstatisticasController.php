<?php

namespace Gallery\Http\Controllers\Admin\Estatisticas;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Morador;
use Gallery\Models\Unidade;

class EstatisticasController extends BaseAdminController
{

  public function getIndex()
  {
    $estatisticas = [];

    $estatisticas['qtd_cadastros_ativos'] = [
      'titulo' => 'Quantidade de cadastros ativos',
      'valor' => $this->quantidade_cadastros_ativos()
    ];

    $estatisticas['qtd_aptos_ativos'] = [
      'titulo' => 'Quantidade de apartamentos com cadastros ativos',
      'valor' => $this->quantidade_aptos_ativos()
    ];

    return view('admin.estatisticas.index', ['estatisticas' => $estatisticas]);
  }

  private function quantidade_cadastros_ativos()
  {
    return count(Morador::ativos()->get());
  }

  private function quantidade_aptos_ativos()
  {
    $ativos = Unidade::whereHas('moradores', function($q){
      $q->where('status', 1);
    })->get();

    $total = Unidade::all();

    return count($ativos) . ' de ' . count($total);
  }

}
