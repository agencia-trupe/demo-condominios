<?php

namespace Gallery\Http\Controllers\Admin\Comunicacao;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Events\AdminVisualizouMensagemPortaria;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\ComunicacaoAdministracao;

class ComunicacaoController extends BaseAdminController
{

  public function index()
  {
    $registros = ComunicacaoAdministracao::paraPerfil(Auth::admin()->get()->tipo)->ordenado()->get();
    return view('admin.comunicacao-administracao.index', compact('registros'));
  }

  public function show($id)
  {
    $comunicacao = ComunicacaoAdministracao::find($id);

    Event::fire(new AdminVisualizouMensagemPortaria($comunicacao));
    $this->carregarNotificacoes();

    return view('admin.comunicacao-administracao.show', ['registro' => $comunicacao]);
  }

  public function destroy(Request $request, $id)
  {
    $object = ComunicacaoAdministracao::find($id);

    Event::fire( new AtividadeRastreavel('mensagem_portaria_removida', $object, Auth::admin()->get()));

    $object->delete();

    $request->session()->flash('sucesso', 'Chamado de Manutenção removido com sucesso.');

    return redirect()->route('admin.comunicacao-administracao.index');
  }

}
