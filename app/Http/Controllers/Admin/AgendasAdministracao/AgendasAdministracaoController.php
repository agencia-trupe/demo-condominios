<?php

namespace Gallery\Http\Controllers\Admin\AgendasAdministracao;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\AgendaAdministracao;

class AgendasAdministracaoController extends BaseAdminController
{

  public function index()
  {
    $registros = AgendaAdministracao::ordenado()->get();
    return view('admin.agenda-administracao.index', compact('registros'));
  }

  public function create()
  {
    return view('admin.agenda-administracao.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'data' => 'required|date_format:d/m/Y',
      'titulo' => 'required',
      'texto' => 'required',
      'horario' => 'required',
      'local' => 'required'
    ]);

    $object = AgendaAdministracao::create($request->input());

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('evento_agenda_criado', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Evento na Agenda criado com sucesso.');

      return redirect()->route('admin.agenda-administracao.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar Evento na Agenda! ('.$e->getMessage().')'));

    }
  }

  public function edit($id)
  {
    return view('admin.agenda-administracao.edit', ['registro' => AgendaAdministracao::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'data' => 'required|date_format:d/m/Y',
      'titulo' => 'required',
      'texto' => 'required',
      'horario' => 'required',
      'local' => 'required'
    ]);

    $object = AgendaAdministracao::find($id);

    $object->fill($request->input());

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('evento_agenda_alterado', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Evento na Agenda alterado com sucesso.');

      return redirect()->route('admin.agenda-administracao.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar Evento na Agenda! ('.$e->getMessage().')'));

    }
  }

  public function destroy(Request $request, $id)
  {
    $object = AgendaAdministracao::find($id);

    Event::fire( new AtividadeRastreavel('evento_agenda_removido', $object, Auth::admin()->get()));

    $object->delete();

    $request->session()->flash('sucesso', 'Evento na Agenda removido com sucesso.');

    return redirect()->route('admin.agenda-administracao.index');
  }

}
