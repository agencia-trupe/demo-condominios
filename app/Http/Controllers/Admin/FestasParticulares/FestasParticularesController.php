<?php

namespace Gallery\Http\Controllers\Admin\FestasParticulares;

use Event;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\FestaParticular;
use Gallery\Events\AdminVisualizouFestaParticular;

class FestasParticularesController extends BaseAdminController
{

  public function index()
  {
    $registros = FestaParticular::ordenado()->get();
    return view('admin.festas-particulares.index', compact('registros'));
  }

  public function show($id)
  {
  	$festa = FestaParticular::find($id);

  	Event::fire( new AdminVisualizouFestaParticular($festa) );

    return view('admin.festas-particulares.show', ['registro' => $festa]);
  }

}
