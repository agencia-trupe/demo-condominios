<?php

namespace Gallery\Http\Controllers\Admin\Mudancas;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\MudancasInfo;

class MudancasInfoController extends BaseAdminController
{

  protected $uploadPath = 'assets/documentos';

  public function index()
  {
    $registros  = MudancasInfo::get();
    return view('admin.mudancas.info.index', compact('registros'));
  }

  public function edit($id)
  {
    return view('admin.mudancas.info.edit', ['registro' => MudancasInfo::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'horario_manha' => 'required',
      'horario_tarde' => 'required',
      'regulamento'   => 'sometimes|mimes:pdf'
    ]);

    $object = MudancasInfo::find($id);

    $object->horario_manha = $request->input('horario_manha');
    $object->horario_tarde = $request->input('horario_tarde');

    if($request->file('regulamento')){
      $arquivo = $request->file('regulamento');
      $filename = 'regulamento_interno_mudancas_e_reformas.'.$arquivo->getClientOriginalExtension();
      if($arquivo->move($this->uploadPath, $filename))
        $object->regulamento = $filename;
    }

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('info_mudancas_atualizadas', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Informações de Mudança alterada com sucesso.');

      return redirect()->route('admin.informacoes-mudancas.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar Informações de Mudança! ('.$e->getMessage().')'));

    }
  }

}
