<?php

namespace Gallery\Http\Controllers\Admin;

use Log;
use View;
use Mail;
use DB;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;
use Gallery\Http\Controllers\Admin\Notificavel;

use Gallery\Models\Morador;

class BaseAdminController extends Controller
{

  use Notificavel;

  public function __construct()
  {
    // Filtro de Usuário
    // As rotas desse controller só são acessíveis para
    // quem estiver logado
    $this->middleware('auth.admin');

	    $this->carregarNotificacoes();
  }

  public function postOrdenar(Request $request)
  {
    /* Só aceita requisições feitas via AJAX */
    if($request->ajax()){

      $registros  = $request->input('data');
      $tabela     = $request->input('tabela');

      for ($i = 0; $i < count($registros); $i++) {
        DB::table($tabela)->where('id', $registros[$i])->update(array('ordem' => $i));
      }

      return json_encode($registros);
    }
  }

  public function getReenviarEmail(Request $request)
  {
    $template_email = 'moradores.templates.emails.register-confirmacao';

    $data['morador'] = Morador::findOrFail($request->input('morador'));

    Mail::send( $template_email, $data, function ($m) use ($data) {
      $m->to($data['morador']->email, $data['morador']->nome)
        ->bcc('bruno@trupe.net')
        ->subject('Ativação de Cadastro - Gallery Online');
    });

    if(config('mail.pretend')) Log::info(View::make($template_email, $data)->render());
  }
}