<?php

namespace Gallery\Http\Controllers\Admin\Lavanderia;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use DB;

use Carbon\Carbon as Carbon;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\LavanderiaFichas;

class FichasLavanderiaController extends BaseAdminController
{

  public function index(Request $request)
  {
    if($request->has('filtro'))
      $filtro = $request->input('filtro');
    else
      $filtro = 'em_aberto';

    $inicio_mes = new Carbon('first day of this month');

    $datas_disponiveis = \DB::table('lavanderia_fichas')
                            ->selectRaw("MONTH(created_at) as mes")
                            ->selectRaw("YEAR(created_at) as ano")
                            ->where("created_at", "<", $inicio_mes->format('Y-m-d'))
                            ->groupBy("mes")
                            ->groupBy("ano")
                            ->orderBy("ano", "desc")
                            ->orderBy("mes", "desc")
                            ->get();

    if($filtro == 'em_aberto'){
      $registros = LavanderiaFichas::ordenado()->naoRemovidos()->emAberto()->paginate(30);
      return view('admin.lavanderia.fichas-em-aberto', compact('registros', 'filtro', 'datas_disponiveis'));
    }

    elseif($filtro == 'fechadas'){
      $registros = LavanderiaFichas::ordenado()->naoRemovidos()->fechadas()->paginate(30);
      return view('admin.lavanderia.fichas-fechadas', compact('registros', 'filtro', 'datas_disponiveis'));
    }

    elseif($filtro == 'retirada_solicitada'){
      $registros = LavanderiaFichas::ordenado()->remocaoSolicitada()->paginate(30);
      return view('admin.lavanderia.fichas-a-retirar', compact('registros', 'filtro', 'datas_disponiveis'));
    }
  }

  public function postDownload(Request $request)
  {
    $data    = $request->periodo;
    $formato = $request->formato;

    list($mes, $ano) = explode('/', $data);

    $filename = 'Gallery-fichas-lavanderia-'.Carbon::createFromFormat('m', $mes)->formatLocalized('%B').'_'.$ano;

    $registros = LavanderiaFichas::with('morador')
                                 ->porPeriodo($mes, $ano)
                                 ->ordenado()
                                 ->get();
    $registrosArray = [];

    foreach ($registros as $key => $value) {

      $value->is_faturado = 1;
      $value->save();

      $registrosArray[$key]['Unidade'] = $value->unidade_resumo;
      $registrosArray[$key]['Quantidade'] = $value->quantidade;
      $registrosArray[$key]['Morador'] = $value->morador->nome;
      $registrosArray[$key]['Data'] = $value->data_formatada;
      $registrosArray[$key]['Solicitação de Exclusão'] = $value->is_retirada_solicitada ? 'Sim. '.$value->motivo_cancelamento : '';
    }

    \Excel::create($filename, function($excel) use($registrosArray) {
	    $excel->sheet('Fichas de Lavanderia', function($sheet) use ($registrosArray) {
	      $sheet->fromArray($registrosArray);
	    });
		})->download($formato);
  }

  public function getNegarRetirada(Request $request, $id)
  {
    $object = LavanderiaFichas::find($id);
    $object->is_retirada_solicitada = 0;
    $object->motivo_cancelamento = '';
    $object->save();

    $request->session()->flash('sucesso', 'Pedido de remoção de Ficha de Lavanderia negada com sucesso.');

    Event::fire( new AtividadeRastreavel('remocao_ficha_lavanderia_negada', $object, Auth::admin()->get()));

    return redirect()->route('admin.fichas-lavanderia.index');
  }

  public function destroy(Request $request, $id)
  {
    $object = LavanderiaFichas::find($id);
    $object->delete();

    $request->session()->flash('sucesso', 'Ficha de Lavanderia removida com sucesso.');

    Event::fire( new AtividadeRastreavel('remocao_ficha_lavanderia', $object, Auth::admin()->get()));

    return redirect()->route('admin.fichas-lavanderia.index');
  }

}
