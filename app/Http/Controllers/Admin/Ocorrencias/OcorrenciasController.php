<?php

namespace Gallery\Http\Controllers\Admin\Ocorrencias;

use Event;
use Auth;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\AdminVisualizouOcorrencia;
use Gallery\Events\AdminRespondeuOcorrencia;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Ocorrencia;

class OcorrenciasController extends BaseAdminController
{

  public function index()
  {
    $registros = Ocorrencia::mensagensIniciais()->ordenado()->get();

    return view('admin.ocorrencias.index', compact('registros'));
  }

  public function create()
  {
    return view('admin.ocorrencias.create');
  }

  // Registrar uma Nova Ocorrência
  public function store(Request $request)
  {
    $this->validate($request, [
      'texto' => 'required|min:10',
    ]);

    $object = new Ocorrencia;

    $object->texto = $request->input('texto');
    $object->autor_id = null;
    $object->moradores_id = null;
    $object->em_resposta_a = null;
    $object->origem = 'admin';

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Ocorrência criada com sucesso.');

      Event::fire( new AtividadeRastreavel('ocorrencia_criada_pela_adm', $object, Auth::admin()->get()));

      return redirect()->route('admin.ocorrencias.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar Ocorrência! ('.$e->getMessage().')'));

    }
  }

  // Visualizar Ocorrência e histórico de respostas
  public function show($id)
  {
    $ocorrencia = Ocorrencia::find($id);

    Event::fire(new AdminVisualizouOcorrencia($ocorrencia));
    $this->carregarNotificacoes();

    return view('admin.ocorrencias.show', ['registro' => $ocorrencia]);
  }

  // Enviar resposta de ocorrência
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'texto'         => 'required|min:10',
      'moradores_id'  => 'sometimes|exists:moradores,id'
    ]);

    $resposta = new Ocorrencia;
    $resposta->texto = $request->input('texto');
    $resposta->autor_id = null;
    $resposta->moradores_id = $request->has('moradores_id') ? $request->input('moradores_id') : null;
    $resposta->em_resposta_a = $id;
    $resposta->origem = 'admin';

    try {

      $resposta->save();

      Event::fire( new AtividadeRastreavel('ocorrencia_alterada', $resposta, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Resposta para Ocorrência criada com sucesso.');

      // Lançar notificação de AdminRespondeuOcorrencia

      // Se estiver respondendo à uma Ocorrência da Administração
      // Não há necessidade de apagar Notificação ou Marcar como Lido

      if(!is_null($resposta->moradores_id))
        Event::fire(new AdminRespondeuOcorrencia($resposta));

      return redirect()->route('admin.ocorrencias.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar resposta! ('.$e->getMessage().')'));

    }
  }

  public function destroy(Request $request, $id)
  {
    $object = Ocorrencia::find($id);

    foreach ($object->respostas as $resposta){
      $resposta->delete();
      Event::fire( new AtividadeRastreavel('ocorrencia_resposta_removida', $resposta, Auth::admin()->get()));
    }

    Event::fire( new AtividadeRastreavel('ocorrencia_removida', $object, Auth::admin()->get()));

    $object->delete();
    $request->session()->flash('sucesso', 'Ocorrência removida com sucesso.');

    return redirect()->route('admin.ocorrencias.index');
  }

  public function getFinalizar(Request $request, $ocorrencia_id)
  {
    $ocorrencia = Ocorrencia::find($ocorrencia_id);

    $ocorrencia->finalizada = 1;
    $ocorrencia->finalizada_em = date('Y-m-d H:i:s');
    $ocorrencia->finalizada_por = null;
    $ocorrencia->save();

    Event::fire( new AtividadeRastreavel('ocorrencia_encerrada', $ocorrencia, Auth::admin()->get()));

    foreach($ocorrencia->respostas as $resposta){
      $resposta->finalizada = 1;
      $resposta->finalizada_em = date('Y-m-d H:i:s');
      $resposta->finalizada_por = null;
      $resposta->save();
    }

    $request->session()->flash('sucesso', 'Ocorrência finalizada com sucesso.');
    return redirect()->route('admin.ocorrencias.index');
  }

}
