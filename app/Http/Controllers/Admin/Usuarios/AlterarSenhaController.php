<?php

namespace Gallery\Http\Controllers\Admin\Usuarios;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Gallery\Events\AtividadeRastreavel;
use Illuminate\Http\Request;
use Auth;
use Hash;
use Event;

use Gallery\Models\Admin;

class AlterarSenhaController extends BaseAdminController
{

    public function getIndex()
    {
      $usuario = Auth::admin()->get();
      return view('admin.usuarios.alterar-senha', compact('usuario'));
    }

    public function postUpdate(Request $request)
    {
      $this->validate($request, [
        'senha_atual'             => 'required',
        'senha_nova'              => 'required|confirmed|min:6',
        'senha_nova_confirmation' => 'required',
      ]);

      $usuario = Auth::admin()->get();

      if(!Hash::check($request->senha_atual, $usuario->senha)){
        $request->flash();
        return back()->withErrors(array('senha atual incorreta.'));
      }

      $usuario->senha = $request->senha_nova;

      try {

        $usuario->save();

        Event::fire( new AtividadeRastreavel('senha_admin_alterada', $usuario, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Senha alterada com sucesso!');

        return redirect()->route('admin.alterar-senha.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar senha!'));

      }
    }

}
