<?php

namespace Gallery\Http\Controllers\Admin\ChamadosDeManutencao;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Events\AdminVisualizouChamadoDeManutencao;
use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\ChamadoDeManutencao;
use Gallery\Models\ChamadoDeManutencaoFoto;
use Gallery\Models\ChamadoDeManutencaoLido;

class ChamadosDeManutencaoController extends BaseAdminController
{

  public function index()
  {
    $registros = ChamadoDeManutencao::ordenado()->get();
    return view('admin.chamados-de-manutencao.index', compact('registros'));
  }

  public function show($id)
  {
    $chamado = ChamadoDeManutencao::find($id);

    Event::fire(new AdminVisualizouChamadoDeManutencao($chamado));
    $this->carregarNotificacoes();

    return view('admin.chamados-de-manutencao.show', ['registro' => $chamado]);
  }

  public function destroy(Request $request, $id)
  {
    $object = ChamadoDeManutencao::find($id);

    $object->delete();

    Event::fire( new AtividadeRastreavel('chamado_manutencao_removido', $object, Auth::admin()->get()));
    $request->session()->flash('sucesso', 'Chamado de Manutenção removido com sucesso.');

    return redirect()->route('admin.chamados-de-manutencao.index');
  }

  public function getVistar(Request $request, $id)
  {
    $object = ChamadoDeManutencao::find($id);
    $object->visto_pela_adm_em = Date('Y-m-d H:i:s');
    $object->visto_pela_adm_id = Auth::admin()->get()->id;
    $object->save();

    Event::fire( new AtividadeRastreavel('chamado_manutencao_marcado_lido', $object, Auth::admin()->get()));
    $request->session()->flash('sucesso', 'Chamado de Manutenção Marcado como Lido com sucesso.');

    return redirect()->route('admin.chamados-de-manutencao.show', $id);
  }

}
