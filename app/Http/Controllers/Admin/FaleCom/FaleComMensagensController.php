<?php

namespace Gallery\Http\Controllers\Admin\FaleCom;

use Log;
use View;
use Mail;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;
use Gallery\Events\AdminVisualizouMensagem;

use Gallery\Models\FaleCom;
use Gallery\Models\FaleComMensagem;

class FaleComMensagensController extends BaseAdminController
{

    public function index()
    {
      $registros = FaleComMensagem::ordenado()
                                  ->mensagensEnviadas()
                                  ->perfil(Auth::admin()->user()->tipo)
                                  ->get();

      return view('admin.fale-com.mensagens.index', ['registros' => $registros]);
    }

    public function show($id)
    {
      $registro = FaleComMensagem::find($id);

      $this->authorize('visualizar-mensagem', $registro);

      Event::fire(new AdminVisualizouMensagem($registro));
      $this->carregarNotificacoes();

      return view('admin.fale-com.mensagens.show', ['registro' => $registro]);
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'mensagem'  => 'required',
        'em_resposta_a' => 'required|exists:fale_com_mensagens,id'
      ]);

      $mensagem_enviada = FaleComMensagem::find($request->input('em_resposta_a'));

      $this->authorize('visualizar-mensagem', $mensagem_enviada);

      $resposta = new FaleComMensagem;
      $resposta->assunto = 'RE: '. $mensagem_enviada->assunto;
      $resposta->mensagem = $request->input('mensagem');
      $resposta->destino = 'morador';
      $resposta->em_resposta_a = $mensagem_enviada->id;

      $this->enviarRespostaPorEmail($mensagem_enviada, $resposta);

      try {

        $resposta->save();

        Event::fire( new AtividadeRastreavel('mensagem_morador_respondida', $resposta, Auth::admin()->get()));

        $request->session()->flash('sucesso', 'Resposta enviada com sucesso.');

        return redirect()->route('admin.fale-com-mensagens.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao enviar resposta! ('.$e->getMessage().')'));

      }
    }

    private function enviarRespostaPorEmail(FaleComMensagem $original, FaleComMensagem $resposta){

      $template_email = 'moradores.templates.emails.fale-com-resposta';

      $mensagem['original'] = $original;
      $mensagem['resposta'] = $resposta;

      Mail::send( $template_email, $mensagem, function ($m) use ($mensagem) {
        $m->to($mensagem['original']->autor->email, $mensagem['original']->autor->nome)
          ->bcc('bruno@trupe.net')
          ->subject($mensagem['resposta']->assunto);
      });

      if(config('mail.pretend')) Log::info(View::make($template_email, $mensagem)->render());
    }

    public function destroy(Request $request, $id){

      $object = FaleComMensagem::find($id);

      Event::fire( new AtividadeRastreavel('mensagem_morador_excluida', $object, Auth::admin()->get()));

      $object->delete();

      $request->session()->flash('sucesso', 'Mensagem removida com sucesso.');

      return redirect()->route('admin.fale-com-mensagens.index');
    }

}
