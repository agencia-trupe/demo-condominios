<?php

namespace Gallery\Http\Controllers\Admin\FaleCom;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\FaleCom;

class FaleComController extends BaseAdminController
{

  public function index()
  {
    $registros = FaleCom::all();
    return view('admin.fale-com.index', compact('registros'));
    }

  public function edit($id)
  {
    return view('admin.fale-com.edit', ['registro' => FaleCom::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'contato_sindico'        => 'required',
      'contato_conselho'       => 'required',
      'contato_zelador'        => 'required',
      'contato_portaria'       => 'required',
      'contato_administradora' => 'required'
    ]);

    $object = FaleCom::find($id);

    $object->fill($request->input());

    try {

      $object->save();

      Event::fire( new AtividadeRastreavel('informações_de_contato_alterada', $object, Auth::admin()->get()));

      $request->session()->flash('sucesso', 'Informações de contato alteradas com sucesso.');

      return redirect()->route('admin.fale-com.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar informações de contato! ('.$e->getMessage().')'));

    }
  }

}
