<?php

namespace Gallery\Http\Controllers\Admin\Fornecedores;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;

use Gallery\Models\FornecedorCategoria;

class CategoriasController extends BaseAdminController
{

    public function index()
    {
      $registros  = FornecedorCategoria::ordenado()->get();
      return view('admin.fornecedores.categorias.index', compact('registros'));
    }

    public function create()
    {
      return view('admin.fornecedores.categorias.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'titulo'  => 'required|unique:fornecedores_categorias,titulo'
      ]);

      $object = new FornecedorCategoria;

      $object->titulo = $request->input('titulo');
      $object->slug = str_slug($object->titulo);

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Categoria de Fornecedor criada com sucesso.');

        Event::fire( new AtividadeRastreavel('categoria_fornecedor_inserida', $object, Auth::admin()->get()));

        return redirect()->route('admin.fornecedores-categorias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Categoria de Fornecedor! ('.$e->getMessage().')'));

      }
    }

    public function edit($id)
    {
      return view('admin.fornecedores.categorias.edit', ['registro' => FornecedorCategoria::find($id)]);
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo'  => 'required|unique:fornecedores_categorias,titulo,'.$id
      ]);

      $object = FornecedorCategoria::find($id);

      $object->titulo = $request->input('titulo');
      $object->slug = str_slug($object->titulo);

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Categoria de Fornecedor alterada com sucesso.');

        Event::fire( new AtividadeRastreavel('categoria_fornecedor_alterada', $object, Auth::admin()->get()));

        return redirect()->route('admin.fornecedores-categorias.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar Categoria de Fornecedor! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id)
    {
      $object = FornecedorCategoria::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Categoria de Fornecedor removida com sucesso.');

      Event::fire( new AtividadeRastreavel('categoria_fornecedor_removida', $object, Auth::admin()->get()));

      return redirect()->route('admin.fornecedores-categorias.index');
    }

}
