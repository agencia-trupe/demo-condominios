<?php

namespace Gallery\Http\Controllers\Admin\InstrucoesDeUso;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\InstrucoesDeUso;

class InstrucoesDeUsoController extends BaseAdminController
{

  public function index()
  {
    $registros = InstrucoesDeUso::ordenado()->get();
    return view('admin.instrucoes-de-uso.index', compact('registros'));
  }

  public function create()
  {
    return view('admin.instrucoes-de-uso.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'questao'  => 'required',
      'resposta' => 'required'
    ]);

    $object = InstrucoesDeUso::create($request->input());

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Instrução de Uso criada com sucesso.');

      Event::fire( new AtividadeRastreavel('instrucao_de_uso_inserida', $object, Auth::admin()->get()));

      return redirect()->route('admin.instrucoes-de-uso.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar Instrução de Uso! ('.$e->getMessage().')'));

    }
  }

  public function edit($id)
  {
    return view('admin.instrucoes-de-uso.edit', ['registro' => InstrucoesDeUso::find($id)]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'questao'  => 'required',
      'resposta' => 'required'
    ]);

    $object = InstrucoesDeUso::find($id);

    $object->fill($request->input());

    try {

      $object->save();

      $request->session()->flash('sucesso', 'Instrução de Uso alterada com sucesso.');

      Event::fire( new AtividadeRastreavel('instrucao_de_uso_alterada', $object, Auth::admin()->get()));

      return redirect()->route('admin.instrucoes-de-uso.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao criar Instrução de Uso! ('.$e->getMessage().')'));

    }
  }

  public function destroy(Request $request, $id)
  {
    $object = InstrucoesDeUso::find($id);
    $object->delete();

    $request->session()->flash('sucesso', 'Instrução de Uso removida com sucesso.');

    Event::fire( new AtividadeRastreavel('instrucao_de_uso_removida', $object, Auth::admin()->get()));

    return redirect()->route('admin.instrucoes-de-uso.index');
  }

}
