<?php

namespace Gallery\Http\Controllers\Admin\Dashboard;

use Gallery\Http\Controllers\Admin\BaseAdminController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

class DashboardController extends BaseAdminController
{

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboard()
    {
        return view('admin.dashboard.index');
    }

}