<?php

namespace Gallery\Http\Controllers\Portaria;

use Auth;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

class BasePortariaController extends Controller
{

	public function __construct()
    {
        // Filtro de Usuário
        // As rotas desse controller só são acessíveis para
        // quem estiver logado
        $this->middleware('auth.portaria');
    }

}