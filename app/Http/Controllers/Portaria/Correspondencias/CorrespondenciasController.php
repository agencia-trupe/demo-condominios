<?php

namespace Gallery\Http\Controllers\Portaria\Correspondencias;

use Gallery\Http\Controllers\Portaria\BasePortariaController;
use Illuminate\Http\Request;

use Auth;
use Event;

use Gallery\Events\AtividadeRastreavel;
use Gallery\Events\PortariaCadastrouCorrespondencia;

use Carbon\Carbon as Carbon;

use Gallery\Models\Correspondencia;
use Gallery\Models\Unidade;
use Gallery\Models\NotificacaoMorador;

class CorrespondenciasController extends BasePortariaController
{

  public function getIndex()
  {
    $hoje = Carbon::now();

    $listaUnidadesJson = Unidade::get()->pluck('resumo')->toJson();

    $listaAvisos = Correspondencia::ordenado()->naoEntregues()->get();

    return view('portaria.correspondencias.index', [
      'hoje' => $hoje,
      'listaUnidadesJson' => $listaUnidadesJson,
      'listaAvisos' => $listaAvisos
    ]);
  }

  public function postArmazenar(Request $request)
  {
    $unidade = $request->input('termo_busca_unidade');

    $unidade = Unidade::acharPorResumo($unidade)->first();

    if($unidade){
      // armazenar aviso de correspondencias
      $correspondencia = $unidade->correspondencias()->create([]);
      Event::fire( new AtividadeRastreavel('portaria_cadastrou_correspondencia', $correspondencia, Auth::portaria()->get()));
      // notificar usuários da unidade
      Event::fire( new PortariaCadastrouCorrespondencia($correspondencia) );

      if($request->ajax()){
        return [
          'resumo' => $correspondencia->unidade->getResumo(),
          'data' => $correspondencia->created_at->format("d/m/Y · H:i \h"),
          'id' => $correspondencia->id
        ];
      }
    }

    return redirect()->back();
  }

  public function getMarcarEntregue(Request $request, $correspondencias_id)
  {
    // MARCAR aviso de correspondencia COMO ENTREGUE
    // Remover notificações para todos os usuários
    $correspondencia = Correspondencia::findOrFail($correspondencias_id);

    $correspondencia->is_entregue = 1;
    $correspondencia->entregue_em = Carbon::now()->format('Y-m-d H:i:s');
    $correspondencia->save();

    Event::fire( new AtividadeRastreavel('portaria_marcou_correspondencia_entregue', $correspondencia, Auth::portaria()->get()));

    // remover notificação
    NotificacaoMorador::where('correspondencias_id', $correspondencias_id)->delete();
  }

  public function getRemover(Request $request, $correspondencias_id)
  {
    $correspondencia = Correspondencia::findOrFail($correspondencias_id);
    Event::fire( new AtividadeRastreavel('portaria_removeu_correspondencia', $correspondencia, Auth::portaria()->get()));
    $correspondencia->delete();
  }

}
