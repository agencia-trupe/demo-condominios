<?php

namespace Gallery\Http\Controllers\Portaria\Dashboard;

use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Carbon\Carbon as Carbon;

use Gallery\Models\AgendamentoDeMudanca;
use Gallery\Models\EspacoReservaPorDiaria;
use Gallery\Models\FestaParticular;

use Gallery\Models\FestaParticularConvidados;
use Gallery\Models\EspacoReservaPorDiariaConvidados;

class DashboardController extends BasePortariaController
{

  public function getDashboard()
  {

  	$hoje = Carbon::now();

  	$mudancas = AgendamentoDeMudanca::with('unidade', 'morador')->agendadasParaHoje()->get();

  	$reservas_salao = EspacoReservaPorDiaria::semBloqueios()->agendadasParaHoje()->espaco(1)->get();

		$reservas_churrasqueira = EspacoReservaPorDiaria::semBloqueios()->agendadasParaHoje()->espaco(2)->get();

		$festas_particulares = FestaParticular::hoje()->get();

    return view('portaria.dashboard.index', [
    	'hoje' => $hoje,
    	'mudancas' => $mudancas,
    	'reservas_salao' => $reservas_salao,
    	'reservas_churrasqueira' => $reservas_churrasqueira,
			'festas_particulares' => $festas_particulares
    ]);
  }

  public function postTogglePresencaConvidados(Request $request)
  {
    $lista = $request->lista;
    $convidado_id = $request->convidado;
    $presenca = $request->presenca;

    if($lista == 'festas')
      $convidado = FestaParticularConvidados::find($convidado_id);
    elseif($lista == 'reservas')
      $convidado = EspacoReservaPorDiariaConvidados::find($convidado_id);

    $convidado->presenca = ($presenca == 1) ? 1 : 0;
    $convidado->save();
  }

}
