<?php

namespace Gallery\Http\Controllers\Portaria\Auth;

use Event;

use View;
use Validator;
use Gallery\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

use Gallery\Models\Porteiro;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $loginPath = 'portaria/auth/login';
    protected $redirectPath = 'portaria';
    protected $username = 'login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Filtro de Guest
        // As rotas desse controller só são acessíveis para
        // quem não estiver logado, exceto @getLogout
        $this->middleware('guest.portaria', ['except' => 'getLogout']);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('portaria.auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'login' => 'required|exists:porteiros,login',
            'password' => 'required',
        ]);

        if (\Auth::portaria()->attempt($request->only(['login', 'password']), $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, false);
        }

        return redirect($this->loginPath())
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => $this->getFailedLoginMessage(),
                ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        /**
        * O método intended() armazena as URL's de todos
        * os módulos no mesmo lugar, desabilitado para
        * não redirecionar errado
        * return redirect()->intended($this->redirectPath());
        */
        return redirect($this->redirectPath());
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        \Auth::portaria()->logout();

        return redirect('portaria/auth/login');
    }

}