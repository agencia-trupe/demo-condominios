<?php

namespace Gallery\Http\Controllers\Portaria\Veiculos;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Illuminate\Http\Request;
use Gallery\Http\Controllers\Portaria\BasePortariaController;

use Gallery\Models\Unidade;
use Gallery\Models\VeiculoDaUnidade;
use Gallery\Models\VeiculoDaUnidadePrivacidade;

class VeiculosController extends BasePortariaController
{

  public function getIndex(){
    $listaUnidadesJson = Unidade::get()->pluck('resumo')->toJson();

    return view('portaria.veiculos.index', ['listaUnidadesJson' => $listaUnidadesJson]);
  }

  public function postStore(Request $request){
    $this->validate($request, [
      'veiculos_da_unidade.unidade'      => 'required|unidade_valida',
      'veiculos_da_unidade.veiculo'      => 'required',
      'veiculos_da_unidade.cor'          => 'required',
      'veiculos_da_unidade.placa'        => 'required'
    ]);

    try {

      $veiculo = new VeiculoDaUnidade;

      $veiculo->veiculo = $request->input('veiculos_da_unidade.veiculo');
      $veiculo->cor = $request->input('veiculos_da_unidade.cor');
      $veiculo->placa = $request->input('veiculos_da_unidade.placa');
      $veiculo->vaga = $request->input('veiculos_da_unidade.vaga');

      $veiculo->unidades_id = Unidade::acharPorResumo($request->input('veiculos_da_unidade.unidade'))->first()->id;

      $veiculo->save();

      Event::fire( new AtividadeRastreavel('veiculo_adicionado_pela_portaria', $veiculo, Auth::portaria()->get()));

      $this->armazenaPrivacidade($veiculo, '1,2,4,5');

      $request->session()->flash('cadastro_veiculo', [
        'unidade' => $request->input('veiculos_da_unidade.unidade'),
        'veiculo' => $veiculo->veiculo,
        'cor' => $veiculo->cor,
        'placa' => $veiculo->placa,
        'vaga' => $veiculo->vaga
      ]);

      $request->session()->flash('veiculo_da_unidade_create', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('veiculo_da_unidade_create', false);
    }

    return redirect()->route('portaria.veiculos.index');
  }

  public function postConsulta(Request $request){
    return VeiculoDaUnidade::acharPorPlaca($request->placa)->get();
  }

  private function armazenaPrivacidade(VeiculoDaUnidade $veiculo, $valores_concatenados){

    $valores = explode(',', $valores_concatenados);

    foreach ($valores as $valor) {
      $veiculo->privacidade()->save(
        new VeiculoDaUnidadePrivacidade([
          'visibilidade' => $valor
        ])
      );
    }
  }
}
