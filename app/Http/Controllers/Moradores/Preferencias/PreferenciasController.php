<?php

namespace Gallery\Http\Controllers\Moradores\Preferencias;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Auth;
use Event;
use Hash;
use Validator;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Models\MoradorPreferencia;

class PreferenciasController extends BaseMoradoresController
{

  public function getIndex()
  {
  	$morador = Auth::moradores()->get();
    return view('moradores.preferencias.form', compact('morador'));
  }

  public function postAlterar(Request $request)
  {
  	try {

    	$preferencias = MoradorPreferencia::firstOrNew(['moradores_id' => Auth::moradores()->get()->id]);

      $preferencias->enviar_email_nova_correspondencia  = $request->has('preferencias.enviar_email_nova_correspondencia') ? 1 : 0;
      $preferencias->enviar_email_novo_aviso            = $request->has('preferencias.enviar_email_novo_aviso') ? 1 : 0;
      $preferencias->enviar_email_novo_documento        = $request->has('preferencias.enviar_email_novo_documento') ? 1 : 0;

      $preferencias->save();

      $request->session()->flash('preferencias_atualizadas', true);
      Event::fire( new AtividadeRastreavel('preferencias_atualizadas', $preferencias, Auth::moradores()->get()));

    } catch (\Exception $e) {
      $request->session()->flash('preferencias_atualizadas_fail', $e->getMessage());
    }

    return redirect()->route('moradores.preferencias.index');
  }
}
