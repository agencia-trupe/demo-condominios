<?php

namespace Gallery\Http\Controllers\Moradores\PessoasAutorizadas;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Gallery\Libs\Thumbs;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\PessoaAutorizada;
use Gallery\Models\PessoaAutorizadaPrivacidade;

class PessoasAutorizadasController extends BaseMoradoresController
{

  public function getIndex()
  {
  	$pessoas = PessoaAutorizada::daUnidade()->ordenado()->get();
    return view('moradores.pessoas-autorizadas.index', compact('pessoas'));
  }

    public function postStore(Request $request)
    {
      $this->validate($request, [
        'pessoas_autorizadas.nome'            => 'required',
        'pessoas_autorizadas.visibilidade'    => 'required',
        'pessoas_autorizadas.data_nascimento' => 'required|date_format:d/m/Y',
        'pessoas_autorizadas.parentesco'      => 'required',
        //'pessoas_autorizadas.foto'            => 'required'
      ]);

      $moradores_id = Auth::moradores()->user()->id;
      $unidades_id = Auth::moradores()->user()->unidade->id;

      try {

        $pessoa = new PessoaAutorizada;

        $visibilidade = $request->input('pessoas_autorizadas.visibilidade');

        $pessoa->nome = $request->input('pessoas_autorizadas.nome');
        $pessoa->data_nascimento = $request->input('pessoas_autorizadas.data_nascimento');
        $pessoa->parentesco = $request->input('pessoas_autorizadas.parentesco');
        $pessoa->foto = $request->input('pessoas_autorizadas.foto');
        $pessoa->moradores_id = $moradores_id;
        $pessoa->unidades_id = $unidades_id;

        $pessoa->save();

        $this->armazenaPrivacidade($pessoa, $visibilidade);

        Event::fire( new AtividadeRastreavel('pessoa_autorizada_inserida', $pessoa, Auth::moradores()->get()));

        $request->session()->flash('pessoa_autorizada_create', true);

      } catch (\Exception $e) {

        $request->flash();

        $request->session()->flash('pessoa_autorizada_create', false);

        $request->session()->flash('exception', $e->getMessage());
      }

      return redirect()->route('moradores.pessoas-autorizadas.index');
    }

    public function getEdit(Request $request, $pessoa_autorizada_id)
    {
      $pessoas = PessoaAutorizada::daUnidade()->ordenado()->get();
      $pessoa_autorizada = PessoaAutorizada::find($pessoa_autorizada_id);

      if(!$pessoa_autorizada) abort('404');

      return view('moradores.pessoas-autorizadas.edit', [
        'pessoas' => $pessoas,
        'pessoa_autorizada' => $pessoa_autorizada,
      ]);
    }


    public function postUpdate(Request $request, $pessoa_autorizada_id)
    {
      $this->validate($request, [
        'pessoas_autorizadas.nome'            => 'required',
        'pessoas_autorizadas.visibilidade'    => 'required',
        'pessoas_autorizadas.data_nascimento' => 'required|date_format:d/m/Y',
        'pessoas_autorizadas.parentesco'      => 'required',
        'pessoas_autorizadas.foto'            => 'required'
      ]);

      $moradores_id = Auth::moradores()->user()->id;
      $unidades_id = Auth::moradores()->user()->unidade->id;

      $pessoa = PessoaAutorizada::find($pessoa_autorizada_id);

      if(!$pessoa) abort('404');

      try {

        $visibilidade = $request->input('pessoas_autorizadas.visibilidade');

        $pessoa->nome = $request->input('pessoas_autorizadas.nome');
        $pessoa->data_nascimento = $request->input('pessoas_autorizadas.data_nascimento');
        $pessoa->parentesco = $request->input('pessoas_autorizadas.parentesco');
        $pessoa->foto = $request->input('pessoas_autorizadas.foto');
        $pessoa->moradores_id = $moradores_id;
        $pessoa->unidades_id = $unidades_id;

        $pessoa->save();

        Event::fire( new AtividadeRastreavel('pessoa_autorizada_alterada', $pessoa, Auth::moradores()->get()));

        $this->armazenaPrivacidade($pessoa, $visibilidade);

        $request->session()->flash('pessoa_autorizada_update', true);

      } catch (\Exception $e) {

        $request->flash();

        $request->session()->flash('pessoa_autorizada_update', false);
      }

      return redirect()->route('moradores.pessoas-autorizadas.index');
    }

    public function getDestroy(Request $request, $pessoa_id)
    {
      try {
        $pessoa = PessoaAutorizada::find($pessoa_id);
        $pessoa->delete();
        Event::fire( new AtividadeRastreavel('pessoa_autorizada_removida', $pessoa, Auth::moradores()->get()));
        $request->session()->flash('pessoa_autorizada_destroy', true);
      } catch (\Exception $e) {
        $request->session()->flash('pessoa_autorizada_destroy', false);
      }

      return redirect()->route('moradores.pessoas-autorizadas.index');
    }

    private function armazenaPrivacidade(PessoaAutorizada $pessoa, $valores_concatenados){

      $valores = explode(',', $valores_concatenados);

      if(sizeof($pessoa->privacidade)){
        foreach($pessoa->privacidade as $priv)
          $priv->delete();
      }

      foreach ($valores as $valor) {
        $pessoa->privacidade()->save(
          new PessoaAutorizadaPrivacidade([
            'visibilidade' => $valor
          ])
        );
      }
    }

    public function postUploadFoto(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'foto' => 'mimes:png,jpg,jpeg|max:4000'
      ]);

      /*
      * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
      * menor que o limite de upload do PHP no servidor, assim quando a imagem
      * for muito grande o erro lançado vai ser o do validador (tratado) e não
      * o do PHP (500 Internal Server Errror)
      */

      if ($validator->fails()) {
          return [
              'success' => 0,
              'msg' => $validator->errors()->first()
          ];
      }

      $arquivo = $request->file('foto');

      $path_original = "assets/images/moradores/pessoas-autorizadas/originais/";
      $path_asset    = "assets/images/moradores/pessoas-autorizadas/redimensionadas/";
      $path_thumb    = "assets/images/moradores/pessoas-autorizadas/thumbs/";
      $path_upload   = public_path($path_original);

      $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

      // Armazenar Original
      $arquivo->move($path_upload, $filename);

      $thumb = $path_thumb.$filename;

      // Armazenar Redimensionada
      Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
      // Armazenar Thumb
      Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

      if(file_exists(public_path($path_original.$filename)) &&
         file_exists(public_path($path_asset.$filename)) &&
         file_exists(public_path($path_thumb.$filename)))
      {
        return [
          'thumb' => $thumb,
          'filename' => $filename,
          'success' => 1,
          'msg' => ''
        ];
      }else{
        return [
          'success' => 0,
          'msg' => 'Não foi possível armazenar a imagem.'
        ];
      }
    }
}
