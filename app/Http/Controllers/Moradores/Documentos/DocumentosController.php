<?php

namespace Gallery\Http\Controllers\Moradores\Documentos;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Documento;
use Gallery\Models\DocumentoCategoria;

class DocumentosController extends BaseMoradoresController
{

    public function getIndex($categoria_slug = false)
    {
        $categoria_selecionada = DocumentoCategoria::findSlug($categoria_slug);

        if(!$categoria_slug || !$categoria_selecionada)
            $categoria_selecionada = DocumentoCategoria::ordenado()->first();

        $listaCategorias = DocumentoCategoria::ordenado()->get();

        return view('moradores.documentos.index', [
            'listaCategorias' => $listaCategorias,
            'categoria_selecionada' => $categoria_selecionada
        ]);
    }

    public function getDownload($documento_id)
    {
        $documento = Documento::find($documento_id);

        $arquivo = $documento->arquivo;
        $caminho_completo = "assets/documentos/{$arquivo}";

        if(file_exists($caminho_completo)){
            return response()->download($caminho_completo);
        }else{
            abort('404');
        }
    }
}