<?php

namespace Gallery\Http\Controllers\Moradores\Dashboard;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Morador;
use Gallery\Models\MoradorPrivacidade;

use Gallery\Models\Aviso;
use Gallery\Models\Documento;

class DashboardController extends BaseMoradoresController
{

  /**
  * Show the application login form.
  *
  * @return \Illuminate\Http\Response
  */

  public function getDashboard()
  {
    $id_usuario_logado = \Auth::moradores()->get()->id;

    return view('moradores.dashboard.index', [

      'avisos' => Aviso::whereDoesntHave('Morador', function ($query) use($id_usuario_logado) {
        $query->where('moradores.id', $id_usuario_logado);
      })
      ->ordenado()
      ->get(),

      'novidades' => Documento::ordenado()->take(3)->get()
    ]);
  }

}
