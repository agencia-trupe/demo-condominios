<?php

namespace Gallery\Http\Controllers\Moradores\FaleCom;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;
use Gallery\Events\MoradorEnviouMensagem;

use Gallery\Models\FaleCom;
use Gallery\Models\FaleComMensagem;

class FaleComController extends BaseMoradoresController
{

    public function getIndex()
    {
    	return view('moradores.fale-com.index', ['registro' => FaleCom::first()]);
    }

	public function getMensagem($destinatario)
	{
		$destinatarios = [
			'sindico'  => 'Síndico',
			'conselho' => 'Conselho',
			'zelador'  => 'Zelador',
			'portaria' => 'Portaria'
		];

		return view('moradores.fale-com.mensagem', [
		    'destinatario'  => $destinatario,
		    'destinatarios'	=> $destinatarios
		]);
	}

	public function postMensagem(Request $request, $destinatario)
	{
		$this->validate($request, [
      'falecom.assunto'  => 'required',
      'falecom.mensagem' => 'required'
    ]);

		try {

	    $mensagem = new FaleComMensagem;

			$mensagem->moradores_id = Auth::moradores()->user()->id;
			$mensagem->assunto 		= $request->input('falecom.assunto');
			$mensagem->mensagem 	= $request->input('falecom.mensagem');
			$mensagem->destino 		= $destinatario;

			$mensagem->save();

			Event::fire(new MoradorEnviouMensagem($mensagem));

			$request->session()->flash('fale_com_create', true);

      Event::fire( new AtividadeRastreavel('morador_envio_mensagem', $mensagem, Auth::moradores()->get()));

			return redirect()->route('moradores.fale-com.index', $destinatario);

		} catch (\Exception $e) {

			$request->flash();

      $request->session()->flash('fale_com_create', false);
      $request->session()->flash('fale_com_create_msg', $e->getMessage());

      return back();
		}

	}
}
