<?php

namespace Gallery\Http\Controllers\Moradores\AlterarSenha;

use Auth;
use Hash;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Morador;

class AlterarSenhaController extends BaseMoradoresController
{

  public function getIndex()
  {

  	$morador = Auth::moradores()->get();

    return view('moradores.alterar-senha.form', compact('morador'));
  }

  public function postAlterar(Request $request)
  {
  	Validator::extend('same_password', function($attribute, $value, $parameters, $validator) {
  		return Hash::check($value, Auth::moradores()->get()->senha);
    });

  	$this->validate($request, [
			'moradores.senha_atual'			=> 'required|same_password',
      'moradores.senha_nova'          => 'required|same:moradores.confirmacao_senha',
      'moradores.confirmacao_senha'   => 'required'
    ]);

    $senha_atual = $request->input('moradores.senha_atual');
    $senha_nova = $request->input('moradores.senha_nova');

    try {

    	$id = Auth::moradores()->get()->id;
  		$morador = Morador::find($id);

      $morador->senha = $senha_nova;
      $morador->save();

      Event::fire( new AtividadeRastreavel('senha_morador_alterada', $morador, Auth::moradores()->get()));

      $request->session()->flash('senha_alterada', true);

    } catch (\Exception $e) {
      $request->session()->flash('senha_alterada_fail', $e->getMessage());
    }

    return redirect()->route('moradores.alterar-senha.index');
  }
}
