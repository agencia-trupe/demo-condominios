<?php

namespace Gallery\Http\Controllers\Moradores\Avisos;

use Event;
use Gallery\Events\AtividadeRastreavel;
use Auth;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\Aviso;
use Gallery\Models\AvisoLido;

class AvisosController extends BaseMoradoresController
{

  public function getIndex()
  {
    $id_usuario_logado = \Auth::moradores()->get()->id;

    $avisos_nao_lidos = Aviso::whereDoesntHave('Morador', function ($query) use($id_usuario_logado) {
                          $query->where('moradores.id', $id_usuario_logado);
                        })
                        ->ordenado()
                        ->get();

    $avisos_lidos = Aviso::whereHas('Morador', function ($query) use($id_usuario_logado) {
                      $query->where('moradores.id', $id_usuario_logado);
                    })
                    ->ordenado()
                    ->paginate(15);

    return view('moradores.avisos.index', [
      'avisos_nao_lidos'  => $avisos_nao_lidos,
      'avisos_lidos'      => $avisos_lidos,
    ]);
  }

  public function getDetalhes($avisos_id)
  {
    $id_usuario_logado = \Auth::moradores()->get()->id;
    $aviso = Aviso::find($avisos_id);

    if(!$aviso) abort('404');

    $check_lido = AvisoLido::where('moradores_id', $id_usuario_logado)
                           ->where('avisos_id', $avisos_id)
                           ->first();

    return view('moradores.avisos.detalhes', [
      'aviso'      => $aviso,
      'aviso_lido_em' => sizeof($check_lido) > 0 ? $check_lido->created_at->format('d/m/Y H:i \h') : false
    ]);
  }

  /**
   * Marca um aviso como lido, via requisição ajax
   *
   * @return \Illuminate\Http\Response
   */
  public function postLido(Request $request)
  {
  	$avisos_id = $request->input('avisos_id');
  	$moradores_id = \Auth::moradores()->get()->id;

  	// A marcação de LIDO deve obedecer as seguintes regras:
  	// o ID do aviso é obrigatório
  	// o ID do aviso deve estar presente na tabela de avisos
  	// o ID do aviso NÃO deve estar presente na tabela de avisos_lidos para o usuário autenticado
  	$this->validate($request, [
      'avisos_id' => "required|exists:avisos,id|unique:avisos_lidos,avisos_id,NULL,id,moradores_id,{$moradores_id}",
    ]);

    $aviso = new AvisoLido();

    $aviso->avisos_id = $avisos_id;
    $aviso->moradores_id = $moradores_id;
    $aviso->save();

    Event::fire( new AtividadeRastreavel('aviso_lido', $aviso, Auth::moradores()->get()));

    // Salvar como lido e retornar created_at
    return [
      'status' => 200,
      'lido_em' => $aviso->created_at->format('d/m/y · H:i \h')
    ];
  }

}
