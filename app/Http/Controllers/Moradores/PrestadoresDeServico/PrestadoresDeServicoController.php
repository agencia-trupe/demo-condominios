<?php

namespace Gallery\Http\Controllers\Moradores\PrestadoresDeServico;

use Auth;
use Event;
use Validator;
use Gallery\Events\AtividadeRastreavel;
use Gallery\Libs\Thumbs;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\PrestadorDeServico;
use Gallery\Models\PrestadorDeServicoPrivacidade;

class PrestadoresDeServicoController extends BaseMoradoresController
{

    public function getIndex()
    {
    	$prestadores_de_servico = PrestadorDeServico::daUnidade()
    								                ->ordenado()
    								                ->get();

        return view('moradores.prestadores-de-servico.index', compact('prestadores_de_servico'));
    }

    public function postStore(Request $request)
    {
        $this->validate($request, [
            'prestadores_de_servico.nome'         => 'required',
            'prestadores_de_servico.visibilidade' => 'required',
            'prestadores_de_servico.documento'    => 'required',
            //'prestadores_de_servico.empresa'      => 'required',
            'prestadores_de_servico.data_nascimento'   => 'sometimes|date_format:d/m/Y',
            'prestadores_de_servico.permissao_inicio'  => 'required|date_format:d/m/Y',
            'prestadores_de_servico.permissao_termino' => 'required|date_format:d/m/Y|after:prestadores_de_servico.permissao_inicio'
        ]);

        $moradores_id = Auth::moradores()->user()->id;
        $unidades_id = Auth::moradores()->user()->unidade->id;

        try {

            $prestador = new PrestadorDeServico;

            $visibilidade = $request->input('prestadores_de_servico.visibilidade');

            $prestador->nome = $request->input('prestadores_de_servico.nome');
            $prestador->documento = $request->input('prestadores_de_servico.documento');
            $prestador->empresa = $request->input('prestadores_de_servico.empresa');

            if($request->has('prestadores_de_servico.data_nascimento'))
                $prestador->data_nascimento = $request->input('prestadores_de_servico.data_nascimento');
            else
                $prestador->data_nascimento = null;

            $prestador->permissao_inicio = $request->input('prestadores_de_servico.permissao_inicio');
            $prestador->permissao_termino = $request->input('prestadores_de_servico.permissao_termino');

            $prestador->moradores_id = $moradores_id;
            $prestador->unidades_id = $unidades_id;

            $prestador->save();

            $this->armazenaPrivacidade($prestador, $visibilidade);

            Event::fire( new AtividadeRastreavel('prestador_servico_inserido', $prestador, Auth::moradores()->get()));

            $request->session()->flash('prestador_de_servico_create', true);

        } catch (\Exception $e) {

            $request->flash();

            $request->session()->flash('prestador_de_servico_create', false);

            $request->session()->flash('exception', $e->getMessage());
        }

        return redirect()->route('moradores.prestadores-de-servico.index');
    }

    public function getEdit(Request $request, $prestador_de_servico_id)
    {
        $prestadores_de_servico = PrestadorDeServico::daUnidade()->ordenado()->get();
        $prestador_de_servico = PrestadorDeServico::find($prestador_de_servico_id);

        if(!$prestador_de_servico) abort('404');

        return view('moradores.prestadores-de-servico.index', [
            'prestadores_de_servico' => $prestadores_de_servico,
            'prestador_de_servico' => $prestador_de_servico,
        ]);
    }


    public function postUpdate(Request $request, $prestador_de_servico_id)
    {
        $this->validate($request, [
            'prestadores_de_servico.nome'         => 'required',
            'prestadores_de_servico.visibilidade' => 'required',
            'prestadores_de_servico.documento'    => 'required',
            //'prestadores_de_servico.empresa'      => 'required',
            'prestadores_de_servico.data_nascimento'   => 'sometimes|date_format:d/m/Y',
            'prestadores_de_servico.permissao_inicio'  => 'required|date_format:d/m/Y',
            'prestadores_de_servico.permissao_termino' => 'required|date_format:d/m/Y|after:prestadores_de_servico.permissao_inicio'
        ]);

        $moradores_id = Auth::moradores()->user()->id;
        $unidades_id = Auth::moradores()->user()->unidade->id;

        $prestador = PrestadorDeServico::find($prestador_de_servico_id);

        if(!$prestador) abort('404');

        try {

            $visibilidade = $request->input('prestadores_de_servico.visibilidade');

            $prestador->nome = $request->input('prestadores_de_servico.nome');
            $prestador->documento = $request->input('prestadores_de_servico.documento');
            $prestador->empresa = $request->input('prestadores_de_servico.empresa');

            if($request->has('prestadores_de_servico.data_nascimento'))
                $prestador->data_nascimento = $request->input('prestadores_de_servico.data_nascimento');
            else
                $prestador->data_nascimento = null;

            $prestador->permissao_inicio = $request->input('prestadores_de_servico.permissao_inicio');
            $prestador->permissao_termino = $request->input('prestadores_de_servico.permissao_termino');

            $prestador->moradores_id = $moradores_id;
            $prestador->unidades_id = $unidades_id;

            $prestador->save();

            $this->armazenaPrivacidade($prestador, $visibilidade);

            $request->session()->flash('prestador_de_servico_update', true);

            Event::fire( new AtividadeRastreavel('prestador_servico_atualizado', $prestador, Auth::moradores()->get()));

        } catch (\Exception $e) {

            $request->flash();

            $request->session()->flash('prestador_de_servico_update', false);
        }

        return redirect()->route('moradores.prestadores-de-servico.index');
    }

    public function getDestroy(Request $request, $prestador_id)
    {
        try {
            $prestador = PrestadorDeServico::find($prestador_id);
            $prestador->delete();
            Event::fire( new AtividadeRastreavel('prestador_servico_removido', $prestador, Auth::moradores()->get()));
            $request->session()->flash('prestador_de_servico_destroy', true);
        } catch (\Exception $e) {
            $request->session()->flash('prestador_de_servico_destroy', false);
        }

        return redirect()->route('moradores.prestadores-de-servico.index');
    }

    private function armazenaPrivacidade(PrestadorDeServico $prestador, $valores_concatenados){

        $valores = explode(',', $valores_concatenados);

        if(sizeof($prestador->privacidade)){
            foreach($prestador->privacidade as $priv)
                $priv->delete();
        }

        foreach ($valores as $valor) {
            $prestador->privacidade()->save(
                new PrestadorDeServicoPrivacidade([
                    'visibilidade' => $valor
                ])
            );
        }
    }

}
