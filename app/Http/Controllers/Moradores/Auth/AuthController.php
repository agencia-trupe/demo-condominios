<?php

namespace Gallery\Http\Controllers\Moradores\Auth;

use DB;
use Log;
use View;
use Mail;
use Validator;
use Gallery\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Gallery\Libs\Thumbs;
use Illuminate\Contracts\Mail\Mailer;

use Gallery\Models\Unidade;
use Gallery\Models\Morador;
use Gallery\Models\MoradorPrivacidade;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $loginPath = 'moradores/auth/login';
    protected $redirectPath = 'moradores/';
    protected $username = 'email';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Filtro de Guest
        // As rotas desse controller só são acessíveis para
        // quem não estiver logado, exceto @getLogout
        $this->middleware('guest.moradores', ['except' => 'getLogout']);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        return view('moradores.auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
          'email' => 'required|email|exists:moradores,email,status,1',
          'password' => 'required',
        ]);

        $credenciais = [
          'email' => $request->input('email'),
          'password' => $request->input('password'),
          'status' => 1
        ];

        if (\Auth::moradores()->attempt($credenciais, $request->has('remember'))) {
          return $this->handleUserWasAuthenticated($request, false);
        }

        return redirect($this->loginPath())
                ->withInput($request->only($this->loginUsername(), 'remember'))
                ->withErrors([
                    $this->loginUsername() => $this->getFailedLoginMessage(),
                ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        /**
        * O método intended() armazena as URL's de todos
        * os módulos no mesmo lugar, desabilitado para
        * não redirecionar errado
        * return redirect()->intended($this->redirectPath());
        */
        return redirect($this->redirectPath());
    }


    /**
     * Handle a image upload to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JSON
     */
    public function postUploadFoto(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'foto' => 'mimes:png,jpg,jpeg|max:4000'
      ]);

      /*
      * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
      * menor que o limite de upload do PHP no servidor, assim quando a imagem
      * for muito grande o erro lançado vai ser o do validador (tratado) e não
      * o do PHP (500 Internal Server Errror)
      */

      if ($validator->fails()) {
        return [
          'success' => 0,
          'msg' => $validator->errors()->first()
        ];
      }

      $arquivo = $request->file('foto');

      $path_original = "assets/images/moradores/fotos/originais/";
      $path_asset    = "assets/images/moradores/fotos/redimensionadas/";
      $path_thumb    = "assets/images/moradores/fotos/thumbs/";
      $path_upload   = public_path($path_original);

      $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

      // Armazenar Original
      $arquivo->move($path_upload, $filename);

      $thumb = $path_thumb.$filename;

      // Armazenar Redimensionada
      Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
      // Armazenar Thumb
      Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

      if(file_exists(public_path($path_original.$filename)) &&
         file_exists(public_path($path_asset.$filename)) &&
         file_exists(public_path($path_thumb.$filename)))
      {
        return [
            'thumb' => $thumb,
            'filename' => $filename,
            'success' => 1,
            'msg' => ''
        ];
      }else{
        return [
            'success' => 0,
            'msg' => 'Não foi possível armazenar a imagem.'
        ];
      }
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        \Auth::moradores()->logout();

        return redirect('moradores/auth/login');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('moradores.auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'unidade.numero_unidade'                    => 'required',
            'unidade.bloco'                             => 'required',
            'unidade.nome_proprietario'                 => 'required',
            'moradores.nome'                            => 'required',
            'moradores.apelido'                         => 'required',
            'moradores.relacao_unidade'                 => 'required|in:locatario,proprietario',
            'moradores.data_nascimento'                 => 'required|date_format:d/m/Y',
            'moradores.data_mudanca'                    => 'required|date_format:d/m/Y',
            'moradores.email'                           => 'required|email|unique:moradores',
            'moradores.visibilidade_email'              => 'required',
            'moradores.senha'                           => 'required|same:moradores.confirmacao_senha',
            'moradores.confirmacao_senha'               => 'required',
            'moradores.telefone_fixo'                   => 'required',
            'moradores.visibilidade_telefone_fixo'      => 'required',
            'moradores.telefone_celular'                => 'required',
            'moradores.visibilidade_telefone_celular'   => 'required',
            'moradores.telefone_comercial'              => 'required',
            'moradores.visibilidade_telefone_comercial' => 'required'
        ]);

        // Verifica dados da unidade

        $unidade['numero_unidade'] = $request->input('unidade.numero_unidade');
        $unidade['bloco'] = $request->input('unidade.bloco');
        $unidade['nome_proprietario'] = $request->input('unidade.nome_proprietario');

        $unidade_informada = Unidade::ordenacaoNatural()->first();
        //$unidade_informada = Unidade::checar($unidade)->first();

        // if(!$unidade_informada){
        //     $request->flash();
        //     $request->session()->flash('cadastro_fail', "Dados da unidade inválidos");
        //     return redirect('moradores/auth/register');
        // }

        $dados_usuario = $request->input('moradores');
        $dados_usuario_filtrados = array_except($dados_usuario, [
            'confirmacao_senha',
            'visibilidade_email',
            'visibilidade_telefone_fixo',
            'visibilidade_telefone_celular',
            'visibilidade_telefone_comercial'
        ]);

        try {

            $dados_usuario['unidades_id'] = $unidade_informada->id;
            $morador = Morador::create($dados_usuario);

            //$morador->activation_code = str_random(30);
            $morador->status = 1;
            $morador->activation_code = '';
            $morador->save();

            // armazenar - configurações de visibilidade
            $this->armazenarPrivacidade($morador, $request);

            // enviar e-mail de confirmação
            $this->enviarEmailConfirmacao($morador);

            $request->session()->flash('cadastro_realizado', true);

        } catch (\Exception $e) {

            $request->session()->flash('cadastro_fail', $e->getMessage());
        }

        return redirect('moradores/auth/register');
    }

    private function armazenarPrivacidade(Morador $morador, Request $request)
    {
        $visibilidades = [
            'email' => $request->input('moradores.visibilidade_email'),
            'telefone_fixo' => $request->input('moradores.visibilidade_telefone_fixo'),
            'telefone_celular' => $request->input('moradores.visibilidade_telefone_celular'),
            'telefone_comercial' => $request->input('moradores.visibilidade_telefone_comercial')
        ];

        foreach ($visibilidades as $dado => $valores_concatenados) {
            $valores = explode(',', $valores_concatenados);
            foreach ($valores as $valor) {
                $morador->privacidade()->save(
                    new MoradorPrivacidade([
                        'dado' => $dado,
                        'visibilidade' => $valor
                    ])
                );
            }
        }
    }

    private function enviarEmailConfirmacao(Morador $morador)
    {
      $template_email = 'moradores.templates.emails.register-confirmacao';

      $data['morador'] = $morador;

      Mail::send( $template_email, $data, function ($m) use ($morador) {
              $m->to($morador->email, $morador->nome)
                ->subject('Ativação de Cadastro - Gallery Online');
          }
      );

      if(config('mail.pretend')) Log::info(View::make($template_email, $data)->render());
    }

    public function getAtivar(Request $request, $email, $activation_code)
    {
        $morador = Morador::where('email', $email)
                          ->where('activation_code', $activation_code)
                          ->first();

        if($morador){

            $morador->status = 1;
            $morador->activation_code = '';
            $morador->save();

            $request->session()->flash('ativacao_realizada', true);

        }else{

            $request->session()->flash('ativacao_fail', true);

        }

        return redirect('moradores/auth/login');
    }
}
