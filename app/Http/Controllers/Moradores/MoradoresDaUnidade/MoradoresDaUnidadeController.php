<?php

namespace Gallery\Http\Controllers\Moradores\MoradoresDaUnidade;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Libs\Thumbs;

use Gallery\Models\Morador;
use Gallery\Models\MoradorDaUnidade;
use Gallery\Models\MoradorDaUnidadePrivacidade;

class MoradoresDaUnidadeController extends BaseMoradoresController
{

  public function getIndex()
  {
    $moradores['com_perfil'] = Morador::outrosDaUnidade()->get();
    $moradores['da_unidade'] = MoradorDaUnidade::daUnidade()->ordenado()->get();

    $lista_nomes_cadastros = Morador::outrosDaUnidade()->get()->pluck('nome')->toArray();

    return view('moradores.moradores-da-unidade.index', compact('moradores', 'lista_nomes_cadastros'));
  }

  public function postStore(Request $request)
  {
    $this->validate($request, [
      'moradores_da_unidade.nome'            => 'required',
      'moradores_da_unidade.visibilidade'    => 'required',
      'moradores_da_unidade.data_nascimento' => 'required|date_format:d/m/Y',
      'moradores_da_unidade.parentesco'      => 'required'
      //'moradores_da_unidade.foto'            => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

    try {

      $morador = new MoradorDaUnidade;

      $visibilidade = $request->input('moradores_da_unidade.visibilidade');

      $morador->nome = $request->input('moradores_da_unidade.nome');
      $morador->data_nascimento = $request->input('moradores_da_unidade.data_nascimento');
      $morador->parentesco = $request->input('moradores_da_unidade.parentesco');
      $morador->foto = $request->input('moradores_da_unidade.foto');
      $morador->moradores_id = $moradores_id;
      $morador->unidades_id = $unidades_id;

      $morador->save();

      Event::fire( new AtividadeRastreavel('morador_unidade_inserido', $morador, Auth::moradores()->get()));

      $this->armazenaPrivacidade($morador, $visibilidade);

      $request->session()->flash('morador_da_unidade_create', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('morador_da_unidade_create', false);
    }

    return redirect()->route('moradores.moradores-da-unidade.index');
  }

  public function getEdit(Request $request, $morador_id)
  {
    $moradores['com_perfil'] = Morador::outrosDaUnidade()->get();
    $moradores['da_unidade'] = MoradorDaUnidade::daUnidade()->ordenado()->get();
    $morador_da_unidade = MoradorDaUnidade::find($morador_id);

    if(!$morador_da_unidade) abort('404');

    $lista_nomes_cadastros = Morador::outrosDaUnidade()->get()->pluck('nome')->toArray();

    return view('moradores.moradores-da-unidade.edit', [
      'moradores' => $moradores,
      'morador_da_unidade' => $morador_da_unidade,
      'lista_nomes_cadastros' => $lista_nomes_cadastros
    ]);
  }


  public function postUpdate(Request $request, $morador_id)
  {
    $this->validate($request, [
      'moradores_da_unidade.nome'            => 'required',
      'moradores_da_unidade.visibilidade'    => 'required',
      'moradores_da_unidade.data_nascimento' => 'required|date_format:d/m/Y',
      'moradores_da_unidade.parentesco'      => 'required',
      'moradores_da_unidade.foto'            => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

    $morador = MoradorDaUnidade::find($morador_id);

    if(!$morador) abort('404');

    try {

      $visibilidade = $request->input('moradores_da_unidade.visibilidade');

      $morador->nome = $request->input('moradores_da_unidade.nome');
      $morador->data_nascimento = $request->input('moradores_da_unidade.data_nascimento');
      $morador->parentesco = $request->input('moradores_da_unidade.parentesco');
      $morador->foto = $request->input('moradores_da_unidade.foto');
      $morador->moradores_id = $moradores_id;
      $morador->unidades_id = $unidades_id;

      $morador->save();

      Event::fire( new AtividadeRastreavel('morador_unidade_alterado', $morador, Auth::moradores()->get()));

      $this->armazenaPrivacidade($morador, $visibilidade);

      $request->session()->flash('morador_da_unidade_update', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('morador_da_unidade_update', false);
    }

    return redirect()->route('moradores.moradores-da-unidade.index');
  }

  public function getDestroy(Request $request, $morador_id)
  {
    try {
      $morador = MoradorDaUnidade::find($morador_id);
      $morador->delete();

      Event::fire( new AtividadeRastreavel('morador_unidade_removido', $morador, Auth::moradores()->get()));

      $request->session()->flash('morador_da_unidade_destroy', true);
    } catch (\Exception $e) {
      $request->session()->flash('morador_da_unidade_destroy', false);
    }

    return redirect()->route('moradores.moradores-da-unidade.index');
  }

  private function armazenaPrivacidade(MoradorDaUnidade $morador, $valores_concatenados){

    $valores = explode(',', $valores_concatenados);

    if(sizeof($morador->privacidade)){
      foreach($morador->privacidade as $priv)
        $priv->delete();
    }

    foreach ($valores as $valor) {
      $morador->privacidade()->save(
        new MoradorDaUnidadePrivacidade([
          'visibilidade' => $valor
        ])
      );
    }
  }

  public function postUploadFoto(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'foto' => 'mimes:png,jpg,jpeg|max:4000'
    ]);

    /*
    * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
    * menor que o limite de upload do PHP no servidor, assim quando a imagem
    * for muito grande o erro lançado vai ser o do validador (tratado) e não
    * o do PHP (500 Internal Server Errror)
    */

    if ($validator->fails()) {
      return [
        'success' => 0,
        'msg' => $validator->errors()->first()
      ];
    }

    $arquivo = $request->file('foto');

    $path_original = "assets/images/moradores/moradores-da-unidade/originais/";
    $path_asset    = "assets/images/moradores/moradores-da-unidade/redimensionadas/";
    $path_thumb    = "assets/images/moradores/moradores-da-unidade/thumbs/";
    $path_upload   = public_path($path_original);

    $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada
    Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

    if(file_exists(public_path($path_original.$filename)) &&
       file_exists(public_path($path_asset.$filename)) &&
       file_exists(public_path($path_thumb.$filename)))
    {
      return [
          'thumb' => $thumb,
          'filename' => $filename,
          'success' => 1,
          'msg' => ''
      ];
    }else{
      return [
        'success' => 0,
        'msg' => 'Não foi possível armazenar a imagem.'
      ];
    }
  }
}
