<?php

namespace Gallery\Http\Controllers\Moradores\Ocorrencias;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Gate;
use Auth;
use Event;

use Gallery\Events\MoradorCriouOcorrencia;
use Gallery\Events\MoradorVisualizouOcorrencias;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Models\Ocorrencia;

class OcorrenciasController extends BaseMoradoresController
{

  public function getIndex()
  {
    Event::fire(new MoradorVisualizouOcorrencias());

    return view('moradores.ocorrencias.index', [
      'ocorrencias' => Ocorrencia::mensagensIniciais()->publicas(Auth::moradores()->get()->id)->ordenado()->paginate(15)
    ]);
  }


    public function postRegistrar(Request $request)
    {
      $this->validate($request, [
        'ocorrencias.texto' => 'required|min:10',
        'ocorrencias.ocorrencia_publica' => 'required|in:0,1'
      ]);

      $ocorrencia = new Ocorrencia();

      $ocorrencia->texto = $request->input('ocorrencias.texto');
      $ocorrencia->autor_id = Auth::moradores()->get()->id;
      $ocorrencia->moradores_id = null;
      $ocorrencia->em_resposta_a = null;
      $ocorrencia->origem = 'morador';
      $ocorrencia->ocorrencia_publica = $request->input('ocorrencias.ocorrencia_publica');

      try {

        $ocorrencia->save();

        $request->session()->flash('registrar_ocorrencia_sucesso', true);

        Event::fire(new MoradorCriouOcorrencia($ocorrencia));
        Event::fire( new AtividadeRastreavel('ocorrencia_registrada_por_morador', $ocorrencia, Auth::moradores()->get()));

        return redirect()->route('moradores.ocorrencias.index');

      } catch (\Exception $e) {

        $request->flash();

        $request->session()->flash('registrar_ocorrencia_erro', 'Erro ao registrar a ocorrência! ('.$e->getMessage().')');

        return back();

      }
    }

    public function postReponder(Request $request, $ocorrencia_id)
    {
        $this->validate($request, [
            'resposta.'.$ocorrencia_id.'.texto' => 'required|min:10'
        ]);

        if (Gate::forUser(Auth::moradores()->get())->denies('responder', Ocorrencia::find($ocorrencia_id))) {
            $request->flash();
            $request->session()->flash('registrar_ocorrencia_erro', 'Erro ao registrar a ocorrência! (403)');
            return back();
        }

        $ocorrencia = new Ocorrencia();

        $ocorrencia->texto = $request->input('resposta.'.$ocorrencia_id.'.texto');
        $ocorrencia->autor_id = Auth::moradores()->get()->id;
        $ocorrencia->moradores_id = null;
        $ocorrencia->em_resposta_a = $ocorrencia_id;
        $ocorrencia->origem = 'morador';

        try {

            $ocorrencia->save();

            $request->session()->flash('registrar_ocorrencia_resposta_sucesso', true);
            Event::fire( new AtividadeRastreavel('ocorrencia_respondida_por_morador', $ocorrencia, Auth::moradores()->get()));
            Event::fire(new MoradorCriouOcorrencia($ocorrencia));

            return redirect()->route('moradores.ocorrencias.index');

        } catch (\Exception $e) {

            $request->flash();

            $request->session()->flash('registrar_ocorrencia_erro', 'Erro ao registrar a ocorrência! ('.$e->getMessage().')');

            return back();

        }
    }

    public function postFinalizar(Request $request)
    {
        $this->validate($request, [
            'ocorrencia_id' => 'required|exists:ocorrencias,id'
        ]);

        $ocorrencia = Ocorrencia::find($request->input('ocorrencia_id'));

        if (Gate::forUser(Auth::moradores()->get())->denies('finalizar', $ocorrencia)) {
            return ['erro' => 'não autorizado'];
        }

        $ocorrencia->finalizada = 1;
        $ocorrencia->finalizada_em = date('Y-m-d H:i:s');
        $ocorrencia->finalizada_por = Auth::moradores()->get()->id;
        $ocorrencia->save();

        foreach($ocorrencia->respostas as $resposta){
            $resposta->finalizada = 1;
            $resposta->finalizada_em = date('Y-m-d H:i:s');
            $resposta->finalizada_por = Auth::moradores()->get()->id;
            $resposta->save();
        }

        Event::fire( new AtividadeRastreavel('ocorrencia_finalizada_por_morador', $ocorrencia, Auth::moradores()->get()));

        return [
          'status' => 200,
          'finalizada_em' => $ocorrencia->finalizada_em->format('d/m/y · H:i \h'),
          'finalizada_por' => Auth::moradores()->get()->getNomeCompleto()
        ];
    }

}
