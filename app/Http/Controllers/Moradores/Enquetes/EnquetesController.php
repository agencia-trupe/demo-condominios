<?php

namespace Gallery\Http\Controllers\Moradores\Enquetes;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Gallery\Models\Enquete;
use Gallery\Models\EnqueteVotos;

class EnquetesController extends BaseMoradoresController
{

  public function getIndex()
  {
    $enquete_ativa = Enquete::ativa()->first();

    $historico = Enquete::historico()->ordenado()->get();

    return view('moradores.enquetes.index', compact('enquete_ativa', 'historico'));
  }

  public function postVotar(Request $request)
  {
    $enquete_ativa = Enquete::ativa()->first();

    if(!$enquete_ativa) return $redirect()->back();

    $check_voto = EnqueteVotos::moradorLogado()->enquete($enquete_ativa->id)->get();

    if(sizeof($check_voto) == 0){

      $voto = $enquete_ativa->votos()->create([
        'moradores_id' => Auth::moradores()->get()->id,
        'enquete_opcoes_id' => $request->input('voto_enquete')
      ]);

      $request->session()->flash('voto_na_enquete_computado', true);
      Event::fire( new AtividadeRastreavel('voto_enquete', $voto, Auth::moradores()->get()));
      
    }else{
      $request->session()->flash('voto_na_enquete_computado', false);
    }

    return redirect()->route('moradores.enquetes.resultados', $enquete_ativa->id);
  }

  public function getResultados(Enquete $enquete)
  {
    return view('moradores.enquetes.resultados', compact('enquete'));
  }

}
