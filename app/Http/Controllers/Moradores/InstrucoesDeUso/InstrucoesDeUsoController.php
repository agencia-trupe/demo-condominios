<?php

namespace Gallery\Http\Controllers\Moradores\InstrucoesDeUso;

use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Http\Controllers\Controller;

use Gallery\Models\InstrucoesDeUso;

class InstrucoesDeUsoController extends BaseMoradoresController
{
    public function getIndex()
    {
        return view('moradores.instrucoes-de-uso.index', ['instrucoes' => InstrucoesDeUso::ordenado()->get()]);
    }
}