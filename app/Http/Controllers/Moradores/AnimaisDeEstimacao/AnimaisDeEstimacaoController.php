<?php

namespace Gallery\Http\Controllers\Moradores\AnimaisDeEstimacao;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;
use Gallery\Libs\Thumbs;

use Gallery\Models\AnimalDeEstimacao;
use Gallery\Models\AnimalDeEstimacaoPrivacidade;

class AnimaisDeEstimacaoController extends BaseMoradoresController
{

  public function getIndex()
  {
    $animais = AnimalDeEstimacao::daUnidade()->ordenado()->get();
    return view('moradores.animais-de-estimacao.index', compact('animais'));
  }

  public function postStore(Request $request)
  {
    $this->validate($request, [
      'animais_de_estimacao.nome'        => 'required',
      'animais_de_estimacao.visibilidade'=> 'required',
      'animais_de_estimacao.especie'     => 'required',
      'animais_de_estimacao.sexo'        => 'required',
      'animais_de_estimacao.foto'        => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

    try {

      $animal = new AnimalDeEstimacao;

      $visibilidade = $request->input('animais_de_estimacao.visibilidade');

      $animal->nome    = $request->input('animais_de_estimacao.nome');
      $animal->especie = $request->input('animais_de_estimacao.especie');
      $animal->sexo    = $request->input('animais_de_estimacao.sexo');
      $animal->foto    = $request->input('animais_de_estimacao.foto');

      $animal->moradores_id = $moradores_id;
      $animal->unidades_id = $unidades_id;

      $animal->save();

      Event::fire( new AtividadeRastreavel('animal_estimacao_inserido', $animal, Auth::moradores()->get()));

      $this->armazenaPrivacidade($animal, $visibilidade);

      $request->session()->flash('animais_de_estimacao_create', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('animais_de_estimacao_create', false);
    }

    return redirect()->route('moradores.animais-de-estimacao.index');
  }

  public function getEdit(Request $request, $animal_id)
  {
    $animais = AnimalDeEstimacao::daUnidade()->ordenado()->get();
    $animal_de_estimacao = AnimalDeEstimacao::find($animal_id);

    if(!$animal_de_estimacao) abort('404');

    return view('moradores.animais-de-estimacao.edit', [
      'animais' => $animais,
      'animal_de_estimacao' => $animal_de_estimacao,
    ]);
  }

  public function postUpdate(Request $request, $animal_id)
  {
    $this->validate($request, [
      'animais_de_estimacao.nome'        => 'required',
      'animais_de_estimacao.visibilidade'=> 'required',
      'animais_de_estimacao.especie'     => 'required',
      'animais_de_estimacao.sexo'        => 'required',
      'animais_de_estimacao.foto'        => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

    $animal = AnimalDeEstimacao::find($animal_id);

    if(!$animal) abort('404');

    try {

      $visibilidade = $request->input('animais_de_estimacao.visibilidade');

      $animal->nome    = $request->input('animais_de_estimacao.nome');
      $animal->especie = $request->input('animais_de_estimacao.especie');
      $animal->sexo    = $request->input('animais_de_estimacao.sexo');
      $animal->foto    = $request->input('animais_de_estimacao.foto');

      $animal->moradores_id = $moradores_id;
      $animal->unidades_id = $unidades_id;

      $animal->save();

      Event::fire( new AtividadeRastreavel('animal_estimacao_alterado', $animal, Auth::moradores()->get()));

      $this->armazenaPrivacidade($animal, $visibilidade);

      $request->session()->flash('animais_de_estimacao_update', true);

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('animais_de_estimacao_update', false);
    }

    return redirect()->route('moradores.animais-de-estimacao.index');
  }

  public function getDestroy(Request $request, $animal_id)
  {
    try {
      $animal = AnimalDeEstimacao::find($animal_id);
      $animal->delete();
      Event::fire( new AtividadeRastreavel('animal_estimacao_removido', $animal, Auth::moradores()->get()));
      $request->session()->flash('animais_de_estimacao_destroy', true);
    } catch (\Exception $e) {
      $request->session()->flash('animais_de_estimacao_destroy', false);
    }

    return redirect()->route('moradores.animais-de-estimacao.index');
  }

  private function armazenaPrivacidade(AnimalDeEstimacao $animal, $valores_concatenados){

    $valores = explode(',', $valores_concatenados);

    if(sizeof($animal->privacidade)){
      foreach($animal->privacidade as $priv)
        $priv->delete();
    }

    foreach ($valores as $valor) {
      $animal->privacidade()->save(
        new AnimalDeEstimacaoPrivacidade([
          'visibilidade' => $valor
        ])
      );
    }
  }

  public function postUploadFoto(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'foto' => 'mimes:png,jpg,jpeg|max:4000'
    ]);

    /*
    * O limite de upload da imagem (4Mb inicialmente) deve sempre ser
    * menor que o limite de upload do PHP no servidor, assim quando a imagem
    * for muito grande o erro lançado vai ser o do validador (tratado) e não
    * o do PHP (500 Internal Server Errror)
    */

    if ($validator->fails()) {
      return [
        'success' => 0,
        'msg' => $validator->errors()->first()
      ];
    }

    $arquivo = $request->file('foto');

    $path_original = "assets/images/moradores/animais-de-estimacao/originais/";
    $path_asset    = "assets/images/moradores/animais-de-estimacao/redimensionadas/";
    $path_thumb    = "assets/images/moradores/animais-de-estimacao/thumbs/";
    $path_upload   = public_path($path_original);

    $filename = filename_slug($arquivo->getClientOriginalName(), $arquivo->getClientOriginalExtension());

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada
    Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

    if(file_exists(public_path($path_original.$filename)) &&
       file_exists(public_path($path_asset.$filename)) &&
       file_exists(public_path($path_thumb.$filename)))
    {
      return [
        'thumb' => $thumb,
        'filename' => $filename,
        'success' => 1,
        'msg' => ''
      ];
    }else{
      return [
        'success' => 0,
        'msg' => 'Não foi possível armazenar a imagem.'
      ];
    }
  }

}
