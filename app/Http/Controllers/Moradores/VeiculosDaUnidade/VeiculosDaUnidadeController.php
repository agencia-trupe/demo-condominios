<?php

namespace Gallery\Http\Controllers\Moradores\VeiculosDaUnidade;

use Auth;
use Event;
use Gallery\Events\AtividadeRastreavel;
use Validator;
use Gallery\Http\Controllers\Moradores\BaseMoradoresController;
use Illuminate\Http\Request;

use Gallery\Models\VeiculoDaUnidade;
use Gallery\Models\VeiculoDaUnidadePrivacidade;

class VeiculosDaUnidadeController extends BaseMoradoresController
{

  public function getIndex()
  {
    $veiculos = VeiculoDaUnidade::daUnidade()->ordenado()->get();

    return view('moradores.veiculos-da-unidade.index', compact('veiculos'));
  }

  public function postStore(Request $request)
  {
    $this->validate($request, [
      'veiculos_da_unidade.veiculo'      => 'required',
      'veiculos_da_unidade.visibilidade' => 'required',
      'veiculos_da_unidade.cor'          => 'required',
      'veiculos_da_unidade.placa'        => 'required'
    ]);

    $moradores_id = Auth::moradores()->user()->id;
    $unidades_id = Auth::moradores()->user()->unidade->id;

    try {

      $veiculo = new VeiculoDaUnidade;

      $visibilidade = $request->input('veiculos_da_unidade.visibilidade');

      $veiculo->veiculo = $request->input('veiculos_da_unidade.veiculo');
      $veiculo->cor = $request->input('veiculos_da_unidade.cor');
      $veiculo->placa = $request->input('veiculos_da_unidade.placa');
      $veiculo->vaga = $request->input('veiculos_da_unidade.vaga');

      $veiculo->moradores_id = $moradores_id;
      $veiculo->unidades_id = $unidades_id;

      $veiculo->save();

      $this->armazenaPrivacidade($veiculo, $visibilidade);

      $request->session()->flash('veiculo_da_unidade_create', true);

      Event::fire( new AtividadeRastreavel('veiculo_unidade_inserido', $veiculo, Auth::moradores()->get()));

    } catch (\Exception $e) {

      $request->flash();

      $request->session()->flash('veiculo_da_unidade_create', false);
    }

    return redirect()->route('moradores.veiculos-da-unidade.index');
  }

    public function getDestroy(Request $request, $veiculo_id)
    {
        try {
            $veiculo = VeiculoDaUnidade::find($veiculo_id);
            $veiculo->delete();
            Event::fire( new AtividadeRastreavel('veiculo_unidade_removido', $veiculo, Auth::moradores()->get()));
            $request->session()->flash('veiculo_da_unidade_destroy', true);
        } catch (\Exception $e) {
            $request->session()->flash('veiculo_da_unidade_destroy', false);
        }

        return redirect()->route('moradores.veiculos-da-unidade.index');
    }

    private function armazenaPrivacidade(VeiculoDaUnidade $veiculo, $valores_concatenados){

        $valores = explode(',', $valores_concatenados);

        foreach ($valores as $valor) {
            $veiculo->privacidade()->save(
                new VeiculoDaUnidadePrivacidade([
                    'visibilidade' => $valor
                ])
            );
        }
    }

}
