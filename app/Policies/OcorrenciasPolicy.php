<?php

namespace Gallery\Policies;

use Gallery\Models\Admin;
use Gallery\Models\Morador;
use Gallery\Models\Ocorrencia;
use Gallery\Models\OcorrenciaLida;

class OcorrenciasPolicy
{
    /**
     * Verifica se o usuário logado pode Responder a Ocorrência
     *
	 * Usuario pode responder a ocorrencia SE
	 * 1. A ultima resposta da ocorrência tem como origem 'admin'
	 * 2. A ocorrência está relacionada ao id do usuário
	 * 3. A ocorrência não pode estar finalizada
	 *
     * @return bool
     */
    public function responder(Morador $morador, Ocorrencia $ocorrencia)
    {
        $ultima_msg = $ocorrencia->respostas('desc')->first();

        // Se ainda não houver resposta, verificar se pode
        // responder a questão original
        if(!$ultima_msg) $ultima_msg = $ocorrencia;

        $origem = ($ultima_msg->origem == 'admin');
        $destinatario = ($ultima_msg->moradores_id == $morador->id);
        $nao_finalizada = $ultima_msg->finalizada == 0;

        return ($destinatario && $origem && $nao_finalizada);
    }

    /**
     * Verifica se o usuário logado pode Finalizar a Ocorrência
     *
     * Usuario pode finalizar a ocorrencia SE
     * 1. A ultima resposta da ocorrência tem como origem 'admin'
     * 2. A ocorrência está relacionada ao id do usuário
     * 3. A ocorrência não pode estar finalizada
     *
     * @return bool
     */
    public function finalizar(Morador $morador, Ocorrencia $ocorrencia)
    {
        $ultima_msg = $ocorrencia->respostas('desc')->first();

        // Se ainda não houver resposta, verificar se pode
        // responder a questão original
        if(!$ultima_msg) $ultima_msg = $ocorrencia;

        $origem = ($ultima_msg->origem == 'admin');
        $destinatario = ($ultima_msg->moradores_id == $morador->id);
        $nao_finalizada = $ultima_msg->finalizada == 0;

        return ($destinatario && $origem && $nao_finalizada);
    }

    /**
     * Verifica se o usuário da administração já visualizou a ocorrência
     *
	 * 1. A ocorrência deve ter sido originada por um morador
     * 2. Não deve haver um OcorrenciaLida para o usuário logado
	 *
     * @return bool
     */
    public function verInedita(Admin $admin, Ocorrencia $ocorrencia)
    {
        $check_lida = OcorrenciaLida::where('ocorrencias_lidas.ocorrencias_id', $ocorrencia->id)
                                    ->where('ocorrencias_lidas.administradores_id', $admin->id)
                                    ->select('ocorrencias.origem')
                                    ->leftJoin('ocorrencias', 'ocorrencias_lidas.ocorrencias_id', '=', 'ocorrencias.id')
                                    ->get();

        if(sizeof($check_lida) == 0){
            // Não lida

            $check_origem = Ocorrencia::find($ocorrencia->id);

            if(isset($check_origem->origem) && $check_origem->origem != 'admin')
                return true; // Não lida - inédita
            else
                return false;// Aberta pela administração

        }else{
            // Já lida
            return false;
        }
    }

    /**
     * Verifica se o usuário da administração tem resposta nova a uma ocorrência
     *
     * 1. A ocorrência precisa ter uma resposta
     * 3. A ocorrência deve ter o campo visto_em == NULL
     * 4. A ocorrência deve ter sido originada por um morador
     *
     * @return bool
     */
    public function verRespostaInedita(Admin $admin, Ocorrencia $ocorrencia)
    {
        $retorno = false;
        foreach($ocorrencia->respostas as $resposta){

            $ocorrencia = Ocorrencia::find($resposta->id);

            $check_lida = OcorrenciaLida::where('ocorrencias_id', $resposta->id)
                                    ->where('administradores_id', $admin->id)
                                    ->get();

            $retorno = sizeof($check_lida) == 0 && $ocorrencia->origem != 'admin' ? true : false;

        }
        return $retorno;
    }
}
