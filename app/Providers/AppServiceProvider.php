<?php

namespace Gallery\Providers;

use Gallery\Models\AgendamentoDeMudanca;
use Gallery\Models\FornecedorCategoria;
use Gallery\Models\Unidade;

use Validator;
use App;
use Auth;
use Carbon\Carbon as Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Validator::extend('agendamento_mudanca_data_valida', function($attribute, $value, $parameters, $validator) {
        // retorna true se:
        // Não houver agendamento para o mesmo bloco,
        // no mesmo período e na mesma data

        // attribute  : agendamento.data
        // $value     : 06/12/1986
        // parametros : periodo,bloco

        $bloco = Auth::moradores()->user()->unidade->bloco;

        $agendamentos = AgendamentoDeMudanca::where('data', Carbon::createFromFormat('d/m/Y', $value)->toDateString())
                                            ->where('periodo', $parameters[0])
                                            ->whereHas('unidade', function ($query) use ($bloco) {
                                                $query->where('bloco', $bloco);
                                            })
                                            ->get();

        return sizeof($agendamentos) > 0 ? false : true;
      });

      Validator::extend('data_maior_que_atual', function($attribute, $value, $parameters, $validator) {
        // retorna true se:
        // Data informada é maior que a atual

        $data_informada = Carbon::createFromFormat('d/m/Y', $value, 'America/Sao_Paulo');
        $data_atual = Carbon::now();

        // gte = greater then or equal ( >= )
        return $data_informada->gte($data_atual);
      });

      Validator::extend('categoria_de_fornecedores_disponiveis', function($attribute, $value, $parameters, $validator){
        /*
        $attribute : fornecedores.categoria
        $value : categoria selecionada
        */

        // retorna true se:
        // A categoria informada existe na base OU é == 'outros'
        if($value == 'outros') return true;
        return ! is_null(FornecedorCategoria::find($value));
      });

      Validator::extend('unidade_valida', function($attribute, $value, $parameters, $validator){
        return ! is_null( Unidade::acharPorResumo($value)->first() );
      });

      Validator::extend('after_equal', function($attribute, $value, $parameters) {
          return strtotime(\Input::get($parameters[0])) <= strtotime($value);
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
