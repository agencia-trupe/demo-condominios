<?php

namespace Gallery\Listeners;

use Log;
use DB;
use Gallery\Events\AtividadeRastreavel;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogarAtividade
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {

  }

  /**
   * Handle the event.
   *
   * @param  AtividadeRastreavel  $event
   * @return void
   */
  public function handle(AtividadeRastreavel $event)
  {
    $acao = $event->acao;
    $registro = $event->registro;
    $usuario = $event->usuario;

    if(isset($usuario->email) && $usuario->email != 'bruno@trupe.net'){

      Log::info('ação: ' . $acao);
      Log::info('registro: '. $registro);
      Log::info('usuário: '. $usuario);

      DB::table('log')->insert([
        'acao' => $acao,
        'registro' => $registro,
        'usuario' => $usuario,
        'created_at' => Date('Y-m-d H:i:s')
      ]);
    }
  }
}
