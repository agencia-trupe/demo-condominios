<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorAgendouMudanca;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminMudanca
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  MoradorAgendouMudanca  $event
     * @return void
     */
    public function handle(MoradorAgendouMudanca $event)
    {
        $morador = $event->agendamento->morador->getNomeCompleto();

        $notificar = [
            'zelador',
            'sindico',
            'master'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'          => 'mudanca',
                    'mensagem'                  => "{$morador} realizou um novo agendamento de mudança.",
                    'administradores_id'        => $admin->id,
                    'mudancas_agendamento_id'   => $event->agendamento->id
                ]);

            }
        }

    }
}
