<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorReservouEspacoPorDiaria;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminReservaPorDiaria
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MoradorReservouEspacoPorDiaria  $event
     * @return void
     */
    public function handle(MoradorReservouEspacoPorDiaria $event)
    {
        $morador = $event->reserva->morador->getNomeCompleto();

        $notificar = [
            'sindico',
            'zelador',
            'master'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'          => 'reserva_diaria',
                    'mensagem'                  => "{$morador} realizou uma nova reserva.",
                    'administradores_id'        => $admin->id,
                    'espacos_reservas_por_diaria_id'   => $event->reserva->id
                ]);

            }
        }

    }
}
