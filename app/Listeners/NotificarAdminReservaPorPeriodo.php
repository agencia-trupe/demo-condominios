<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorReservouEspacoPorPeriodo;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminReservaPorPeriodo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MoradorReservouEspacoPorPeriodo  $event
     * @return void
     */
    public function handle(MoradorReservouEspacoPorPeriodo $event)
    {
        $morador = $event->reserva->morador->getNomeCompleto();

        $notificar = [
            'sindico',
            'zelador',
            'master'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'          => 'reserva_periodo',
                    'mensagem'                  => "{$morador} realizou uma nova reserva.",
                    'administradores_id'        => $admin->id,
                    'espacos_reservas_por_periodo_id'   => $event->reserva->id
                ]);

            }
        }

    }
}
