<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorCancelouDiariaComMulta;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminCancelamentoComMulta
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MoradorCancelouDiariaComMulta  $event
     * @return void
     */
    public function handle(MoradorCancelouDiariaComMulta $event)
    {
        $morador = $event->reserva->morador->getNomeCompleto();

        $notificar = [
            'sindico',
            'zelador',
            'master'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'    => 'reserva_cancelada_com_multa',
                    'mensagem'            => "{$morador} cancelou uma reserva gerando multa.",
                    'administradores_id'  => $admin->id,
                    'diaria_com_multa_id' => $event->reserva->id
                ]);

            }
        }
    }
}