<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouReservaPorDiaria;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\EspacoReservaPorDiariaLido;

class ExcluirNotificacaoReservaPorDiaria
{

    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoAdmin $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouReservaPorDiaria  $event
     * @return void
     */
    public function handle(AdminVisualizouReservaPorDiaria $event)
    {
        $link_visitado = 'admin/reservas-de-espacos/'.$event->reserva->id.'?tipo=diaria';

        $this->notificacao
             ->reservaDeEspacoDiaria()
             ->paraAdministrador(Auth::admin()->get()->id)
             ->where('espacos_reservas_por_diaria_id', $event->reserva->id)
             ->delete();

        // Marcar Mensagem original como lida se
        // não tiver sido lida ainda
        $check_lida = EspacoReservaPorDiariaLido::where('espacos_reservas_id', $event->reserva->id)
                                                ->where('administradores_id', Auth::admin()->get()->id)
                                                ->get();

        if(sizeof($check_lida) == 0){
            EspacoReservaPorDiariaLido::create([
                'administradores_id'  => Auth::admin()->get()->id,
                'espacos_reservas_id' => $event->reserva->id
            ]);
        }
    }
}
