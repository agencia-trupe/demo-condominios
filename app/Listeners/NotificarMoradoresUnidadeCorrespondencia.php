<?php

namespace Gallery\Listeners;

use Gallery\Events\PortariaCadastrouCorrespondencia;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoMorador;
use Gallery\Models\Unidade;
use Gallery\Listeners\Traits\NotificarMoradorPorEmail;

class NotificarMoradoresUnidadeCorrespondencia
{

  use NotificarMoradorPorEmail;

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {

  }

  /**
   * Handle the event.
   *
   * @param  PortariaCadastrouCorrespondencia  $event
   * @return void
   */
  public function handle(PortariaCadastrouCorrespondencia $event)
  {
    $unidade = Unidade::find($event->correspondencia->unidades_id);

    foreach ($unidade->moradores as $key => $morador) {

      $notificacao = new NotificacaoMorador;
      $notificacao->tipo_notificacao      = 'correspondencia';
      $notificacao->mensagem              = "Tem correspondência que precisa da sua assinatura na portaria.";
      $notificacao->moradores_id          = $morador->id;
      $notificacao->correspondencias_id   = $event->correspondencia->id;
      $notificacao->save();

      \Log::info('morador notificado da correspondencia');

      if(sizeof($morador->preferencias)){
        \Log::info('morador tem preferencias');
        if($morador->preferencias->enviar_email_nova_correspondencia){
          \Log::info('morador quer receber e-mail de aviso');
          $this->enviar_email_nova_correspondencia($morador);
        }else{
          \Log::info('morador nao quer receber e-mail de aviso');
        }
      }else{
        \Log::info('morador nao tem preferencias');
      }

    }

  }

}
