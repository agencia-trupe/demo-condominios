<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorCriouOcorrencia;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminOcorrencia
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  MoradorCriouOcorrencia  $event
     * @return void
     */
    public function handle(MoradorCriouOcorrencia $event)
    {
        $morador = $event->ocorrencia->autor->getNomeCompleto();

        $notificar = [
            'sindico',
            'conselho',
            'zelador',
            'portaria',
            'master'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'          => 'ocorrencia',
                    'mensagem'                  => "{$morador} registrou uma nova ocorrência.",
                    'administradores_id'        => $admin->id,
                    'ocorrencias_id'            => $event->ocorrencia->id
                ]);

            }
        }

    }
}
