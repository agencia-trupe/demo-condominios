<?php

namespace Gallery\Listeners\Traits;

use Gallery\Models\Morador;
use Gallery\Models\FilaEmails;
use Mail;
use Log;
use View;

trait NotificarAdminPorEmail{

  protected $template_email = 'moradores.templates.emails.notificacao-mensagem';
  protected $titulo_notificacao;
  protected $morador;

  public function enviar_email_nova_mensagem($mensagem, $email) {

    $data['mensagem'] = $mensagem;
    $data['email'] = $email;

    try {

      Mail::send( $this->template_email , $data, function ($m) use ($data) {
        $m->to($data['email'])
          ->subject('Nova Mensagem de Morador - Sistema Gallery');
      });

      if(config('mail.pretend')) Log::info(View::make($this->template_email, $data)->render());

    } catch (Exception $e) {
      Log::info('Falha ao enviar o email. '.$e->getMessage());
    }

  }

}
