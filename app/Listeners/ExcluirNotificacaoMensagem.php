<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouMensagem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\FaleComMensagemLida;

class ExcluirNotificacaoMensagem
{

  protected $notificacao;

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct(NotificacaoAdmin $notificacao)
  {
    $this->notificacao = $notificacao;
  }

  /**
   * Handle the event.
   *
   * @param  AdminVisualizouMensagem  $event
   * @return void
   */
  public function handle(AdminVisualizouMensagem $event)
  {
    // Remover notificação da Mensagem original
    // mas só remover se for o perfil destinatário
    // lançando o evento.

    $this->notificacao
         ->mensagens()
         ->paraAdministrador(Auth::admin()->get()->id)
         ->where('fale_com_mensagens_id', $event->mensagem->id)
         ->delete();


    // Marcar Mensagem original como lida se
    // não tiver sido lida ainda
    $check_lida = FaleComMensagemLida::where('fale_com_mensagens_id', $event->mensagem->id)
                              ->where('administradores_id', Auth::admin()->get()->id)
                              ->get();

    if(sizeof($check_lida) == 0){
        FaleComMensagemLida::create([
            'administradores_id'    => Auth::admin()->get()->id,
            'fale_com_mensagens_id' => $event->mensagem->id
        ]);
    }
  }
}
