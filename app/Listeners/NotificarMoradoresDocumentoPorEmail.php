<?php

namespace Gallery\Listeners;

use Gallery\Events\AdminCriouDocumento;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Gallery\Listeners\Traits\NotificarMoradorPorEmail;

use Gallery\Models\Morador;

class NotificarMoradoresDocumentoPorEmail
{

  use NotificarMoradorPorEmail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminCriouDocumento  $event
     * @return void
     */
    public function handle(AdminCriouDocumento $event)
    {
      $moradores_que_recebem_email_doc = Morador::whereHas('preferencias', function($query) {
        $query->where('enviar_email_novo_documento', '=', 1);
      })->get();

      foreach($moradores_que_recebem_email_doc as $morador){
        $this->enviar_email_novo_documento($morador);
      }
    }
}
