<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouReservaPorPeriodo;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\EspacoReservaPorPeriodoLido;

class ExcluirNotificacaoReservaPorPeriodo
{

    protected $notificacao;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificacaoAdmin $notificacao)
    {
        $this->notificacao = $notificacao;
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouReservaPorPeriodo  $event
     * @return void
     */
    public function handle(AdminVisualizouReservaPorPeriodo $event)
    {
        $link_visitado = 'admin/reservas-de-espacos/'.$event->reserva->id.'?tipo=por_periodo';

        $this->notificacao
             ->reservaDeEspacoPeriodo()
             ->paraAdministrador(Auth::admin()->get()->id)
             ->where('espacos_reservas_por_periodo_id', $event->reserva->id)
             ->delete();


        // Marcar Mensagem original como lida se
        // não tiver sido lida ainda
        $check_lida = EspacoReservaPorPeriodoLido::where('espacos_reservas_id', $event->reserva->id)
                                                ->where('administradores_id', Auth::admin()->get()->id)
                                                ->get();

        if(sizeof($check_lida) == 0){
            EspacoReservaPorPeriodoLido::create([
                'administradores_id'  => Auth::admin()->get()->id,
                'espacos_reservas_id' => $event->reserva->id
            ]);
        }
    }
}
