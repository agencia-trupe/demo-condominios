<?php

namespace Gallery\Listeners;

use Gallery\Events\MoradorAbriuChamadoDeManutencao;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\Admin;

class NotificarAdminChamadoManutencao
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MoradorAbriuChamadoDeManutencao  $event
     * @return void
     */
    public function handle(MoradorAbriuChamadoDeManutencao $event)
    {
        $morador = $event->chamado->autor->getNomeCompleto();

        $notificar = [
            'master',
            'zelador',
            'sindico',
            'conselho'
        ];

        foreach ($notificar as $perfil) {

            $admins = Admin::where('tipo', $perfil)->get();

            foreach($admins as $admin){

                NotificacaoAdmin::create([
                    'tipo_notificacao'          => 'chamado_manutencao',
                    'mensagem'                  => "{$morador} abriu um novo chamado de Manutenção.",
                    'administradores_id'        => $admin->id,
                    'chamados_de_manutencao_id' => $event->chamado->id
                ]);

            }
        }

    }
}
