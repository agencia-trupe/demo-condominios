<?php

namespace Gallery\Listeners;

use Auth;
use Gallery\Events\AdminVisualizouReservaComMulta;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Gallery\Models\NotificacaoAdmin;
use Gallery\Models\EspacoReservaPorDiariaLido;

class ExcluirNotificacaoCancelamentoComMulta
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminVisualizouReservaComMulta  $event
     * @return void
     */
    public function handle(AdminVisualizouReservaComMulta $event)
    {
        $link_visitado = 'admin/cancelamentos-com-multa/'.$event->reserva->id;

        NotificacaoAdmin::cancelamentoComMulta()
                        ->paraAdministrador(Auth::admin()->get()->id)
                        ->where('diaria_com_multa_id', $event->reserva->id)
                        ->delete();

        // Marcar Mensagem original como lida se
        // não tiver sido lida ainda
        $check_lida = EspacoReservaPorDiariaLido::where('espacos_reservas_id', $event->reserva->id)
                                                ->where('administradores_id', Auth::admin()->get()->id)
                                                ->get();

        if(sizeof($check_lida) == 0){
            EspacoReservaPorDiariaLido::create([
                'administradores_id'  => Auth::admin()->get()->id,
                'espacos_reservas_id' => $event->reserva->id
            ]);
        }
    }
}
