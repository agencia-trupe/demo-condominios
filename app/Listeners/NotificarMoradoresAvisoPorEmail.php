<?php

namespace Gallery\Listeners;

use Gallery\Events\AdminCriouAviso;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Gallery\Listeners\Traits\NotificarMoradorPorEmail;

use Gallery\Models\Morador;

class NotificarMoradoresAvisoPorEmail
{

  use NotificarMoradorPorEmail;

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  AdminCriouAviso  $event
   * @return void
   */
  public function handle(AdminCriouAviso $event)
  {
    $moradores_que_recebem_email_aviso = Morador::whereHas('preferencias', function($query) {
      $query->where('enviar_email_novo_aviso', '=', 1);
    })->get();

    foreach($moradores_que_recebem_email_aviso as $morador){
      $this->enviar_email_novo_aviso($morador);
    }
  }
}
